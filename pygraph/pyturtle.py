#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------python---------------------pyturtle.py--#
#                                                                       #
#                              Pyturtle                                 #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Classes TurtlePlane and Turtle
"""

from math import atan2, cos, hypot, pi, sin

#from pygraph import pygrapherror as pyerr
from pygraph import colors
from pygraph.pycart import Pen, Plane


#import Tkinter as tk

def version():
    """Return version of pyturtle."""
    import pygraph
    return pygraph.__version__


class TurtlePlane(Plane):
    """Plane for Turtles."""

    def __init__(self, name="Turtle geometry",
                 w=600, h=400,
                 sx=1, sy=None,
                 ox=None, oy=None,
                 axes=False, grid=False,
                 axescolor='black', gridcolor='black',
                 color='white',
                 parent=None):
        Plane.__init__(self, name,
                       w, h, sx, sy, ox, oy,
                       axes, grid, axescolor, gridcolor, color, parent)
        self._turtles = []

    def newTurtle(self, x=0, y=0, d=0, color='black', width=1):
        """Return a new Turtle for this Plane."""
        return Turtle(x, y, d, color, width, self)

    def turtles(self):
        """Return list of turtles."""
        return self._turtles[:]

    def delturtles(self):
        """Deletes existing Turtles."""
        self._turtles = []

    def clean(self):
        """Clean grapic windows without reset turtle."""
        Plane.clean(self)
        Plane._init(self)
        for t in self._turtles:
            t._createturtle()
            t._draw_turtle()
        self._canvas.update()

    def reset(self):
        """Reset graphic window."""
        for t in self._turtles:
            #      t.fill(0)
            #      t._init()
            t.reset()
        Plane.reset(self)


class Turtle(Pen):
    """Class for turtle geometry."""

    def __init__(self, x=0, y=0, d=0, color='black', width=1, plane=None):
        self._defd = d
        self._attesa = 0
        Pen.__init__(self, x=x, y=y, color=color, width=width, plane=plane)
        self._plane._turtles.append(self)
        self._createturtle()
        self._draw_turtle()

    def reset(self):
        """Initialize all attributes."""
        Pen.reset(self)
        self.degrees()
        self._x = self._defx
        self._y = self._defy
        self._angle = self._defd % self._fullcircle
        angle = self._angle * self._invradian
        self._cos = cos(angle)
        self._sin = sin(angle)
        self._drawing = 1
        self._filling = 0
        self._path = []
        self._tofill = []

    def _createturtle(self):
        """Create a turtle."""
        x, y = self._plane._p2s((self._x, self._y))
        self._turtle = self._canvas.create_line(x + 8, y, x, y,
                                                fill=self._color, arrow='last')
        self._turtledim = (self._width + 7, self._width +
                           9, self._width // 2 + 3)
        self._canvas.itemconfigure(self._turtle, arrowshape=self._turtledim)

    def _draw_turtle(self, position=None):
        """Draw turtle."""
        if not position:
            position = self._plane._p2s((self._x, self._y))
        x, y = position
        distance = self._width + 7
        dx = distance * self._cos
        dy = distance * self._sin
#  Pen on head:
#    self._canvas.coords(self._turtle, (x-dx, y+dy, x, y))
#  Pen on tail:
        self._canvas.coords(self._turtle, (x, y, x + dx, y - dy))
        self._canvas.update()

    def degrees(self, fullcircle=360.0):
        """Translate radians in degree."""
        self._fullcircle = fullcircle
        self._invradian = pi / (fullcircle * 0.5)

    def radians(self):
        """Translate degrees in radians."""
        self.degrees(2.0 * pi)

    def tracer(self, attesa):
        """Set a brake for turtle."""
        self._attesa = int(abs(attesa))

    def _setcolor(self, color):
        """Sets turtle and pen color."""
        self._color = colors.codecolor(color)
        self._canvas.itemconfigure(self._turtle, fill=self._color)
        self._canvas.update()

    def _getcolor(self):
        """Gets turtle and pen color."""
        return colors.namecolor(self._color)
    color = property(_getcolor, _setcolor)

    def _setwidth(self, width):
        """Sets the turtle and pen width."""
        if width > 1:
            self._width = float(width)
        else:
            self._width = 1.0
        self._turtledim = (self._width + 7, self._width +
                           9, self._width / 2 + 3)
        self._canvas.itemconfigure(self._turtle, arrowshape=self._turtledim)
        self._draw_turtle()

    def _getwidth(self):
        """Return the turtle and pen width."""
        return self._width
    width = property(_getwidth, _setwidth)

    def _setdir(self, angle):
        """Turn the turtle ad angle direction."""
        self._angle = angle % self._fullcircle
        angle *= self._invradian
        self._cos = cos(angle)
        self._sin = sin(angle)
        self._draw_turtle()

    def getdir(self):
        """Return the turtle direction."""
        return self._angle
    direction = property(getdir, _setdir)

    def _setpos(self, position):
        """Move the turtle in position."""
        x, y = position
        x0, y0 = self._plane._p2s((self._x, self._y))
        self._x, self._y = float(x), float(y)
        if self._filling:
            self._path.append(self._plane._p2s((x, y)))
        if self._drawing:
            x, y = self._plane._p2s((x, y))
#      try:
#        item = self._canvas.create_line(x0, y0, x0, y0,
#                                        width=self._width,
#                                        capstyle="round",
#                                        fill=self._color)
#      except:
#        raise pyerr("Probably the window was closed!")
            item = self._canvas.create_line(x0, y0, x0, y0,
                                            width=self._width,
                                            capstyle="round",
                                            fill=self._color)
            if self._attesa:
                dx, dy = float(x) - x0, float(y) - y0
                nhops = int(hypot(dx, dy))
                if nhops:
                    dx /= nhops
                    dy /= nhops
                    x1, y1 = x0, y0
                    # TODO
                    for i in range(nhops):
                        x1 += dx
                        y1 += dy
                        self._canvas.coords(item, x0, y0, x1, y1)
                        self._draw_turtle((x1, y1))
                        self._canvas.after(self._attesa)
            self._canvas.coords(item, x0, y0, x, y)
        else:
            item = None
        self._draw_turtle()
        return item

    def _getpos(self):
        """Get turtle position."""
        return round(self._x, 8), round(self._y, 8)
    position = property(_getpos, _setpos)

    def forward(self, distance):
        """Move the turtle forward distance step."""
        return self._setpos((self._x + distance * self._cos,
                             self._y + distance * self._sin))

    def back(self, distance):
        """Move the turtle back distance step."""
        return self.forward(-distance)

    def left(self, angle):
        """Turn the turtle left angle degree."""
        self._setdir((self._angle + angle) % self._fullcircle)

    def right(self, angle):
        """Turn the turtle right angle degree."""
        self.left(-angle)

    def up(self):
        """Lifts the turtle's pen up."""
        self._drawing = False

    def down(self):
        """Puts the turtle pen down."""
        self._drawing = True

    def distance(self, point):
        """Return the distance enter turtle and a point."""
        xother, yother = point
        return hypot(self._x - xother, self._y - yother)

    def dirto(self, point):
        """Return the angle enter turtle direction and a point."""
        xother, yother = point
        return ((atan2(yother - self._y, xother - self._x) / self._invradian)
                - self._angle) % self._fullcircle

    def whereis(self, point):
        """Return distance and angle enter turtle and point."""
        return self.distance(point), self.dirto(point)

    def lookat(self, point):
        """Turn turtle towards point."""
        self.left(self.dirto(point))

    def write(self, arg, move=False):
        """Writes a text from the position of the turtle."""
        x, y = self._plane._p2s((self._x, self._y))
        x = x - 1  # correction -- calibrated for Windows
        item = self._canvas.create_text(x, y,
                                        text=str(arg), anchor="nw",
                                        fill=self._color)
        if move:
            x0, y0, x1, y1 = self._canvas.bbox(item)
            self._setpos((self._plane._s2x(x1), self._plane._s2y(y0)))
        self._draw_turtle()

    def fill(self, flag):
        """Start or end filling."""
        if self._filling:
            path = tuple(self._path)
            smooth = self._filling < 0
            if len(path) > 2:
                item = self._canvas._create('polygon', path,
                                            {'fill': self._color, 'smooth': smooth})
                self._canvas.lower(item)
            if self._tofill:
                for item in self._tofill:
                    self._canvas.itemconfigure(item, fill=self._color)
        self._path = []
        self._tofill = []
        self._filling = flag
        if flag:
            self._path.append(self._plane._p2s((self._x, self._y)))

    def circle(self, radius, extent=None):
        """Draw a circle starts in turtle position."""
        xc = self._x - radius * self._sin
        yc = self._y + radius * self._cos
        xsc, ysc = self._plane._p2s((xc, yc))
        item = self._circle(
            xsc, ysc, radius, self._angle - 90, extent, 'chord')
        self._x = xc + radius * self._sin
        self._y = yc - radius * self._cos
        self._draw_turtle()
        if self._filling:
            self._path.append(self._plane._p2s((self._x, self._y)))
        return item

    def ccircle(self, radius, extent=None):
        """Draw a circle with center in turtle position."""
        xc, yc = self._plane._p2s((self._x, self._y))
        item = self._circle(xc, yc, radius, self._angle, extent, 'pieslice')
        return item

    def _circle(self, xc, yc, radius, start, extent, sty):
        """Draw a circle."""
        if not extent:
            extent = self._fullcircle
        if radius < 0.0:
            start = start + 180.0
        out = self._drawing and self._color or ''
        if abs(extent) >= self._fullcircle:
            item = self._canvas.create_oval(xc - radius, yc - radius,
                                            xc + radius, yc + radius,
                                            width=self._width,
                                            outline=out)
        else:
            if not self._filling:
                sty = 'arc'
            item = self._canvas.create_arc(xc - radius, yc - radius,
                                           xc + radius, yc + radius,
                                           style=sty,
                                           start=start,
                                           extent=extent,
                                           width=self._width,
                                           outline=out)
        if self._filling:
            self._tofill.append(item)
        self.left(extent)
        return item

    def hide(self):
        """Hide turtle."""
        self._canvas.itemconfigure(self._turtle, arrow='')

    def show(self):
        """Show turtle."""
        self._canvas.itemconfigure(self._turtle, arrow='last')

#  def save(self, filename):
#    """Save the design."""
#    self._plane.save(filename)

    def delete(self):
        """Delete this turtle."""
        self._canvas.delete(self._turtle)
        self._plane._turtles.remove(self)    # is politically correct?
        self._canvas.update()

    def clone(self):
        """Clone this turtle."""
        return self._plane.newTurtle(x=self._x, y=self._y, d=self._angle,
                                     color=self._color, width=self._width)


if __name__ == "__main__":
    from sys import path
    from os.path import join
    path.append(join('..', 'test'))
    import test_pyturtle
    test_pyturtle.alltests(test_pyturtle.loc)
