# -*- coding: utf-8 -*-
#-------------------------------python-----------------------pyplot.py--#
#                                                                       #
#                      Versione easy di pyplot                          #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2010---#

from pygraph.pyplot import PlotPlane, Plot

plotplane = PlotPlane(w=400, h=400, sx=20, sy=20, axes=True, grid=True)
plot = plotplane.newPlot()
