#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------python-----------------------pyplot.py--#
#                                                                       #
#                    traccia grafici di funzioni                        #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, ma  02111-1307  USA
#
"""
Plot functions
"""

from __future__ import division, print_function

import math

#from pygraph import colors
import pygraph.pycart as cg


def version():
    """Return pyplot version."""
    import pygraph
    return pygraph.__version__


class PlotPlane(cg.Plane):
    """Cartesian plane for plotting function."""

    def __init__(self, name="Functions Plotter",
                 w=400, h=400,
                 sx=20, sy=None,
                 ox=None, oy=None,
                 axes=True, grid=True,
                 axescolor='black', gridcolor='black',
                 color='white',
                 parent=None,
                 x_min=0, x_max=1, y_min=0, y_max=1,
                 margin=10, numxlab=0, numylab=0):
        cg.Plane.__init__(self, name,
                          w, h, sx, sy, ox, oy,
                          axes, grid, axescolor, gridcolor, color, parent)
        p = Plot()
        if numylab:
            posx = -margin / 2
            incposy = (y_max - y_min) / numylab
            for count in range(numylab + 1):
                p.drawtext("{0}".format(round(y_min + count / numylab, 4)),
                           (posx, count * incposy))
        if numxlab:
            posy = -float(margin) / h / 2
            incposx = (w - 2 * margin) / numxlab
            deltax = (x_max - x_min) / numxlab
            for count in range(numxlab + 1):
                p.drawtext("{0}".format(round(x_min + count * deltax, 4)),
                           (count * incposx, posy))
        self._plotters = []

    def newPlot(self, color='black', width=1):
        """Return a new plot in this plane."""
        return Plot(color, width, self)

    def plotters(self):
        """Return all plotters."""
        return self._plotters[:]

    def delplotters(self):
        """Delete all plotters."""
        self._plotters = []


def _eval(f, *var):
    """Eval function f."""
    try:
        return f(*var)
    except ZeroDivisionError:
        return cg.INF
    except (ValueError, OverflowError):
        return cg.NaN


class Plot(cg.Pen):
    """Class for plotting functions."""

    def __init__(self, color='black', width=1, plane=None, shape=cg.ROUND):
        cg.Pen.__init__(self, color=color, width=width, shape=shape,
                        plane=plane)
#    self._init()
#
#  def _init(self):
#    """Initialize all attributes."""
#    self._width = self._defwidth
#    self._color = self._defcolor

    def _drawto(self, pos, color, width):
        """Set pen position to pos and draw line from last position to pos."""
        poss0 = self._poss
        self._poss = self._plane._p2s(pos)
        try:
            self._canvas.create_line(poss0, self._poss,
                                     width=width, capstyle='round',
                                     fill=color)
            self._canvas.update()
    #    except tk.TclError:
        except:
            pass

    def xy(self, f, d_from=None, d_to=None, color=None, width=None):
        """y=f(x).

        example:
          >>> p=Plot()
          >>> p.xy(".2*x*x+2*x-3")
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        if d_from == None:
            d_from = self._plane._s2x(0)
        if d_to == None:
            d_to = self._plane._s2x(self._plane.getcanvaswidth())
        self._poss = (cg.NaN, cg.NaN)
        for x1 in range(self._plane._x2s(d_from), self._plane._x2s(d_to)):
            x = self._plane._s2x(x1)
            y = _eval(f, x)
            self._drawto((x, y), color, width)
            if type(y) == str:
                if defined:
                    if y == cg.INF:
                        print("Probable asymptote for x={0}".format(x))
                    if d_from != x:
                        print("Defined from {0} to {1}".format(d_from,
                                                               self._plane._s2x(x1 - 1)))
                    defined = False
            else:
                if not defined:
                    defined = True
                    d_from = x
        print("Defined from {0} to {1}".format(d_from, x))

    def yx(self, f, d_from=None, d_to=None, color=None, width=None):
        """y=f(x).

        example:
          >>> p=Plot()
          >>> p.xy(".2*x*x+2*x-3")
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        if d_from == None:
            d_from = self._plane._s2x(0)
        if d_to == None:
            d_to = self._plane._s2x(self._plane.getcanvasheight())
        print(self._plane._y2s(d_from), self._plane._y2s(d_to))
        self._poss = (cg.NaN, cg.NaN)
        for y1 in range(self._plane._y2s(d_to), self._plane._y2s(d_from)):
            y = self._plane._s2y(y1)
            x = _eval(f, y)
            self._drawto((x, y), color, width)
            if type(x) == str:
                if defined:
                    if x == cg.INF:
                        print("Probable asymptote for y={0}".format(y))
                    if d_from != y:
                        print("Defined from {0} to {1}".format(d_from,
                                                               self._plane._s2y(y1 - 1)))
                    defined = False
            else:
                if not defined:
                    defined = True
                    d_from = y
        print("Defined from {0} to {1}".format(d_from, y))

    def polar(self, f, d_from=0, d_to=360, color=None, width=None):
        """ro=f(th).

        example:
          >>> p=Plot()
          >>> p.polar("th", 720)
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        conv = math.pi / 180
        th = 0
        self._poss = (cg.NaN, cg.NaN)
        for i in range(d_from, d_to):
            th = i * conv
            ro = _eval(f, th)
            self._drawto((ro * math.cos(th), ro * math.sin(th)), color, width)
            if type(ro) == str:
                if defined:
                    if ro == cg.INF:
                        print(
                            "Probable point to the infinite for th={0}".format(th))
                    if d_from != th:
                        print("Defined from {0} to {1}".format(d_from, th - 1))
                    defined = False
            else:
                if not defined:
                    defined = True
                    d_from = th
        print("Defined from {0} to {1}".format(d_from, th))

    def param(self, fx, fy, d_from=0, d_to=100, color=None, width=None):
        """x=fx(t); y=fy(t).

        example:
          >>> p=Plot()
          >>> p.param("5+2*t", "1+3t", -50, 50)
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        self._poss = (cg.NaN, cg.NaN)
        for t in range(int(d_from), int(d_to)):
            x = _eval(fx, t)
            y = _eval(fy, t)
            self._drawto((x, y), color, width)
            if type(x) == str or type(x) == str:
                if defined:
                    if x == cg.INF or y == cg.INF:
                        print(
                            "Probable point to the infinite for t={0}".format(t))
                    if d_from != t:
                        print("Defined from {0} to {1}".format(d_from, t - 1))
                    defined = False
            else:
                if not defined:
                    defined = True
                    d_from = t
        print("Defined from {0} to {1}".format(d_from, t))

    def ny(self, f, d_from=None, d_to=None, trace=False, values=False,
           color=None, width=None, shape=cg.ROUND):
        """y=f(n).

        example:
          >>> p=Plot()
          >>> p.ny("3*sin(3*n)")
        """
        if d_from == None:
            d_from = self._plane._s2x(0)
        d_from = int(d_from)
        if d_to == None:
            d_to = self._plane._s2x(self._plane.getcanvaswidth())
        d_to = int(d_to)
        numbers = [_eval(f, float(ni)) for ni in
                   range(d_from, d_to)]
        self._succ(numbers, d_from, trace, values, color, width, shape)

    def succ(self, a0, fan1, trace=False, values=False,
             color=None, width=None, shape=cg.ROUND):
        """y=f(n).

        example:
          >>> p=Plot()
          >>> p.succ(50, lambda an, n, a0: (an+a0/an)/2, values=True)
        """
        numbers = [a0]
        an = a0
        for ni in range(1,
                        int(self._plane._s2x(self._plane.getcanvaswidth()))):
            n = float(ni)
            try:
                an = _eval(fan1, an)
            except TypeError:
                try:
                    an = _eval(fan1, an, n)
                except TypeError:
                    try:
                        an = _eval(fan1, an, n, a0)
                    except Exception:
                        raise Exception('error in function evaluation')
            numbers.append(an)
        self._succ(numbers, 0, trace, values, color, width, shape)

    def _succ(self, numlist, d_from=0, trace=False, values=False,
              color=None, width=None, shape=cg.ROUND):
        """a(n+1)=fa1(an).

        example:
          >>> piano = PlotPlane()
          >>> p = Plot()
          >>> p._succ([3, 5, 2, 'infinity', 9, 4, 'NaN', 7])
        """
        color = self._colordefault(color)
        width = width or self._width
        if trace:
            self._poss = (cg.NaN, cg.NaN)
            for n, a in enumerate(numlist):
                self._drawto((n + d_from, a), color='#909090', width=1)
        for n, a in enumerate(numlist):
            if values:
                print(a)
            self.drawpoint((n + d_from, a), color, width, shape)

    def xpoints(self, x_coord, y_list,
                color=None, width=None):
        """a(n+1)=fa1(an).

        example:
          >>> p=Plot()
          >>> p.succ(3, [3, 5, 2, 'infinity', 9, 4, 'NaN', 7])
        """
        color = self._colordefault(color)
        width = width or self._width
        for y in y_list:
            a_x, a_y = self._plane._p2s((x_coord, y))
            if a_x != cg.NaN:
                b_x, b_y = a_x, a_y + 1
                self._canvas.create_line((a_x, a_y), (b_x, b_y),
                                         width=width, capstyle='round',
                                         fill=color)
        self._canvas.update()


if __name__ == "__main__":
    from sys import path
    from os.path import join
    path.append(join('..', 'test'))
    import test_pyplot
    test_pyplot.alltests(test_pyplot.loc)
