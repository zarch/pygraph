#/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python-------------------------pyig.py--#
#                                                                       #
#                     Python Interactive Geometry                       #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Classes InteractivePlane and other pyig object
"""

from __future__ import division, print_function

import math

from pygraph import colors
from pygraph.pycart import Plane
from pygraph.pygrapherror import PlaneError  # , IgError
from pygraph.pygrapherror import IgError

try:
    import Tkinter as tk
#  pyversion = 'py2'
except ImportError:
    #  pyversion = 'py3'
    import tkinter as tk
try:
    import tkFont as tkfont
#  pyversion = 'py2'
except ImportError:
    #  pyversion = 'py3'
    import tkinter.font as tkfont

####
# Costanti relative a dimensioni e posizioni
###
_NOOBJ2 = (-1000, -1000)
_NOOBJ4 = (0, 0, 0, 0)
_PI = math.pi
_CONV = 180 / _PI
_NUMDEC = 4
_EPS = 0.5e-5
_M = 1.e4
_M2 = 1.e6


def version():
    """Return version of pycart."""
    import pygraph
    return pygraph.__version__

####
# General functions
###


def _loadparameter(val, defval):
    """ Set parameter default value."""
    if val == None:
        return defval
    else:
        return val


def _equ2(a, b, c, which):
    """Solve 2� equation.
    a, b, c are coefficients, which is -1 for 1st solution, 1 for 2nd solution.
    return None if delta<0"""
    delta = b * b - 4 * a * c
    if delta < 0:
        return None
    else:
        rdelta = math.sqrt(delta)
        if which == -1:
            return (-b - rdelta) / 2 / a
        elif which == 1:
            return (-b + rdelta) / 2 / a


def _near(a, b):
    """Return True if a is near b."""
    return abs(a - b) < _EPS


def _verybig(a):
    """Return True if a is veri big."""
    return abs(a) > _M


def _verysmall(a):
    """Return True if a is veri small."""
    return abs(a) < _EPS


def _atandeg(dx, dy):
    """Return arctan(dy/dx) in degree or None if dx == dy == 0."""
    if _verysmall(dx) and _verysmall(dy):
        return None
    if dy < 0:
        return 360 + math.atan2(dy, dx) * _CONV
    else:
        return math.atan2(dy, dx) * _CONV


def _atan2(dx, dy):
    """Return arctan(dy/dx) or None if dx == dy == 0."""
    if _verysmall(dx) and _verysmall(dy):
        return None
    if dy < 0:
        return 2 + math.atan2(dy, dx) / _PI
    else:
        return math.atan2(dy, dx) / _PI


def _lli(line0, line1):
    """Intersection enter line0 and line1."""
    m0, q0 = line0
    m1, q1 = line1
    if m0 == None:
        if m1 == None:
            return None, None
        else:
            return q0, m1 * q0 + q1
    elif m1 == None:
        return q1, m0 * q1 + q0
    elif _near(m0, m1):
        return None, None
    else:
        d = float(m1 - m0)
        return (q0 - q1) / d, (m1 * q0 - m0 * q1) / d


def _mq(p0, p1):
    """Sets coefficients equation of line support."""
    x0, y0 = p0
    x1, y1 = p1
    if x0 == None or x1 == None:
        return None, None
    elif _near(x0, x1):
        if _near(y0, y1):
            return None, None
        else:
            return None, x0
    elif _near(y0, y1):
        return 0, y0
    else:
        m = (y1 - y0) / (x1 - x0)
        return m, y0 - m * x0


def getiplane(iplane):
    if iplane == None:
        try:
            result = Plane._planes[-1]
        except IndexError:
            raise PlaneError("Ther are no Planes")
        return result
    else:
        return iplane

####
# Interactive Cartesian Plane
###


class InteractivePlane(Plane):
    """Interactive Cartesian Plane."""

    def __init__(self, name="Interactive geometry",
                 w=600, h=600,
                 sx=20, sy=None,
                 ox=None, oy=None,
                 axes=True, grid=True,
                 axescolor='#080808', gridcolor='#080808',
                 color='white',
                 parent=None):
        Plane.__init__(self,
                       name, w, h, sx, sy, ox, oy,
                       axes, grid, axescolor, gridcolor, color, parent)
        self._dragables = []
        # defaults values for new objects
        self.defvisible = True
        self.defname = ''
        self.defwidth = 3
        self.defwidthtext = 10
        self.defcolor = '#0505a0'
        self.defdragcolor = '#05a005'
        self.defintcolor = ''
        self.defangledim = 4

    def newPoint(self, x, y,
                 visible=None, color=None, width=None, name=None):
        """Reurn a Point."""
        return Point(x, y, visible, color, width, name, self)

    def newText(self, x, y, text,
                visible=None, color=None, width=None, name=None):
        """Reurn a Text."""
        return Text(x, y, text, visible, color, width, name, self)

    def newVarText(self, x, y, text, variables,
                   visible=None, color=None, width=None, name=None):
        """Reurn a VarText."""
        return VarText(x, y, text, variables, visible, color, width, name, self)

    def mainloop(self):
        """Mainloop."""
        for objid in self._dragables:
            self._canvas.tag_raise(objid)
        Plane.mainloop(self)

    def reset(self):
        """Reset the plain."""
        self._dragables = []
        Plane.reset(self)

####
# Base class for Pyig objects
###


class _PyigObject(object):
    """Base class for Pyig objects."""

    def __init__(self, parents, type):
        self._children = []
        for e in parents:
            e._children.append(self)
        self._type = type

    def _move(self):
        """Move this object and all its children."""
        for o in self._children:
            o._move()

    def _gettype(self):
        """Return the _PyigObject type."""
        return self._type

    def type(self):
        """Return data object type."""
        return _DataObject(self, self._gettype, 'Type')

####
# Base class for Data objects
###


class _DataObject(_PyigObject):
    """Base class for Data objects."""

    def __init__(self, obj, data, _type):
        self.data = data    # contain a function that return the value
        self._type = _type
        _PyigObject.__init__(self, (obj,), _type)

    def __str__(self):
        """self.data to string."""
        if self._exist():
            try:
                self.data() + 0
                return '{0:< 8}'.format(round(self.data(), _NUMDEC))
            except:
                try:
                    x, y = self.data()
                    return '({0:< 8}; {1:< 8})'.format(round(x, _NUMDEC),
                                                       round(y, _NUMDEC))
                except:
                    return str(self.data())
        else:
            return str(None)

    def _exist(self):
        """True if self.data exist."""
        return self.data != None

####
# Calc
###


class Calc(_DataObject):
    """(function, variables) -> result."""

    def __init__(self, function, variables):
        if type(variables) != tuple:
            variables = (variables,)
        self._variables = variables
        self._function = function
        _PyigObject.__init__(self, variables, "Calc")

    def data(self):
        """Calculates self.data value."""
        var = [v.data() for v in self._variables]
        if not None in var:
            return self._function(*var)
        else:
            return None

    def _exist(self):
        """True if self.data exist."""
        return not (None in [e.data() for e in self._variables])
#    return not (False in [e._exist() for e in self._variables])

####
# Base class for Pyig visibles objects
###


class _ViewObject(_PyigObject):
    """  Base class for Pyig visibles objects.
    constructor takes many argouments which are filled with defaults values
    of Pyig if there are None."""

    def __init__(self, parents,
                 iplane, visible, color, width, name, nobounds, _type):
        #    _PyigObject.__init__(self, parents)
        _PyigObject.__init__(self, parents, _type)
        self._iplane = iplane
        self._cv = iplane._canvas
        self._visible = _loadparameter(visible, iplane.defvisible)
        self._name = _loadparameter(name, iplane.defname)
        self._width = _loadparameter(width, iplane.defwidth)
        self._color = colors.codecolor(_loadparameter(color, iplane.defcolor))
        self._nobounds = nobounds
        self._type = _type
        self._setcoords()           # set _x and _y and some other attributes
        if self.visible and (name != None):
            # Qui diventa circolare la chiamata alla libreria!!!
            self._label = Label(self, 10, -10 - self._width, name, color=color)
        else:
            self._label = None
        self._draw()

    def _move(self):
        """Move this object and all your children."""
        self._setcoords()
        self._cv.coords(self._id, self._getbounds())
        for o in self._children:
            o._move()

    def _draw(self):
        """Draws the object as a point Point."""
        self._id = self._cv.create_oval(self._getbounds(), fill=self._color)

    def _getbounds(self):
        """Return Points bounds."""
        if self._exist() and self._visible:
            sx, sy = self._iplane._p2s((self._x, self._y))
            return (sx - self._width, sy - self._width,
                    sx + self._width, sy + self._width)
        else:
            return self._nobounds
# def get

    def _setvisible(self, v):
        """Set object "visible"=v."""
        self._visible = v
        self._cv.coords(self._id, self._getbounds())

    def _getvisible(self):
        """Return the turtle and pen width."""
        return self._visible
    visible = property(_getvisible, _setvisible)

    def _setcolor(self, color):
        """Set object "color"=color."""
        self._color = colors.codecolor(color)
        self._cv.itemconfigure(self._id, fill=self._color)

    def _getcolor(self):
        """Gets turtle and pen color."""
        return colors.namecolor(self._color)
    color = property(_getcolor, _setcolor)

    def _setwidth(self, w):
        """Set object "width"=w."""
        self._width = w
        self._cv.coords(self._id, self._getbounds())

    def _getwidth(self):
        """Return the object width."""
        return self._width
    width = property(_getwidth, _setwidth)

    def _get_name(self):
        """Return the object name."""
        return self._name

    def _setname(self, name):
        """Set object "name" = name."""
        self._name = name
        if self._label != None:
            self._cv.delete(self._label._id)
        self._label = Label(self, 10, -10 - self._width, name,
                            color=self._color)

    def _getname(self):
        """Get object name."""
        return _DataObject(self, self._get_name, 'Name')
    name = property(_getname, _setname)

    def _setcoords(self):
        pass

    def _getscoords(self):
        if self._exist():
            return self._iplane._p2s((self._x, self._y))
        else:
            return None, None

    def _getcoords(self):
        return self._x, self._y

    def _getx(self):
        return self._x

    def _gety(self):
        return self._y

    def _exist(self):
        return self._x != None

    def coords(self):
        """Return data object coords."""
        return _DataObject(self, self._getcoords, 'Coords')

    def xcoord(self):
        """Return data object xcoords."""
        return _DataObject(self, self._getx, 'Xcoord')

    def ycoord(self):
        """Return data object ycoords."""
        return _DataObject(self, self._gety, 'Ycoord')

####
# Base class for Point objects
###


class _PointObject(_ViewObject):
    """Base class for Point object."""

    def __add__(self, other):
        if isinstance(other, Vector):
            return PointPsumV(self, other)
        else:
            return PointPsumP(self, other)

    def __sub__(self, other):
        if isinstance(other, Vector):
            return PointPdiffV(self, other)
        else:
            return PointPdiffP(self, other)

    def __mul__(self, other):
        if isinstance(other, _PointObject):
            return PointPprodP(self, other)
        else:
            return PointPprodN(self, other)

####
# Base class for Dragables objects
###


class _DragableObject(_PointObject):
    """Dragable object class."""

    def __init__(self, parents,
                 iplane, visible, color, width, name, nobounds, type):
        color = colors.codecolor(_loadparameter(color, iplane.defdragcolor))
        _ViewObject.__init__(self, parents,
                             iplane, visible, color, width, name, nobounds, type)
        self._cv.tag_bind(self._id, "<Any-Enter>", self._mouseEnter)
        self._cv.tag_bind(self._id, "<Any-Leave>", self._mouseLeave)
        self._cv.tag_bind(self._id, "<1>", self._mouseDown)
        self._cv.tag_bind(self._id, "<B1-Motion>", self._mouseMove)
        iplane._dragables.append(self._id)

    def _setcoords(self):
        pass

# Methods for mouse interaction

    def _mouseEnter(self, event):
        # propagare l'effetto a tutti i children
        self._cv.itemconfig(tk.CURRENT, fill=colors.invcolor(self._color))

    def _mouseLeave(self, event):
        # propagare l'effetto a tutti i children
        self._cv.itemconfig(tk.CURRENT, fill=self._color)

    def _mouseDown(self, event):
        self._lastx = event.x
        self._lasty = event.y

    def _mouseMove(self, event):
        dx = event.x - self._lastx
        dy = event.y - self._lasty
        self._lastx = event.x
        self._lasty = event.y
        self._drag(dx, dy)

    def _drag(self, dx, dy):
        xs, ys = self._iplane.scale
        self._x += float(dx) / xs
        self._y -= float(dy) / ys
        self._move()

####
# Point class
###


class Point(_DragableObject):
    """Return a point."""

    def __init__(self, x, y,
                 visible=None, color=None, width=None, name=None, iplane=None):
        iplane = getiplane(iplane)
#    if iplane == None:
#      try:
#        iplane = Plane._planes[-1]
#      except IndexError:
#        raise PlaneError("Ther are no Planes")
        parents = []
        if isinstance(x, _DataObject):
            self._objx = x
            parents.append(x)
        else:
            self._objx = None
            self._x = float(x)
        if isinstance(y, _DataObject):
            self._objy = y
            parents.append(y)
        else:
            self._objy = None
            self._y = y
        _DragableObject.__init__(self, parents,
                                 iplane, visible, color, width, name, _NOOBJ4,
                                 "Point")

    def _setcoords(self):
        if self._objx != None:
            self._x = self._objx.data()
        if self._objy != None:
            self._y = self._objy.data()
        if self._y == None:
            self._x = None

    def _drag(self, dx, dy):
        xs, ys = self._iplane.scale
        if self._objx == None:
            self._x += float(dx) / xs
        if self._objy == None:
            self._y -= float(dy) / ys
        self._move()

# class PointPsumV(_PointObject):
##  """Return Point sum of point and vect."""
##
# def __init__(self, point, vect,
# visible=None, color=None, width=None, name=None):
##    self._point, self._vect = point, vect
##    if visible == None: visible = point.visible
##    if color == None: color = point.color
##    if width == None: width = point.width
##    if name == None: name = str(point.name) + "'"
# _ViewObject.__init__(self, (point, vect),
# point._iplane,
##                                visible, color, width, name, _NOOBJ4,
# "PointPsumV")
##


class _PointVectOperation(_PointObject):
    """Return result from Point and vect operation."""

    def __init__(self, point, vect,
                 visible=None, color=None, width=None, name=None, _type=None):
        self._point, self._vect = point, vect
        if visible == None:
            visible = point.visible
        if color == None:
            color = point.color
        if width == None:
            width = point.width
        if name == None:
            name = str(point.name) + "'"
        _ViewObject.__init__(self, (point, vect),
                             point._iplane,
                             visible, color, width, name, _NOOBJ4,
                             _type)


class PointPsumV(_PointVectOperation):
    """Return Point sum of point and vect."""

    def __init__(self, point, vect,
                 visible=None, color=None, width=None, name=None):
        _PointVectOperation.__init__(self, point, vect,
                                     visible, color, width, name, "PointPsumV")

    def _setcoords(self):
        if self._point._exist() and self._vect._exist():
            x0, y0 = self._point._getcoords()
            dx, dy = self._vect._getdxdy()
            self._x = x0 + dx
            self._y = y0 + dy
        else:
            self._x = self._y = None

# class PointPdiffV(_PointObject):
##  """Return Point sum of point and vect."""
##
# def __init__(self, point, vect,
# visible=None, color=None, width=None, name=None):
##    self._point, self._vect = point, vect
##    if visible == None: visible = point.visible
##    if color == None: color = point.color
##    if width == None: width = point.width
##    if name == None: name = str(point.name) + "'"
# _ViewObject.__init__(self, (point, vect),
# point._iplane,
##                                visible, color, width, name, _NOOBJ4,
# "PointPdiffV")


class PointPdiffV(_PointVectOperation):
    """Return Point diff of point and vect."""

    def __init__(self, point, vect,
                 visible=None, color=None, width=None, name=None):
        _PointVectOperation.__init__(self, point, vect,
                                     visible, color, width, name, "PointPdiffV")

    def _setcoords(self):
        if self._point._exist() and self._vect._exist():
            x0, y0 = self._point._getcoords()
            dx, dy = self._vect._getdxdy()
            self._x = x0 - dx
            self._y = y0 - dy
        else:
            self._x = self._y = None

# class PointPsumP(_PointObject):
##  """Return Point sum of point0 and point1."""
##
# def __init__(self, point0, point1,
# visible=None, color=None, width=None, name=None):
##    self._point0, self._point1 = point0, point1
# _ViewObject.__init__(self, (point0, point1),
# point0._iplane,
##                                visible, color, width, name, _NOOBJ4,
# "PointPsumP")
##


class _PointPointOperation(_PointObject):
    """Return result from Point and vect operation."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None, _type=None,
                 symb=None):
        self._point0, self._point1 = point0, point1
        if visible == None:
            visible = point0.visible
        if color == None:
            color = point0.color
        if width == None:
            width = point0.width
        if name == None:
            name = str(point0.name) + symb + str(point1.name)
        _ViewObject.__init__(self, (point0, point1),
                             point0._iplane,
                             visible, color, width, name, _NOOBJ4,
                             _type)


class PointPsumP(_PointPointOperation):
    """Return Point sum of point0 and point1."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None):
        _PointPointOperation.__init__(self, point0, point1,
                                      visible, color, width, name, "PointPsumP",
                                      '+')

    def _setcoords(self):
        if self._point0._exist() and self._point1._exist():
            x0, y0 = self._point0._getcoords()
            x1, y1 = self._point1._getcoords()
            self._x = x0 + x1
            self._y = y0 + y1
        else:
            self._x = self._y = None


class PointPdiffP(_PointPointOperation):
    """Return Point diff of point0 and point1."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None):
        _PointPointOperation.__init__(self, point0, point1,
                                      visible, color, width, name, "PointPdiffP",
                                      '-')

    def _setcoords(self):
        if self._point0._exist() and self._point1._exist():
            x0, y0 = self._point0._getcoords()
            x1, y1 = self._point1._getcoords()
            self._x = x0 - x1
            self._y = y0 - y1
        else:
            self._x = self._y = None


class PointPprodP(_PointPointOperation):
    """Return Point product of point0 and point1."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None):
        _PointPointOperation.__init__(self, point0, point1,
                                      visible, color, width, name, "PointPprodP",
                                      '*')

    def _setcoords(self):
        if self._point0._exist() and self._point1._exist():
            x0, y0 = self._point0._getcoords()
            x1, y1 = self._point1._getcoords()
            self._x = x0 * x1 - y0 * y1
            self._y = x0 * y1 + x1 * y0
        else:
            self._x = self._y = None


class PointPprodN(_PointObject):
    """Return Point product of point0 and n."""

    def __init__(self, point, n,
                 visible=None, color=None, width=None, name=None):
        self._point, self._n = point, n
        if visible == None:
            visible = point.visible
        if color == None:
            color = point.color
        if width == None:
            width = point.width
        if name == None:
            name = str(point.name) + '*' + str(n)
        _ViewObject.__init__(self, (point, ),
                             point._iplane,
                             visible, color, width, name, _NOOBJ4,
                             "PointPprodN")

    def _setcoords(self):
        if self._point._exist():
            x0, y0 = self._point._getcoords()
            self._x = x0 * self._n
            self._y = y0 * self._n
        else:
            self._x = self._y = None

####
# MidPoiunts class
###


class MidPoints(_PointObject):
    """Return the middle enter tue points."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None):
        self._point0 = point0
        self._point1 = point1
        if point0._iplane == point1._iplane:
            iplane = point0._iplane
        else:
            raise IgError('Points not in the same window')
        _ViewObject.__init__(self, (point0, point1),
                             iplane, visible, color, width, name, _NOOBJ4,
                             "MidPoints")

    def _setcoords(self):
        if self._point0._exist() and self._point1._exist():
            x0, y0 = self._point0._getcoords()
            x1, y1 = self._point1._getcoords()
            self._x = (x0 + x1) / 2.
            self._y = (y0 + y1) / 2.
        else:
            self._x = self._y = None

####
# MidPoint class
###


class MidPoint(MidPoints):
    """Return segment middle point."""

    def __init__(self, segment,
                 visible=None, color=None, width=None, name=None):
        self._point0 = segment._point0
        self._point1 = segment._point1
        _ViewObject.__init__(self, (self._point0, self._point1),
                             self._point0._iplane,
                             visible, color, width, name, _NOOBJ4,
                             "MidPoint")
####
# Intersection class
###


class Intersection(_PointObject):
    """Return obj0 - obj1 intersection."""

    def __init__(self, obj0, obj1, which=None,
                 visible=None, color=None, width=None, name=None):
        self._obj0 = obj0
        self._obj1 = obj1
        self._which = which
        methname = '_intersection{0}{1}'.format(self.instance(obj0),
                                                self.instance(obj1))
        self._setcoords = getattr(self, methname)
        _ViewObject.__init__(self, (obj0, obj1),
                             obj0._iplane,
                             visible, color, width, name, _NOOBJ4,
                             "Intersection")

    def instance(self, obj):
        for cls in (_Straight, _CircleObject):
            if isinstance(obj, cls):
                return cls.__name__
        return "X"

    def _intersection_Straight_Straight(self):
        """(self._x, self._y) is intersection from two lines"""
        if self._obj0._exist() and self._obj1._exist():
            self._x, self._y = _lli(self._obj0._getmq(), self._obj1._getmq())
        else:
            self._x = self._y = None

    def _intersection_Straight_CircleObject(self):
        """(self._x, self._y) is intersection from a line and a circle"""
        if not self._which in (-1, +1):
            raise IgError('Intersection need which point: -1 or +1')
        if self._obj0._exist() and self._obj1._exist():
            m, q = self._obj0._getmq()
            a, b, c = self._obj1._getcoeff()
            if m == None or _verybig(m):
                factor = -self._obj0._getfactory()
                if factor == 0:
                    self._y = None
                else:
                    self._x = self._obj0._point0._x
                    self._y = _equ2(1, b, self._x * self._x + a * self._x - c,
                                    self._which * factor)
                if self._y == None:
                    self._x = None
            else:
                factor = self._obj0._getfactorx()
                if factor == 0:
                    self._x = None
                else:
                    self._x = _equ2(1 + m * m, 2 * m * q + b * m + a,
                                    q * q + b * q - c, self._which * factor)
                if self._x != None:
                    self._y = m * self._x + q
                else:
                    self._y = None
        else:
            self._x = self._y = None

    def _intersection_CircleObject_Straight(self):
        """(self._x, self._y) is intersection from a circle and a line"""
        if self._obj0._exist() and self._obj1._exist():
            m, q = self._obj1._getmq()
            a, b, c = self._obj0._getcoeff()
            if m == None or _verybig(m):
                factor = -self._obj1._getfactory()
                if factor == 0:
                    self._y = None
                else:
                    self._x = self._obj1._point0._x
                    self._y = _equ2(1, b, self._x * self._x + a * self._x - c,
                                    self._which * factor)
                if self._y == None:
                    self._x = None
            else:
                factor = self._obj1._getfactorx()
                if factor == 0:
                    self._x = None
                else:
                    self._x = _equ2(1 + m * m, 2 * m * q + b * m + a,
                                    q * q + b * q - c, self._which * factor)
                if self._x != None:
                    self._y = m * self._x + q
                else:
                    self._y = None
        else:
            self._x = self._y = None

    def _intersection_CircleObject_CircleObject(self):
        """(self._x, self._y) is intersection from two circles"""
        if self._obj0._exist() and self._obj1._exist():
            yc0, yc1 = self._obj0._center._y, self._obj1._center._y
            dyc = yc0 - yc1
            if dyc > 0:
                factor = 1
            else:
                factor = -1
            a0, b0, c0 = self._obj0._getcoeff()
            a1, b1, c1 = self._obj1._getcoeff()
            if _near(b0, b1):   # circ. con i centri con la stessa ordinata.
                if _near(a0, a1):
                    self._x = self._y = None
                else:
                    if a0 > a1:
                        factor = 1
                    else:
                        factor = -1
                    self._x = (c1 - c0) / (a1 - a0)
                    self._y = _equ2(1, b0,
                                    self._x * self._x + a0 * self._x - c0, self._which * factor)
                    if self._y == None:
                        self._x = None
            else:
                m = (a0 - a1) / (b1 - b0)
                q = (c1 - c0) / (b1 - b0)
                self._x = _equ2(1 + m * m, 2 * m * q + b1 * m + a1,
                                q * q + b1 * q - c1, self._which * factor)
                if self._x != None:
                    self._y = m * self._x + q
                else:
                    self._y = None
        else:
            self._x = self._y = None

####
# PointOn class
###


class PointOn(_PointObject):
    """Return obj constrained point."""

    def __init__(self, obj, parameter=0,
                 visible=None, color=None, width=None, name=None):
        self._obj = obj
        self._parameter = parameter
        _ViewObject.__init__(self, [obj],
                             obj._iplane, visible, color, width, name, _NOOBJ4,
                             "PointOn")

    def _setcoords(self):
        if self._obj._exist():
            self._x, self._y = self._obj._constrainedcoords(self._parameter)
        else:
            self._x = None

    def _getparameter(self):
        """Return ConstrainedPoint parameter."""
        return self._parameter

    def parameter(self):
        """Return ConstrainedPoint parameter."""
        return _DataObject(self, self._getparameter, 'Parameter')

####
# ConstrainedPoint class
###


class ConstrainedPoint(PointOn, _DragableObject):
    """Return obj constrained point."""

    def __init__(self, obj, parameter=0,
                 visible=None, color=None, width=None, name=None):
        self._obj = obj
        self._parameter = parameter
        _DragableObject.__init__(self, [obj],
                                 obj._iplane, visible, color, width, name, _NOOBJ4,
                                 "ConstrainedPoint")

    def _mouseMove(self, event):
        self._drag(event.x, event.y)
#    xs, ys = self._iplane.scale
        for o in self._children:
            o._move()

    def _drag(self, x, y):
        self._parameter = self._obj._getparameter(x, y)
        self._move()

####
# Base class for Straight lines
###


class _Straight(_ViewObject):  # view methods names of Intersections
    """Base class for Straight lines."""

    def __init__(self, parents, point0, point1,
                 visible, color, width, name, type):
        self._point0 = point0
        self._point1 = point1
        self._m = self._q = None
        _ViewObject.__init__(self, parents,
                             point0._iplane,
                             visible, color, width, name, _NOOBJ4, type)

    def _setcoords(self):
        """Set coords for draw."""
        if self._point0._exist() and self._point1._exist():
            self._m, self._q = _mq(self._point0._getcoords(),
                                   self._point1._getcoords())
            if self._q == None:
                self._x = None
            else:
                self._x, self._y = self._point0._getcoords()
        else:
            self._x = self._y = None

    def _draw(self):
        self._id = self._cv.create_line(self._getbounds(), fill=self._color,
                                        width=self._width, capstyle=tk.ROUND)

    def _getscoords(self):
        return self._point0._getscoords()

    def _getcoords(self):
        return self._point0._getcoords()

    def _getmq(self):
        return self._m, self._q

    def _getequation(self):
        if not self._exist():
            return None
        m, q = self._m, self._q
        if m == None:
            if q == None:
                return None
            else:
                return "x={0:8.4f}".format(self._x)
        else:
            #      if q < 0:
            #        sq = "{0:8.4f}".format(q)
            #      elif q > 0:
            #        sq = "+{0:8.4f}".format(q)
            #      else:
            #        sq = ""
            if _near(q, 0):
                sq = ""
            else:
                sq = "{0:+8.4f}".format(q)
            if _near(m, 0):
                if _near(q, 0):
                    return "y = 0"
                else:
                    return "y=" + sq
            elif _near(m, 1):
                return "y = x" + sq
            elif _near(m, -1):
                return "y=-x" + sq
            elif _near(1. / m, 0):
                return "x={0:8.4f}".format(self._x)
            else:
                return "y={0:8.4f}x{1}".format(m, sq)

    def _getslope(self):
        if not self._exist():
            return None
        if self._m == None:
            return None
        elif self._m != 0 and _near(1. / self._m, 0):
            return "infinity"
        else:
            return self._m

    def _getintercept(self):
        if not self._exist():
            return None
        if self._m == None:
            return None
        elif self._q != 0 and _near(1. / self._q, 0):
            return "infinity"
        else:
            return self._q

    def _getlinebounds(self, xa, ya, m):
        """Return Line bounds."""
        iplane = self._point0._iplane
        width = iplane._canvaswidth
        height = iplane._canvasheight
        if m == None:
            return xa, 0, xa, height
        else:
            num = ya + m * xa
            if abs(m) > 1:
                return num / m, 0, (-height + num) / m, height
            else:
                return 0, num, width, num - m * width

    def _getbounds(self):
        """Return Line bounds."""
        if self._exist() and self._visible:
            xa, ya = self._point0._getscoords()
            return self._getlinebounds(xa, ya, self._m)
        else:
            return self._nobounds

    def _constrainedcoords(self, t):
        """Return coords of the point with parameter t."""
        x0, y0 = self._point0._getcoords()
        x1, y1 = self._point1._getcoords()
        if x0 == None or x1 == None:
            return None, None
        else:
            return t * (x1 - x0) + x0, t * (y1 - y0) + y0

    def _getdx(self):
        """Return components."""
        return self._getdxdy()[0]

    def _getdy(self):
        """Return components."""
        return self._getdxdy()[1]

    def _getdxdy(self):
        """Return components."""
        if self._exist():
            x0, y0 = self._point0._getcoords()
            x1, y1 = self._point1._getcoords()
            return (x1 - x0), (y1 - y0)
        else:
            return None, None

    def _getparameter(self, x, y):
        m0 = self._m
        x0, y0 = self._point0._getcoords()
        x1, y1 = self._point1._getcoords()
        if m0 == None:
            yh = self._iplane._s2y(y)
            return (yh - y0) / (y1 - y0)
        elif _verysmall(m0):
            xh = self._iplane._s2x(x)
            return (xh - x0) / (x1 - x0)
        else:
            xp, yp = self._iplane._s2p((x, y))
            m1 = -1. / m0
            q1 = yp - m1 * xp
            xh = (self._q - q1) / (m1 - self._m)
        return (xh - x0) / (x1 - x0)

    def _getfactorx(self):
        x0 = self._point0._x
        x1 = self._point1._x
        if x0 == None or x1 == None:
            return 0
        elif x0 - x1 < 0:
            return 1
        else:
            return -1

    def _getfactory(self):
        y0 = self._point0._y
        y1 = self._point1._y
        if y0 == None or y1 == None:
            return 0
        elif y0 - y1 > 0:
            return 1
        else:
            return -1

    def equation(self):
        return _DataObject(self, self._getequation, 'Equation')

    def slope(self):
        """Return line slope."""
        return _DataObject(self, self._getslope, 'Slope')

    def intercept(self):
        """Return line slope."""
        return _DataObject(self, self._getintercept, 'Intercept')

    def qm(self):
        """Return line slope."""
        return _DataObject(self, self._getqm, 'qm')

    def point0(self):
        """Return line point0."""
        return self._point0

    def point1(self):
        """Return line point1."""
        return self._point1

    def endpointA(self):  # for compatibility with pykig
        """Return line point0."""
        return self._point0

    def endpointB(self):  # for compatibility with pykig
        """Return line point1."""
        return self._point1

####
# Line class
###


class Line(_Straight):
    """Return line enter point0 and point1."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None):
        _Straight.__init__(self, (point0, point1), point0, point1,
                           visible, color, width, name, "Line")

####
# Base class for Segments
###


class Segment(_Straight):
    """Return segment from point0 to point1."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None):
        if point0._iplane != point1._iplane:
            raise IgError('Points not in the same window')
        _Straight.__init__(self, (point0, point1),
                           point0, point1,
                           visible, color, width, name, "Segment")

    def _setcoords(self):
        """Set coords for draw."""
        if self._point0._exist() and self._point1._exist():
            self._x, self._y = self._point0._getcoords()
            self._m, self._q = _mq(self._point0._getcoords(),
                                   self._point1._getcoords())
        else:
            self._x = self._y = None

    def _getbounds(self):
        """Return Segment bounds."""
        if self._exist() and self._visible:
            xa, ya = self._point0._getscoords()
            xb, yb = self._point1._getscoords()
            return xa, ya, xb, yb
        else:
            return self._nobounds

    def _getlength(self):
        """Return Segment lenght."""
        if self._exist():
            xa, ya = self._point0._getcoords()
            xb, yb = self._point1._getcoords()
            return math.hypot(xb - xa, yb - ya)
        else:
            return None

    def _getparameter(self, x, y):
        """Cut the parameter to segment bounds."""
        t = _Straight._getparameter(self, x, y)
        if t < 0:
            return 0
        elif t > 1:
            return 1
        else:
            return t

#  def _constrainedcoords(self, t):
#    """Return coords of the point with parameter t."""
#    if t < 0:
#        t = 0
#    elif t > 1:
#        t = 1
#    return _Straight._constrainedcoords(self, t)

    def length(self):
        """Return segment length."""
        return _DataObject(self, self._getlength, 'Length')

    def midpoint(self, visible=None, color=None, width=None,
                 name=None):
        """Return the middle point of segment."""
        return MidPoint(self, visible, color, width, name)

####
# Ray class
###


class Ray(_Straight):
    """Return Ray from point0 by point1."""

    def __init__(self, point0, point1,
                 visible=None, color=None, width=None, name=None):
        _Straight.__init__(self, (point0, point1), point0, point1,
                           visible, color, width, name, "Ray")

    def _getbounds(self):
        """Return Line bounds."""
        if self._exist() and self._visible:
            iplane = self._point0._iplane
            width = iplane._canvaswidth
            height = iplane._canvasheight
            xa, ya = self._point0._getscoords()
            xb, yb = self._point1._getscoords()
            if self._m == None:
                if yb > ya:
                    return xa, ya, xa, height
                elif yb < ya:
                    return xa, 0, xa, ya
                else:
                    return self._nobounds
            else:
                num = ya + self._m * xa
                if abs(self._m) > 1:
                    if yb > ya:
                        return xa, ya, (-height + num) / self._m, height
                    else:
                        return num / self._m, 0, xa, ya
                else:
                    if xb > xa:
                        return xa, ya, width, num - self._m * width
                    else:
                        return 0, num, xa, ya
        else:
            return self._nobounds

    def _getparameter(self, x, y):
        """Cut the parameter to ray bound."""
        t = _Straight._getparameter(self, x, y)
        if t < 0:
            return 0
        else:
            return t

#  def _constrainedcoords(self, t):
#    """Return coords of the point with parameter t."""
#    if t < 0:
#        t = 0
#    return _Straight._constrainedcoords(self, t)

####
# AngleSide class
###


class AngleSide(Ray):
    """Return Ray from point0 by point1."""

    def __init__(self, angle, which,
                 visible=None, color=None, width=None, name=None):
        self._angle = angle
        point0 = angle._vertex
        if which == 0:
            point1 = angle._point0
        else:
            point1 = angle._point1
        _Straight.__init__(self, (angle,),
                           point0, point1,
                           visible, color, width, name, "AngleSide")

    def _setcoords(self):
        """Set coefficients equation of line support."""
        if self._angle._exist() and self._point0._exist() and self._point1._exist():
            Ray._setcoords(self)
        else:
            self._x = self._y = None

####
# Bisector class
###


class Bisector(_Straight):
    """Return angle bisector."""

    def __init__(self, angle,
                 visible=None, color=None, width=None, name=None):
        self._angle = angle
        _Straight.__init__(self, (angle,), angle._vertex,
                           Point(0, 0, visible=False, iplane=angle._iplane),
                           visible, color, width, name, "Bisector")

    def _setcoords(self):
        """Set coefficients equation of line support
        and _point1 coords."""
        if self._angle._exist():
            ang = self._angle
            p0x, p0y = ang._point0._getcoords()
            p1x, p1y = ang._point1._getcoords()
            self._x, self._y = self._point0._getcoords()
            e = ang._getextent()
            a = ang._start + e / 2
            if a < 0:
                a += 360
            self._a = a
            if a == 90 or a == 270:
                self._m = None
                self._q = self._x
                self._point1._x = p0x
                self._point1._y = (p1y - p0y) * (self._x -
                                                 p0x) / (p1x - p0x) + p0y
            elif a == 0 or a == 180:
                self._m = 0
                self._q = self._y
                self._point1._x = (p1x - p0x) * (self._y -
                                                 p0y) / (p1y - p0y) + p0x
                self._point1._y = self._y
            else:
                self._m = math.tan(a / _CONV)
                self._q = self._y - self._m * self._x
                r = _mq((p0x, p0y), (p1x, p1y))
                self._point1._x, self._point1._y = _lli((self._m, self._q), r)
        else:
            self._x = self._y = None

    def _getfactorx(self):
        if 0 < self._a < 90 or 270 < self._a < 360:
            return 1
        else:
            return -1

    def _getfactory(self):
        if 0 < self._a < 180:
            return 1
        else:
            return -1

####
# Vector class
###


class Vector(Segment):
    """Return Vector."""

#  def __init__(self, obj0, obj1, which=None,
#               visible=None, color=None, width=None, name=None):
    def __init__(self, point, point_vect,
                 visible=None, color=None, width=None, name=None):
        if point_vect == None:
            point, point_vect = (Point(0, 0, visible=False, iplane=point._iplane),
                                 point)
        if isinstance(point_vect, _PointObject):
            point1 = point_vect
        elif isinstance(point_vect, Vector):
            self._vect = point_vect
            point1 = Point(0, 0, visible=False, iplane=point._iplane)
            self._setcoords = self._setcoordspv
        else:
            raise IgError('point_vect accept only Points or Vectors')
        _Straight.__init__(self, (point, point_vect),
                           point, point1,
                           visible, color, width, name, "Vector")
        if isinstance(point_vect, Vector):
            self._children.append(point1)

    def _setcoordspv(self):
        """Set coords for draw."""
        if self._point0._exist() and self._vect._exist():
            xa, ya = self._point0._getcoords()
            dx, dy = self._vect._getdxdy()
            xb, yb = xa + dx, ya + dy
            self._point1._x, self._point1._y = xb, yb
            self._m, self._q = _mq((xa, ya), (xb, yb))
            if self._q == None:
                self._x = None
            else:
                self._x, self._y = xa, ya
        else:
            self._x = self._y = None

    def _draw(self):
        Segment._draw(self)
        self._cv.itemconfigure(self._id, arrow='last')

#  def __add__(self, other):
#    return VectorVsumV(self, other)
#
#  def __sub__(self, other):
#    return VectorVdiffV(self, other)
#
    def __add__(self, other):
        return VectorVopV(self, other, '+')

    def __sub__(self, other):
        return VectorVopV(self, other, '-')

#  def __mul__(self, number):
#    return VectorVmulS(self, number)
#
#  def __truediv__(self, number):
#    return VectorVdivS(self, number)
#
    def __neg__(self):
        return VectorVopS(self, None, '-')

    def __mul__(self, number):
        return VectorVopS(self, number, '*')

    def __truediv__(self, number):
        return VectorVopS(self, number, '/')

    def prodscal(self, other):
        xa, ya = self._getdxdy()
        xb, yb = other._getdxdy()
        return xa * xb + ya * yb

    def xcomponent(self):
        """Return vector components."""
        return _DataObject(self, self._getdx, 'xcomponent')

    def ycomponent(self):
        """Return vector components."""
        return _DataObject(self, self._getdy, 'ycomponent')

    def components(self):
        """Return vector components."""
        return _DataObject(self, self._getdxdy, 'Components')

####
# Sum and diff of 2 Vectors
###


class VectorVopV(Vector):
    """Return Vector sum from vect0 and vect1."""

    def __init__(self, vect0, vect1, op,
                 visible=None, color=None, width=None, name=None):
        self._vect0, self._vect1 = vect0, vect1
        point0 = Point(0, 0, visible=False, iplane=vect0._iplane)
        point1 = Point(0, 0, visible=False, iplane=vect0._iplane)
        if op == '+':
            _type = "VectorVsumV"
            self._getxbyb = self._getxbybsum
        elif op == '-':
            _type = "VectorVdiffV"
            self._getxbyb = self._getxbybdiff
        _Straight.__init__(self, (vect0, vect1),
                           point0, point1,
                           visible, color, width, name, _type)
        self._children.append(point1)

    def _setcoords(self):
        """Set coords for draw."""
        if self._vect0._exist() and self._vect1._exist():
            xda, yda = self._vect0._getdxdy()
            xdb, ydb = self._vect1._getdxdy()
            xb, yb = self._getxbyb(xda, xdb, yda, ydb)
            self._point1._x, self._point1._y = xb, yb
            self._m, self._q = _mq((0, 0), (xb, yb))
            if self._q == None:
                self._x = None
            else:
                self._x, self._y = 0, 0
        else:
            self._x = self._y = None

    def _getxbybsum(self, xda, xdb, yda, ydb):
        return xda + xdb, yda + ydb

    def _getxbybdiff(self, xda, xdb, yda, ydb):
        return xda - xdb, yda - ydb

####
# Vectors operations
###


class VectorVopS(Vector):
    """Return Vector product from vect0 and scalar."""

    def __init__(self, vect0, scalar, op,
                 visible=None, color=None, width=None, name=None):
        self._vect0, self._scalar = vect0, scalar
        point0 = Point(0, 0, visible=False, iplane=vect0._iplane)
        point1 = Point(0, 0, visible=False, iplane=vect0._iplane)
        if isinstance(scalar, _DataObject):
            parents = (vect0, scalar)
        else:
            parents = (vect0,)
        if op == '-':
            _type = "VectorVneg"
            self._getxbyb = self._getxbybneg
        elif op == '*':
            _type = "VectorVmulV"
            self._getxbyb = self._getxbybmul
        elif op == '/':
            _type = "VectorVdivV"
            self._getxbyb = self._getxbybdiv
        _Straight.__init__(self, parents,
                           point0, point1,
                           visible, color, width, name, _type)
        self._children.append(point1)

    def _setcoords(self):
        """Set coords for draw."""
        if (self._vect0._exist() and
                (not isinstance(self._scalar, _DataObject) or self._scalar._exist())):
            if isinstance(self._scalar, _DataObject):
                s = self._scalar._data()
            else:
                s = self._scalar
            xda, yda = self._vect0._getdxdy()
            xb, yb = self._getxbyb(xda, yda, s)
            self._point1._x, self._point1._y = xb, yb
            self._m, self._q = _mq((0, 0), (xb, yb))
            if self._q == None:
                self._x = None
            else:
                self._x, self._y = 0, 0
        else:
            self._x = self._y = None

    def _getxbybneg(self, xda, yda, s):
        return -xda, -yda

    def _getxbybmul(self, xda, yda, s):
        return xda * s, yda * s

    def _getxbybdiv(self, xda, yda, s):
        return xda / s, yda / s

####
# Orthogonl class
###


class Orthogonal(_Straight):
    """Return a line orthogonal to line by point."""

    def __init__(self, line, point,
                 visible=None, color=None, width=None, name=None):
        self._line = line
        _Straight.__init__(self, (line, point),
                           Point(0, 0, visible=False,
                                 iplane=line._iplane), point,
                           visible, color, width, name, "Orthogonal")

    def _setcoords(self):
        """Set coefficients equation of line support
        and _point1 coords."""
        if self._point1._exist() and self._line._exist():
            self._x, self._y = self._point1._getcoords()
            m, q = self._line._getmq()
            if m == None:
                self._m = 0
                self._q = self._y
                self._point0._x = q
                self._point0._y = self._y
            elif _verysmall(m):
                self._m = None
                self._q = self._x
                self._point0._x = self._x
                self._point0._y = q
            else:
                self._m = -1. / m
                self._q = self._y - self._m * self._x
                self._point0._x = (-q + self._q) / (m - self._m)
                self._point0._y = (-self._m * q + m * self._q) / (m - self._m)
        else:
            self._x = self._y = None

####
# Parallel class
###


class Parallel(_Straight):
    """Return  line parallel to line by point."""

    def __init__(self, line, point,
                 visible=None, color=None, width=None, name=None):
        self._line = line
        _Straight.__init__(self, (line, point),
                           point, Point(0, 0, visible=False,
                                        iplane=line._iplane),
                           visible, color, width, name, "Parallel")

    def _setcoords(self):
        """Set coefficients equation of line support
        and _point1 coords."""
        if self._point0._exist() and self._line._exist():
            self._x, self._y = self._point0._getcoords()
            m, q = self._line._getmq()
            self._m = m
            if m == None:
                self._q = self._x
            elif _verysmall(m):
                self._q = self._y
            else:
                self._q = self._y - self._m * self._x
            dx, dy = self._line._getdxdy()
            self._point1._x = self._x + dx
            self._point1._y = self._y + dy
        else:
            self._x = self._y = None

####
# FigureObject class
###


class _FigureObject(_ViewObject):
    """Base class for geometrical figures."""

    def __init__(self, parents, intcolor, visible, color, width, name, type):
        self._iplane = parents[0]._iplane
        self._intern = _loadparameter(intcolor, self._iplane.defintcolor)
        _ViewObject.__init__(self, parents, parents[0]._iplane,
                             visible, color, width, name, _NOOBJ4, type)

    def setcolor(self, *args):
        """Set out line color of figure."""
        self._color = colors.codecolor(args)
        self._cv.itemconfigure(self._id, outline=self._color)

    def setintcolor(self, *args):
        """Set intern color of figure."""
        self._intern = colors.codecolor(args)
        self._cv.itemconfigure(self._id, fill=self._intern)

    def perimeter(self):
        """Return figure perimeter."""
        return _DataObject(self, self._getperimeter, 'Perimeter')

    def surface(self):
        """Return figure surface."""
        return _DataObject(self, self._getsurface, 'Surface')

####
# CircleObject class
###


class _CircleObject(_FigureObject):  # view methods names of Intersections
    """Base class for circles."""

#  def __init__(self, parents, intcolor, visible, color, width, name):
#    _FigureObject.__init__(self, parents, intcolor,
#                           visible, color, width, name)

    def _draw(self):
        self._id = self._cv.create_oval(self._getbounds(), outline=self._color,
                                        fill=self._intern, width=self._width)

    def _setcoords(self):
        """Set coefficients equation of circle."""
        if self._center._exist():
            self._x, self._y = self._center._getcoords()
            xc, yc = self._center._getcoords()
            self._setr2()
            if self._exist():
                self._a = -2 * xc
                self._b = -2 * yc
                self._c = -xc * xc - yc * yc + self._r2
            else:
                self._x = None
        else:
            self._x = None

    def _getbounds(self):
        """Return Circle bounds."""
        if self._exist() and self._visible:
            r = math.sqrt(self._r2) * self._iplane.scale[0]
            xc, yc = self._iplane._p2s(self._center._getcoords())
            return xc - r, yc - r, xc + r, yc + r
        else:
            return self._nobounds

    def _getcoords(self):
        return self._x, self._y

    def _getperimeter(self):
        """Return Circle perimeter."""
        if self._exist():
            return 2 * _PI * self._radius
        else:
            return None

    def _getsurface(self):
        """Return Circle area."""
        if self._exist():
            return math.pi * self._radius * self._radius
        else:
            return None

    def _getcenter(self):
        return self._center

    def _getr(self):
        if self._exist():
            return math.sqrt(self._r2)
        else:
            return None

    def _getcoeff(self):
        return self._a, self._b, self._c

    def center(self):
        """Return circle center."""
        return self._center

    def radius(self):
        """Return circle radius."""
        return _DataObject(self, self._getr, 'Radius')

    def _constrainedcoords(self, t):
        x0, y0 = self._center._getcoords()
        x1, y1 = self._point._getcoords()
        if x0 == None or x1 == None:
            return None, None
        else:
            a = math.atan2(y1 - y0, x1 - x0) + t * math.pi
            return x0 + self._radius * math.cos(a), y0 + self._radius * math.sin(a)

    def _getparameter(self, x, y):
        x0, y0 = self._center._getscoords()
        x1, y1 = self._point._getscoords()
        at0 = _atan2(x - x0, y0 - y)
        if at0 != None:
            result = at0 - _atan2(x1 - x0, y0 - y1)
        else:
            result = _atan2(x1 - x0, y0 - y1)
        if result < 0:
            result += 2
        return result

####
# Circle class
###


class Circle(_CircleObject):
    """Return circle with center in center by point."""

    def __init__(self, center, point_seg, intcolor=None,
                 visible=None, color=None, width=None, name=None):
        if isinstance(point_seg, _PointObject):
            self._setr2 = self._setr2_pp
            self._point = point_seg
        elif isinstance(point_seg, Segment):
            self._setr2 = self._setr2_ps
            self._segment = point_seg
            self._point = Point(0, 0, visible=False, iplane=point_seg._iplane)
        else:
            raise IgError('point_seg accept only Points or Segments')
        self._center = center
        self._x, self._y = center._getcoords()
        _CircleObject.__init__(self, (center, point_seg), intcolor,
                               visible, color, width, name, "Circle")
        if isinstance(point_seg, Segment):
            self._children.append(self._point)

    def _setr2_pp(self):
        if self._point._exist():
            xp, yp = self._point._getcoords()
            dx = xp - self._x
            dy = yp - self._y
            self._r2 = dx * dx + dy * dy
            self._radius = math.sqrt(self._r2)
            if self._r2 > _M2:
                self._x = None
        else:
            self._x = None

    def _setr2_ps(self):
        if self._segment._exist():
            xa, ya = self._segment._point0._getcoords()
            xb, yb = self._segment._point1._getcoords()
            dx = xb - xa
            dy = yb - ya
            self._r2 = dx * dx + dy * dy
            self._radius = math.sqrt(self._r2)
            if self._r2 > _M2:
                self._x = None
            else:
                xc, yc = self._center._getcoords()
                self._point._x = xc + dx
                self._point._y = yc + dy
        else:
            self._r2 = None
            self._x = None

####
# Polygonal class
###


class Polygonal(_FigureObject):
    """Returns a polygonal given vertices."""

    def __init__(self, vertices, intcolor=None,
                 visible=None, color=None, width=None, name=None,
                 type="Polygonal"):
        self.vertices = vertices
        _FigureObject.__init__(self, vertices, intcolor,
                               visible, color, width, name, type)

    def _setcoords(self):
        self._x, self._y = self.vertices[0]._getcoords()

    def _draw(self):
        self._id = self._cv.create_line(self._getbounds(), fill=self._color,
                                        width=self._width)

    def _getbounds(self):
        """Return Polygon bounds."""
        if self._visible:
            result = []
            for p in self.vertices:
                x, y = p._getscoords()
                if x == None:
                    self._x = None
                    return self._nobounds
                else:
                    result.append(x)
                    result.append(y)
            return tuple(result)
        else:
            return self._nobounds

    def _getnumofsides(self):
        return len(self.vertices)

    def numofsides(self):
        """Return polygonal num of sides."""
        return _DataObject(self, self._getnumofsides, 'NumOfSides')

####
# CurviLine class
###


class CurviLine(Polygonal):
    """Return a curviline."""

    def __init__(self, vertices, intcolor=None,
                 visible=None, color=None, width=None, name=None):
        Polygonal.__init__(self, vertices, intcolor,
                           visible, color, width, name, "CurviLine")

    def _draw(self):
        self._id = self._cv.create_line(self._getbounds(), fill=self._color,
                                        width=self._width, smooth=True,
                                        joinstyle='round')
#                                   joinstyle = tk.MITER, smooth = True)
#                                   joinstyle = tk.ROUND, smooth = True)
#                                   joinstyle = tk.BEVEL)
#                                   joinstyle = tk.MITER)
#                                   joinstyle = tk.ROUND)

####
# Polygon class
###


class Polygon(Polygonal):
    """Returns a polygon given vertices."""

    def __init__(self, vertices, intcolor=None,
                 visible=None, color=None, width=None, name=None):
        Polygonal.__init__(self, vertices, intcolor,
                           visible, color, width, name, "Polygon")

    def _draw(self):
        self._id = self._cv.create_polygon(self._getbounds(), outline=self._color,
                                           fill=self._intern, width=self._width)

    def _getperimeter(self):
        """Return Polygon perimeter."""
        if self._exist():
            result = 0
            x0, y0 = self.vertices[-1]._getcoords()
            for _vertex in self.vertices:
                x1, y1 = _vertex._getcoords()
                result += math.hypot(x1 - x0, y1 - y0)
                x0, y0 = x1, y1
            return result
        else:
            return None

    def _getsurface(self):
        """Return Polygon area."""
        if self._exist():
            result = 0
            x0, y0 = self.vertices[-1]._getcoords()
            for _vertex in self.vertices:
                x1, y1 = _vertex._getcoords()
                result += (x1 - x0) * (y1 + y0)
                x0, y0 = x1, y1
            return abs(result / 2)
        else:
            return None

####
# Angle class
###


class Angle(_ViewObject):
    """Return a AngleObject."""

    def __init__(self, point0, vertex, point_ang, sides=tuple(),
                 visible=None, color=None, width=None, name=None):
        if isinstance(point_ang, _PointObject):
            self._setcoords = self._setcoords_ppp
            point1 = point_ang
        elif isinstance(point_ang, Angle):
            self._setcoords = self._setcoords_ppa
            self._angle = point_ang
            point1 = Point(0, 0, visible=False, iplane=point0._iplane)
        else:
            raise IgError('point_ang accept only Points or Angles')
        self._point0 = point0
        self._vertex = vertex
        self._point1 = point1
        self._start = None
        self._extent = None
        _ViewObject.__init__(self, (point0, vertex, point_ang),
                             point0._iplane, visible, color, width, name, _NOOBJ4,
                             "Angle")
        if isinstance(point_ang, Angle):
            self._children.append(point1)
        if sides == True:
            sides = (0, 1)
        elif type(sides) == int:
            sides = (sides,)
        if 0 in sides:
            self._side0 = self.side0(visible, color, width)
        if 1 in sides:
            self._side1 = self.side1(visible, color, width)

    def _draw(self):
        self._id = self._cv.create_arc(self._getbounds(),
                                       start=self._start, extent=self._extent,
                                       outline=self._color, width=self._width,
                                       style=tk.ARC)

    def _move(self):
        _ViewObject._move(self)
        self._cv.itemconfigure(
            self._id, start=self._start, extent=self._extent)

    def _getbounds(self):
        """Return Polygon bounds."""
        if self._exist() and self._visible:
            dim = self._width * self._point0._iplane.defangledim
            sx, sy = self._iplane._p2s((self._x, self._y))
            return sx - dim, sy - dim, sx + dim, sy + dim
        else:
            return self._nobounds

    def _getscoords(self):
        return self._vertex._getscoords()

    def _getcoords(self):
        return self._vertex._getcoords()

    def _getextent(self):
        if self._exist():
            return self._extent
        else:
            return None

    def extent(self):
        """Return angle extent."""
        return _DataObject(self, self._getextent, 'Extent')

    def bisector(self, *arg, **args):
        """Return angle bisector."""
        return Bisector(self, *arg, **args)

    def vertex(self):
        """Return angle vertex."""
        return self._vertex

    def point0(self):
        """Return angle point0."""
        return self._point0

    def point1(self):
        """Return angle point1."""
        return self._point1

    def side0(self,
              visible=None, color=None, width=None, name=None):
        """Ray from vertex to point0."""
        try:
            return self._side0
        except:
            return AngleSide(self, 0, visible, color, width, name)

    def side1(self,
              visible=None, color=None, width=None, name=None):
        """Ray from vertex to point1."""
        try:
            return self._side1
        except:
            return AngleSide(self, 1, visible, color, width, name)

    def _setcoords_ppp(self):                 # Point, Point, Point
        if (self._vertex._exist() and self._point0._exist()
                and self._point1._exist()):
            xv, yv = self._x, self._y = self._vertex._getcoords()
            x0, y0 = self._point0._getcoords()
            x1, y1 = self._point1._getcoords()
            self._start = _atandeg(x0 - xv, y0 - yv)
            self._stop = _atandeg(x1 - xv, y1 - yv)
            if self._start != None and self._stop != None:
                self._extent = self._stop - self._start
                if self._extent < 0:
                    self._extent += 360
            else:
                self._x = None
        else:
            self._x = None

    def _setcoords_ppa(self):                 # Point, Point, Angle
        if (self._angle._exist() and
                self._vertex._exist() and self._point0._exist()):
            xv, yv = self._x, self._y = self._vertex._getcoords()
            xp0, yp0 = self._point0._getcoords()
            a = self._angle
            xp1, yp1 = a._point1._getcoords()
            xva, yva = a._vertex._getcoords()
            self._start = _atandeg(xp0 - xv, yp0 - yv)
            if self._start != None:
                self._extent = a._extent
                self._stop = self._start + self._extent
                l = math.hypot(xp1 - xva, yp1 - yva)
                stop = self._stop / _CONV
                self._point1._x = xv + l * math.cos(stop)
                self._point1._y = yv + l * math.sin(stop)
            else:
                self._x = None
        else:
            self._x = None

####
# Text class
###


class Text(_DragableObject):
    """Return text written in x, y position."""

    def __init__(self, x, y, text,
                 visible=None, color=None, width=None, name=None, iplane=None):
        iplane = getiplane(iplane)
        self._x = x
        self._y = y
        self._text = text
        self._width = _loadparameter(width, iplane.defwidthtext)
        self._font = tkfont.Font(family="Nimbus Sans L", size=self._width)
##    self._font = tkfont.Font(family="Helvetica", size = self._width)
        _DragableObject.__init__(self, tuple(),
                                 iplane, visible, color, self._width, name,
                                 _NOOBJ2, "Text")

    def _draw(self):
        try:
            self._id = self._cv.create_text(self._getbounds(),
                                            text=self._gettext(),
                                            fill=self._color, font=self._font)
        except IndexError:
            raise IndexError(
                "VarText needs more objects to complete the string")

    def _gettext(self):
        return self._text

    def setwidth(self, w):
        """Sets text width to w."""
        self._width = w
        self._font.config(size=w)

    def _getbounds(self):
        if self._exist() and self._visible:
            sx, sy = self._iplane._p2s((self._x, self._y))
            return sx, sy
        else:
            return self._nobounds

####
# Label class
###


class Label(Text):
    """Return a label of obj."""

    def __init__(self, obj, x, y, text,
                 visible=None, color=None, width=None, name=None):
        self._dx = x
        self._dy = -y
        self._obj = obj
        self._text = text
        self._width = _loadparameter(width, obj._iplane.defwidthtext)
        self._font = tkfont.Font(family="Nimbus Sans L", size=self._width)
##    self._font = tkfont.Font(family="Helvetica", size = self._width)
        if obj._exist():
            self._x = obj._x + x
            self._y = obj._y - y
        else:
            self._x = None
        _DragableObject.__init__(self, (obj,),
                                 obj._iplane, visible, color,
                                 self._width, name, _NOOBJ2, "Label")

    def _getbounds(self):
        """Return Text coords."""
        if self._exist() and self._visible:
            ox, oy = self._obj._getscoords()
            sx = ox + self._dx
            sy = oy + self._dy
            return sx, sy
        else:
            return self._nobounds

    def _drag(self, dx, dy):
        self._dx += dx
        self._dy += dy
        self._move()

    def _exist(self):
        """True if self.data exist."""
        return self._obj._exist()

####
# VarText class
###


class VarText(Text):
    """Return a text filled with variables written in x, y position."""

    def __init__(self, x, y, text, variables,
                 visible=None, color=None, width=None, name=None, iplane=None):
        iplane = getiplane(iplane)
        self._x = x
        self._y = y
        self._text = text
        self._width = _loadparameter(width, iplane.defwidthtext)
        self._font = tkfont.Font(family="Nimbus Sans L", size=self._width)
        if type(variables) != tuple:
            variables = (variables,)
        self._var = variables
        _DragableObject.__init__(self, variables,
                                 iplane, visible, color, self._width, name,
                                 _NOOBJ2, "VarText")

    def _move(self):
        Text._move(self)
        self._cv.itemconfig(self._id, text=self._gettext())

    def _gettext(self):
        #    return self._text % tuple([str(e) for e in self._var])
        return self._text.format(*self._var)

####
# VarLabel class
###


class VarLabel(Label):
    """Return a label of obj filled with variables written in x, y position."""

    def __init__(self, obj, x, y, text, variables,
                 visible=None, color=None, width=None, name=None):
        self._dx = x
        self._dy = -y
        self._obj = obj
        self._text = text
        self._width = _loadparameter(width, obj._iplane.defwidthtext)
        self._font = tkfont.Font(family="Nimbus Sans L", size=self._width)
##    self._font = tkfont.Font(family="Helvetica", size = self._width)
        if obj._exist():
            self._x = obj._x + x
            self._y = obj._y - y
        else:
            self._x = None
        if type(variables) != tuple:
            variables = (variables,)
        self._var = variables
        _DragableObject.__init__(self, variables + (obj,),
                                 obj._iplane, visible, color,
                                 self._width, name, _NOOBJ2, "VarLabel")

    def _gettext(self):
        #    return self._text % tuple([str(e) for e in self._var])
        return self._text.format(*self._var)

    def _move(self):
        Label._move(self)
        self._cv.itemconfig(self._id, text=self._gettext())


if __name__ == "__main__":
    from sys import path
    from os.path import join
    path.append(join('..', 'test'))
    import test_pyig
#  test_pyig.alltests(test_pyig.loc)
