#!/usr/bin/env python

import os
import sys

from setuptools import setup

os.chdir(os.path.dirname(sys.argv[0]) or ".")

from pygraph import __version__


setup(
    name="pygraph",
    version=__version__,
    description="A python library to plot interactive geometries, turtle graphics",
    long_description=open("readme.txt", "rt").read(),
    url="https://bitbucket.org/zambu/pygraph",
    author="Daniele Zambelli",
    author_email="daniele.zambelli@gmail.com",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
    ],
    packages=['pygraph'],
    package_dir={'pygraph': 'pygraph',},
    package_data={'pygraph': ['rgb.txt', ],},
    keywords=['turtle', 'geometry'],
)
