18-1-2018 versione 3.2.5 = 3.3.0 RC01
 Rivisto tutto il materiale.
6-1-2018 versione 3.2.4
 pycart aggiunta la forma del punto in drawpoint.
 pyplot aggiunto il dominio, parametri "d_from" e "d_to" nel disegno di 
 funzioni
 Aggiunto setup.py per l'installazione automatica.
 Modificata la documentazione in particolare per la chiamata deelle 
 librerie: "import pygraph.pycart as cg".
13-02-2017 versione 3.02
 Aggiunta la gestione dei tasti freccia.
 Aggiunto un abbozzo del gioco snake.
26-12-2013 versione 3.01
 Compatibile con Python 3
 Aggiunti i collegamenti per rendere reattivo il piano.
 Aggiunta la possibilit� di scegliere un colore per lo sfondo del piano.
25-09-2010 versione 2.6rc01
 grande lavoro di ristrutturazione. Conviene guardare il manuale e gli  
 esempi. 
 Ristrutturato il codice, modificato un po' tutto, anche l'interfaccia 
 utente.
10-1-2008 versione 2.03.03
 Corretto errore in _CircleObject._getparameter
 Basta darlo in mano agli alunni che ti beccano subito un altro errore!
8-1-2008 versione 2.03.02
 Modificato il comportamento di Calc.
 Aggiunto l'esempio: 1400CircGonio.py
 Aggiunto l'esempio: 1390AngCirc.py
 Ripuliti anche gli altri sorgenti.
 Avevo lasciato un mucchio di spazzatura in pyig, tolta.
 Aggiunti la classe AngleSide e modificati i metodi side0 e side1.
 Accidenti gi� trovato un errore: corretto errore relativo a Angle
6-1-2008 versione 2.03.00
 Aggiornato il manuale.
 Aggiunti: oggetto Name e metodo name
 Commentato il codice di pyig.
 Anche i testi hanno un parametro width funzionante.
 Modificati gli esempi relaivi alle trasformazioni geometriche, e agli
   inviluppi in modo da utilizzare in modo sensato PointOn e
   ConstrainedPoint
 Funzione e metodo che restituisce il parametro di un ConstrainedPoint o di
   di un PointOn.
 Aggiunta la classe PointOn che crea un punto su una linea o una
   circonferenza in una posizione determinata da un parametro e non
   modificabile per trascinamento con il mouse.
 I ConstrainedPoint possono essere trascinati con il mouse.
 I punti trascinabili con il mouse hanno un colore di default diverso
 dal colore di default degli altri oggetti.
 Anche in pyig ci si pu� riferire ai colori usando il nome.
 Modificata la gestione dei colori.
 Modificata la gerarchia delle classi, ora il comando help con un
 oggetto di pygraph � un po' pi� sensato.
15-11-2006 versione 2.01.01
 Aggiunto un esempio per pyplot
 Aggiunte a pycart le funzioni drawpoint e drawsegment aggiornato il manuale
 Corretto bug di CircleCircleIntersection nel caso di circ. coincidenti
15-06-2006 versione 2.01.00
 Ripassati gli esempi di pyig
 Ridisegnata la gerarchia degli oggetti Text
 Corretto bug in _getfactory
 Aggiunta nel manuale la sezione: Avviare idle con le librerie
 Aggiunto esempio Concoide2
 Aggiunti oggetti: Polygonal e CurviLine
 Aggiunti esempi: Nefroide, InvParabola, InvConica, Concoide1
 Modificati i commenti nel sorgente in modo da migliorare la documentazione
   automatica e i suggerimenti durante la scrittura delle funzioni.
 Aggiunti gli esempi: Traslazione, Rotazione, Simmetria e Omotetia
 Corretto bug in viewfun.py
 Aggiunti gli esempi criteri*
 Aggiunti gli esempi inviluppi*
 Aggiunto la classe AngleRA e aggiornato il manuale
 corretti alcuni errori nel manuale.
 Aggiunto la classe ConstrainedPoint e aggiornato il manuale.
