Pyplot
======

*Dove viene presentata la libreria Pyplot*

Introduzione
------------

``pyplot`` è una libreria che permette di tracciare grafici di funzioni
matematiche. Fornisce alcuni metodi che permettono di tracciare grafici di:

* funzioni del tipo: y=f(x) e x=f(y);
* funzioni polari: ro=f(th);
* funzioni parametriche: x=f(t), y=g(t);
* e varie successioni.

Nella libreria sono presenti:

* Una funzione che restituisce il numero di versione corrente.
* La classe ``PlotPlane`` con alcuni metodi che permettono di gestire gli 
  elementi base di un piano cartesiano.
* La classe ``Plot`` con alcuni metodi che permettono di tracciare il grafico 
  di successioni e di funzioni matematiche.

Di seguito vengono descritti questi elementi. Per ognuno verrà indicato:

* lo scopo
* la sintassi
* alcune osservazioni
* un esempio di uso

``version``
-----------

**Scopo**

È una funzione che restituisce il numero di versione della libreria.

**Sintassi** 

  ``version()``

**Osservazioni**

Non richiede parametri, restituisce la stringa che contiene la versione.

**Esempio**

Controllare la versione della libreria. ::

  import pygraph.pyplot as pp
  if pp.version()<"3.1.00":
      print "versione un po\' vecchiotta"
  else:
      print "versione:", pp.version()

``class pp.PlotPlane``
-------------------

``PlotPlane`` Estende la classe ``Plane``, quindi è in grado di fare tutto 
quello che fa ``Plane``. Cambiano i valori predefiniti che vengono 
utilizzati quando viene creato un oggetto di questa classe e c'è un 
ulteriore metodo.

In questa sezione presento solo le differenze e aggiunte alla classe 
``Plane``.
Per tutto ciò che rimane invariato si veda il capitolo relativo a 
``pycart``.

.. note::
  Per poter creare oggetti della libreria ``pyplot`` bisogna prima 
  di tutto importarla. 
  In tutti gli esempi seguenti si suppone che sia già stato eseguito 
  il comando:

    ``import pygraph.pyplot as pp``

  Se nell'eseguire un esempio ottenete un messaggio che termina con:
  ``NameError: name 'pp' is not defined``, forse avete dimenticato 
  di caricare la libreria con il comando scritto sopra.

Se nell'eseguire gli esempi osservate dei comportamenti strani della 
finestra che contiene il piano cartesiano, portate pazienza, o riguardate 
quanto scritto nella descrizione del metodo ``mainloop`` di ``Plane`` nel 
capitolo su ``pycart`` dove ho cercato di dare delle indicazioni in 
proposito.

``pyplot`` si appoggia su altre due librerie:

* ``math`` per avere a disposizione tutte le funzioni matematiche.
* ``pycart`` per le funzioni del piano cartesiano.

``__init__``
............

**Scopo**

Crea il piano cartesiano e inizializza gli attributi di ``PlotPlane``.

**Sintassi** 

  ``<nome_variabile> = pl.PlotPlane(<parametri>)``

**Osservazioni**

Questo metodo non viene chiamato esplicitamente, ma viene eseguito
quando si crea un oggetto di questa classe.
L'intestazione di questo metodo è::

  def __init__(self, name="Functions pp.Plotter",
               w=400, h=400,
               sx=20, sy=None,
               ox=None, oy=None,
               axes=True, grid=True,
               axescolor='black', gridcolor='black',
               parent=None):

Si può vedere che presenta molti parametri tutti con un valore predefinito.
Nel momento in cui si crea un piano cartesiano si possono quindi decidere
le sue caratteristiche. Vediamole in dettaglio:

* titolo della finestra, valore predefinito: "Functions pp.Plotter";
* dimensione, valori predefiniti: larghezza=400, altezza=400;
* scala di rappresentazione, valori predefiniti: una unità = 20 pixel;
* posizione dell'origine, valore predefinito: il centro della finestra;
* rappresentazione degli assi cartesiani, valore predefinito: ``True``;
* rappresentazione di una griglia di punti, valore predefinito: ``True``;
* colore degli assi valore predefinito: 'black'.
* colore della griglia valore predefinito: 'black'.
* riferimento alla finestra che contiene il piano cartesiano,
  valore predefinito: ``None``.

Poiché tutti i parametri hanno un valore predefinito, possiamo creare un
oggetto della classe ``PlotPlane`` senza specificare alcun argomento: 
verranno
usati tutti i valori predefiniti. Oppure possiamo specificare per nome gli
argomenti che vogliamo siano diversi dal comportamento predefinito, si 
vedano 
di seguito alcuni esempi.

**Esempio**

Si vedano tutti gli esempi seguenti.

``newPlot``
...........

**Scopo**

Per creare un nuovo *tracciatore di funzioni*.

**Sintassi** 

  ``<nome_variabile> = <piano>.newPlot([color=<colore>, width=<numero>])``

**Osservazioni**

Normalmente è utile assegnare ad un identificatore 
il *tracciatore di funzioni* creato.

L'intestazione di questo metodo è:

  ``def newPlot(self, color='black', width=1)``

I parametri hanno un valore predefinito, per creare un tracciatore di 
funzioni di colore nero e larghezza 1 non occorre passare argomenti alla 
funzione::

  plot = piano.newPlot()

**Esempio**

Disegna una parabola di equazione: :math:`y = 2x^2-x+1` ::

  piano = pp.PlotPlane("Piano 2")
  plot = piano.newPlot(color='red', width=2)
  plot.xy(lambda x: 0.2*x*x-x+1)
  piano.mainloop()

``class pp.Plot``
--------------

``Plot`` discende da ``Pen`` e mette a disposizione, quindi,
tutti gli attributi e i metodi di questa classe. Oltre a questi, che sono
descritti nel capitolo precedente, contiene alcuni *metodi* che permettono
di tracciare, facilmente, svariati tipi di funzioni.

``__init__``
............

**Scopo**
Crea il Tracciatore di funzioni.

**Sintassi** 

  ``<nome_variabile> = pp.Plot(<parametri>)``

**Osservazioni**

L'intestazione di questo metodo è:

  ``def __init__(self, color='black', width=1)``

L'oggetto come per l'oggetto ``Pen``, anche ``Plot`` viene inserito 
nell'ultimo piano creato, a meno che non si chieda esplicitamente un 
comportamento diverso.
Come appare dall'intestazione del metodo, alla creazione di un oggetto 
``Plot``, si possono anche specificare:

* il colore, valore predefinito: color='black';
* lo spessore del tratto, valore predefinito: width=1.

Dato che i parametri hanno valori predefiniti, possiamo creare oggetti 
``Plot``
passando un numero variabile di argomenti. Gli argomenti 
non specificati saranno sostituiti con i valori predefiniti.

Il metodo ``__init__`` non viene chiamato esplicitamente, ma viene eseguito
quando si crea un oggetto di questa classe. 

.. note::
  vedi il metodo ``newPlot`` di ``PlotPlane`` per un altro modo per creare 
  tracciatori di funzioni. Le due istruzioni::
  
    piano = pp.PlotPlane(sx=100, sy=100, axes=True)
    plot = pp.Plot(color='violet', width=3)
    
  e::
  
    piano = Plane(sx=100, sy=100, axes=True)
    plot = piano.newPlot(color='violet', width=3)
  
  sono del tutto equivalenti.

**Esempio**

Costruire un oggetto della classe ``Plot``, tracciare una parabola e 
disegnare un quadrilatero inscritto.::

  def parabola(x):
      return x**2-7

  piano = pp.PlotPlane('x^2-7')
  plot = pp.Plot(color='red', width=2)
  plot.xy(parabola)
  plot.drawpoly(((-1, parabola(-1)), (1, parabola(1)), 
                 (3, parabola(3)), (-3, parabola(-3))), 'gray')
  piano.mainloop()

``xy``
......

**Scopo**

Traccia il grafico di una funzione nella forma ``y=f(x)``.

**Sintassi** 

  ``<plot>.xy(<funzione>[, d_from=<numero>][, d_to=<numero>]``
  ``[, color=<colore>][,width=<numero>])``

**Osservazioni**

* ``funzione`` è una funzione ``Python`` con un parametro, può anche essere 
  definita con una funzione ``lambda``;
* ``d_from`` e ``d_to`` estremi dell'intervallo in cui tracciare la funzione;
* ``color`` e ``width`` colore e spessore del grafico.

Solo ``funzione`` è un argomento obbligatorio, gli altri hanno tutti valori 
predefiniti.

**Esempio**

Disegnare la funzione: y=sin(1./x). ::

  def fun(x):
      return math.sin(1./x)

  piano = pp.PlotPlane('sin(1./x)', w=600, h=250, sx=100, sy=100)
  plot = pp.Plot(color='green', width=2)
  plot.xy(fun)
  piano.mainloop()

Usando una funzione ``lambda``, lo stesso programma può essere scritto in 
modo più semplice così::

  piano = pp.PlotPlane('sin(1./x)', w=600, h=250, sx=100, sy=100)
  plot = pp.Plot(color='brown', width=2)
  plot.xy(lambda x: math.sin(1./x))
  piano.mainloop()

.. note::
  Nel primo caso viene creata una funzione ``Python`` che viene associata 
  a un identificatore (in questo caso ``fun``), poi la funzione viene 
  passata come argomento al metodo ``xy`` dell'oggetto di nome ``plot``.
  Nel secondo caso, la funzione viene creata e passata direttamente come
  argomento al metodo ``xy``.

Se vogliamo disegnare la funzione seno tra ``-90°`` e ``+90°``::

  piano = pp.PlotPlane('sin(x)', w=600, h=250, sx=100, sy=100)
  plot = pp.Plot(color='red', width=2)
  plot.xy(lambda x: math.sin(x), d_from=-math.pi/2, d_to=+math.pi/2)
  piano.mainloop()

``yx``
......

**Scopo**

Traccia il grafico di una funzione nella forma ``x=f(y)``.

**Sintassi** 

  ``<plot>.yx(<funzione>[, color=<colore>][,width=<numero>])``

**Osservazioni**

Vedi le osservazioni a `xy`.

**Esempio**

Disegnare una parabola con l'asse di simmetria parallelo all'asse y 
e una con l'asse di simmetria parallelo all'asse x. ::

  def parabola(var):
      return .3*var**2+3*var+4

  piano = pp.PlotPlane("Parabole: xy, yx", w=600, h=600)
  plot = pp.Plot(width=2)
  plot.xy(parabola, color='green')
  plot.yx(parabola, color='magenta')
  piano.mainloop()

Anche nel il metodo ``yx`` è possibile definire gli estremi dell'intervallo 
del grafico della funzione::

  piano = pp.PlotPlane('Funzione a tratti yx')
  p = pp.Plot(width=2)
  p.yx(lambda y: 1/2*y+2, color='gold', width=6, d_to=-3.5)
  p.yx(lambda y: -1/2*y*y, color='maroon', width=4, d_from=-3.5, d_to=2.5)
  p.yx(lambda y: 1/2*y+2, color='red', d_from=2.5)
  piano.mainloop()

``polar``
.........

**Scopo**

Traccia il grafico di una funzione polare nella forma: ro=f(th).

**Sintassi** 

  ``<plot>.polar(<funzione>[, <ma>][, color=<colore>][,width=<numero>])``

**Osservazioni**

L'intestazione di questo metodo è:

  ``def polar(self, f, ma=360, color=None, width=None)``

Oltre alle osservazioni fatte per ``xy``, il parametro ``ma`` contiene il 
massimo valore per la variabile ``th`` (il minimo è ``0``). Il valore
predefinito di ``ma`` è ``360``.

**Esempio**

Disegna tre funzioni polari in tre finestre diverse. ::

  piano_0 = pp.PlotPlane("quadrifoglio", w=300, h=300)
  piano_1 = pp.PlotPlane("cardioide", w=400, h=400)
  piano_2 = pp.PlotPlane("spirale", w=500, h=500)
  plot_0 = piano_0.newPlot(color='navy', width=2)
  plot_1 = pp.Plot(plane=piano_1, color='maroon', width=4)
  plot_2 = pp.Plot(color='gold', width=6)
  plot_0.polar(lambda th: 5*math.cos(2*th))
  plot_1.polar(lambda th: 8*math.sin(th/2))
  plot_2.polar(lambda th: th, 720)
  piano_0.mainloop()
  
Possiamo limitare anche il garfico di una funzione polare::

  piano = pp.PlotPlane("test_14: from-to polar")
  p = piano.newPlot(width=2)
  p.polar(lambda th: 5*math.cos(th), d_from=90, d_to=180, color='navy')
  p.polar(lambda th: 8*math.sin(th), d_from=270, d_to=360, color='maroon')
  piano.mainloop()

``param``
.........

**Scopo**

Traccia il grafico di una funzione parametrica nella forma: x=fx(t) y=fy(t).

**Sintassi** 

  ``<plot>.param(<fx>, <fy>[, d_from][, d_to][, color=<colore>]`` 
  ``[,width=<numero>])``

**Osservazioni**

L'intestazione di questo metodo è:

  ``def param(self, fx, fy, d_from=0, d_to=100, color=None, width=None)``

``<fx>`` e ``<fy>`` sono funzioni ``Python`` con un parametro che possono
essere create con una funzione ``lambda``.
Il parametro ``<mi>`` ha come valore predefinito 0. 
Il parametro ``<ma>`` ha come valore predefinito 100.
Si possono specificare il colore e lo spessore del tratto.

**Esempio**

Disegnare una funzione parametrica. ::

  piano = pp.PlotPlane("test_15: from-to param", w=500, h=500)
  p = pp.Plot(color='blue', width=3)
  p.param(lambda t: 7*math.cos(3*t/180.)+3*math.cos(3*t/180.+8*t/180.),
          lambda t: 7*math.sin(3*t/180.)-3*math.sin(3*t/180.+8*t/180.),
          d_from=0, d_to=math.pi*90)
  piano.mainloop()

``ny``
......

**Scopo**

Traccia il grafico di una successione nella forma ``y=f(n)`` dove ``n`` è
un numero naturale.

**Sintassi** 

  ``<plot>.ny(<funzione>[, d_from][, d_to][, trace=True][, values=True]`` 
  ``[, color=<colore>][,width=<numero>])``

**Osservazioni**

Per ``<funzione>`` valgono le solite considerazioni tenendo presente che
la variabile indipendente è un numero naturale il grafico sarà quindi
costituito da punti isolati.

``trace``, se posto uguale a ``True``, traccia una linea che collega i punti
consecutivi.

``value``, se posto uguale a ``True``, stampa i valori della successione.

**Esempio**

Disegnare alcune successioni. ::

  piano = pp.PlotPlane("ex_06: ny", sx=10, sy=10, ox=10)
  plot = pp.Plot(color='blue', width=3)
  plot.ny(lambda n: n/2+3)
  plot.ny(lambda n: 3*math.sin(3*n), trace=True, values=True, color='gold')
  plot.ny(lambda n: (-10*n-6)/(n), trace=True, color='maroon')
  piano.mainloop()
  
Anche in questo caso possiamo limitare il dominio delle funzioni::

  piano = pp.PlotPlane("test_16: from-to ny", sx=10, sy=10, ox=10)
  p = piano.newPlot(width=3)
  p.ny(lambda n: n/2+3, color='blue', d_from=0, d_to=10)
  p.ny(lambda n: 3*math.sin(3*n), d_from=10, d_to=20, 
       trace=True, values=True, color='gold')
  p.ny(lambda n: (-10*n-6)/(n), d_from=20, 
       trace=True, color='maroon')
  piano.mainloop()

``succ``
........

**Scopo**

Traccia il grafico di una successione dato il primo elemento e la
funzione che restituisce l'"enne_più_unesimo" dato l'"ennesimo".

**Sintassi** 

  ``<plot>.succ(<a0>, <fan> [, trace=True][, values=True]``
  ``[, color=<colore>][,width=<numero>])``

**Osservazioni**

``<a0>`` è il primo valore della successione,

``<fan1>`` è una funzione che può avere:
* un solo parametro: ``an``
* due parametri: ``an`` e ``n``
* tre parametri: ``an``, ``n`` e ``a0``

``trace``, se posto uguale a ``True``, traccia una linea che collega i punti
consecutivi.

``value``, se posto uguale a ``True``, stampa i valori della successione.

**Esempio**

Disegnare alcune successioni. ::

  def grandine(an):
      if an % 2: return 3*an+1
      else:      return an//2
    
  piano = pp.PlotPlane("succ", sx=10, sy=10, ox=10)
  plot = pp.Plot(color='green', width=3)
  plot.succ(7, grandine, trace=True, values=True)
  plot.succ(2, lambda an, n: an/10-an, trace=True, 
            values=True, color='blue')
  plot.succ(-2, lambda an, n: an-(n*2-1))
  plot.succ(18, lambda an, n: an**0.5-(-1)**(n+1)*2./3+3, 
            trace=True, values=True, color='maroon')
  plot.succ(50, lambda an, n, a0: (an+a0/an)/2, values=True, color='pink')
  piano.mainloop()

Libreria ``pyplot``
-------------------

Funzioni
........

.. glossary::

  ``version``
    Restituisce la versione della libreria.

Classi
......

.. glossary::

  ``PlotPlane``
    Piano cartesiano che permette di tracciare grafici di funzioni piane.
    Ha tutti gli attributi e i metodi di Plane.

  ``Plot``
    Tracciatore di grafici di funzioni piane.
    Ha tutti gli attributi e i metodi di pp.Pen.

Attributi
.........

.. glossary::

  ``<oggetto_visibile>.color`` 
    Attributo degli oggetti geometrici: imposta il colore dell'oggetto;
    

Metodi
......

.. glossary::

  ``<plot>.__init__``
    Crea un tracciatore grafico.

  ``<plot>.ny``
    Traccia una funzione del tipo: y=f(n) con n numero naturale.

  ``<plot>.param``
    Traccia una funzione parametrica.

  ``<plot>.polar``
    Traccia una funzione in coordinate polari.

  ``<plot>.succ``
    Traccia una successione.

  ``<plot>.xy``
    Traccia una funzione del tipo: y=f(x).

  ``<plot>.yx``
    Traccia una funzione del tipo: x=f(y).

  ``<plotplane>.__init__``
    Crea un piano.

  ``<plotplane>.newPlot``
    Crea un tracciatore di funzioni.

  ``<plotplane>.delplotters``
    Elimina tutti i tracciatori di funzioni presenti.

  ``<plotplane>.plotters``
    Restituisce una lista con tutti i tracciatori di funzioni presenti nel
    piano.

