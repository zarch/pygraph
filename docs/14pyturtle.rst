Pyturtle
========

*Dove viene presentata la libreria Pyturtle*

Introduzione
------------

``pyturtle`` è una libreria che implementa la grafica della tartaruga.
Contiene la classe ``Turtle`` che ha alcuni metodi che permettono di:

* Muovere la tartaruga avanti o indietro e farla ruotare su sé stessa.
* Sollevare la penna o riappoggiarla sul piano di disegno.
* Cambiare alcuni parametri relativi alla penna e al disegno.
* ``pyturtle`` fornisce una funzione: ``version`` e
  due classi: ``TurtlePlane`` e ``Turtle``.

.. note::
  Per poter creare tartarughe  bisogna importare la libreria 
  ``pyturtle``. 
  In tutti gli esempi seguenti si suppone che sia già stato eseguito 
  il comando:

    ``import pyTurtle as tg``

  Se nell'eseguire un esempio ottenete un messaggio che termina con:
  ``NameError: name 'tg' is not defined``, forse avete dimenticato 
  di caricare la libreria ``pyturtle`` con il comando scritto sopra.

Nell'eseguire gli esempi può darsi che ci siano dei comportamenti strani 
della finestra che contiene il piano delle tartarughe:

* La finestra stessa non risponde ai comandi: si può ignorare il fatto o 
  eseguire il metodo ``mainloop()`` del piano.
* Il programma non termina neppure quando viene chiusa la finestra: bisogna 
  **non** eseguire il metodo ``mainloop()`` del piano.

Per ulteriori informazioni riguardate quanto scritto
nella descrizione del metodo ``mainloop`` di ``Plane`` nel capitolo su 
``pycart`` dove ho cercato di dare delle indicazioni in proposito.

Se vengono creati più piani basta chiamare il ``mainloop()`` per un solo 
piano (si veda il primo esempio della classe ``TurtlePlane`` dove si può 
provare a *decommentare* (togliere il carattere cancelletto '#') 
all'istruzione:

  ``#tp0.mainloop()``
  
per vedere la differenza.

``pyturtle`` si appoggia sulle librerie:

* ``math`` per alcune funzioni matematiche,
* ``pycart`` per il piano cartesiano,
* ``pygrapherror`` per i messaggi di errore.

Nella libreria ``pyturtle`` sono presenti:
* la funzione ``version``, 
* la classe ``TurtlePlane`` che fornisce lo spazio dove possono vivere le 
tartarughe,
* la classe ``Turtle`` che mette a disposizione i metodi fondamentali per 
la grafica della tartaruga.

Di seguito vengono presentati questi tre oggetti. Per le due classi verranno
descritti gli attributi e i metodi e per ognuno verrà indicato:

* lo Scopo
* la Sintassi
* alcune Osservazioni
* un Esempio di uso

``version``
-----------

**Scopo**

Restituisce il numero di versione della libreria.

**Sintassi**

   ``version()``

**Osservazioni**

Non richiede parametri.

**Esempio**

Controllare la versione della libreria. ::

  import pygraph.pyturtle as tg
  print tg.version()

``class tg.TurtlePlane``
---------------------

La classe ``TurtlePlane`` rappresenta lo spazio dove creare e far muovere le
tartarughe e fornisce alcuni metodi che permettono di pulire la superficie, 
tracciare assi e griglie e gestire le tartarughe.

``TurtlePlane`` è derivata da ``Plane``, ne eredita quindi tutti gli 
attributi 
e i metodi, per questi si veda il capitolo relativo a ``pycart``. 
Di seguito sono riportati i metodi (ri)definiti in ``TurtlePlane``.

``__init__``
............

**Scopo**

Crea un piano in cui far muovere tartarughe.

**Sintassi** 

  ``<piano> = tg.TurtlePlane(<parametri>)``

**Osservazioni**

Il metodo ``__init__`` non viene chiamato esplicitamente ma viene eseguito 
quando 
si crea un oggetto di questa classe. 
Ha esattamente gli stessi parametri di ``Plane``, vedi ``Plane`` per una
illustrazione di ognuno di essi.
I valori predefiniti però sono diversi, l'intestazione di questo metodo è::

  def __init__(self, name="Turtle geometry",
               w=600, h=400,
               sx=1, sy=None,
               ox=None, oy=None,
               axes=False, grid=False,
               axescolor='black', gridcolor='black',
               master=None)

**Esempio**

Creare quattro piani con caratteristiche diverse e disegnare in ciascuno di 
essi un quadrato. ::

  piano_0 = tg.TurtlePlane()
  piano_1 = tg.TurtlePlane("TurtlePlane con gli assi", sx=10, axes=True)
  piano_2 = tg.TurtlePlane("TurtlePlane con gli griglia", sx=20, grid=True)
  piano_3 = tg.TurtlePlane("TurtlePlane con assi e griglia", sx=30, sy=20, 
                        axes=True, grid=True)
  piani = [piano_0, piano_1, piano_2, piano_3]
  for p in piani:
      tarta = p.Turtle()
      for cont in range(4):
          tarta.forward(7)
          tarta.left(90)
  piano_0.mainloop()

.. note::
  Se vengono creati più piani basta chiamare il ``mainloop()`` per un solo 
  piano (si veda il primo esempio della classe ``TurtlePlane``.

``newTurtle``
.............

**Scopo**

Crea una nuova tartaruga cioè un nuovo oggetto della classe ``Turtle`` in
questo piano.

**Sintassi** 

  ``<tarta> = <piano>.newTurtle(<parametri>)``

**Osservazioni**

L'intestazione di questo metodo è:

  ``def newTurtle(self, x=0, y=0, d=0, color='black', width=1)``

Si può vedere che presenta molti parametri tutti con un valore di default.
Nel momento in cui si crea una nuova tartaruga si possono quindi decidere
le sue caratteristiche. Vediamole in dettaglio:

* ascissa e ordinata della sua posizione iniziale: ``x=0, y=0``;
* direzione iniziale verso cui è rivolta: ``d=0``;
* colore iniziale: ``color='black'``.
* larghezza iniziale: ``width=1``.

Poiché tutti i parametri hanno un valore predefinito, possiamo creare un
oggetto della classe ``Turtle`` senza specificare alcun argomento: verranno
usati tutti i valori predefiniti. Oppure possiamo specificare per nome gli
argomenti che vogliamo abbiano valori diversi da quelli predefiniti.
Si vedano di seguito alcuni esempi.

**Esempio**

Creare un piano con tre tartarughe e le fa avanzare di 100 unità. ::

  piano = tg.TurtlePlane()
  tina = piano.newTurtle(x=-100, y=-100, d=90, color='red')
  gina = piano.newTurtle(y=-100, d=90, color='green')
  pina = piano.newTurtle(x=100, y=-100, d=90, color='blue')
  tina.forward(100)
  pina.forward(100)
  gina.forward(100)
  piano.mainloop()

``turtles``
...........

**Scopo**

Restituisce una lista che contiene tutte le tartarughe presenti in un piano.

**Sintassi** 

  ``<piano>.turtles()``

**Osservazioni**

Ogni volta che viene creata o viene cancellata una tartaruga viene anche
aggiornata questa lista.

**Esempio**

Creare un piano con tre tartarughe anonime e le fa avanzare di 10 unità. ::

  piano = tg.TurtlePlane("Piano con tre tartarughe",
                   sx=20, sy=20, grid=True, gridcolor="grey")
  tg.Turtle(x=-10, y=-8, d=90, color='red')
  tg.Turtle(y=-8, d=90, color='green')
  tg.Turtle(x=10, y=-8, d=90, color='blue')
  for tarta in piano.turtles():
      tarta.forward(10)
  piano.mainloop()

``delturtles``
..............

**Scopo**

Elimina tutte le tartarughe presenti in un piano.

**Sintassi** 

  ``<piano>.delturtles()``

**Osservazioni**

Elimina tutte le tartarughe create.

**Esempio**

Crea 2 tartarughe e fa muovere tutte le tartarughe, dopo un secondo le 
elimina.
Crea altre 2 tartarughe e fa muovere tutte le tartarughe.::

  piano = tg.TurtlePlane(name='2+2 Tartarughe', w=400, h=400,
                   sx=20, sy=20, grid=True)
  g.Turtle(x=-3, y=-3, d=225, color='orange red', width=10)
  tg.Turtle(x=+3, y=-3, d=315, color='orange', width=10)
  piano.after(500)
  for tarta in piano.turtles():
      tarta.forward(5)
  piano.after(500)
  piano.delturtles()
  tg.Turtle(x=+3, y=+3, d= 45, color='pale green', width=10)
  tg.Turtle(x=-3, y=+3, d=135, color='pale turquoise', width=10)
  piano.after(500)
  for tarta in piano.turtles():
      tarta.forward(5)
  piano.mainloop()

``clean``
.........

**Scopo**

Cancella tutti i segni tracciati dalle tartarughe.

**Sintassi** 

  ``<piano>.clean()``

**Osservazioni**

Non modifica la posizione e lo stato delle tartarughe e neppure eventuali 
assi 
e griglia.

**Esempio**

Disegna un pentagono, si sposta, disegna un altro pentagono,
applica ``clean`` e ridisegna un pentagono::

  import random
  
  def pentagono(lato):
      for cont in range(5):
          tina.forward(lato)
          tina.left(72)

  piano = TurtlePlane(name="Clean", sx=10, grid=True)
  tina = tg.Turtle(color='pink', width=3)
  tina.tracer(5)
  pentagono(5)
  tina.color = (random.random(), random.random(), random.random())
  tina.width = randrange(2, 20, 2)
  tina.right(randrange(360))
  tina.forward(randrange(5)+5)
  pentagono(5)
  piano.after(700)
  piano.clean()
  pentagono(5)
  piano.mainloop()

``reset``
.........

**Scopo**

Ripulisce il piano riportando le tartarughe nella situazione iniziale.

**Sintassi** 

  ``<piano>.reset()``

**Osservazioni**

Ogni tartaruga memorizza lo stato che ha quando viene creata, reset riporta
tutte le tartarughe al loro stato iniziale.

**Esempio**

Disegna un pentagono, si sposta, disegna un altro pentagono,
applica ``reset`` e ridisegna un pentagono. ::

  from random import random, randrange
  
  def pentagono(lato):
      for cont in range(5):
          tina.forward(lato)
          tina.left(72)

  piano = TurtlePlane(name="reset", sx=10, axes=True)
  tina = tg.Turtle(color='pink', width=3)
  tina.tracer(5)
  pentagono(5)
  tina.color = (random(), random(), random())
  tina.width = randrange(2, 20, 2)
  tina.right(randrange(360))
  tina.forward(randrange(5)+5)
  pentagono(5)
  piano.after(700)
  piano.reset()
  pentagono(5)
  piano.mainloop()

``class Turtle:``
-----------------

La classe ``Turtle`` permette di costruire oggetti tartaruga, possiede 
diversi
metodi che permettono di esplorare la geometria della tartaruga. ``Turtle`` 
è derivata da ``Pen``, quindi possiede anche tutti i metodi di ``Pen``, per 
questi si veda il capitolo su ``pycart``.

.. note::
  Gli esempi riportati di seguito sono, in generale, semplici, alcuni anche 
  banali, ma verso la fine si possono trovare come esempi dei programmi 
  piuttosto complessi ispirati da: Abelson, Di Sessa, *La geometria della 
  tartaruga*, Padova 1986. Possono essere utili per approfondire alcuni 
  elementi del linguaggio, ma la primitiva di cui si parla può essere 
  utilizzata anche in programmi più semplici di quelli proposti.

``position``
............

**Scopo**

È un attributo della tartaruga, permette di memorizzare o di cambiare la 
sua posizione relativa al sistema di riferimento assoluto.

**Sintassi** 

  ``<tarta>.position = (<ascissa>, <ordinata>)``

**Osservazioni**

Le coordinate sono costituite da una tupla contenente due numeri, cioè da 
una
coppia di numeri racchiusa tra parentesi tonde.

**Esempio**

Sposta casualmente la tartaruga. ::

  def trirett(cateto1, cateto2):
      """Disegna un triangolo rettangolo dati i due cateti"""
      tina.forward(cateto1)
      a = tina.position
      tina.back(cateto1)
      tina.left(90)
      tina.forward(cateto2)
      tina.position = a
      tina.right(90)
      tina.back(cateto1)

  def inviluppo(l1, l2, inc=10):
      """Disegna un inviluppo di triangoli rettangoli."""
      if l1 < 0: return
      trirett(l1, l2)
      inviluppo(l1-inc, l2+inc)

  piano = tg.TurtlePlane(name="Inviluppo di triangoli rettangoli")
  tina = tg.Turtle(color='turquoise')
  for i in range(4):
      inviluppo(200, 0)
      tina.left(90)
  piano.mainloop()

``direction``
.............

**Scopo**

È un attributo della tartaruga, permette di memorizzare o di cambiare la 
sua 
direzione relativa al sistema di riferimento assoluto.

**Sintassi** 

  ``<tarta>.direction = <angolo>``

**Osservazioni**

La direzione assoluta ha angolo 0 verso destra e considera come verso 
positivo di rotazione il verso antiorario.

**Esempio**

Disegna un cielo stellato. ::

  from random import random, randrange

  def spostaacaso(raggio):
      """Sceglie una direzione e sposta la tartaruga di una
      lunghezza casuale minore a raggio."""
      tina.up()
      tina.position = (0, 0)
      tina.direction = randrange(360)
      tina.forward(randrange(raggio))
      tina.down()

  def stella(lung, n):
      """Disegna una stellina con n raggi lunghi lung."""
      for cont in range(n):
          tina.forward(lung)
          tina.back(lung)
          tina.left(360./n)

  def cielo():
      """Disegna un cielo stellato"""
      tina.fill(1)
      tina.ccircle(200)
      tina.fill(0)
      for cont in range(100):
          spostaacaso(190)
          tina.color = (random(), random(), random())
          stella(10, 5)

  piano = tg.TurtlePlane()
  tina = tg.Turtle()
  cielo()
  piano.mainloop()

``color``
.........

**Scopo**

È un attributo della tartaruga, permette di memorizzare o di cambiare il 
suo 
colore.

**Sintassi** 

  ``<tarta>.color = <colore>``

**Osservazioni**

``<colore>`` può essere definito in 3 modi diversi (vedi Pycart), può 
essere:

#. una stringa che contiene il nome di un colore,
#. una tupla che contiene tre numeri compresi tra 0 e 1 che indicano
   l'intensità delle componenti rossa, verde, blu,
#. Una stringa nel formato: ``#RRGGBB``, dove ``RR``, ``GG``, ``BB`` sono 
   numeri esadecimali, da ``00`` a ``ff``, 
   che indicano i valori delle ``Red``, ``Green``, ``Blue``).

**Esempio**

Disegna tre tracce colorate su uno sfondo di altro colore. ::

  piano = tg.TurtlePlane()
  fina = tg.Turtle()
  fina.color = 'light blue'        # colore passato per nome
  fina.fill(True)
  fina.ccircle(200)
  fina.fill(False)
  fina.delete()
  tina = tg.Turtle(y=-200, d=100)
  tina.color = (1, 0, 0  )         # tupla che contiene 3 numeri
  gina = tg.Turtle(y=-200, d=120)
  gina.color = (1, 1, 1)           # tupla che contiene 3 numeri
  pina = tg.Turtle(y=-200, d=140)
  pina.color = '#00ff00'           # stringa nel formato '#rrggbb'
  for cont in range(80):
      for tarta in piano.turtles():
          tarta.width = tarta.width+1
          tarta.forward(4)
          tarta.right(1)
  piano.mainloop()

``width``
.........

**Scopo**

È un attributo della tartaruga, permette di memorizzare o di cambiare la sua
dimensione e lo spessore della sua penna.

**Sintassi** 

  ``<tarta>.width = <larghezza>``

**Osservazioni**

Il valore minimo della larghezza della traccia è 1. Modificando la larghezza
della traccia viene modificata anche la dimensione del disegno di Tartaruga.

**Esempio**

Disegna una fila di 8 tra quadrati e cerchi alternati. ::

  def quadrato(lato):
      """Traccia un quadrato di lato lato."""
      for cont in range(4):
          tina.forward(lato)
          tina.left(90)

  piano = tg.TurtlePlane()
  tina = tg.Turtle()
  tina.up()
  tina.back(290)
  for i in range(8):
      tina.down()
      if i % 2:
          tina.width = 5
          tina.color = 'orange'
          tina.circle(20)
      else:
          tina.width = 10
          tina.color = 'maroon'
          quadrato(40)
      tina.up()
      tina.forward(80)
  tina.back(350)
  tina.down()
  piano.mainloop()

``__init__``
............

**Scopo**

Crea una nuova Tartaruga.

**Sintassi** 

  ``<tarta> = tg.Turtle(self, plane=<piano>)``

  **Osservazioni**

Il metodo ``__init__`` non viene chiamato direttamente, ma è eseguito 
quando 
viene creato un oggetto di questa classe. Ha un parametro obbligatorio, 
``plane``, che indica il piano in cui deve essere creata la tartaruga.
L'intestazione di questo metodo è:

  ``def __init__(self, x=0, y=0, d=0, color='black', width=1, plane=None)``

Se viene chiamato senza parametri, verranno usati i valori predefiniti come
valori iniziali. Vedi anche ``newTurtle``.

**Esempio**

Creare un piano con tre tartarughe e farle avanzare di 15 unità. ::

  piano = tg.TurtlePlane(name='Tre tartarughe', sx=20, sy=20,
                         axes=True, grid=True)
  tina = tg.Turtle(x=-10, y=-8, d=90, width=5, color='red')
  gina = tg.Turtle(y=-8, d=90, width=5, color='green')
  pina = tg.Turtle(x=10, y=-8, d=90, width=5, color='blue')
  tina.forward(15)
  pina.forward(15)
  gina.forward(15)
  piano.mainloop()

``forward``
...........

**Scopo**

Muove in avanti il cursore grafico, la tartaruga, di un certo numero di 
passi.

**Sintassi** 

  ``<tarta>.forward(<passi>)``

**Osservazioni**

Il movimento è relativo alla direzione di Tartaruga. Lo spostamento dipende
dalla scala del piano su cui si muove Tartaruga.
``forward``, assieme a ``back``, ``right`` e ``left``, costituiscono i 
comandi
base della geometria della tartaruga. Una geometria basata su un riferimento
intrinseco al cursore. 

``forward`` è una funzione che restituisce il riferimento al segmento
tracciato, questo riferimento può essere utile per cancellare un singolo
segmento (vedi il programma orologio.py presente nella directory 
``examples/pyturtle``).

**Esempio**

Disegna un quadrato con il lato lungo 47 unità. ::

  def quadrato(lato):
      """Disegna un quadrato con un certo lato"""
      for count in range(4):
          tina.forward(lato)
          tina.left(90)

  piano = tg.TurtlePlane()
  tina = tg.Turtle()
  quadrato(47)
  piano.mainloop()

``back``
........

**Scopo**

Muove indietro il cursore grafico, la tartaruga, di un certo numero di 
passi.

**Sintassi** 

  ``<tarta>.back(<passi>)``

**Osservazioni**

Il movimento è relativo alla direzione di Tartaruga. Lo spostamento dipende
dalla scala del piano su cui si muove Tartaruga.
Anche ``back`` è una funzione che restituisce un riferimento al segmento
tracciato.

**Esempio**

Disegna una stella con colori fumati. ::

  piano = tg.TurtlePlane()
  tina = tg.Turtle()
  n = 200
  for i in range(n):
      tina.color = '#ff{0:02x}00'.format(i*256//n)
      tina.forward(150)
      tina.back(150)
      tina.right(360./n)
  piano.mainloop()

``left``
........

**Scopo**

Ruota verso sinistra la tartaruga di un certo angolo. All'avvio l'angolo
viene misurato in gradi, ma si può anche dire alla tartaruga di misurarlo
in radianti (vedi i metodi ``degree`` e ``radians``).

**Sintassi** 

  ``<tarta>.left(<angolo>)``

**Osservazioni**

Tartaruga non si sposta, ma ruota semplicemente in senso antiorario.
All'inizio Tartaruga viene creata rivolta verso destra, inclinazione di 0 
gradi.

**Esempio**

Disegna una stellina dato il numero e la lunghezza dei raggi. ::

  def stella(num, lung):
    """Disegna una stella con enne raggi lunghi elle"""
    for i in range(num):
        tina.forward(lung)
        tina.back(lung)
        tina.right(360./num)

  piano = tg.TurtlePlane()
  tina = tg.Turtle()
  stella(20, 50)
  piano.mainloop()

``right``
.........

**Scopo**

Ruota verso destra la tartaruga di un certo angolo. All'avvio l'angolo
viene misurato in gradi, ma si può anche dire alla tartaruga di misurarlo
in radianti (vedi i metodi ``degree`` e ``radians``).

**Sintassi** 

  ``<tarta>.right(<angolo>)``

**Osservazioni**

Tartaruga non si sposta, ma ruota semplicemente in senso rario.
All'inizio Tartaruga viene creata rivolta verso destra, inclinazione di 0 
gradi.

**Esempio**

Disegna un albero binario con una procedura ricorsiva. ::

  def albero(lung=100):
      """Albero binario"""
      if lung > 2:
          tina.forward(lung)
          tina.left(45)
          albero(lung*0.7)
          tina.right(90)
          albero(lung*0.6)
          tina.left(45)
          tina.back(lung)

  piano = tg.TurtlePlane()
  tina = tg.Turtle(y=-100, d=90)
  albero()
  piano.mainloop()

``up``
......

**Scopo**

Solleva la penna della tartaruga in modo che, muovendosi, non tracci alcun
segno.

**Sintassi** 

  ``<tarta>.up()``

**Osservazioni**

Non muove Tartaruga. Se si vuole mandare la tartaruga verso la parte alta
dello schermo, bisogna ruotarla verso l'alto e poi mandarla avanti.

**Esempio**

Trasla la tartaruga delle componenti x e y relative alla propria direzione. 
::

  def sposta(x, y):
      """Trasla Tartaruga delle componenti x e y"""
      tina.up()
      tina.forward(x)
      tina.left(90)
      tina.forward(y)
      tina.right(90)
      tina.down()

  piano = tg.TurtlePlane()
  tina = tg.Turtle()
  tina.left(45)
  sposta(100, -50)
  piano.mainloop()

``down``
........

**Scopo**

Abbassa la penna di Tartaruga in modo che muovendosi lasci un segno.

**Sintassi** 

  ``<tarta>.down()``

**Osservazioni**

Esegue l'operazione opposta di ``up`` e come il metodo ``up``, non muove
la tartaruga.

**Esempio**

Crea una nuova classe di tartarughe che sa tracciare linee tratteggiate 
e disegna un poligono tratteggiato. ::

  class MyTurtle(tg.Turtle):
      """Una nuova classe di tartarughe 
      che traccia anche linee tratteggiate."""
      def tratteggia(self,  lung, pieno=5, vuoto=5):
          """Traccia una linea tratteggiata luga lung. """
          q, r = divmod(lung, pieno+vuoto)
          for cont in range(q):
              self.forward(pieno)
              self.up()
              self.forward(vuoto)
              self.down()
          self.forward(r)

  piano = tg.TurtlePlane(name="Pentagono tratteggiato")
  tina = MyTurtle(color='navy', width=3)
  for cont in range(5):
      tina.tratteggia(87)
      tina.left(72)
  piano.mainloop()

``write``
.........

**Scopo**

Scrive un testo sullo schermo a partire dalla posizione di Tartaruga.

**Sintassi** 

  ``<tarta>.write(<messaggio>[, move=False|True])``

**Osservazioni**

Se viene chiamato con un unico parametro, la tartaruga scrive il testo e 
resta
nella posizione attuale. Se oltre al messaggio, viene passato anche un
argomento ``True``, la tartaruga viene spostata alla fine della scritta.
I metodi ``up`` e ``down`` non hanno effetto su ``write``.

**Esempio**

Scrivi alcune parole nel piano della tartaruga. ::

  piano = tg.TurtlePlane(name='Testo nella finestra grafica', w=400, h=200)
  tina = tg.Turtle()
  position = tina.position
  tina.up()
  tina.position = (-140, 80)
  tina.color = 'green'
  tina.write('Parole')
  tina.color = 'pink'
  tina.write('sovrapposte!')
  tina.position = (-140, -80)
  tina.color = 'red'
  tina.write('Parole ', move=True)
  tina.color = 'orange'
  tina.write('scritte di seguito!')
  tina.position = position
  tina.down()
  piano.mainloop()

``fill``
........

**Scopo**

Permette di colorare l'interno delle figure realizzate dalla tartaruga.

**Sintassi** 

  ``<tarta>.fill(True|False)``

**Osservazioni**

``fill(True)`` inizia a memorizzare le figure da riempire, ``fill(False)`` 
le
riempie con il colore attuale.

**Esempio**

Una funzione che disegna un quadrato con l'interno colorato. ::

  def quadratopieno(lato, coloreinterno):
      """Disegna un quadrato dati il lato e il colore della sup. interna."""
      tina.fill(True)
      for count in range(4):
          tina.forward(lato)
          tina.left(90)
      oldcol = tina.color
      tina.color = coloreinterno
      tina.fill(False)
      tina.color = oldcol

  piano = tg.TurtlePlane()
  tina = tg.Turtle(color='blue', width=3)
  quadratopieno(100, 'green')
  piano.mainloop()

``tracer``
..........

**Scopo**

Rallenta la velocità di Tartaruga.

**Sintassi** 

  ``<tarta>.tracer(<ritardo>)``

**Osservazioni**

Più è elevato l'argomento, più lentamente si muove Tartaruga nei comandi
``forward``, ``back`` e ``setpos``. 
Se l'argomento è 0, Tartaruga traccia un intero segmento in un solo passo.

**Esempio**

Disegna una traccia circolare variando la velocita' e lo spessore. ::

  from random import random
  
  piano = tg.TurtlePlane(name="Giro della morte")
  tina = tg.Turtle(y=-180)
  tina.color = (random(), random(), random())
  passi = 72
  angolo = 180./72
  for i in range(passi):
    altezza = (tina.position[1]+200)/20
    tina.width = altezza
    tina.tracer(altezza)
    tina.left(angolo)
    tina.forward(15)
    tina.left(angolo)
  piano.mainloop()

``circle``
..........

**Scopo**

Traccia una circonferenza o un arco di circonferenza.

**Sintassi** 

  ``<tarta>.circle(<raggio> [, <estensione>])``

**Osservazioni**

Se è presente un solo argomento viene tracciata una circonferenza di dato
raggio.

Se ci sono due argomenti, viene disegnato un arco di circonferenza e il
secondo argomento indica l'estensione dell'arco.

**Esempio**

Disegna 25 archi e 25 segmenti circolari alternati. ::

  from random import random, randrange

  piano = tg.TurtlePlane(w=600, h=600)
  tina = tg.Turtle(width=5)
  for j in range(50):
      tina.color = (random(), random(), random())
      tina.up()
      tina.position = (randrange(-250, 250), randrange(-250, 250))
      tina.down()
      tina.fill(j % 2 == 1)
      tina.circle(randrange(25)+25, randrange(360))
      tina.color = (random(), random(), random())
      tina.fill(0)
  piano.mainloop()

``ccircle``
...........

**Scopo**

Disegna una circonferenza o un arco centrati nella posizione di Tartaruga.

**Sintassi** 

  ``<tarta>.ccircle(<raggio> [, <estensione>])``

**Osservazioni**

Se ci sono due argomenti, viene disegnato un arco di circonferenza e il
secondo argomento indica l'estensione dell'arco. In questo caso la tartaruga
ruota dell'ampiezza indicata dal secondo argomento.

**Esempio**

Disegna 25 archi e 25 settori circolari alternati. ::

  from random import random, randrange

  piano = tg.TurtlePlane(w=600, h=600)
  tina = tg.Turtle(width=5)
  for j in range(50):
      tina.color = (random(), random(), random())
      tina.up()
      tina.position = (randrange(-250, 250), randrange(-250, 250))
      tina.down()
      tina.fill(j % 2 == 1)
      tina.ccircle(randrange(25)+25, randrange(360))
      tina.color = (random(), random(), random())
      tina.fill(0)
  piano.mainloop()

``radians``
...........

**Scopo**

Dopo aver invocato questo metodo, Tartaruga misura gli angoli in radianti.

**Sintassi** 

  ``<tarta>.radians()``

**Osservazioni**

Ogni tartaruga, appena creata misura gli angoli in gradi sessagesimali,
se si preferisce lavorare con i radianti basta dirglielo.

**Esempio**

Vedi l'esempio di ``degree``.

``degrees``
...........

**Scopo**

Dopo la chiamata a questo metodo, Tartaruga misura gli angoli in gradi.

**Sintassi** 

  ``<tarta>.degrees([<angolo giro>])``

**Osservazioni**

Se usato senza argomenti, l'angolo giro sarà di 360°, l'argomento permette
di specificare il numero di gradi di un angolo giro.
Io non l'ho mai usato, ma si potrebbe decidere che l'angolo giro sia di 400
gradi o di un qualunque altro valore.

**Esempio**

Disegna un orologio analogico che riporta l'ora del sistema (mescolare in
uno stesso programma gradi e radianti, non mi sembra una grande idea). ::

  from time import time, localtime
  from math import pi

  def lancette(d, ore, minuti, secondi):
      """Disegna le lancette di un orologio che segna ore:minuti:secondi"""
      tina.degrees()
      aore = round(90 - ore*30 - minuti*0.5)       # Angoli in gradi
      aminuti = round(90 - minuti*6 - secondi*0.1)
      asecondi = round(90 - secondi*6)
      tina.direction = aore
      tina.width = d*0.1
      tina.forward(d*0.6)
      tina.back(d*0.6)
      tina.direction = aminuti
      tina.width = d*0.05
      tina.forward(d*0.8)
      tina.back(d*0.8)
      tina.direction = asecondi
      tina.width = d*0.02
      tina.forward(d*0.9)
      tina.back(d*0.9)

  def quadrante(dimensione):
      """Il quadrante di un orologio"""
      tina.radians()
      tina.ccircle(dimensione)
      d1 = dimensione*0.9
      d2 = dimensione-d1
      for i in range(12):
          tina.up()
          tina.forward(d1)
          tina.down()
          tina.forward(d2)
          tina.up()
          tina.back(dimensione)
          tina.down()
          tina.left(pi/6)                          # Angolo in radianti
      tina.degrees()

  def orologio(dimensione):
      """Disegna un orologio che indica l'ora corrente."""
      quadrante(dimensione)
      ore, minuti, secondi = localtime(time())[3:6]
      lancette(dimensione, ore, minuti, secondi)

  piano = tg.TurtlePlane(name="Ora esatta")
  tina = tg.Turtle()
  orologio(100)
  piano.mainloop()

``distance``
............

**Scopo**

Restituisce la distanza tra Tartaruga e il punto dato come argomento.

**Sintassi** 

  ``<tarta>.distance(<coordinate>)``

**Osservazioni**

L'argomento deve essere una coppia di numeri racchiusa tra parentesi.

**Esempio**

Dirige Tartaruga verso un bersaglio emulando l'olfatto. ::

  from random import randrange

  def saltaacaso():
      """Sposta Tartaruga in una posizione casuale."""
      tina.up()
      tina.position = (randrange(-280, 280), randrange(-180, 180))
      tina.down()

  def bersaglio():
      """Disegna un bersaglio"""
      saltaacaso()
      for cont in range(4):
          tina.forward(5)
          tina.back(5)
          tina.left(90)
      larghezza = tina.width
      tina.width = 2
      tina.ccircle(20)
      tina.width = larghezza
      tina.ccircle(10)
      return(tina.position)

  def odore(p):
      """Riporta un numero inversamente proporzionale
      al quadrato della distanza da p"""
      return 1./(tina.distance(p)**2)

  def ricerca_per_odore(bersaglio):
      """Muove Tartaruga in base ad una regola olfattiva"""
      tina.tracer(10)
      ricordo_odore = odore(bersaglio)
      while tina.distance(bersaglio)>10:
          tina.forward(randrange(10))
          nuovo_odore = odore(bersaglio)
          if nuovo_odore < ricordo_odore:
              tina.right(randrange(80))
          ricordo_odore = nuovo_odore

  piano = tg.TurtlePlane("Ricerca in base all'odore")
  tina = tg.Turtle()
  b = bersaglio()
  saltaacaso()
  ricerca_per_odore(b)
  piano.mainloop()

``dirto``
.........

**Scopo**

Restituisce la direzione sotto cui Tartaruga vede un certo punto

**Sintassi** 

  ``<tarta>.dirto(<coordinate>)``

**Osservazioni**

Le coordinate sono una coppia di numeri. Il valore restituito è relativo 
alla
direzione di Tartaruga: se il valore è 0 significa che il punto è a ore 12
cioè esattamente davanti a Tartaruga, se è 180 si trova a ore 6, cioè
esattamente dietro, se è 90 si trova a ore 9, se è 270 si trova a ore 3, ...

**Esempio**

Simula l'inseguimento tra un gatto e un topo. ::

  from random import randrange

  class Topo(tg.Turtle):
      """Classe che simula il comportamento di un topo"""
      def __init__(self, **args):
          Turtle.__init__(self, **args)
          piano = self._plane
          self.up()
          self.position = (piano._s2x(20), piano._s2y(20))
          self.write("inseguimento in base alla vista")
          self.position = (randrange(120)-60, randrange(80)-40)
          self.down()

      def scappa(self, da):
          """Fa muovere il topo di un passo cercando di scappare da da"""
          dir_gatto = self.dirto(da.position)
          if dir_gatto < 170:
              self.right(randrange(20)-10)
          elif dir_gatto > -170:
              self.left(randrange(20)-10)
          else:
              self.left(randrange(80)-40)
          self.forward(1)

  class Gatto(tg.Turtle):
      """Classe che simula il comportamento di un gatto"""
      def __init__(self, **args):
          Turtle.__init__(self, **args)
          self.color = "red"
          self.width = 3

      def insegui(self, chi):
          """Fa muovere il gatto di un passo come se inseguisse chi
          ritorna 0 se ha raggiunto chi"""
          if self.distance(chi.position) < 3:
              self.position = chi.position
              return 0
          dir_topo = self.dirto(chi.position)
          if dir_topo < 180:
              self.left(randrange(5))
          else:
              self.right(randrange(5))
          self.forward(2)
          return 1

  piano = tg.TurtlePlane()
  topo = Topo()
  gatto = Gatto()
  while gatto.insegui(topo):
      topo.scappa(gatto)
  piano.mainloop()

``whereis``
...........

**Scopo**

Restituisce una coppia di numeri che indicano la distanza di un punto e
l'angolo sotto cui Tartaruga lo vede.

**Sintassi** 

  ``<tarta>.whereis(<coordinate>)``

**Osservazioni**

Il risultato di questo metodo è una coppia di numeri.

**Esempio**

Simula la ricerca in base all'udito. ::

  from random import randrange
  from math import pi, sin

  def saltaacaso():
      """Sposta tartaruga in una posizione casuale."""
      tina.up()
      tina.position = (randrange(-280, 280), randrange(-180, 180))
      tina.down()

  def bersaglio():
      """Disegna un bersaglio"""
      saltaacaso()
      for cont in range(4):
          tina.forward(5)
          tina.back(5)
          tina.left(90)
      larghezza = tina.width
      tina.width = 2
      tina.ccircle(20)
      tina.swidth = larghezza
      tina.ccircle(10)
      return(tina.position)

  def intensita_destra(distanza, angolo):
      """Restituisce un numero che indica l'intensita' con cui
      l'orecchio destro ode un suono proveniente da un bersaglio
      posto ad una certa distanza e con un certo angolo"""
      senangolo = sin((angolo % 360)*pi/180)
      if senangolo < 0:
          return -10.*senangolo/(distanza**2)
      else:
          return 5.*senangolo/(distanza**2)

  def intensita_sinistra(distanza, angolo):
      """Restituisce un numero che indica l'intensita' con cui
      l'orecchio sinistro ode un suono proveniente da un bersaglio
      posto ad una certa distanza e con un certo angolo"""
      senangolo = sin((angolo % 360)*pi/180)
      if senangolo > 0:
          return 10.*senangolo/(distanza**2)
      else:
          return -5.*senangolo/(distanza**2)

  def ricerca_per_udito(bersaglio):
      """Simula la ricerca di un bersaglio utilizzando l'udito"""
      tina.tracer(10)
      while tina.distance(bersaglio)>10:
          d, a = tina.whereis(bersaglio)
          tina.forward(randrange(5))
          if intensita_destra(d, a) > intensita_sinistra(d, a):
              tina.right(randrange(30))
          else:
              tina.left(randrange(30))

  piano = tg.TurtlePlane("Ricerca in base all'udito")
  tina = tg.Turtle()
  b = bersaglio()
  saltaacaso()
  ricerca_per_udito(b)
  piano.mainloop()

``lookat``
..........

**Scopo**

Ruota Tartaruga in modo che sia rivolta verso un certo punto.

**Sintassi** 

  ``<tarta>.lookat(<coordinate>)``

**Osservazioni**

A differenza dei tre metodi precedenti, non dà risultato, ma provoca una
rotazione di Tartaruga.

**Esempio**

Disegna una parabola per mezzo dell'inviluppo di rette. ::

  def asse(punto):
      """Disegna l'asse del segmnento che ha per estremi Tartaruga e 
         punto"""
      direzione = tina.direction
      position = tina.position
      tina.up()
      tina.lookat(punto)
      tina.forward(tina.distance(punto)/2.)
      tina.left(90)
      tina.back(1000)
      tina.down()
      tina.forward(2000)
      tina.up()
      tina.position = position
      tina.direction = direzione
      tina.down()

  piano = tg.TurtlePlane(name="Parabola formata da un inviluppo di rette")
  tina = tg.Turtle()
  fuoco = (0, 50)
  numpassi = 80
  lato = 5
  tina.up()
  tina.back(200)
  tina.down()
  for i in range(numpassi):
      asse(fuoco)
      tina.forward(lato)
  piano.mainloop()

``hide``
........

**Scopo**

Nasconde la tartaruga.

**Sintassi** 

  ``<tarta>.hide()``

**Osservazioni**

Il metodo non richiede argomenti.

**Esempio**

Vedi il metodo ``show``

``show``
........

**Scopo**

Mostra la tartaruga.

**Sintassi** 

  ``<tarta>.show()``

**Osservazioni**

Il metodo non richiede argomenti.

**Esempio**

Traccia una linea mostrando e nascondendo Tartaruga. ::

  piano = tg.TurtlePlane("Cucu")
  tina = tg.Turtle()
  tina.up()
  tina.back(295)
  tina.down()
  tina.tracer(10)
  for i in range(15):
      tina.forward(19)
      tina.hide()
      tina.forward(19)
      tina.show()
  piano.mainloop()

``clone``
.........

**Scopo**

Crea un clone della tartaruga.

**Sintassi** 

  ``<tarta>.clone()``

**Osservazioni**

Non richiede argomenti.

**Esempio**

Fa collaborare alcune tartarughe per disegnare un albero. ::

  piano = tg.TurtlePlane("Albero realizzato da una famiglia di tartarughe",
                   w=500, h=500)
  tg.Turtle(y=-200, d=90, color='olive drab')
  dim = 100
  angl = 25
  angr = 35
  while dim > 5:
      for tarta in piano.turtles():
          tarta.width = dim//5
          tarta.forward(dim)
          t1 = tarta.clone()
          tarta.left(angl)
          t1.right(angr)
      piano.after(200)
      dim *= 0.7
      print(len(piano.turtles()))
  for tarta in piano.turtles():
      tarta.color = 'lime green'
  piano.mainloop()

``delete``
..........

**Scopo**

Elimina la tartaruga dal piano.

**Sintassi** 

  ``<tarta>.delete()``

**Osservazioni**

Il metodo non richiede argomenti. I segni già tracciati rimangono.

**Esempio**

Crea 36 tartarughe all'interno dello stesso piano mettendole in una lista,
poi le fa muovere. ::

  piano = tg.TurtlePlane("36 tartarughe che eseguono gli stessi comandi",
                   w=500, h=500, sx=20, sy=20, grid=True)
  n = 36
  l = 4.
  for i in range(n):
      tg.Turtle(color='#ff{0:02x}00'.format(i*256//n), d=(360/n*i), width=5)
  for tarta in piano.turtles(): tarta.forward(l)
  for tarta in piano.turtles(): tarta.left(90)
  for tarta in piano.turtles(): tarta.forward(l/4)
  for tarta in piano.turtles(): tarta.right(45)
  for tarta in piano.turtles(): tarta.forward(l)
  for tarta in piano.turtles(): tarta.right(120)
  for tarta in piano.turtles(): tarta.forward(l/2)
  for tarta in piano.turtles(): tarta.left(60)
  for tarta in piano.turtles(): tarta.forward(l/2)
  piano.after(500)
  piano.clean()
  for tarta in piano.turtles(): tarta.forward(2)
  piano.after(500)
  piano.reset()
  for tarta in piano.turtles(): tarta.forward(5)
  for tarta in piano.turtles():
      piano.after(100)
      tarta.delete()
  piano.after(500)
  piano.clean()
  piano.mainloop()

Libreria ``pyturtle``
---------------------

Funzioni
........

.. glossary::

  ``version``
    Restituisce la versione della libreria.

Classi
......

.. glossary::

  ``TurtlePlane``
    Spazio dove creare e far muovere le tartarughe e fornisce alcuni metodi 
    che permettono di pulire la superficie, tracciare assi e griglie e 
    gestire le tartarughe.

  ``Turtle``
    Puntatore grafico che permette di realizzare la geometria della tartaruga.

Attributi
.........

.. glossary::

   ``<tartaruga>.color``
      Colore della tartaruga.

   `<tartaruga>.`direction``
      Restituisce l'attuale direzione di Tartaruga.

   ``<tartaruga>.position``
      Restituisce l'attuale posizione di Tartaruga.

   ``<tartaruga>.width``
      Restituisce l'attuale larghezza della penna.
    

Metodi
......

.. glossary::

   ``<piano_per_tartarughe>.__init__``
      Crea un piano in cui far muovere tartarughe.

   ``<piano_per_tartarughe>.clear``
      Cancella tutti i segni tracciati dalle tartarughe.

   ``<piano_per_tartarughe>.delturtles``
      Elimina tutte le tartarughe presenti in un piano.

   ``<piano_per_tartarughe>.newTurtle``
      Crea una nuova tartaruga cioè un nuovo oggetto della classe ``Turtle``
      in questo piano.

   ``<piano_per_tartarughe>.reset``
      Ripulisce il piano riportando le tartarughe nella situazione iniziale.

   ``<piano_per_tartarughe>.turtles``
      Restituisce una lista che contiene tutte le tartarughe presenti in un
      piano.

   ``<tartaruga>.__init__``
      Crea una nuova Tartaruga.

   ``<tartaruga>.back``
      Muove indietro il cursore grafico, Tartaruga, di un certo numero di passi.

   ``<tartaruga>.ccircle``
      Disegna una circonferenza o un arco centrati nella posizione della
      tartaruga.

   ``<tartaruga>.circle``
      Traccia una circonferenza o un arco di circonferenza.

   ``<tartaruga>.clone``
      Crea un clone della tartaruga.

   ``<tartaruga>.degrees``
      Dopo la chiamata a questo metodo, Tartaruga misura gli angoli in gradi.

   ``<tartaruga>.delete``
      Elimina la tartaruga dal piano.

   ``<tartaruga>.dirto``
      Restituisce la direzione sotto cui Tartaruga vede un certo punto

   ``<tartaruga>.distance``
      Restituisce la distanza tra Tartaruga e il punto dato come argomento.

   ``<tartaruga>.down``
      Abbassa la penna di Tartaruga in modo che muovendosi lasci un segno.

   ``<tartaruga>.fill``
      Permette di colorare l'interno delle figure realizzate dalla tartaruga.

   ``<tartaruga>.forward``
      Muove in avanti il cursore grafico, Tartaruga, di un certo numero
      di passi.

   ``<tartaruga>.hide``
      Nasconde la tartaruga.

   ``<tartaruga>.left``
      Ruota verso sinistra la tartaruga di un certo angolo. All'avvio
      l'angolo viene misurato in gradi, ma si può anche dire alla tartaruga
      di misurarlo in radianti (vedi i metodi ``degree`` e ``radians``).

   ``<tartaruga>.lookat``
      Ruota Tartaruga in modo che sia rivolta verso un certo punto.

   ``<tartaruga>.radians``
      Dopo aver invocato questo metodo, Tartaruga misura gli angoli in
      radianti.

   ``right``
      Ruota verso destra la tartaruga di un certo angolo. All'avvio
      l'angolo viene misurato in gradi, ma si può anche dire alla tartaruga
      di misurarlo in radianti (vedi i metodi ``degree`` e ``radians``).

   ``<tartaruga>.show``
      Mostra la tartaruga.

   ``<tartaruga>.tracer``
      Rallenta la velocità di Tartaruga.

   ``<tartaruga>.up``
      Solleva la penna della tartaruga in modo che, muovendosi, non tracci
      alcun segno.

   ``<tartaruga>.whereis``
      Restituisce una coppia di numeri che indicano la distanza di un punto
      e l'angolo sotto cui Tartaruga lo vede.

   ``<tartaruga>.write``
      Scrive un testo sullo schermo a partire dalla posizione di Tartaruga.

