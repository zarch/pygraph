Pycart
======

*Dove viene presentata la libreria Pycart.*

Introduzione
------------

``Pycart`` è una libreria che implementa la classe *Piano cartesiano* con
alcuni metodi che permettono di:

* Modificare alcuni parametri come la posizione dell'origine e la scala di
  visualizzazione.
* Effettuare le trasformazioni di coordinate necessarie.
* Disegnare gli assi.

``Pycart`` si appoggia su alcune altre librerie:

* ``Tkinter`` per l'output grafico,
* ``os`` per eseguire un programma di conversione del formato grafico nel
  metodo ``save(self, filename)``,
* ``colors`` per la gestione dei colori,
* ``pygrapherror`` per i messaggi di errore

Nella libreria sono presenti:

* la funzione ``version``, che riporta la versione;
* la classe ``Plane``, che implementa un piano cartesiano;
* la classe ``Pen``, che implementa un tracciatore grafico.

Per le due classi verranno descritti gli attributi e i metodi e per ognuno 
verrà indicato:

* lo scopo
* la sintassi
* alcune osservazioni
* un esempio di uso

``version``
-----------

**Scopo**

È una funzione che restituisce il numero di versione della libreria.

**Sintassi** 

  ``version()``

**Osservazioni**

Non richiede parametri, restituisce la stringa che contiene la versione.

**Esempio**

Controllare la versione della libreria. ::

  import pygraph.pycart as cg
  if cg.version()<"2.9.00":
      print "versione un po' vecchiotta"
  else:
      print "versione:", cg.version()

``class Plane``
---------------

``Plane`` contiene alcuni *attributi* e *metodi* che permettono di:

* cambiare la posizione dell'origine rispetto alla finestra grafica,
* cambiare la scala di rappresentazione,
* tracciare gli assi o una griglia di punti,
* salvare il grafico in un file,
* ...

.. note::
  Per poter creare oggetti della classi ``Plane`` bisogna importarla dalla 
  libreria. In tutti gli esempi seguenti si suppone che sia già stato 
  eseguito il comando:

    ``import pygraph.pycart as cg``

  Se nell'esegure un esempio ottenete un messaggio che termina con:
  ``NameError: name 'cg' is not defined``, forse avete dimenticato di 
  caricare la libreria ``pycart`` assegnandole il nome ``cg``
  con il comando scritto sopra.

Se nell'eseguire gli esempi osservate dei comportamenti strani della 
finestra 
che contiene il piano cartesiano, portate pazienza, o guardate quanto 
scritto
nella descrizione del metodo ``mainloop`` di ``Plane`` più avanti dove ho 
cercato di dare delle indicazioni in proposito.

.. note::
  Chi è curioso e va a guardare il sorgente della libreria, scoprirà  che
  ci sono alcuni *attributi* e *metodi* che non vengono descritti in
  questo manuale. Il loro nome inizia con il carattere "_" e sono
  *privati*. L'utente non dovrebbe aver bisogno di usarli e se lo fa, lo fa
  a suo rischio e pericolo perché potrebbero venir modificati nelle 
  prossime versioni.

``origin``
..........

**Scopo**

Permette di definire la posizione dell'origine degli assi all'interno della
finestra grafica.

**Sintassi** 

  ``<piano>.origin = <coppia di numeri>``

**Osservazioni**

Il valore predefinito è al centro della finestra. Gli argomenti indicano
lo spostamento in pixel rispetto all'angolo in alto a sinistra della 
finestra
grafica.

**Esempio**

Crea un piano, sposta l'origine a sinistra, disegna gli assi. ::

  piano = cg.Plane('ex_01')
  piano.origin = (10, 300)
  piano.axes()

``scale``
.........

**Scopo**

Definisce la scala di rappresentazione sui due assi.

**Sintassi** 

  ``<piano>.scale = <coppia di numeri>``

**Osservazioni**

I due valori indicano rispettivamente la scala relativa all'asse x e
all'asse y. I due valori sono numeri naturali che indicano il numero di 
pixel
corrispondenti all'unità di misura.

**Esempio**

Crea un piano, modifica la scala e l'origine e colore 
poi ridisegna assi e griglia. ::

  piano = cg.Plane('modifiche', sx=15, 
               axescolor='green', color='AntiqueWhite3')
  piano.after(500)
  x_o, y_o = piano.origin
  sx, sy = piano.scale
  piano.origin = (x_o-4*sx, y_o+6*sy)
  piano.scale = (30, 20)
  prevcolor = piano.color
  piano.color='yellow'
  piano.axes(color='blue')
  piano.grid(color='red')
  piano.after(1000)
  piano.color = prevcolor

``__init__``
............

**Scopo**

Crea il piano cartesiano e inizializza gli attributi di ``Plane``.

**Sintassi** 

  ``<nome_variabile> = cg.Plane(<parametri>)``

**Osservazioni**

Questo metodo non viene chiamato esplicitamente, ma viene eseguito
quando si crea un oggetto di questa classe.
L'intestazione di questo metodo è::

  def __init__(self, name="Cartesian Plane",
               w=600, h=400,
               sx=20, sy=None,
               ox=None, oy=None,
               axes=True, grid=True,
               axescolor='black', gridcolor=None,
               color='white',
               parent=None):

Si può vedere che presenta molti parametri tutti con un valore predefinito.
Nel momento in cui si crea un piano cartesiano si possono quindi decidere
le sue caratteristiche. Vediamole in dettaglio:

* titolo della finestra, valore predefinito: "Cartesian Plane";
* dimensione, valori predefiniti: larghezza=600, altezza=400;
* scala di rappresentazione, valori predefiniti: una unità = 20 pixel;
* posizione dell'origine, valore predefinito: il centro della finestra;
* rappresentazione degli assi cartesiani, valore predefinito: ``True``;
* rappresentazione di una griglia di punti, valore predefinito: ``True``;
* colore degli assi valore predefinito: 'black'.
* colore della griglia valore predefinito: lo stesso degli assi.
* colore dello sfondo valore predefinito: 'white'.
* riferimento alla finestra che contiene il piano cartesiano,
  valore predefinito: ``None``.

Poiché tutti i parametri hanno un valore predefinito, possiamo creare un
oggetto della classe ``Plane`` senza specificare alcun argomento: verranno
usati tutti i valori predefiniti. Oppure possiamo specificare per nome gli
argomenti che vogliamo siano diversi dal comportamento predefinito, si 
vedano 
di seguito alcuni esempi.

**Esempio**

Creare 3 piani cartesiani,
il primo: con tutti i valori di default,
il secondo: quadrato, con un titolo e con gli assi,
il terzo: con un titolo, con le dimensioni di 400 per 200,
con la scala e l'origine cambiati, con assi, griglia e sfondo colorati. ::

  p0 = cg.Plane()
  p1 = cg.Plane(name="Secondo piano", w=400, h=400, axes=True)
  p2 = cg.Plane(name="Terzo piano",
                w=400, h=200,
                sx=10, sy=30,
                ox=40, oy=None,
                axescolor='orange', gridcolor='red', color='green')

``mainloop``
............

**Scopo**

Rende attiva la finestra grafica.

**Sintassi** 

  ``<piano>.mainloop()``

**Osservazioni**

Questa istruzione è fondamentale nella geometria interattiva, mentre nei
programmi con la geometria cartesiana e della tartaruga, può anche non 
essere
usata. 

.. note::
  Se la libreria è usata dall'interno di ``IDLE``, possono sorgere dei 
  problemi a seconda di come è stato avviato ``IDLE`` stesso.
  
  * Se ``IDLE`` è stato avviato **con** il parametro "-n" allora **non** si 
    deve usare il metodo ``mainloop()`` altrimenti, alla chiusura del piano 
    cartesiano ``IDLE`` non risponde più ai comandi. 
  * Se ``IDLE`` è stato avviato **senza** il parametro "-n" allora si 
    deve usare il metodo ``mainloop()`` altrimenti la finestra con il piano 
    cartesiano non è attiva.
    
  Nel resto degli esempi di questo capitolo non riporto questa istruzione, 
  ma se la finestra creata non risponde al mouse aggiungetela come ultima 
  istruzione.

**Esempio**

Disegna un piano se ``IDLE`` è stato avviato senza sottoprocessi,
con il parametro "-n". 

  ``piano = cg.Plane('ex_051: un piano', axescolor='pink')``

Disegna un piano se ``IDLE`` è stato avviato con sottoprocessi,
senza il parametro "-n". 

  ``piano = cg.Plane('ex_052: altro piano', axescolor='olive drab')``
  
  ``piano.mainloop()``

``newPen``
..........

**Scopo**

Crea un nuovo tracciatore grafico, una nuova *penna* per disegnare 
figure geometriche elementari.

**Sintassi** 

  ``<piano>.newPen([x=<num>, y=<num>, color=<colore>, width=<num>,``
  ``shape=<shapes>])``

**Osservazioni**

Normalmente è utile assegare la *penna* creata ad un identificatore.

L'intestazione di questo metodo è:

  ``def newPen(self, x=0, y=0, color='black', width=1, shape=ROUND)``

Tutti i parametri hanno un valore predefinito, per creare una penna 
nell'origine degli assi di colore nero e larghezza 1 non occorre passare
argomenti alla funzione:

  ``penna = piano.newPen()``

Per maggiori informazioni sulle *penne* vedi la documentazione della classe 
``Pen``.

**Esempio**

Disegna un quadrato blu con il contorno spesso 4 pixel. ::

  piano = cg.Plane('ex_11: Quadrato', w=400, h=400)
  biro = piano.newPen(width=4, color='blue')
  biro.drawpoly(((-7, -7), (7, -7), (7, 7), (-7, 7)))
  piano.mainloop()

``after``
.........

**Scopo**

Permette di inserire un ritardo in un programma.

**Sintassi** 

  ``<piano>.after(<numero>)``

**Osservazioni**

Il parametro rappresenta il numero di millisecondi di attesa.

**Esempio**

Un quadrato in movimento, 
(Vedi gli esempi di ``clear``, ``reset``, ``delete``). ::

  piano = cg.Plane('ex_06: Quadrato in movimento', 
               sx=1, sy=1, axes=False, grid=False)
  biro = piano.newPen(width=4)
  for i in range(0, 500, 2):
      color = '#ff{:02x}00'.format(i//2)
      verts = ((-300+i, -30), (-220+i, -50), (-200+i, 30), (-280+i, 50))
      id = biro.drawpoly(verts, color)
      piano.after(10)
      piano.delete(id)
  id = biro.drawpoly(verts, 'green')
  piano.mainloop()

``axes``
........

**Scopo**

Disegna una coppia di assi cartesiani ortogonali.

**Sintassi** 

  ``<piano>.axes(color=None)``

**Osservazioni**

Se non è specificato il parametro, gli assi vengono disegnati con il colore
che ha il cursore grafico in quel momento, altrimenti con il colore
specificato.
L'intestazione di questo metodo è:

  ``def axes(self, color=None, ax=True, ay=True)``

Come si vede dai parametri è anche possibile disegnare solo l'asse *x* o 
l'asse *y*.

**Esempio**

Disegna due sistemi di riferimento con uguale scala ma origini diverse. ::

  piano = cg.Plane('ex_07: Sistema di riferimento traslato')
  piano.origin = (80, 60)
  piano.axes('red')
  piano.mainloop()

``grid``
........

**Scopo**

Disegna una griglia di punti.

**Sintassi** 

  ``<piano>.grid(color=None)``

**Osservazioni**

Se non è specificato il parametro, i punti vengono disegnati con il colore
predefinito o con il colore specificato come parametro.

**Esempio**

Disegna in sequenza un piano con assi e griglia, senza assi,
senza griglia e completamente bianco. ::
  
  piano = cg.Plane('ex_08: Assi e griglie',  sx=12,  sy=36,
               axescolor='green', gridcolor='red')
  biro = cg.Pen(x=-10, y=3, color='red', width=2)
  biro.drawtext('assi e griglia, attendere... 4')
  ritardo = 1000
  piano.after(ritardo)
  piano.clean()
  piano.axes('purple')
  biro.drawtext('solo assi, attendere... 3')
  piano.after(ritardo)
  piano.clean()
  piano.grid('#22aa55')
  biro.drawtext('solo griglia, attendere... 2')
  piano.after(ritardo)
  piano.reset()
  biro.drawtext('situazione iniziale, attendere... 1')
  piano.after(ritardo)
  piano.clean()
  biro.drawtext('bianco, finito!')
  piano.mainloop()

``clean``
.........

**Scopo**

Cancella tutti gli elementi disegnati nella finestra grafica, ridisegnando, 
se presenti, assi e griglia, ma non modifica i puntatori grafici, le 
*penne*.

**Sintassi** 

  ``<piano>.clean()``

**Osservazioni**

Non ha parametri.

**Esempio**

Disegna un quadrato che scoppia. ::

  piano = cg.Plane('ex_13: Quadrato che scoppia', sx=1,
               axes=False, grid=False)
  biro = piano.newPen(width=4, color='blue')
  a = 3
  b = 1
  for i in range (162):
      verts = (((-a*i, -b*i), (b*i, -a*i), (a*i,  b*i), (-b*i, a*i)))
      biro.drawpoly(verts)
      piano.after(10)
      piano.clean()
      biro.drawpoly(verts)
  piano.mainloop()

``reset``
.........

**Scopo**

Cancella tutti gli elementi disegnati nella finestra grafica, ridisegnando, 
se presenti, assi e griglia, riporta nella condizione iniziale i puntatori 
grafici, le *penne*.

**Sintassi** 

  ``<plot>.reset()``

**Osservazioni**

Non ha parametri.

**Esempio**

Disegna gli assi con la griglia, un poligono verde, attendere un mezzo secondo,
poi cancellare tutto ridisegnando gli assi e ririsegna il poligono con un
altro colore.::

  piano = cg.Plane('ex_14: Poligono', axes=False, grid=False)
  piano.axes()
  vertici = ((-2, -3), (4, -1), (6, 4), (-5, 6))
  biro = cg.Pen(width=20, color='blue')
  biro.drawpoly(vertici)
  piano.after(500)
  piano.reset()
  biro.color="green"
  biro.width=10
  biro.drawpoly(vertici)
  piano.mainloop()

``delete``
..........

**Scopo**

Permette di cancellare un elemento grafico disegnato.

**Sintassi** 

  ``<piano>.delete(<id>)``

**Osservazioni**

Ogni elemento grafico che viene disegnato nel piano ha un suo numero
identificativo. Facendo riferimento a questo numero lo si può cancellare.

**Esempio**

Disegna un segmento che scivola sugli assi. ::

  piano = cg.Plane('ex_15: Segmento che scivola sugli assi', sx=1)
  biro = cg.Pen(width=5, color='navy')
  lung = 200
  for y in range(200, 0, -1):
      x = (lung*lung-y*y)**.5
      s = biro.drawsegment((0, y), (x, 0))
      piano.after(4)
      piano.delete(s)
  biro.drawsegment((0, 0), (lung, 0))
  piano.mainloop()

``save``
........

**Scopo**

Salva in un file l'immagine della finestra grafica.

**Sintassi** 

  ``<piano>.save(<filename>)``

**Osservazioni**

``<filename>`` è una stringa che contiene il nome del file. L'mmagine è 
salvata nel formato Postscript o 'png' e a <nomefile> viene aggiunta 
l'estensione ".ps" o ".png" a seconda se ``pygraph`` trova il programma per 
la conversione in "png".

**Esempio**

Produrre un file che contiene il disegno di un quadrato. ::

  piano = cg.Plane('ex_16: Quadrato')
  biro = cg.Pen(width=6, color='pink')
  q = ((-5, -3),(3, -5),(5, 3),(-3, 5))
  biro.drawpoly(q)
  piano.save('quadrato')
  piano.mainloop()

``getcanvaswidth`` e ``getcanvasheight``
........................................

**Scopo**

Restituiscono le dimensioni in pixel della finestra grafica.

**Sintassi** 

  ``<piano>.getcanvaswidth()``
  
  ``<piano>.getcanvasheight()``

**Osservazioni**

Restituiscono un numero intero.

**Esempio**

Disegna un rettangolo che circonda la finestra grafica. ::

  piano = cg.Plane('ex_17: Cornice', sx=1, axes=False, grid=False)
  biro = piano.newPen(width=4, color='green')
  bordo = 10
  w = piano.getcanvaswidth()//2-bordo
  h = piano.getcanvasheight()//2-bordo
  biro.drawpoly(((-w, -h), (w, -h), (w, h), (-w, h)))
  piano.mainloop()

``getcanvas``
.............

**Scopo**

Restituisce un riferimento alla finestra grafica.

**Sintassi** 

  ``<piano>.getcanvas()``

**Osservazioni**

Il riferimento restituito può essere utilizzato per operare direttamente 
sulla
finestra grafica senza passare attraverso i metodi messi a disposizione da
Plane. Per saper cosa farsene bisogna conoscere la classe Canvas della 
libreria
Tkinter.

**Esempio**

Questo metodo prevede la conoscenza della
libreria Tkinter, essendo ciò al di fuori della portata di questo manuale, 
non
viene proposto nessun esempio.

Si può trovare un suo uso nel programma ``viewfun.py`` distribuito
assieme alla libreria pygraph.

``onkeypress``, ``onpress1``, ``onrelease1``, ...
-------------------------------------------------

**Scopo**

Sono delle funzioni che vengono chiamate quando si presentano certi eventi:

* ``onkeypress``: è stato premuto un tasto;
* ``onkeyrelease``: è stato rilasciato un tasto;
* ``onenter``: il mouse è entrato nel piano;
* ``onleave``: il mouse è uscito dal piano;
* ``onpress1``: è stato premuto il tasto sinistro del mouse nel piano;
* ``onpress2``: è stato premuto il tasto centrale del mouse nel piano;
* ``onpress3``: è stato premuto il tasto destro del mouse nel piano;
* ``onmotion1``: è stato mosso il mouse con il tasto sinistro premuto;
* ``onmotion2``: è stato mosso il mouse con il tasto centrale premuto;
* ``onmotion3``: è stato mosso il mouse con il tasto destro premuto;
* ``onrelease1``: è stato rilasciato il tasto sinistro del mouse nel piano;
* ``onrelease2``: è stato rilasciato il tasto centrale del mouse nel piano;
* ``onrelease3``: è stato rilasciato il tasto destro del mouse nel piano;

**Sintassi** 

  ``onlclick = <miafunzione>``

**Osservazioni**

La funzione ha un parametro che viene legato ad un oggetto evento.

**Esempio**

Stampa le azioni del mouse. ::

    def onkeypress(event):
        print("Key pressed:", event.char)
    def onkeyrelease(event):
        print("Key released:", event.char)
    def onenter(event):
        print("Enter at", event)
    def onleave(event):
        print("Leave at", event)
    def onpress1(event):
        print("Left clicked at", event.x, event.y)
    def onpress2(event):
        print("Center clicked at", event.x, event.y)
    def onpress3(event):
        print("Right clicked at", event.x, event.y)
    def onmotion1(event):
        print("Left motion at", event.x, event.y)
    def onmotion2(event):
        print("Center motion at", event.x, event.y)
    def onmotion3(event):
        print("Right motion at", event.x, event.y)
    def onrelease1(event):
        print("Left released at", event.x, event.y)
    def onrelease2(event):
        print("Center released at", event.x, event.y)
    def onrelease3(event):
        print("Right released at", event.x, event.y)
    piano = cg.Plane('04. events', color='pink')
    piano.onkeypress(onkeypress)
    piano.onkeyrelease(onkeyrelease)
    piano.onenter(onenter)
    piano.onleave(onleave)
    piano.onpress1(onpress1)
    piano.onpress2(onpress2)
    piano.onpress3(onpress3)
    piano.onmotion1(onmotion1)
    piano.onmotion2(onmotion2)
    piano.onmotion3(onmotion3)
    piano.onrelease1(onrelease1)
    piano.onrelease2(onrelease2)
    piano.onrelease3(onrelease3)
    biro = cg.Pen()
    biro.drawtext('Clicca in vari punti del piano', (0, 9), 'navy', 2)
    biro.drawtext('con i tre tasti del mouse', (0, 7), 'navy', 2)
    piano.mainloop()

``class Pen``
-------------

La classe ``Pen`` contiene alcuni *attributi* e *metodi* che permettono di:

* Modificare le caratteristiche della penna.
* Modificare la posizione.
* Disegnare segmenti.
* Disegnare punti.
* Disegnare cerchi.
* Disegnare poligoni.
* Scrivere del testo.

``__init__``
............

**Scopo**

Crea una penna nel piano cartesiano e inizializza i suoi attributi.

**Sintassi** 

  ``<nome_variabile>= cg.Pen(<parametri>)``

**Osservazioni**

L'intestazione di questo metodo è:

  ``def __init__(self, x=0, y=0, color='black', width=1, shape=ROUND,``
  ``plane=None)``

che esplicita i valori predefiniti dei diversi attributi di una penna.
Quando si crea una nuova penna, questa viene inserita didefault nell'ultimo 
piano creato.
Nel creare una penna si possono anche specificare alcune caratteristiche, 
vediamole in dettaglio:

* la posizione del punto di partenza, valori predefiniti: x=0, y=0;
* il colore, valore predefinito: color='black';
* lo spessore del tratto, valore predefinito: width=1;
* il piano su cui scrive, valore predefinito: l'ultimo creato.

Dato che i parametri hanno valori predefiniti, 
possiamo creare penne passando un diverso numero di argomenti. Gli 
argomenti 
non specificati saranno sostituiti con i valori di default.

Questo metodo non viene chiamato esplicitamente, ma viene eseguito
quando si crea un oggetto di questa classe.

.. note::
  vedi il metodo ``newPen`` di ``Plane`` per un altro modo per creare 
  *penne*.
  Le due istruzioni::
  
    piano = cg.Plane(sx=100, sy=100, axes=True)
    p0= cg.Pen(x=1,  y=1,  color='violet', width=9)
    
  e::
  
    piano = cg.Plane(sx=100, sy=100, axes=True)
    p0 = piano.newPen(x=1,  y=1,  color='violet', width=9)
  
  sono del tutto equivalenti.

**Esempio**

Creare 4 penne diverse e le fa convergere nell'origine::

  piano = cg.Plane('ex_30: __init__', sx=100, sy=100, grid=False)
  p_0 = cg.Pen(x=+1, y=+1, color='violet', width=9)
  p_1 = cg.Pen(x=-1, y=+1, color='magenta', width=7)
  p_2 = cg.Pen(x=-1, y=-1, color='gold', width=5)
  p_3 = cg.Pen(x=+1, y=-1, color='navy', width=3)
  for biro in (p_0, p_1, p_2, p_3):
      biro.drawto((0, 0))
  piano.mainloop()


La libreria ``pygraph`` permette di aprire più finestre grafiche 
contemporaneamente, l'oggetto ``Pen`` viene creato all'interno dell'ultimo
piano creato. È possibile creare una penna in un piano qualsiasi o 
utilizzando il metodo ``newPen`` o specificando l'argomento ``plane`` nel
costruttore della penna. Ad esempio se vogliamo quattro piani con una penna
in ciascuno possiamo costruire la penna in vari modi::
  
    piano_0 = cg.Plane('ex_31: piano 0 violet', sx=100, sy=100, grid=False)
    biro0 = cg.Pen(x=+1, y=+1, color='violet', width=20)
    piano_1 = cg.Plane('ex_31: piano 1 green', sx=100, sy=100, grid=False)
    biro1 = cg.Pen(x=-1, y=+1, color='green', width=20)
    piano_2 = cg.Plane('ex_31: piano 2 gold', sx=100, sy=100, grid=False)
    piano_3 = cg.Plane('ex_31: piano 3 navy', sx=100, sy=100, grid=False)
    biro2 = cg.Pen(x=-1, y=-1, color='gold', width=20, plane=piano_2)
    biro3 = piano_3.newPen(x=+1, y=-1, color='navy', width=20)
    for biro in (biro0, biro1, biro2, biro3):
        biro.drawpoint()
    piano_0.mainloop()

``biro0`` viene costruita nel piano ``piano_0``, ``biro1`` nel piano 
``piano_1``, come è abbastanza logico aspettarsi; ``biro2`` verrebbe 
costruita nel piano ``p3`` se non fosse specificato l'argomento 
``plane = piano_2``; ``biro3`` verrebbe realizzata comunque nel piano 
``piano_3`` dato che è l'ultimo piano creato, ma 
comunque l'istruzione ``piano_3.newPen()`` non lascia dubbi.

``position``
............

**Scopo**

È un attributo collegato alla posizione del cursore grafico.
Permette di ottenere o modificare la posizione della penna.

**Sintassi** 

  ``<penna>.position = (<numero>, <numero>)``

**Osservazioni**

I due numeri rappresentano le coordinate della posizione.

**Esempio**

Disegna due quadrati in posizioni casuali. ::
  
  import random
  piano = cg.Plane('ex_32: position')
  biro = cg.Pen()
  lato = random.randrange(5)+3
  biro.position = (random.randrange(-14, 10), random.randrange(-9, 5))
  x, y = biro.position
  vertici = ((x, y), (x+lato, y), (x+lato, y+lato), (x, y+lato))
  biro.drawpoly(vertici, color='gold', width=6)
  lato = random.randrange(5)+3
  biro.position = (random.randrange(-14, 10), random.randrange(-9, 5))
  x, y = biro.position
  vertici = ((x, y), (x+lato, y), (x+lato, y+lato), (x, y+lato))
  biro.drawpoly(vertici, color='pink', width=6)
  piano.mainloop()

``color``
.........

**Scopo**

È un attributo collegato al colore del cursore grafico.
Permette di tracciare disegni con linee di differenti colori.

**Sintassi** 

  ``<penna>.color = <colore>``

**Osservazioni**

Il colore può essere indicato in diversi modi:

* Usando il nome di un colore (in inglese).
  I colori attualmente disponibili si trovano nel file ``rgb.txt``.
* Usando una stringa nel formato '#RRGGBB' dove RR, GG, BB sono le
  numeri di due cifre scritti in forma esadecimale che rappresentano le
  componenti rossa, verde e blu del colore.
* Una tupla di tre numeri compresi tra 0 e 1 che rappresentano il livello
  delle componenti Rossa, Verde e Blu del colore.

**Esempio** 

Disegna linee di diverso colore. ::

  piano = cg.Plane('ex_33: color')
  colors = ['red', 'green', 'blue', 'pink', 'yellow',
            'navy', 'gold', 'magenta', '#a0a0a0', (0.7, 0.5, 0.1)]
  biro = cg.Pen(width=10)
  for i, color in enumerate(colors):
      biro.color = color
      print(biro.color)
      biro.drawsegment((-5, i-4), (5, i-4))
  piano.mainloop()

``width``
.........

**Scopo**

È un attributo collegato allo spessore del cursore grafico.
Permette di tracciare disegni con linee di differenti spessori.

**Sintassi** 

  ``<penna>.width = <numero>``

**Osservazioni**

Numero indica la larghezza in pixel della traccia lasciata dal cursore
grafico.

**Esempio**

Disegna linee di diverso spessore. ::

  piano = cg.Plane('ex_34: width')
  biro = cg.Pen()
  for i in range(10):
      biro.width = i*2
      biro.drawsegment((-5, i-4), (5, i-4))
  piano.mainloop()

``shape``
.........

**Scopo**

È un attributo collegato alla forma del disegno usato per rappresentare un 
punto.

**Sintassi** 

  ``<penna>.shape = <shapes>``

**Osservazioni**

Le forme disponibili sono:

* ``ROUND``: cerchio; 
* ``SQUARE``: quadrato 
* ``PLUS``: ``+``; 
* ``ICS``: ``x``; 
* ``EROUND``: circonferenza; 
* ``ESQUARE``: quadrato vuoto; 
* ``BROUND``: cerchio con bordo; 
* ``BSQUARE``: quadrato con bordo;

**Esempio**

Disegna "punti" con diverse forme.::

    piano = cg.Plane('"Punti"', w=400, h=400, sx=10, axes=False, grid=False)
    biro = piano.newPen(width=15, shape=cg.ICS)
    colors = ('red', 'gold', 'blue', 'green', 'pink', 'violet', 
              'turquoise', 'light cyan')
    for forma, colore in zip(cg.SHAPES, colors):
        posizione = (random.randrange(-10, 10),
                     random.randrange(-10, 10))
        biro.drawpoint(position=posizione, color=colore, shape=forma)
    piano.mainloop()

``drawto``
..........

**Scopo**

Sposta il cursore grafico nella posizione indicata dal parametro tracciando
una linea che congiunge la vecchia alla nuova posizione. Il parametro è
una coppia di numeri.

**Sintassi** 

  ``<penna>.drawto(<punto>)``

**Osservazioni**

``<punto>`` è una coppia di numeri. Sposta il puntatore grafico dalla
posizione attuale al nuovo ``<punto>`` tracciando un segmento.
Restituisce un numero che è il riferimento all'oggetto disegnato.

**Esempio**

Tracciare una linea tratteggiata in diagonale sullo schermo. ::

  piano = cg.Plane('ex_35: drawto', sx=1, axes=False, grid=False)
  biro = cg.Pen(width=2, color='blue')
  x = -200
  y = -100
  biro.position = (x, y)
  for i in range(25):
      x += 10
      y += 5
      biro.drawto((x, y))
      x += 6
      y += 3
      biro.position = (x, y)
  piano.mainloop()

``drawsegment``
...............

**Scopo**

Disegna un segmento nel piano cartesiano.

**Sintassi** 

  ``<penna>.drawsegment(p0, p1=None)``

**Osservazioni**

Può essere chiamato:

* con un argomento, formato da una coppia di numeri, disegna un segmento
  dalla posizione indicata dal cursore grafico al punto passato come
  argomento.
* con due argomenti, due coppie di numeri, traccia il segmento che congiunge
  i due punti.

Non sposta il cursore grafico.
Restituisce un numero che è il riferimento all'oggetto disegnato.

**Esempio**

Disegna i lati e le diagonali di un pentagono. ::

  piano = cg.Plane('ex_36: drawsegment', sx=30)
  biro = cg.Pen(width=4, color='gold')
  vertici = ((-3, -4), (+3, -4), (+5, +1), (0, +5), (-5, +1))
  for i, v0 in enumerate(vertici):
      for v1 in vertici[i+1:]:
          biro.drawsegment(v0, v1)
  piano.mainloop()

``drawpoint``
.............

**Scopo**

Disegna un punto nel piano cartesiano.

**Sintassi** 

  ``<penna>.drawpoint(p=None, color=None, width=None)``

**Osservazioni**

Può essere chiamato:

* senza argomenti, disegna un punto nell'attuale posizione del cursore
  grafico;
* con un argomento, formato da una coppia di numeri, disegna un punto nella
  posizione indicata dall'argomento.
* specificando anche il colore o lo spessore e la forma del punto.

.. note::
   Attualmente le forme sono: ``cg.CIRC`` e ``cg.RECT``.

Non sposta il cursore grafico.
Restituisce un numero che è il riferimento all'oggetto disegnato.

**Esempio**

Disegna 100 punti di colore e spessore e forma casuali. ::

  import random

  def randcolor():
      return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                             random.randrange(256),
                                             random.randrange(256))
                                           
  
    piano = cg.Plane('27. punti', w=420, h=420, sx=1,
                     axes=False, grid=False)
    biro = cg.Pen(color='red', width=200)
    biro.drawpoint(shape=cg.RECT)
    for _ in range(100):
        biro.drawpoint((random.randrange(-100, 100),
                       random.randrange(-100, 100)),
                       color=randcolor(),
                       width=random.randrange(30)+1,
                       shape=random.randrange(2))
  piano.mainloop()

``drawcircle``
...............

**Scopo**

Disegna un cerchio nel piano cartesiano.

**Sintassi** 

``<penna>.drawcircle(radius, center=None, color=None, width=None,`` 
``incolor='')``

**Osservazioni**

Può essere chiamato:

* con un argomento: il raggio del cerchio con centro nell'attuale posizione 
  del cursore grafico;
* con due argomenti: un numero che rappresenta il raggio e una coppia di 
  numeri, che indicano il centro.
* specificando anche il colore o lo spessore della circonferenza.
* specificando anche il colore dell'interno.

Non sposta il cursore grafico.
Restituisce un numero che è il riferimento all'oggetto disegnato.

**Esempio**

Disegna 100 circonferenze. ::

  import random

  def randcolor():
      return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                             random.randrange(256),
                                             random.randrange(256))
                                           
  piano = cg.Plane('28. cerchi', w=420, h=420, sx=1,
                      axes=False, grid=False)
    biro = cg.Pen(color='blue', width=4)
    biro.drawcircle(200, incolor='pink')
    for _ in range(100):
        biro.drawcircle(radius=random.randrange(80),
                        center=(random.randrange(-100, 100),
                                random.randrange(-100, 100)),
                        width=random.randrange(10),
                        color=randcolor(), incolor=randcolor())
  piano.mainloop()

``drawpoly``
............

**Scopo**

Disegna un poligono dati i vertici

**Sintassi** 

  ``<penna>.drawpoly(<vertici>)``

**Osservazioni**

* Il primo argomento è una sequenza, (lista o tupla) che contiene coppie di 
  numeri.
* Si può specificare anche il colore o lo spessore della poligonale.
* Si può specificare anche il colore dell'interno.
* Restituisce un numero che è il riferimento all'oggetto disegnato.

**Esempio**

Disegna un pentagono casuale. ::

  import random

  piano = cg.Plane('ex_39: drawpoly', sx=10, grid=False)
  biro = cg.Pen(width=4, color='blue')
  pentagono = []
  for i in range(5):
      pentagono.append((10-random.randrange(20), 10-random.randrange(20)))
  biro.drawpoly(pentagono, incolor='pink')
  piano.mainloop()

``drawtext``
............

**Scopo**

Scrive un testo nel piano cartesiano.

**Sintassi** 

  ``<penna>.drawtext(text, p=None, color=None, width=None)``

**Osservazioni**

* Il primo argomento è il testo da scrivere.
* Si può specificare la posizione in cui scrivere.
* Si può specificare anche il colore o lo spessore dei caratteri.
* Restituisce un numero che è il riferimento all'oggetto disegnato.

**Esempio**

Scrive alcune parole poco sensate. ::

  piano = cg.Plane('ex_40: drawpoly')
  biro = cg.Pen(x=-5, y=7, color='blue')
  biro.drawtext('testino')
  biro.position = (-5, 3)
  biro.drawtext('testo', color='magenta', width=2)
  biro.position = (-5, -2)
  biro.drawtext('testone', (-5, -2), color='green', width=4)
  piano.mainloop()

Libreria ``pycart``
-------------------

Funzioni
........

.. glossary::

  ``version``
    Restituisce la versione della libreria.

Classi
......

.. glossary::

  ``Plane``
    Piano cartesiano.

  ``Pen``
    Tracciatore grafico per il piano cartesiano.

Attributi
.........

.. glossary::

   ``<penna>.color``
      Colore della penna.

   ``<penna>.position``
      Restituisce l'attuale posizione della penna.

   ``<penna>.width``
      Restituisce l'attuale larghezza della penna.

   ``<piano>.origin``
      Coordinate, in pixel dell'origine del piano.

   ``<piano>.scale``
      Scala di rappresentazione.

Metodi
......

.. glossary::

  ``<penna>.__init__``
    Crea una penna.

  ``<penna>.drawcircle``
    Disegna un cerchio.

  ``<penna>.drawpoint``
    Disegna un punto.

  ``<penna>.drawpoly``
    Disegna un poligono.

  ``<penna>.drawsegment``
    Traccia un segmento.

  ``<penna>.drawtext``
    Scrive un texto.

  ``<penna>.drawto``
    Traccia una linea dalla posizione iniziale alla nuova posizione e sposta 
    la penna.

  ``<penna>.reset``
    Riporta lo stato della penna alla situazione iniziale.

  ``<piano>.__init__``
    Crea un piano cartesiano.

  ``<piano>.after``
    Permette di effettuare un ritardo.

  ``<piano>.axes``
    Traccia gli assi.

  ``<piano>.clean``
    Cancella la finestra grafica.

  ``<piano>.delete``
    Cancella dal piano l'elemento specificato.

  ``<piano>.getcanvasheight``
    Restituisce l'altezza del piano.

  ``<piano>.getcanvaswidth``
    Restituisce la larghezza del piano.

  ``<piano>.grid``
    Traccia una griglia di punti.

  ``<piano>.mainloop``
    Rende attive le finestre grafiche.

  ``<piano>.newPen``
    Crea una nuova penna nel piano.

  ``<piano>.reset``
    Riporta il piano alla situazione iniziale.

  ``<piano>.save``
    Crea un file grafico con il contenuto del piano.

