# utf-8

"""
Funzioni di esempio per Matematica Dolce.
"""

from __future__ import print_function, division
import random

def media(n_0, n_1):
    """Restituisce la media dei numeri n_1 e n_2.
>>> print(media(5, 9))
7.0
"""
    return (n_0 + n_1) / 2

def acaso():
    """Restituisce un numero a caso nell'intervallo [0; 1[.
>>> random.seed(0)
>>> print(acaso())
0.8444218515250481
"""
    return random.random()

def concatena(testo_0, testo_1):
    """Restituisce la lunghezza di un testo.
>>> print(concatena("mara", "meo"))
marameo
"""
    return testo_0 + testo_1

def lunghezza(sequenza):
    """Restituisce la lunghezza di un testo.
>>> print(lunghezza("pippo"))
5
>>> print(lunghezza(""))
0
>>> ins = insieme("pippo")
>>> print(lunghezza(ins))
3
"""
    return len(sequenza)

def primalettera(testo):
    """Restituisce la prima lettera di un testo.
>>> print(primalettera("pippo"))
p
>>> print(primalettera(""))
Traceback (most recent call last):
  File "<pyshell#1>", line 1, in <module>
    print(primalettera(""))
  File "/home/daniele/Documenti/funzioni.py", line 35, in primalettera
    return testo[0]
IndexError: string index out of range
"""
    return testo[0]

def ultimalettera(testo):
    """Restituisce l'ultima lettera di un testo.
>>> print(ultimalettera("pippo"))
o
>>> print(ultimalettera(""))
Traceback (most recent call last):
  File "<pyshell#1>", line 1, in <module>
    print(primalettera(""))
  File "/home/daniele/Documenti/funzioni.py", line 35, in ultimalettera
    return testo[0]
IndexError: string index out of range
"""
    return testo[-1]

def primaultima(testo):
    """Restituisce la parola formata dalla prima e dall'ultima lettera
di una parola.
>>> print(primaultima("pippo"))
po
"""
    return concatena(primalettera(testo), ultimalettera(testo))

def ultimaprima(testo):
    """Restituisce la parola formata dall'ultima e dalla prima lettera
di una parola.
>>> print(ultimaprima("pippo"))
op
"""
    return concatena(ultimalettera(testo), primalettera(testo))

def inizio(testo, numlettere):
    """Restituisce la prima lettera di un testo.
>>> print(inizio("pippo", 3))
p
>>> print(inizio("po", 3))
po
"""
    return testo[:numlettere]

def fine(testo, numlettere):
    """Restituisce la prima lettera di un testo.
>>> print(fine("pippo", 3))
ppo
>>> print(fine(""))

"""
    return testo[-numlettere:]

def insieme(sequenza):
    """Restituisce l'insieme degli elementi di una sequenza.
>>> print(insieme(insieme("pippo")))
{'o', 'p', 'i'}

"""
    return set(sequenza)

def unione(i_0, i_1):
    """Restituisce l'unione di due insiemi.
>>> print(unione(insieme("pippo"), insieme("pluto")))
{'t', 'l', 'i', 'o', 'p', 'u'}

"""
    return i_0 | i_1

def intersezione(i_0, i_1):
    """Restituisce l'intersezione di due insiemi.
>>> print(intersezione(insieme("pippo"), insieme("pluto")))
{'o', 'p'}

"""
    return i_0 & i_1

def differenza(i_0, i_1):
    """Restituisce la differenza tra due insiemi.
>>> print(differenza (insieme("pippo"), insieme("pluto")))
{'i'}
>>> print(differenza (insieme("pluto"), insieme("pippo")))
{'l', 'u', 't'}
"""
    return i_0 & i_1

def sottosequenza(sequenza, da, numel):
    """Restituisce la sottosequenza di sequenza
    a partire da da e formata da numel elementi.
>>> print(sottosequenza("Sopra la panca la capra campa", 18, 5))
capra
"""
    return sequenza[da:da+numel]

# =============================================================================
#
# Funzioni in R
#
# =============================================================================

def identita(x):
    """Restitutisce l'argomento invariato."""
    return x

def costante(x):
    """Restitutisce sempre lo stesso valore qualunque sia x."""
    return -4

def doppio(x):
    """Restituisce il doppio di x."""
    return 2*x

def triplo(x):
    """Restituisce il triplo di x."""
    return 3*x

def proporzionalita(m):
    """Restituisce una funzione direttamente proporzionale a x."""
    def f(x):
        return m*x
    return f

def menotre(x):
    """Restitutisce l'argomento diminuito di 3."""
    return x - 3

def trasla(q):
    """Restituisce una funzione direttamente proporzionale a x."""
    def f(x):
        return x + q
    return f

def lineare(m, q):
    """Restituisce una funzione direttamente proporzionale a x."""
    def f(x):
        return m*x + q
    return f

def quadrato(x):
    """Restitutisce il quadrato della variabile indipendente."""
    return x**2


def main():
    print('media(5, 9)-> ', media(5, 9))
    print('acaso()-> ', acaso())
    print('lunghezza("pippo")-> ', lunghezza("pippo"))
    print('lunghezza("")-> ', lunghezza(""))
    print('concatena("mara", "meo")-> ', concatena("mara", "meo"))
    print('primalettera("pippo")-> ', primalettera("pippo"))
##    print(primalettera(""))
    print('ultimalettera("pippo")-> ', ultimalettera("pippo"))
##    print(ultimalettera(""))
    print('primaultima("pippo")-> ', primaultima("pippo"))
    print('ultimaprima("pippo")-> ', ultimaprima("pippo"))
    print('inizio("pippo", 3)-> ', inizio("pippo", 3))
    print('inizio("po", 3)-> ', inizio("po", 3))
    print('fine("pippo", 3)-> ', fine("pippo", 3))
    print('fine("", 3)-> ', fine("", 3))
    print('insieme("pippo")-> ', insieme("pippo"))
    print('unione(insieme("pippo"), insieme("pluto"))-> ',
          unione(insieme("pippo"), insieme("pluto")))
    print('intersezione(insieme("pippo"), insieme("pluto"))-> ',
          intersezione(insieme("pippo"), insieme("pluto")))
    print('differenza (insieme("pippo"), insieme("pluto"))-> ',
          differenza(insieme("pippo"), insieme("pluto")))
    print('differenza(insieme("pluto"), insieme("pippo"))-> ',
          differenza(insieme("pluto"), insieme("pippo")))
    print('sottosequenza("Sopra la panca la capra campa", 18, 5)-> ',
          sottosequenza("Sopra la panca la capra campa", 18, 5))
    print('proporzionalita(-3)(5)-> ', proporzionalita(-3)(5))
    print('trasla(-3)(5)-> ', trasla(-3)(5))
    print('lineare(-2, 4)(5)-> ', lineare(-2, 4)(5))

if __name__ == "__main__":
    main()
