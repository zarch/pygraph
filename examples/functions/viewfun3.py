#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------python---------------------viewfun3.py--#
#                                                                       #
#                     Visualizzatore di funzioni                        #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""functions plotter."""

from __future__ import print_function, division

#from math import *  # per le funzioni matematiche

from pygraph.colorch import Palette
from pygraph.pyplot import PlotPlane
from pygraph.pycart import PLUS, EROUND

_viewfun_version = '03.01.01'

try:
    #  pyversion = 'py2'
    import Tkinter as tk
#    input = raw_input
except ImportError:
    #  pyversion = 'py3'
    import tkinter as tk
#import os


####
# Costanti relative a dimensioni e posizioni
###
DIMX = DIMY = 600
HEIGHT = 30
GRID = True
POSX = 1 / 2 * DIMX
POSY = 3 / 4 * HEIGHT
DIMCUR = 1 / 2 * HEIGHT
SCALAX = 20
SCALAY = 20
COLORDES = "blue"
COLORSEL = "red"
D = 3
INIFUNC = ".5*x**2"

P1X_CX, P1Y_CX, P2X_CX, P2Y_CX = POSX, POSY, POSX, POSY - DIMCUR
P1X_CY, P1Y_CY, P2X_CY, P2Y_CY = (HEIGHT -  POSY, POSX,
                                  HEIGHT - POSY + DIMCUR, POSX)
P1X_XY, P1Y_XY, P2X_XY, P2Y_XY = POSX - D, POSX - D, POSX + D, POSX + D

####
# Costanti che definiscono nomi
###
CURSORE = 0
ENTRY = 1


def onrange(n, mi, ma):
    if n < mi:
        return mi
    elif n > ma:
        return ma
    else:
        return n


class App(tk.Frame):
    """Classe che realizza l'applicazione."""

    def __init__(self, master):
        """Crea tutta l'interfaccia grafica."""
        tk.Frame.__init__(self, master)
        self.sfunc = tk.StringVar()
        self.sx = tk.StringVar()
        self.sy = tk.StringVar()
        self.sox = tk.StringVar()
        self.soy = tk.StringVar()
        self.sscalax = tk.StringVar()
        self.sscalay = tk.StringVar()
        self.reset_var()
        self.createWidgets()
        self.bindhandlers()

    def reset_var(self):
        """Inizializza le variabili."""
        self.x = 0.
        self.y = 0.
        self.ox = DIMX // 2
        self.oy = DIMY // 2
        self.lastx = self.ox
        self.lasty = self.oy
        self.scalax = SCALAX
        self.scalay = SCALAY
        self.points = []
        self.colore = "#000000"
        self.sfunc.set(INIFUNC)
        self.sx.set(str(self.x))
        self.sy.set(str(self.x))
        self.sox.set(str(self.ox))
        self.soy.set(str(self.ox))
        self.sscalax.set(str(self.scalax))
        self.sscalay.set(str(self.scalay))

#    def set_str_var(self):
#        """Crea le stringhe e le collega ad un entry widget."""
#        def getStringVar(inistring):
#            s_v = tk.StringVar()
#            s_v.set(inistring)
#            return s_v
#        self.sfunc = getStringVar(".5*x**2")
#        self.sx = getStringVar("0.0")
#        self.sy = getStringVar("0.0")
#        self.sox = getStringVar(str(DIMX // 2))
#        self.soy = getStringVar(str(DIMY // 2))
#        self.sscalax = getStringVar(str(SCALAX))
#        self.sscalay = getStringVar(str(SCALAY))

    def createWidgets(self):
        """Crea i widgets."""

        def insor(m, r, c, t, s, w, tv=None):
            """Inserisce all'interno del master m nella riga r colonna c,
            un entry formato da un'etichetta t, con sticky = a s,
            seguita da un oggetto Entry collegato alla variabile tv."""
            tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
            e = tk.Entry(m, width=w, textvariable=tv)
            e.grid(row=r, column=c + 1, sticky=s)
            return e

        def insve(m, r, c, t, s, w, tv=None):
            """Inserisce all'interno del master m nella riga r un entry
            formato da un'etichetta t, con sotto un oggetto Entry collegato
            alla variabile tv."""
            tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
            e = tk.Entry(m, width=w, textvariable=tv)
            e.grid(row=r + 1, column=c, sticky=s)
            return e

        def visuaor(m, r, c, t, s, w, tv=None):
            """Visualizza due etichette affiancate,
            la seconda collegata alla variabile tv."""
            tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
            tk.Label(m, width=w, textvariable=tv).grid(
                row=r, column=c + 1, sticky=s)

        def visuave(m, r, c, t, s, w, tv=None):
            """Visualizza due etichette sovrapposte,
            la seconda collegata alla variabile tv."""
            tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
            tk.Label(m, width=w, textvariable=tv).grid(
                row=r + 1, column=c, sticky=s)

        top = self.master
# Pannello principale (pp)
        pp = tk.Frame(top)
        pp.grid(row=0, column=0)
# Pannello comandi (pc)
        pc = tk.Frame(top, relief=tk.RIDGE, bd=4)
        pc.grid(row=0, column=1, sticky=tk.N + tk.S)
    # Pannello modifica (pcm)
        pcm = tk.Frame(pc, relief=tk.RIDGE, bd=4)
        pcm.grid(row=0, column=0, sticky=tk.N)
        l = tk.Label(pcm, text="Modifica parametri")
        l.grid(row=0, column=0, columnspan=2)
        self.eox = insor(pcm, 1, 0, "Ox= (1-{})".format(DIMX),
                         tk.N, 8, self.sox)
        self.eoy = insor(
            pcm, 2, 0, "Oy= (1-{})".format(DIMY,), tk.N, 8, self.soy)
        self.escalax = insor(pcm, 3, 0, "Scalax=", tk.N, 8, self.sscalax)
        self.escalay = insor(pcm, 4, 0, "Scalay=", tk.N, 8, self.sscalay)
    # Pannello colori (pcc)
        pcc = tk.Frame(pc, relief=tk.RIDGE, bd=4)
        pcc.grid(row=1, column=0, sticky=tk.N)
        self.tavolozza = Palette(pcc)
        self.tavolozza.grid(row=0, column=0)
    # Pannello bottoni (pcb)
        pcb = tk.Frame(pc, relief=tk.RIDGE, bd=4)
        pcb.grid(row=3, column=0, sticky=tk.N)
        d = tk.Button(pcb, text='Disegna',
                      foreground='blue', command=self.draw)
        d.grid(row=0, column=0)
        d = tk.Button(pcb, text='Cancella',
                      foreground='blue', command=self.clean)
        d.grid(row=1, column=0)
        d = tk.Button(pcb, text='Riavvia',
                      foreground='blue', command=self.reset)
        d.grid(row=2, column=0)
        q = tk.Button(pcb, text='Esci', foreground='red', command=self.quit)
        q.grid(row=3, column=0)
# Pannello Funzione (pf)
        pf = tk.Frame(pp)
        pf.grid(row=0, column=0)
        self.efunc = insor(pf, 0, 0, "Funzione:  f(x)=", tk.W, 80, self.sfunc)
        self.efunc.focus()
# Pannello input-output (pio)
        pio = tk.Frame(pp)
        pio.grid(row=1, column=0)
    # Piano cartesiano
        self.cplane = PlotPlane(name="", w=DIMX, h=DIMY,
                            sx=SCALAX, sy=SCALAY,
                            axes=True, grid=GRID, parent=pio)
        self.canvas = self.cplane.getcanvas()
        self.canvas.grid(row=0, column=2)
        self.plot = self.cplane.newPlot()
        self.createpoints()
    # Cursore X
        self.pcurx = tk.Canvas(pio, width=DIMX, height=HEIGHT)
        self.pcurx.grid(row=1, column=2)
        self.curx = self.pcurx.create_line(P1X_CX, P1Y_CX, P2X_CX, P2Y_CX,
                                           fill=COLORDES, arrow='last')
#        self.pointx = self.plot.drawpoint((self.x, self.y), shape=PLUS,
#                                          width=D, color='red')
    # Cursore Y
        self.pcury = tk.Canvas(pio, width=HEIGHT, height=DIMY)
        self.pcury.grid(row=0, column=1)
        self.cury = self.pcury.create_line(P1X_CY, P1Y_CY, P2X_CY, P2Y_CY,
                                           fill=COLORDES, arrow='last')
#        self.pointy = self.plot.drawpoint((self.x, self.y), shape=PLUS,
#                                          width=D, color='blue')
    # Messaggio (pcm)
        self.mess = tk.Text(pio, width=30, height=7,
                            fg="red", state=tk.DISABLED)
        self.mess.grid(row=1, column=0, rowspan=2, columnspan=2)
# Pannello y (py)
        py = tk.Frame(pio, relief=tk.RIDGE, bd=4)
        py.grid(row=0, column=0)
        self.ey = visuaor(py, 0, 0, "y=", None, 20, self.sy)
# Pannello x (px)
        px = tk.Frame(pio, relief=tk.RIDGE, bd=4)
        px.grid(row=2, column=2)
        self.ex = insve(px, 0, 0, "x", None, 12, self.sx)

    def bindhandlers(self):
        """Collega gli eventi ai metodi."""
        self.pcurx.tag_bind(self.curx, "<Any-Enter>", self.mouseEnter)
        self.pcurx.tag_bind(self.curx, "<Any-Leave>", self.mouseLeave)
        self.pcurx.tag_bind(self.curx, "<1>", self.mouseDown)
        self.pcurx.tag_bind(self.curx, "<B1-Motion>", self.movex)
        self.bind_all("<Return>", self.aggiorna)  # aggiungere KP_Enter
        self.bind_all("<FocusOut>", self.aggiorna)
        self.bind_all("<Pause>", self.aggiornacolore)

    def createpoints(self):
        """Create points in cartesian plane."""
    # Cursore XY
        self.curxy = self.plot.drawpoint((self.x, self.y), shape=EROUND,
                                          width=D, color=self.colore)
    # Punti sugli assi
        self.pointx = self.plot.drawpoint((self.x, 0), shape=PLUS,
                                          width=D, color='red')
        self.pointy = self.plot.drawpoint((0, self.y), shape=PLUS,
                                          width=D, color='blue')
#        for s_x, s_y in self.points:
#            self.canvas.create_line(s_x, s_y, s_x + 1, s_y + 1,
#                                    fill=self.colore)

####
# Metodi che gestiscono il trascinamento del cursore x
###
    def mouseDown(self, event):
        """Chiamato quando viene premuto il mouse sul cursore x.
        Ricorda dove il mouse è stato premuto."""
#        self.lastx = event.x
        e = tk.Event()
        e.widget = self.focus_get()
        self.aggiorna(e)

    def mouseEnter(self, event):
        """Chiamato quando il mouse sormonta il cursore x."""
        self.pcurx.itemconfig(tk.CURRENT, fill=COLORSEL)

    def mouseLeave(self, event):
        """Chiamato quando il mouse esce dal cursore x."""
        self.pcurx.itemconfig(tk.CURRENT, fill=COLORDES)

    def quit(self):
        """Chiamato quando viene premuto il pulsante Esci."""
        self.master.quit()
        self.master.destroy()

    def movex(self, event):
        """Chiamato quando il mouse trascina il cursore x."""
        self.pcurx.move(tk.CURRENT, event.x - self.lastx, 0)
#        self.lastx = event.x
        self.x = self.cplane._s2x(self.pcurx.coords(self.curx)[0])
        self.aggiornax(CURSORE)

    def aggiornax(self, da):
        """Chiamato quando viene modificato il valore di x.
        da indica chi ha provocato il cambiamento,
        se il cursore x o la casella di inserimento."""
        sfunc = self.efunc.get()
        self.y = self.calcola(self.x, sfunc)
        s_x = self.cplane._x2s(self.x)
        s_y = self.cplane._y2s(self.y)
        d_x = s_x - self.lastx
        d_y = s_y - self.lasty
        self.canvas.move(self.pointx, d_x, 0)
        self.canvas.move(self.pointy, 0, d_y)
        self.pcury.move(self.cury, 0, d_y)
        self.canvas.move(self.curxy, d_x, d_y)
#        if not (self.x, self.y) in self.points:       # PERCHE' NON VA???
#            self.plot.drawpoint((self.x, self.y), width=.1, color=self.colore)
#            self.points.append((self.x, self.y))
        if not (s_x, s_y) in self.points:
            self.canvas.create_oval(s_x-.5, s_y-.5, s_x+.5, s_y+.5,
                                    outline=self.colore)
            self.points.append((s_x, s_y))
        self.lastx = s_x
        self.lasty = s_y
        self.sx.set("{:7.2f}".format(self.x))
        self.sy.set("{:7.2f}".format(self.y))
        if da != CURSORE:
            self.pcurx.coords(self.curx, (s_x, P1Y_CX, s_x, P2Y_CX))

    def aggiorna(self, event):
        """Modifica x o il piano in base a quanto inserito dall'utente."""
        if event.widget == self.ex:
            self.x = float(self.ex.get())
            self.aggiornax(ENTRY)
        else:
            if event.widget == self.eox:
                v = onrange(int(self.eox.get()), 1, DIMX)
                if v != self.ox:
                    d_x = v - self.ox
                    self.ox = v
                    self.cplane.origin = (self.ox, self.oy)
                    self.lastx += d_x
                    self.pcurx.move(self.curx, d_x, 0)
                    self.points = []
                    self.redraw()
            elif event.widget == self.eoy:
                v = DIMY - onrange(int(self.eoy.get()), 1, DIMY) + 1
                if v != self.oy:
                    d_y = v - self.oy
                    self.oy = v
                    self.cplane.origin = (self.ox, self.oy)
                    self.lasty += d_y
                    self.pcury.move(self.cury, 0, d_y)
                    self.points = []
                    self.redraw()
            elif event.widget == self.escalax:
                v = int(self.escalax.get())
                if v != self.scalax:
                    s_x0 = self.cplane._x2s(self.x)
                    self.scalax = v
                    self.cplane.scale = (self.scalax, self.scalay)
                    s_x1 = self.cplane._x2s(self.x)
                    d_x = s_x1 - s_x0
                    self.lastx += d_x
                    self.pcurx.move(self.curx, d_x, 0)
                    self.points = []
                    self.redraw()
            elif event.widget == self.escalay:
                v = int(self.escalay.get())
                if v != self.cplane.scale[1]:
                    s_y0 = self.cplane._y2s(self.y)
                    self.scalay = v
                    self.cplane.scale = (self.scalax, self.scalay)
                    s_y1 = self.cplane._y2s(self.y)
                    d_y = s_y1 - s_y0
                    self.lasty += d_y
                    self.pcury.move(self.cury, 0, d_y)
                    self.points = []
                    self.redraw()

    def aggiornacolore(self, event):
        """Modifica x o il piano in base a quanto inserito dall'utente."""
        self.colore = self.tavolozza.getcolor()
        self.canvas.itemconfig(self.curxy, outline=self.colore)

    def redraw(self):
        self.cplane.clean()
        self.cplane.axes()
        self.cplane.grid()
        self.createpoints()

    def reset(self):   # TODO deve sistemare anche i cursori
        s_x0 = self.lastx
        s_y0 = self.lasty
        self.reset_var()
        self.pcurx.move(self.curx, self.lastx-s_x0, 0)
        self.pcury.move(self.cury, 0, self.lasty-s_y0)
        self.cplane.origin = (self.ox, self.oy)
        self.cplane.scale = (self.scalax, self.scalay)
        self.redraw()

    def clean(self):
        self.points = []
        self.redraw()

    def draw(self):
        """Disegna il grafico della funzione."""
        e = tk.Event()
        e.widget = self.focus_get()
        self.aggiorna(e)
        sfunc = self.efunc.get()
        if sfunc:
            self.plot.xy(eval('lambda x: {}'.format(sfunc)),
                         color=self.colore)

    def setmess(self, m):
        self.mess.config(state=tk.NORMAL)
        self.mess.insert(tk.END, m)
        self.mess.config(state=tk.DISABLED)
        self.after(3000, self.resetmess)

    def resetmess(self):
        self.mess.config(state=tk.NORMAL)
        self.mess.delete(1.0, tk.END)
        self.mess.config(state=tk.DISABLED)

    def calcola(self, x, f):
        y = 0
        if f:
            try:
                y = eval(f)
                return y
            except(ZeroDivisionError, e):
                self.setmess("Divisione per zero se x={}\n".format(x))
            except(ValueError, e):
                self.setmess("Errore nel calcolo se x={}\n".format(x))
            except(OverflowError, e):
                self.setmess("Errore di Overflow per x={}\n".format(x))
            except(NameError, e):
                self.setmess("Parola sconosciuta in {}\n".format(f))
            except(SyntaxError, e):
                self.setmess("Errore di sintassi in {}\n".format(f))
        return y


def main():
    root = tk.Tk()
    a = App(root)
    root.mainloop()


if __name__ == "__main__":
    main()
