#!/usr/bin/env python
#-------------------------------python------------------------palle.py--#
#                                                                       #
#                         Palle in movimento                            #
#                                                                       #
#--Daniele Zambelli----------------------------------------------2003---#

import pygraph.pycart as cg


class Palla_statica:
    def __init__(self, piano, x=0, y=0, raggio=0, colore='black'):
        """Crea una palla dati: posizione, raggio e colore"""
        self.penna = piano.newPen(width=raggio, color=colore, shape=cg.BROUND)
        self.canvas = piano.getcanvas()
        self.o = self.penna.drawpoint((x, y))

    def setpos(self, x, y):
        """Sposta la palla in un punto del piano"""
        self.canvas.delete(self.o)
        self.o = self.penna.drawpoint((x, y))


class Palla_cinetica_v(Palla_statica):
    def __init__(self, piano, x=0, y=0, raggio=0, colore='black',
                 vx=0, vy=0):
        """Crea una palla dati: posizione, raggio, colore e velocita'"""
        Palla_statica.__init__(self, piano, x, y, raggio, colore)
        self.x, self.y = x, y
        self.vx, self.vy = vx, vy

    def muovi(self):
        """ Muove la palla delle componenti la sua velocita'"""
        self.x += self.vx
        self.y += self.vy
        self.canvas.move(self.o, self.vx, self.vy)
        self.canvas.update()

    def setv(self, vx, vy):
        """Modifica le componenti della velocita' della palla"""
        self.vx, self.vy = vx, vy


class Palla_cinematica_a(Palla_cinetica_v):
    def __init__(self, piano, x=0, y=0, raggio=0, colore='black',
                 vx=0, vy=0, ax=0, ay=0):
        """Crea una palla dati: posizione, raggio, colore, velocita'
        e accelerazione"""
        Palla_cinetica_v.__init__(self, piano, x, y, raggio, colore, vx, vy)
        self.ax, self.ay = ax, ay

    def muovi(self):
        """ Applica l'accelerazione poi
        muove la palla delle componenti la sua velocita'"""
        self.vx += self.ax
        self.vy += self.ay
        Palla_cinetica_v.muovi(self)

    def seta(self, ax, ay):
        """Modifica le componenti dell'accelerazione della palla"""
        self.ax, self.ay = ax, ay


def test():
    # Creo una piano con dimensioni 600x600
    piano = cg.Plane("Una Palla per Vittorio",
                     w=600, h=600, sx=1, axes=False, grid=False)

# Creo una palla statica rossa
    palla1 = Palla_statica(piano, raggio=20, x=-50, y=-50, colore='red')

# Salta da una parte all'altra dello schermo, dove voglio io
    piano.after(1000)
    palla1.setpos(50, -50)
    piano.after(1000)
    palla1.setpos(50, 50)
    piano.after(1000)
    palla1.setpos(-50, 50)
    piano.after(1000)

# Creo una palla cinematica blu posizionata in alto a sinistra
# e con velocita' (3, 2)
# palla1=Palla_cinetica_v(p, raggio=20, x=-280, y=280, colore='blue',
# vx=3, vy=2)
    palla1 = Palla_cinetica_v(piano, raggio=20, x=-280, y=280, colore='blue')
    palla1.setv(3, 2)

# Si muove di moto rettilineo uniforme per un tratto
    for i in range(180):
        palla1.muovi()
        piano.after(1)

# Creo una palla dinamica verde posizionata in alto a sinistra
# e con velocita' iniziale (3, 0) e accelerazione (0, 1)
# palla1=Palla_cinematica_a(p, raggio=20, x=0, y=280, colore='green',
# vx=10, vy=0,
# ax=0, ay=1)
    palla1 = Palla_cinematica_a(piano, raggio=20, x=0, y=280, colore='green')
    palla1.setv(7, 0)
    palla1.seta(0, 1)

# Cade con moto uniformemente accelerato

    piano.after(1000)
    for i in range(32):
        palla1.muovi()
        piano.after(10)

    piano.mainloop()


if __name__ == "__main__":
    test()
