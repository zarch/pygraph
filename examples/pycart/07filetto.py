#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python----------------reactiveplane.py--#
#                                                                       #
#                           Piano reattivo                              #
#                                                                       #
# Copyright (c) 2013 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Reactive Plane
"""

from __future__ import division, print_function
import pygraph.pycart as cg

SEMIBASE = 20
BASE = SEMIBASE * 2
#SEMIDIM = SEMIBASE * 3
DIM = BASE * 3
CONVERT = {(0, 0): 0, (1, 0): 1, (2, 0): 2,
           (0, 1): 3, (1, 1): 4, (2, 1): 5,
           (0, 2): 6, (1, 2): 7, (2, 2): 8, }


def onclick(event):
    """Gestisci il click."""
    x, y = event.x, event.y
    print('Cliccato in', x, y)
    x_c = y_c = 1
    while x >= x_c * BASE:
        x_c += 1
    x_c -= 1
    while y >= y_c * BASE:
        y_c += 1
    y_c -= 1
#    for cont in range(1, 4):            # altro modo
#        if x < cont * BASE:
#            x = cont - 1
#            break
#    for cont in range(1, 4):
#        if y < cont * BASE:
#            y = cont - 1
#            break
#    if  x < BASE:                       # altro modo
#        x = 0
#    elif x < 2*BASE:
#        x = 1
#    elif x < 3*BASE:
#        x = 2
#    if  y < BASE:
#        y = 0
#    elif y < 2*BASE:
#        y = 1
#    elif y < 3*BASE:
#        y = 2
    print('posizione:', CONVERT[(x_c, y_c)])


piano = cg.Plane(w=DIM, h=DIM, ox=0, oy=DIM, sx=1, axes=False, grid=False)
piano.onpress1(onclick)
penna = cg.Pen()
penna.drawsegment((BASE, DIM), (BASE, 0))
penna.drawsegment((BASE * 2, DIM), (BASE * 2, 0))
penna.drawsegment((0, BASE), (DIM, BASE))
penna.drawsegment((0, BASE * 2), (DIM, BASE * 2))

piano.mainloop()
