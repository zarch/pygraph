#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python-------------------frazioni02.py--#
#                                                                       #
#               Funzioni che operano con le Frazioni                    #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from __future__ import print_function


def mcd(a, b):
    '''Massimo Comune Divisore tra a e b'''
    a, b = abs(a), abs(b)
    while a != b and a != 1 and b != 1:
        if a > b:
            a -= b
        else:
            b -= a
    if b == 1:
        return b
    else:
        return a


def frazione(num, den):
    """Restituisce una frazione dati numeratore e denominatore.

    Solleva un'eccezione se il denominatore � nullo.
    Il denominatore � sempre positivo.
    La frazione � ridotta ai minimi termini."""
    if den == 0:
        raise ZeroDivisionError
    elif num == 0:
        return (0, 1)
    else:
        if den > 0:
            return riduci((num, den))
        else:
            return riduci((-num, -den))


def riduci(f):
    """Riduce una coppia di numeri dividendoli per il loro MCD."""
    num, den = f
    divisore = mcd(num, den)
    return (num // divisore, den // divisore)


def sommaf(f1, f2):
    """Restituisce la somma di due frazioni."""
    num1, den1 = f1
    num2, den2 = f2
    num = num1 * den2 + num2 * den1
    den = den1 * den2
    return frazione(num, den)


def oppostof(f):
    """Restituisce l'opposto di una frazione."""
    num, den = f
    return frazione(-num, den)


def differenzaf(f1, f2):
    """Restituisce la differenza di due frazioni."""
    return sommaf(oppostof(f1), f2)


def prodottof(f1, f2):
    """Restituisce il prodotto di due frazioni."""
    num1, den1 = f1
    num2, den2 = f2
    return frazione(num1 * num2, den1 * den2)


def reciprocof(f):
    """Restituisce il reciproco di una frazione."""
    num, den = f
    return frazione(den, num)


def quozientef(f1, f2):
    """Restituisce il quoziente di due frazioni."""
    return prodottof(f1, reciprocof(f2))


def randf(num_mi, num_ma, den_mi, den_ma):
    """Restituisce una frazione casuale.

    Il numeratore � compreso tra num_mi e num_ma;
    Il denominatore � compreso tra den_mi e den_ma"""
    from random import randrange
    num = randrange(num_mi, num_ma)
    while True:
        den = randrange(den_mi, den_ma)
        if den:
            break
    return frazione(num, den)


def strf(f):
    """Restituisce una stringa che rappresenta una frazione."""
    num, den = f
    return "{}/{}".format(num, den)


def test():
    """Esegue un test sulle varie funzioni di questa libreria."""
    a, b = 42, 56
    print("MCD({}; {})={}".format(a, b, mcd(a, b)))
    a, b = 18, 12
    f = frazione(a, b)
    print("{} = {}".format(f, riduci(f)))
    a = frazione(2, 5)
    b = frazione(3, 4)
    print("{} + {} = {}".format(a, b, sommaf(a, b)))
    print("{} - {} = {}".format(a, b, differenzaf(a, b)))
    print("{} * {} = {}".format(a, b, prodottof(a, b)))
    print("{} / {} = {}".format(a, b, quozientef(a, b)))
    c = randf(-10, 10, -10, 10)
    d = randf(-10, 10, -10, 10)
    print("Espressione")
    print("{} / {} + {} * {} = {}".format(strf(a), strf(b), strf(c), strf(d),
                                          strf(sommaf(quozientef(a, b),
                                                      prodottof(c, d)))))
    print("Frazione con divisore nullo:")
    print(frazione(5, 0))


if __name__ == "__main__":
    test()
