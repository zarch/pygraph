#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python-------------------frazioni01.py--#
#                                                                       #
#             Esercitatore di calcolo con le Frazioni                   #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from __future__ import print_function

try:
    #  pyversion = 'py2'
    input = raw_input
except:
    pass

''' Primo livello
def eser_fraz():
  """ Esercitatore di calcolo frazionario."""
  while True:
    menu()
    scelta=input("La tua scelta: ")
    if scelta=="0": break
    elif scelta=="1": addizione()
    elif scelta=="2": sottrazione()
    elif scelta=="3": moltiplicazione()
    elif scelta=="4": divisione()

def menu():
  """Menu di scelta degli esercizi."""
  print(
"""
Frazioni

Scegli il tipo di esercizi:
1: Addizione
2: Sottrazione
3: Moltiplicazione
4: Divisione

0: Fine
""")

def addizione():
  """Esercizi sull'addizione di frazioni."""
  print("ecco le addizioni")

def sottrazione():
  """Esercizi sulla sottrazione di frazioni."""
  print("ecco le sottrazioni")

def moltiplicazione():
  """Esercizi sulla moltiplicazione di frazioni."""
  print("ecco le moltiplicazioni")

def divisione():
  """Esercizi sulla divisione di frazioni."""
  print("ecco le divisioni")

eser_fraz()

fine primo livello '''

''' inizio secondo livello
import random

NUM_ESERCIZI=3
MAX_VAL=11

def eser_fraz():
  """ Esercitatore di calcolo frazionario."""
  while True:
    menu()                                # Stampa il menu
    scelta=input("La tua scelta: ")   # legge la scelta dell'utente
    if scelta=="0": break                 # con zero termina il ciclo
    elif scelta=="1": addizione()         # le varie scelte...
    elif scelta=="2": sottrazione()
    elif scelta=="3": moltiplicazione()
    elif scelta=="4": divisione()

def menu():
  """Menu di scelta degli esercizi."""
  print(
"""
Frazioni

Scegli il tipo di esercizi:
1: Addizione
2: Sottrazione
3: Moltiplicazione
4: Divisione

0: Fine
""")

def addizione():
  """Esercizi sull'addizione di frazioni."""
  for i in range(NUM_ESERCIZI):          # ciclo
    num1=random.randrange(1, MAX_VAL)     # scelta casuale dei valori
    den1=random.randrange(1, MAX_VAL)
    num2=random.randrange(1, MAX_VAL)
    den2=random.randrange(1, MAX_VAL)
    numr=num1*den2+num2*den1              # numeratore del risultato
    denr=den1*den2                        # denominatore del risultato
    divisore=2                            # semplificazione della frazione
    while divisore<=denr/2:
      if numr % divisore == 0 and denr % divisore == 0:
        numr/=divisore
        denr/=divisore
      else:
        divisore+=1
    risposta=input("{}/{}+{}/{} = ".format(num1, den1, num2, den2))
    if risposta == "{}/{}".format(numr, denr):
      giusto()
    else:
      sbagliato(numr, denr)

def sottrazione():
  """Esercizi sulla sottrazione di frazioni."""
  print("ecco le sottrazioni")

def moltiplicazione():
  """Esercizi sulla moltiplicazione di frazioni."""
  print("ecco le moltiplicazioni")

def divisione():
  """Esercizi sulla divisione di frazioni."""
  print("ecco le divisioni")

def giusto():
  """Azioni da svolgare quando la risposta � giusta."""
  print("Bravo, la risposta � giusta!")

def sbagliato(num, den):
  """Azioni da svolgare quando la risposta � sbagliata."""
  print("Sbagliato, la risposta era: {}/{}".format(num, den))
  c=input("premi <invio>")

eser_fraz()

fine secondo livello '''

import random

NUM_ESERCIZI = 3
MAX_VAL = 11


def eser_fraz():
    """ Esercitatore di calcolo frazionario."""
    while True:
        menu()                                # Stampa il menu
        scelta = input("La tua scelta: ")   # legge la scelta dell'utente
        if scelta == "0":
            break                 # con zero termina il ciclo
        elif scelta == "1":
            addizione()         # le varie scelte...
        elif scelta == "2":
            sottrazione()
        elif scelta == "3":
            moltiplicazione()
        elif scelta == "4":
            divisione()


def menu():
    """Menu di scelta degli esercizi."""
    print(
        """
Frazioni

Scegli il tipo di esercizi:
1: Addizione
2: Sottrazione
3: Moltiplicazione
4: Divisione

0: Fine
""")


def addizione():
    """Esercizi sull'addizione di frazioni."""
    giuste = 0
    sbagliate = 0
    for i in range(NUM_ESERCIZI):          # ciclo
        num1 = random.randrange(1, MAX_VAL)     # scelta casuale dei valori
        den1 = random.randrange(1, MAX_VAL)
        num2 = random.randrange(1, MAX_VAL)
        den2 = random.randrange(1, MAX_VAL)
        numr = num1 * den2 + num2 * den1              # numeratore del risultato
        denr = den1 * den2                        # denominatore del risultato
        divisore = 2                            # semplificazione della frazione
        while divisore <= denr / 2:
            if numr % divisore == 0 and denr % divisore == 0:
                numr //= divisore
                denr //= divisore
            else:
                divisore += 1
        risposta = input("{}/{}+{}/{} = ".format(num1, den1, num2, den2))
        if risposta == "{}/{}".format(numr, denr):
            giusto()
            giuste += 1
        else:
            sbagliato(numr, denr)
            sbagliate += 1
    risultati("addizioni", giuste, sbagliate)


def sottrazione():
    """Esercizi sulla sottrazione di frazioni."""
    print("ecco le sottrazioni")


def moltiplicazione():
    """Esercizi sulla moltiplicazione di frazioni."""
    print("ecco le moltiplicazioni")


def divisione():
    """Esercizi sulla divisione di frazioni."""
    print("ecco le divisioni")


def giusto():
    """Azioni da svolgare quando la risposta � giusta."""
    print("Bravo, la risposta � giusta!")


def sbagliato(num, den):
    """Azioni da svolgare quando la risposta � sbagliata."""
    print("Sbagliato, la risposta era: {}/{}".format(num, den))
    c = input("premi <invio>")


def risultati(tipo, g, s):
    """Visualizza i risultati ottenuti."""
    print("\n\nNelle {}, hai ottenuto {}% di risposte esatte!\n".format(
          tipo, 100 * g // (g + s)))
    d = input("premi <invio> per continuare")


eser_fraz()
