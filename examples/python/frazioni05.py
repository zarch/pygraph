#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python-------------------frazioni05.py--#
#                                                                       #
#             Esercitatore di calcolo con le Frazioni                   #
#    Versione che utilizza la programmazione orientata agli oggetti     #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from __future__ import print_function

import frazioni04 as fraz

try:
    #  pyversion = 'py2'
    input = raw_input
except:
    pass

NUM_ESERCIZI = 3
MAX_VAL = 11


def eser_fraz():
    """ Esercitatore di calcolo frazionario."""
    f = fraz.Frazione(1, 1)
    f.txtstr()
    add = Addizione()
    sot = Sottrazione()
    mol = Moltiplicazione()
    div = Divisione()
    while True:
        menu()                                  # Stampa il menu
        scelta = input("La tua scelta: ")       # legge la scelta dell'utente
        if scelta == "0":
            break                 # con zero termina il ciclo
        elif scelta == "1":
            add.run()           # le varie scelte...
        elif scelta == "2":
            sot.run()
        elif scelta == "3":
            mol.run()
        elif scelta == "4":
            div.run()


def menu():
    """Menu di scelta degli esercizi."""
    print(
        """
Frazioni

Scegli il tipo di esercizi:
1: Addizione
2: Sottrazione
3: Moltiplicazione
4: Divisione

0: Fine
""")


class Esercizio(object):
    """Generico esercizio."""

    def run(self):
        """Esercizi sull'addizione di frazioni."""
        giuste = 0
        sbagliate = 0
        for i in range(NUM_ESERCIZI):                 # ciclo
            self.f1 = fraz.randf(1, MAX_VAL, 1, MAX_VAL)   # primo operando
            self.f2 = fraz.randf(1, MAX_VAL, 1, MAX_VAL)   # secondo operando
            fr = self.risultato()                          # risultato
            risposta = input(self.domanda())
            if risposta == str(fr):
                self.giusto()
                giuste += 1
            else:
                self.sbagliato(fr)
                sbagliate += 1
        self.risultati(self.nome(), giuste, sbagliate)

    def risultato(self):
        """Restituisce il risultato dell'operazione.

        Cambia a seconda dell'operazione da eseguire."""
        pass

    def domanda(self):
        """Restituisce la domanda dell'esercizio.

        Cambia a seconda dell'operazione da eseguire."""
        pass

    def nome(self):
        """Restituisce il tipo di esercizi.

        Cambia a seconda dell'operazione da eseguire."""
        pass

    def giusto(self):
        """Azioni da svolgare quando la risposta � giusta."""
        print("Bravo, la risposta � giusta!")

    def sbagliato(self, risultato):
        """Azioni da svolgare quando la risposta � sbagliata."""
        print("Sbagliato, la risposta era: {}".format(risultato))
        c = input("premi <invio>")

    def risultati(self, tipo, g, s):
        """Visualizza i risultati ottenuti."""
        print("\n\nNelle {}, hai ottenuto {}% di risposte esatte!\n".format(
              tipo, 100 * g / (g + s)))
        d = input("premi <invio> per continuare")


class Addizione(Esercizio):
    """Esercizi sull'addizione di frazioni."""

    def risultato(self):
        return self.f1 + self.f2

    def domanda(self):
        return "{} + {} = ".format(self.f1, self.f2)

    def nome(self):
        return "addizioni"


class Sottrazione(Esercizio):
    """Esercizi sulla sottrazione di frazioni."""

    def risultato(self):
        return self.f1 - self.f2

    def domanda(self):
        return "{} - {} = ".format(self.f1, self.f2)

    def nome(self):
        return "sottrazioni"


class Moltiplicazione(Esercizio):
    """Esercizi sulla moltiplicazione di frazioni."""

    def risultato(self):
        return self.f1 * self.f2

    def domanda(self):
        return "{} * {} = ".format(self.f1, self.f2)

    def nome(self):
        return "moltiplicazioni"


class Divisione(Esercizio):
    """Esercizi sulla divisione di frazioni."""

    def risultato(self):
        return self.f1 / self.f2

    def domanda(self):
        return "{} / {} = ".format(self.f1, self.f2)

    def nome(self):
        return "divisioni"


eser_fraz()
