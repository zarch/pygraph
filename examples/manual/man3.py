# man3.py
# 3 gennaio 2018
# Daniele Zambelli

"""
Il programma deve creare una classe di oggetti
capaci di disegnare un muro di mattoni quadrati
Disposti in posizione parzialmente casuale.
"""

# Lettura delle librerie
import pyturtle as tg
import man2
import random

# Definizione di classi


class Architetto(man2.Ingegnere):
    """Una tartaruga che sa costruire muri scombinati."""

    def quadrato(self, lato):
        """Disegna un mattone spostato rispetto alla posizione
        attuale di Tartaruga."""
        angolo = random.randrange(30)
        spostamento = random.randrange(30)
        self.up()
        self.right(angolo)
        self.forward(spostamento)
        self.down()
        man2.Ingegnere.quadrato(self, lato)
        self.up()
        self.back(spostamento)
        self.left(angolo)
        self.down()


if __name__ == '__main__':
    # Programma principale
    piano = tg.TurtlePlane()
    michelangelo = Architetto()
    michelangelo.sposta(-250, -190)
    michelangelo.muro(20, 15, 20)

    # Rende attiva la finestra grafica
    piano.mainloop()
