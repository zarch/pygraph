#!/usr/bin/env python
#-------------------------------python---------------------libmanex.py--#
#                                                                       #
#                         Function for tests                            #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2010---#

from __future__ import print_function


def end(p):
    print('fine')
    p.mainloop()


def doall(locdict, pref, first=0):
    lpref = len(pref)
    funcs = [n for n in locdict.keys() if n[:len(pref)] == pref]
    funcs.sort()
    for e in funcs:
        print(e)
    tot = len(funcs)
    first -= 1
    for n, f in enumerate(funcs):
        if n < first:
            continue
        print('\n', f, n + 1, 'of', tot, ':')
        print(locdict[f].__doc__)
        locdict[f]()
