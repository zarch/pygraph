# man0.py
# 2 gennaio 2018
# Daniele Zambelli

"""
Il programma deve disegnare un muro di mattoni quadrati.
"""

# Lettura delle librerie
import pyturtle as tg

# Definizione di funzioni


def sposta(avanti=0, sinistra=0):
    """Effettua uno spostamento orizzontale e verticale
       di Tartaruga senza disegnare la traccia."""
    tina.up()
    tina.forward(avanti)
    tina.left(90)
    tina.forward(sinistra)
    tina.right(90)
    tina.down()


def quadrato(lato):
    """Disegna un quadrato di dato lato vuoto o pieno."""
    for cont in range(4):
        tina.forward(lato)
        tina.left(90)


def fila(lato, numero, spazio_colonne=5):
    """Disegna una fila di mattoni quadrati."""
    for cont_col in range(numero):
        quadrato(lato)
        sposta(avanti=lato + spazio_colonne)
    sposta(avanti=-numero * (lato + spazio_colonne))


def muro(lato, righe, colonne, spazio_colonne=5, spazio_righe=5):
    """Disegna un muro di mattoni."""
    for cont_rig in range(righe):
        fila(lato, colonne, spazio_colonne)
        sposta(sinistra=lato + spazio_righe)
    sposta(sinistra=-righe * (lato + spazio_righe))


# Programma principale
piano = tg.TurtlePlane()
tina = tg.Turtle()

sposta(-250, -190)
muro(20, 15, 20)

# Rende attiva la finestra grafica
piano.mainloop()
