# man4.py
# 3 gennaio 2018
# Daniele Zambelli

"""
Il programma deve creare una classe di oggetti
capaci di disegnare un muro di mattoni quadrati
Disposti in posizione parzialmente casuale e
con colori casuali.
"""

# Lettura delle librerie
import pyturtle as tg
import man3
import random

# Definizione di classi


class Artista(man3.Architetto):
    """Una tartaruga che sa costruire muri scombinati e colorati."""

    def quadrato(self, lato):
        """Disegna un mattone scombinato e colorato."""
        self.color = (random.random(), random.random(), random.random())
        self.fill(1)
        man3.Architetto.quadrato(self, lato)
        self.fill(0)


if __name__ == '__main__':
    # Programma principale
    piano = tg.TurtlePlane()
    raffaello = Artista()
    raffaello.sposta(-250, -190)
    raffaello.muro(20, 15, 20)

    # Rende attiva la finestra grafica
    piano.mainloop()
