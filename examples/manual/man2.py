# man2.py
# 3 gennaio 2018
# Daniele Zambelli

"""
Il programma deve creare una classe di oggetti
capaci di disegnare un muro di mattoni quadrati.
"""

# Lettura delle librerie
import pyturtle as tg

# Definizione di classi


class Ingegnere(tg.Turtle):
    """Una tartaruga che sa costruire muri."""

    def sposta(self, avanti=0, sinistra=0):
        """Effettua uno spostamento orizzontale e verticale di
        Tartaruga senza disegnare la traccia."""
        self.up()
        self.forward(avanti)
        self.left(90)
        self.forward(sinistra)
        self.right(90)
        self.down()

    def quadrato(self, lato):
        """Disegna un quadrato di dato lato."""
        for i in range(4):
            self.forward(lato)
            self.left(90)

    def muro(self, lato, righe, colonne,
             spazio_righe=5, spazio_colonne=5):
        """Disegna un muro di mattoni quadrati."""
        for i in range(righe):
            for j in range(colonne):
                self.quadrato(lato)
                self.sposta(avanti=lato + spazio_colonne)
            self.sposta(avanti=-colonne * (lato + spazio_colonne),
                        sinistra=lato + spazio_righe)
        self.sposta(sinistra=-righe * (lato + spazio_righe))


# Programma principale
if __name__ == '__main__':
    piano = tg.TurtlePlane()
    leonardo = Ingegnere()
    leonardo.sposta(-250, -190)
    leonardo.muro(20, 15, 20)

    # Rende attiva la finestra grafica
    piano.mainloop()
