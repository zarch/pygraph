# man1.py
# 2 gennaio 2018
# Daniele Zambelli

"""
Il programma deve disegnare un muro di mattoni quadrati.
"""

# Lettura delle librerie
import pyturtle as tg

# Definizione di funzioni


def sposta(avanti=0, sinistra=0):
    """Effettua uno spostamento orizzontale e verticale
       di Tartaruga senza disegnare la traccia."""
    tina.up()
    tina.forward(avanti)
    tina.left(90)
    tina.forward(sinistra)
    tina.right(90)
    tina.down()


def quadrato(lato):
    """Disegna un quadrato di dato lato vuoto o pieno."""
    for cont in range(4):
        tina.forward(lato)
        tina.left(90)


def muro(lato, righe, colonne, spazio_righe=5, spazio_colonne=5):
    """Disegna un muro di mattoni."""
    for cont_rig in range(righe):
        for cont_col in range(colonne):
            quadrato(lato)
            sposta(avanti=lato + spazio_colonne)
        sposta(-colonne * (lato + spazio_colonne), lato + spazio_righe)
    sposta(sinistra=-righe * (lato + spazio_righe))


# Programma principale
piano = tg.TurtlePlane()
tina = tg.Turtle()

sposta(-250, -190)
muro(20, 15, 20)

# Rende attiva la finestra grafica
piano.mainloop()
