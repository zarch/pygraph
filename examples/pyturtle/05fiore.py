#!/usr/bin/env python
#--------------------------python-pyturtle--------------------fiore.py--#
#                                                                       #
#                               Fiore                                   #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2002--#

import pygraph.pyturtle as tg


def petalo(r):
    tina.right(45)
    tina.circle(r, 90)
    tina.left(90)
    tina.circle(r, 90)
    tina.left(135)


def corolla(r):
    tina.color = 'gold'
    for i in range(12):
        petalo(r)
        tina.left(30)


def dalia(l):
    tina.color = 'green'
    tina.forward(l)
    tina.left(45)
    petalo(l / 2)
    tina.right(45)
    tina.forward(l / 2)
    tina.right(45)
    petalo(l / 2)
    tina.left(45)
    tina.forward(l)
    corolla(l / 2)
    tina.up()
    tina.back(l * 2.5)


piano = tg.TurtlePlane()
tina = tg.Turtle(y=-190, d=90, width=2)
dalia(100)
piano.mainloop()
