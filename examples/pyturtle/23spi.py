#!/usr/bin/env python
#--------------------------python-pyturtle------------------2spirad.py--#
#                                                                       #
#             La spirale delle radici dei numeri interi                 #
#                                                                       #
#--Daniele Zambelli-------------GPL3-------------------------------2012-#

import pygraph.pyturtle as tg


def spi():
    """Disegna una "spirale" poligonale."""
    for lato in range(200):
        tina.forward(lato)
        tina.left(121)


piano = tg.TurtlePlane()
tina = tg.Turtle()
spi()

piano.mainloop()
