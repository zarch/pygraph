#!/usr/bin/env python
#--------------------------python-pyturtle-------------06sierpinski.py--#
#                                                                       #
#                      Triangolo di Sierpinski                          #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

import pygraph.pyturtle as tg


def triangolo_sierpinski(lato):
    if lato < 5:
        return
    for i in range(3):
        triangolo_sierpinski(lato / 2)
        tina.forward(lato)
        tina.left(120)


def angolotri_sierpinski(lato):
    if lato < 5:
        return
    for i in range(3):
        tina.forward(lato)
        angolotri_sierpinski(lato / 2)
        tina.left(120)


def triangolo_esterno(lato):
    if lato < 5:
        return
    for i in range(3):
        tina.forward(lato / 2)
        tina.right(120)
        triangolo_esterno(lato / 2)
        tina.left(120)
        tina.forward(lato / 2)
        tina.left(120)


piano = tg.TurtlePlane()
tina = tg.Turtle(color='brown', x=-200, y=-180)
tina.hide()
triangolo_sierpinski(400)

piano = tg.TurtlePlane()
tina = tg.Turtle(color='brown', x=-100, y=-70)
tina.hide()
angolotri_sierpinski(150)

piano = tg.TurtlePlane()
tina = tg.Turtle(color='brown', x=-100, y=0)
triangolo_esterno(200)

piano.mainloop()
