#!/usr/bin/env python
# -*- coding: utf-8 -*-
#--------------------------python-pyturtle-------------24levyccurve.py--#
#                                                                       #
#                            Lévy C curve                               #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2018--#

import pygraph.pyturtle as tg


def ccurve(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    ccurve(lato, livello-1)
    tina.right(90)
    ccurve(lato, livello-1)
    tina.left(90)

piano = tg.TurtlePlane(0)
tina = tg.Turtle(color='brown')
ccurve(50, 0)

piano = tg.TurtlePlane(1)
tina = tg.Turtle(color='brown')
ccurve(50, 1)

piano = tg.TurtlePlane(2)
tina = tg.Turtle(color='brown')
ccurve(50, 2)

piano = tg.TurtlePlane(10)
tina = tg.Turtle(color='brown')
ccurve(5, 10)

piano = tg.TurtlePlane(14, h=600)
tina = tg.Turtle(color='brown', y=-150)
tina.hide()
ccurve(2, 14)

piano.mainloop()
