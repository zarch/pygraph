#!/usr/bin/env python
# -*- coding: utf-8 -*-
#--------------------------python-pyturtle-------------25dragoncurve.py--#
#                                                                       #
#                            Dragon curve                               #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2018--#

import pygraph.pyturtle as tg

def dragonsin(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsin(lato, livello-1)
    tina.left(90)
    dragondes(lato, livello-1)

def dragondes(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsin(lato, livello-1)
    tina.right(90)
    dragondes(lato, livello-1)

def dragonsinb(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsinb(lato, livello-1)
    tina.left(45); tina.forward(lato/5); tina.left(45)
    dragondesb(lato, livello-1)

def dragondesb(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsinb(lato, livello-1)
    tina.right(45); tina.forward(lato/5); tina.right(45)
    dragondesb(lato, livello-1)

for liv in range(7):
    piano = tg.TurtlePlane(f"livello={liv}")
    tina = tg.Turtle(color='brown', y=100)
    dragonsinb(25, liv)

piano = tg.TurtlePlane(14, h=600)
tina = tg.Turtle(color='brown', y=100)
tina.hide()
dragonsin(2, 14)

piano.mainloop()
