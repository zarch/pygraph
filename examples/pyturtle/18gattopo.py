#!/usr/bin/env python
#-------------------------------python-------------------07gattopo.py--#
#                                                                      #
#                    Un gatto che insegue un topo                      #
#                                                                      #
#--Daniele Zambelli----------------------------------------------2005--#

import pygraph.pyturtle as tg
import random


VTOPO = .65
VGATTO = .6

# dirto


class Topo(tg.Turtle):
    """Classe che simula il comportamento di un topo."""

    def __init__(self):
        tg.Turtle.__init__(self, color='black')
        self.up()
        self.position = (-280, 180)
        self.write("inseguimento in base alla vista")
        self.position = (random.randrange(120) - 60, random.randrange(80) - 40)
        self.down()

    def scappa(self, da):
        """Fa muovere il topo di un passo
        cercando di scappare da da"""
        dir_gatto = self.dirto(da.position)
        if 170 < dir_gatto < 190 or -10 < dir_gatto < 10:
            self.right(random.randrange(180) - 90)
        else:
            self.left(random.randrange(20) - 10)
        self.forward(VTOPO)


class Gatto(tg.Turtle):
    """Classe che simula il comportamento di un gatto"""

    def __init__(self):
        tg.Turtle.__init__(self)
        self.color = "gray"
        self.width = 3

    def insegui(self, chi):
        """Fa muovere il gatto di un passo come se inseguisse chi.
        restituisce 0 se ha raggiunto chi"""
        if self.distance(chi.position) < 3:
            self.position = chi.position
            self.color = "red"
            self.write("   Buono questo topo!!!")
            return 0
        dir_topo = self.dirto(chi.position)
        if dir_topo < 180:
            self.left(random.randrange(5))
        else:
            self.right(random.randrange(5))
        self.forward(VGATTO)
        return 1


def insegui():
    """Simula l'inseguimento tra un gatto e un topo"""
    topo = Topo()
    gatto = Gatto()
    while gatto.insegui(topo):
        topo.scappa(gatto)


piano = tg.TurtlePlane(h=600)
insegui()

piano.mainloop()
