#!/usr/bin/env python
#-------------------------------python-----------------------archi.py--#
#                                                                      #
#                      Demo di circle e ccircle                        #
#                                                                      #
#--Daniele Zambelli----------------------------------------------2003--#

import random
import pygraph.pyturtle as tg


class MiaTurtle(tg.Turtle):

    def spostati(self, avanti, sinistra):
        """ Effettua uno spostamento orizzontale e verticale di Tartaruga
        senza disegnare la traccia """
        self.up()
        self.forward(avanti)
        self.left(90)
        self.forward(sinistra)
        self.right(90)
        self.down()

    def saltaacaso(self):
        """Sposta casualmente Tartaruga all'interno del rettangolo
        di vertici (0, 0) e (600, 400)."""
        self.up()
        self.pos = (random.randrange(600) - 300, random.randrange(400) - 200)
        self.down()

    def coloraacaso(self):
        """Assegna a Tartaruga un colore con componenti RGB casuali."""
        self.color = (random.random(), random.random(), random.random())

    def settorecirc(self, r):
        self.left(45)
        self.ccircle(r, 90)
        self.right(135)

    def segmentocirc(self, r):
        self.circle(r, 180)
        self.left(90)
        self.forward(r + r)
        self.left(90)

    def petalo(self, r):
        self.right(45)
        self.circle(r, 90)
        self.left(90)
        self.circle(r, 90)
        self.left(135)

    def radio(self, r):
        for i in range(3):
            self.ccircle(r, 60)
            self.left(60)


def macchie(tarta, n):
    for j in range(n):
        tarta.saltaacaso()
        tarta.coloraacaso()
        tarta.fill(1)
        tarta.circle(random.randrange(100), random.randrange(360))
        tarta.fill(0)


def cmacchie(tarta, n):
    for j in range(n):
        tarta.saltaacaso()
        tarta.coloraacaso()
        tarta.fill(1)
        tarta.ccircle(random.randrange(100), random.randrange(360))
        tarta.fill(0)


def tricircle(tarta):
    tarta.tracer(1)
    tarta.width = 10
    tarta.fill(1)
    for i in range(3):
        tarta.forward(100)
        tarta.right(90)
        tarta.color = "green"
        tarta.circle(30)
        tarta.color = "black"
        tarta.left(90)
        tarta.back(80)
        tarta.left(120)
    tarta.color = "maroon"
    tarta.fill(0)


def sequenza1(tarta):
    tarta.coloraacaso()
    tarta.segmentocirc(50)   # semicerchio vuoto
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.fill(1)
    tarta.segmentocirc(50)   # semicerchio con
    tarta.coloraacaso()      # bordo e interno
    tarta.fill(0)            # colorati
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.up()
    tarta.fill(1)
    tarta.segmentocirc(50)   # semicerchio colorato
    tarta.coloraacaso()      # senza bordo
    tarta.fill(0)
    tarta.down()
    tarta.spostati(100, 0)
    tarta.left(90)
    tarta.coloraacaso()
    tarta.petalo(70)         # petalo vuoto
    tarta.right(90)
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.left(90)
    tarta.fill(1)
    tarta.petalo(70)          # petalo con
    tarta.coloraacaso()       # bordo e interno
    tarta.fill(0)             # colorati
    tarta.right(90)
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.left(90)
    tarta.up()
    tarta.fill(1)
    tarta.petalo(70)         # petalo colorato
    tarta.coloraacaso()       # senza bordo
    tarta.fill(0)
    tarta.down()
    tarta.right(90)


def sequenza2(tarta):
    tarta.coloraacaso()
    tarta.settorecirc(50)          # settorecirc vuoto
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.fill(1)
    tarta.settorecirc(50)          # settorecirc con
    tarta.coloraacaso()            # bordo e interno
    tarta.fill(0)                  # colorati
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.up()
    tarta.fill(1)
    tarta.settorecirc(50)          # settorecirc colorato
    tarta.coloraacaso()            # senza bordo
    tarta.fill(0)
    tarta.down()
    tarta.spostati(100, 0)
    tarta.left(90)
    tarta.coloraacaso()
    tarta.radio(50)                # radio vuoto
    tarta.right(90)
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.left(90)
    tarta.fill(1)
    tarta.radio(50)                # radio con
    tarta.coloraacaso()            # bordo e interno
    tarta.fill(0)                  # colorati
    tarta.right(90)
    tarta.spostati(100, 0)
    tarta.coloraacaso()
    tarta.left(90)
    tarta.up()
    tarta.fill(1)
    tarta.radio(50)                # radio colorato
    tarta.coloraacaso()            # senza bordo
    tarta.fill(0)
    tarta.down()
    tarta.right(90)


def main():
    piano = tg.TurtlePlane()
    gina = MiaTurtle()
    macchie(gina, 50)
    piano = tg.TurtlePlane()
    gina = MiaTurtle()
    cmacchie(gina, 50)
    piano = tg.TurtlePlane()
    gina = MiaTurtle()
    tricircle(gina)
    piano = tg.TurtlePlane()
    gina = MiaTurtle(x=-250, y=98, width=3)
    sequenza1(gina)
    piano = tg.TurtlePlane()
    gina = MiaTurtle(x=-250, y=-98)
    sequenza2(gina)
    piano.mainloop()


if __name__ == "__main__":
    main()
