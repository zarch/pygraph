#!/usr/bin/env python
#--------------------------python-pyturtle---------------ricorsione.py--#
#                                                                       #
#                         Figure ricorsive                              #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2003--#

import pygraph.pyturtle as tg
import random


def albero(lung):
    if lung < 2:
        return
    tina.forward(lung)
    tina.left(45)
    albero(lung / 2)
    tina.right(90)
    albero(lung / 2)
    tina.left(45)
    tina.back(lung)

# def alberobin(lung, angolo):
# if lung<2: return
# tina.forward(lung)
# tina.left(angolo)
##  alberobin(lung/2, angolo)
# tina.right(2*angolo)
##  alberobin(lung/2, angolo)
# tina.left(angolo)
# tina.back(lung)

# def alberobin(lung, angolo):
# if lung<2: return
# tina.width(lung/5)
# tina.forward(lung)
# tina.left(angolo)
##  alberobin(lung/2, angolo)
# tina.right(2*angolo)
##  alberobin(lung/2, angolo)
# tina.left(angolo)
# tina.back(lung)


def alberobin(lung, angolo, larg, decsx, decdx):
    if lung < 2:
        return
    tina.width = lung * larg
    tina.forward(lung)
    tina.left(angolo)
    alberobin(lung * decsx, angolo, larg, decsx, decdx)
    tina.right(2 * angolo)
    alberobin(lung * decdx, angolo, larg, decsx, decdx)
    tina.left(angolo)
    tina.back(lung)


def alberobincas(lung, angolo, larg, decsx, decdx, caos):
    if lung < 2:
        return
    var = int(lung * caos) + 1
    l = lung - var + random.randrange(2 * var)
    tina.width = lung * larg
    tina.forward(lung)
    tina.left(angolo)
    alberobincas(l * decsx, angolo, larg, decsx, decdx, caos)
    tina.right(2 * angolo)
    alberobincas(l * decdx, angolo, larg, decsx, decdx, caos)
    tina.left(angolo)
    tina.back(lung)


def alberoter(lung, angolo):
    if lung < 2:
        return
    tina.width = lung / 5
    tina.forward(lung)
    tina.left(angolo)
    alberoter(lung / 2, angolo)
    tina.right(angolo)
    alberoter(lung / 2, angolo)
    tina.right(angolo)
    alberoter(lung / 2, angolo)
    tina.left(angolo)
    tina.back(lung)


def koch(lung, liv):
    if liv == 0:
        tina.forward(lung)
        return
    koch(lung / 3.0, liv - 1)
    tina.left(60)
    koch(lung / 3.0, liv - 1)
    tina.right(120)
    koch(lung / 3.0, liv - 1)
    tina.left(60)
    koch(lung / 3.0, liv - 1)


def fiocco(lato, liv):
    for i in range(3):
        koch(lato, liv)
        tina.right(120)


def fioccol(lato, liv):
    for i in range(3):
        koch(lato, liv)
        tina.left(120)


def kochcas(lung, liv):
    if liv == 0:
        tina.forward(lung)
        return
    verso = random.randrange(-1, 2, 2)
    kochcas(lung / 3.0, liv - 1)
    tina.left(60 * verso)
    koch(lung / 3.0, liv - 1)
    tina.right(120 * verso)
    kochcas(lung / 3.0, liv - 1)
    tina.left(60 * verso)
    koch(lung / 3.0, liv - 1)


def fioccocas(lato, liv):
    for i in range(3):
        kochcas(lato, liv)
        tina.right(120)


piano = tg.TurtlePlane()
tina = tg.Turtle(y=-100, d=90)
# albero(100)
#alberobin(100, 60, 0.1, 0.8, 0.6)
alberobincas(100, 60, 0.1, 0.7, 0.5, 0.3)
piano = tg.TurtlePlane()
tina = tg.Turtle(d=90)
alberoter(100, 60)
piano = tg.TurtlePlane()
tina = tg.Turtle()
koch(200, 4)
piano = tg.TurtlePlane()
tina = tg.Turtle()
fioccocas(200, 4)
piano.mainloop()
