#!/usr/bin/env python
#--------------------------python-pyturtle-------------------albero.py--#
#                                                                       #
#                               Albero                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2003--#

import pygraph.pyturtle as tg
import random



def albero(lung):
    if lung < 2:
        return
    tina.forward(lung)
    tina.left(45)
    albero(lung / 2)
    tina.right(90)
    albero(lung / 2)
    tina.left(45)
    tina.back(lung)

# def alberobin(lung, angolo):
# if lung<2: return
# tina.forward(lung)
# tina.left(angolo)
##  alberobin(lung/2, angolo)
# tina.right(2*angolo)
##  alberobin(lung/2, angolo)
# tina.left(angolo)
# tina.back(lung)

# def alberobin(lung, angolo):
# if lung<2: return
# tina.width(lung/5)
# tina.forward(lung)
# tina.left(angolo)
##  alberobin(lung/2, angolo)
# tina.right(2*angolo)
##  alberobin(lung/2, angolo)
# tina.left(angolo)
# tina.back(lung)

# def alberobin(lung, angolo, larg, decsx, decdx):
# if lung<2: return
##  tina.width = lung*larg
# tina.forward(lung)
# tina.left(angolo)
##  alberobin(lung*decsx, angolo, larg, decsx, decdx)
# tina.right(2*angolo)
##  alberobin(lung*decdx, angolo, larg, decsx, decdx)
# tina.left(angolo)
# tina.back(lung)


def alberobincas(lung, angolo, larg, decsx, decdx, caos):
    if lung < 2:
        return
    var = int(lung * caos) + 1
    l = lung - var + random.randrange(2 * var)
    tina.width = lung * larg
    tina.forward(lung)
    tina.left(angolo)
    alberobincas(l * decsx, angolo, larg, decsx, decdx, caos)
    tina.right(2 * angolo)
    alberobincas(l * decdx, angolo, larg, decsx, decdx, caos)
    tina.left(angolo)
    tina.back(lung)


def alberoter(lung, angolo):
    if lung < 2:
        return
    tina.width = lung / 5
    tina.forward(lung)
    tina.left(angolo)
    alberoter(lung / 2, angolo)
    tina.right(angolo)
    alberoter(lung / 2, angolo)
    tina.right(angolo)
    alberoter(lung / 2, angolo)
    tina.left(angolo)
    tina.back(lung)


piano = tg.TurtlePlane()
tina = tg.Turtle(d=90, y=-100)
tina.tracer(0)
# albero(100)
#alberobin(100, 60, 0.1, 0.8, 0.6)
alberobincas(90, 60, 0.1, 0.7, 0.6, 0.3)
#alberoter(100, 60)
piano.mainloop()
