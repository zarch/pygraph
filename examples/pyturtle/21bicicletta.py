#!/usr/bin/env python
#--------------------------python-pyturtle-------------21bicicletta.py--#
#                                                                       #
#                            Bicicletta                                 #
#                                                                       #
#--Daniele Zambelli-------------GPL3-------------------------------2012-#

import pygraph.pyturtle as tg


def ruota(raggio, raggi):
    """Disegna una ruota di bicicletta."""
    tina.ccircle(raggio)
    for cont in range(raggi):
        tina.forward(raggio)
        tina.back(raggio)
        tina.left(360. / raggi)


def sella(dim):
    """Disegna la sella."""
    tina.forward(dim * 0.3)
    tina.left(50)
    tina.forward(dim * 0.3)
    tina.back(dim * 0.6)
    tina.forward(dim * 0.3)
    tina.right(50)
    tina.back(dim * 0.3)


def manubrio(dim):
    """Disegna il manubrio."""
    tina.forward(dim * 0.3)
    tina.left(90)
    tina.circle(dim * 0.3, -150)
    tina.circle(dim * 0.3, 150)
    tina.right(90)
    tina.back(dim * 0.3)


def bici(dim):
    """Bicicletta, punto di ancoraggio il centro della corona."""
    pi = tina.position
    di = tina.direction
    tina.back(dim * 1.4)
    ruota(dim * 0.85, 8)       # ruota posteriore
    tina.left(60)
    tina.forward(dim * 1.4)   # forcella posteriore
    tina.left(60)
    sella(dim)
    tina.left(180)
    tina.forward(dim * 1.4)
    tina.ccircle(dim * 0.25)  # corona
    tina.left(110)
    tina.forward(dim * 1.5)
    tina.left(55)
    tina.forward(dim * 0.1)
    tina.right(100)
    tina.back(dim * 1.7)      # canna
    tina.forward(dim * 1.7)  # "
    tina.left(100)
    manubrio(dim)
    tina.back(dim * 1.3)      # forcella anteriore
    ruota(dim * 0.85, 8)       # ruota anteriore
    tina.up()               # riporta tina nella posizione iniziale
    tina.position = pi
    tina.direction = di
    tina.down()


piano = tg.TurtlePlane()
tina = tg.Turtle(width=3, color='blue')
bici(90)

piano.mainloop()
