#!/usr/bin/env python
#--------------------------python-pyturtle--------------04parametri.py--#
#                                                                       #
#                             Poligoni                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

import pygraph.pyturtle as tg


def quadratino():
    for i in range(4):
        tina.forward(23)
        tina.left(90)


def quadratone():
    for i in range(4):
        tina.forward(107)
        tina.left(90)


def quadrato(lato):
    for i in range(4):
        tina.forward(lato)
        tina.left(90)


def triangolo(lato):
    for i in range(3):
        tina.forward(lato)
        tina.left(120)


def esagono(lato):
    for i in range(6):
        tina.forward(lato)
        tina.left(60)


def pentagono(lato):
    for i in range(5):
        tina.forward(lato)
        tina.left(72)


def poligono(n, lato):
    for i in range(n):
        tina.forward(lato)
        tina.left(360. / n)


def poligoni(lato, n):
    for i in range(3, n):
        poligono(i, lato)


piano = tg.TurtlePlane()
tina = tg.Turtle()
poligoni(30, 20)

piano.mainloop()
