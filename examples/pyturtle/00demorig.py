#!/usr/bin/env python
#-------------------------------python------------------------demo.py--#
#                                                                       #
#                       Demo di turtlegraphics                          #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2002--#

import pygraph.pyturtle as tg
piano = tg.TurtlePlane()
tina = tg.Turtle()


def demo():
    tina.tracer(1)
    tina.up()
    tina.back(100)
    tina.down()
    # draw 3 squares; the last filled
    tina.width = 3
    for i in range(3):
        if i == 2:
            tina.fill(1)
        for j in range(4):
            tina.forward(20)
            tina.left(90)
        if i == 2:
            tina.color = "maroon"
            tina.fill(0)
        tina.up()
        tina.forward(30)
        tina.down()
    tina.up()
    tina.forward(10)
    tina.down()
    tina.color = "black"
    tina.circle(10)
    tina.up()
#  tina.fill(1)
    tina.forward(30)
    tina.fill(1)
    tina.down()
    tina.circle(10)
    tina.color = "maroon"
    tina.fill(0)
    tina.width = 1
    tina.color = "black"
    # move out of the way
    tina.tracer(0)
    tina.up()
    tina.right(90)
    tina.forward(100)
    tina.right(90)
    tina.forward(100)
    tina.right(180)
    tina.down()
    # some text
    tina.up()
    tina.write("startstart", 1)
    tina.color = "green"
    tina.write("start", 1)
    tina.down()
    # staircase
    tina.color = "red"
    for i in range(5):
        tina.forward(20)
        tina.left(90)
        tina.forward(20)
        tina.right(90)
    # filled staircase
    tina.fill(1)
    for i in range(5):
        tina.forward(20)
        tina.left(90)
        tina.forward(20)
        tina.right(90)
    tina.fill(0)
    # more text
    tina.color = "green"
    tina.write("end")


demo()
piano.mainloop()
