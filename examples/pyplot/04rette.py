#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python------------------------rette.py--#
#                                                                       #
#                 Fasci di rette nel piano cartesiano                   #
#                                                                       #
#--Daniele Zambelli--------------GPL------------------------------2018--#

from __future__ import division
import pygraph.pyplot as pp


def retta(m, q):
    """Restituisce una funzione lineare in x con coeff angolare m e quota q."""
    def f(x):
        return m * x + q
    return f


def fascio1(k):
    """Restituisce una retta del fascio."""
    def f(x):
        return (k - 2) * x + (k + 4)
    return f


# Disegna rette parallele
piano = pp.PlotPlane('rette parallele')
penna = pp.Plot(width=3)
penna.color = 'blue'
for q in range(-5, 2):
    penna.xy(retta(0.5, q))

# Disegna rette con uguale quota e diverse pendenze
piano = pp.PlotPlane('rette con uguale quota')
penna = pp.Plot(width=3)
penna.color = 'green'
for m in [1 / 3, 1 / 2, 1, 2, 3, -3, -2, -1, -1 / 2, -1 / 3]:
    penna.xy(retta(m, 2))

# Disegna rette di un fascio
piano = pp.PlotPlane('rette di un fascio')
penna = pp.Plot(width=3)
penna.color = 'red'
for k in range(-5, 5):
    penna.xy(fascio1(k))

piano.mainloop()
