#!/usr/bin/env python
#-------------------------------python----------------rettaper2pt02.py--#
#                                                                       #
#        Calcola l'equazione e disegna una retta per 2 punti            #
#                                                                       #
#--Daniele Zambelli----------------------------------------------2018---#

import pygraph.pyplot as pp

# def f_retta(m, q):
##  """Restituisce la funzione di una retta dati m e q."""
# def f(x):
# return m*x+q
# return f


def f_retta(m, q):
    """Restituisce la funzione di una retta dati m e q."""
    return lambda x: m * x + q


def rettaxduepti(penna, x0, y0, x1, y1):
    penna.width = 3
    penna.drawpoint((x0, y0))
    penna.drawpoint((x1, y1))
    penna.width = 1
    if x0 == x1:
        penna.yx(f_retta(0, x0))
    else:
        m = float(y0 - y1) / (x0 - x1)
        q = y0 - m * x0
        penna.xy(f_retta(m, q))


def demo():
    piano = pp.PlotPlane("retta per due punti", sx=30, sy=30)
    penna = pp.Plot(width=3)
    rettaxduepti(penna, -2, 1, -2, 5.)
    rettaxduepti(penna, -5, -3, 2, -3)
    rettaxduepti(penna, -6, -4, 6, 4)
    piano.mainloop()


demo()
