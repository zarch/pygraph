#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------1310Traslazione2.py--#
#                                                                       #
#                             Traslazione                               #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Traslazioni

Problema
- Disegna un poligono.
- Disegna una retta che passi per un suo vertice.
- Disegna il poligono traslato rispetto al primo.

Soluzione
- Disegnare il fascio di rette parallele a quella data, passanti
  per i vertici del poligono.
- Utilizzare i punti fissati ad un oggetto, PointOn.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Poligono0
###
a0 = ig.Point(-7, 5, color="#00f050", width=6, name="A")
b0 = ig.Point(-2, -2, color="#00f050", width=6, name="B")
c0 = ig.Point(-3, 6, color="#00f050", width=6, name="C")
d0 = ig.Point(-4, 2, color="#00f050", width=6, name="C")
ig.Polygon((a0, b0, c0, d0), color="#a87234")

# Retta
a1 = ig.Point(1, 8, color="#00f050", width=6, name="A'")
ra = ig.Line(a0, a1, width=1)

# Rette parallele a r0
rb = ig.Parallel(ra, b0, width=1)
rc = ig.Parallel(ra, c0, width=1)
rd = ig.Parallel(ra, d0, width=1)

# Vertici del poligono traslato
b1 = ig.PointOn(rb, 1, name="B'")
c1 = ig.PointOn(rc, 1, name="C'")
d1 = ig.PointOn(rd, 1, name="D'")

# Poligono traslato
ig.Polygon((a1, b1, c1, d1), color="#72a834")

ig.Text(-5, -6, """Traslazione di un poligono.""")

piano.mainloop()
