#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#--------------------------------pykig----------------1400Concoide1.py--#
#                                                                       #
#                        Concoide di Nicomede                           #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
La concoide di Nicomede � una curva che permette di risolvere il problema 
della trisezione dell'angolo.
Per disegnare una concoide si parte da un punto P e una retta r. Per il 
punto P si tracciano delle semirette che intersecano r e su queste si 
segna un punto alla distanza fissa a dalla retta. l'insieme di questi 
punti forma la concoide.

Problema
Disegnare la concoide di Nicomede.

Soluzione
- Crea il punto P.
- Crea la retta r.
- Crea un segmento AB che sia la distanza a.
- Crea un certo numero di semirette per il punto P che intersecano la retta.
- Su queste semirette disegna i punti della concoide.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane("Concide di Nicomede", w=800, h=400)

# Numero di rette dell'inviluppo,
N = 20

# Per visualizzare le linee di costruzione cambia il valore
# della seguente variabile in True
linee_di_costruzione = False

# Retta e Punto
p0 = ig.Point(-9, 9, color="#00f050", width=10)
p1 = ig.Point(-6, 9, color="#00f050", width=10)
segmento = ig.Segment(p0, p1, color="#a87234", name="s")
p0 = ig.Point(0, 1, color="#00f050", width=10)
p1 = ig.Point(7, 1, color="#00f050", width=10)
retta = ig.Line(p0, p1, color="#a87234", name="r")
p = ig.Point(0, -1, color="#00f050", width=10, name="P")


def punto(p0, p1, segmento):
    """Funzione che restituisce un punto della concoide dati:
       un punto (p0), un punto sulla retta (p1) e un segmento."""
    l = ig.Ray(p0, p1, visible=linee_di_costruzione, width=1)
    c = ig.Circle(p1, segmento, visible=linee_di_costruzione, width=1)
    p = ig.Intersection(l, c, 1, color="#72a834", width=5)
    ig.Segment(p1, p, width=1)
    return p


for i in range(-N, N + 1):
    punto(p, ig.PointOn(retta, float(i) / N), segmento)

pp = punto(p, ig.ConstrainedPoint(retta, 0.5, width=7), segmento)
pp.color = "yellow"

ig.Text(-5, -6, """Concoide di Nicomede (vissuto circa 200 a.c.).

Modifica la lunghezza del segmento s
e la posizione di P.""")

piano.mainloop()
