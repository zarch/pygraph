#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1270Criteri2.py--#
#                                                                       #
#            Secondo criterio di congruenza di triangoli                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Applicare il secondo criterio di congruenza di triangoli.

Problema
- Disegna un triangolo.
- Disegna una semiretta.
- Disegna, sulla semiretta, il triangolo con un angolo e i due lati adiacenti
  congruenti al primo.

Soluzione
- da inventare.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Primo triangolo
###
a0 = ig.Point(-7, 2, color="#00f050", width=6, name="A")
b0 = ig.Point(-4, -2, color="#00f050", width=6, name="B")
c0 = ig.Point(-5, 6, color="#00f050", width=6, name="C")
t0 = ig.Polygon((a0, b0, c0), color="#a87234")

# Elementi congruenti
ab0 = ig.Segment(a0, b0)
bac0 = ig.Angle(b0, a0, c0)
abc0 = ig.Angle(a0, b0, c0)

# Semiretta
a1 = ig.Point(2, 2, color="#00f050", width=6, name="A'")
d = ig.Point(8, -6, color="#00f050", width=6, name="D")
r1 = ig.Ray(a1, d, width=1)

# Triangolo congruente
circ1 = ig.Circle(a1, ab0, width=1)
b1 = ig.Intersection(r1, circ1, 1, name="B'")
bac1 = ig.Angle(b1, a1, bac0)
ac1 = bac1.side1(width=1)
abc1 = ig.Angle(a1, b1, abc0)
bc1 = abc1.side1(width=1)
c1 = ig.Intersection(ac1, bc1, name="C'")
t1 = ig.Polygon((a1, b1, c1), color="#72a834")

ig.Text(-5, -6, """Secondo criterio di congruenza di triangoli.""")

piano.mainloop()
