#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1350Omotetia.py--#
#                                                                       #
#                               Omotetia                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Omotetia

Problema
- Disegna un poligono.
- Il centro di omotetia
- Disegna i poligoni di omotetia 2 e -1.

Soluzione
- Traccia le rette passanti per il centro e per i vertici
  del poligono
- Usa i punti vincolati.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Poligono0
###
a0 = ig.Point(-5, 1, color="#00f050", width=6, name="A")
b0 = ig.Point(-2, 0, color="#00f050", width=6, name="B")
c0 = ig.Point(-1, 2, color="#00f050", width=6, name="C")
d0 = ig.Point(-2, 1, color="#00f050", width=6, name="D")
vertici0 = (a0, b0, c0, d0)
ig.Polygon(vertici0, color="#a87234")

# Centro di omotetia
o = ig.Point(0, -2, color="#00f050", width=6)

# Punti omotetici


def omopunto(centro, punto, costante):
    retta = ig.Line(centro, punto, width=1)
    return ig.PointOn(retta, costante)

# Di seguito costruisco la lista con i vertici del poligono trasformato
# in due modi diversi.


# Soluzione con un ciclo. Inizializzo una lista vuota, poi avvio un
# ciclo che scorre la lista "vertici0" e per ogni elemento ne costruisce
# un altro che aggiunge alla lista "vertici1"
vertici1 = []
for vertice in vertici0:
    vertici1.append(omopunto(o, vertice, 2))

# Soluzione che usa un costrutto particolare del linguaggio Python
# la "list compreension" o costruzione di liste. L'istruzione seguente
# prende una lista, "vertici0" la scorre ponendo in "vertice", uno alla
# volta i suoi elementi e ne costruisce un'altra con gli elemnti prodotti
# dalla funzione "omopunto"
vertici2 = [omopunto(o, vertice, -1) for vertice in vertici0]

# Questa seconda soluzione � quella preferita dai programmatori Python;
# una volta fatto l'occhio, risulta molto comatta ed espressiva.

# Poligoni omotetici
ig.Polygon(vertici1, color="#72a834")
ig.Polygon(vertici2, color="#72a834")

ig.Text(-5, -6, """Omotetia centrale di un poligono.""")

piano.mainloop()
