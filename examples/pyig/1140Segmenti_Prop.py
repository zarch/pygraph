#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------1140Segmenti_Prop.py--#
#                                                                       #
#                         ProprietÓ dei Segmenti                        #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
I segmenti hanno le seguenti metodi:
- type: restituisce il tipo dell'oggetto,
- point0: restituisce il primo etremo,
- point1: restituisce il secondo estremo,
- midpoint: crea un oggetto punto centrale,
- length: crea come dato la lunghezza,
- equation: crea come dato l'equazione della retta,
- elope: crea come dato la pendenza.

Si possono creare i seguenti oggetti legati ai segmenti, rette e semirette:
- Midpoint(segmento): l'oggetto puntoi medio di segmento,
- Length(segmento): il dato lunghezza di segmento,
- Equation(segmento): il dato equazione della retta,
- Slope(segmento): il dato pendenza.

Problema
Disegnare:
- un segmento,
- un testo variabile per ogni proprietÓ dell'oggetto,

Soluzione
Analoga a quella del problema precedente.
"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

# Primo metodo

s = ig.Segment(ig.Point(-2, 2), ig.Point(-1, -1))

ig.VarText(-5, 8, "Tipo: {0}", s.type())

o = s.point0()
c = o.coords()
ig.VarLabel(o, 0, 10, "Primo estremo: {0}", c)

o = s.point1()
c = o.coords()
ig.VarLabel(o, 0, -10, "Secondo estremo: {0}", c)

o = ig.MidPoint(s)
c = o.coords()
ig.VarLabel(o, 0, -5, "Punto medio: {0}", c)

ig.VarText(-5, 7, "Lunghezza: {0}", s.length())

ig.VarText(-5, 6, "Equazione della retta: {0}", s.equation())

ig.VarText(-5, 5, "Pendenza: {0}", s.slope())

# Secondo metodo

s = ig.Segment(ig.Point(1, 3), ig.Point(2, 0))

ig.VarText(5, 9, "Tipo: {0}", s.type())

o = s.point0()
c = o.coords()
ig.VarLabel(o, 0, 10, "Primo estremo: {0}", c)

o = s.point1()
c = o.coords()
ig.VarLabel(o, 0, -10, "Secondo estremo: {0}", c)

o = s.midpoint()
c = o.coords()
ig.VarLabel(o, 0, 5, "Punto medio: {0}", c)

ig.VarText(5, 8, "Lunghezza: {0}", s.length())

ig.VarText(5, 7, "Equazione della retta: {0}", s.equation())

ig.VarText(5, 6, "Pendenza: {0}", s.slope())

###
# Messaggio finale
###

ig.Text(-2, -5, """Intorno al segmento vengono scritte delle etichette
che contengono le proprietÓ del segmento.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
