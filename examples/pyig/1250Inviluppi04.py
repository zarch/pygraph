#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------1250Inviluppi04.py--#
#                                                                       #
#                               inviluppi                               #
#                                                                       #
#--Bruno Stecca-Daniele Zambelli--GPL----------------------------2006---#

"""
Problema
Dividi due segmenti consecutivi ad intervalli regolari
e congiungi li con il metodo degli intervalli isoperimetrici
muovi poi gli estremi liberi

Soluzione
1. Costruire due segmenti consecutivi
2. Dividere i segmenti in parti uguali memorizzando i punti in due liste
3. Disegnare i segmenti che congiungono ogni elemento della prima lista con
   un elemento della seconda in ordine inverso.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

NUM = 10

import random


def coloreacaso():
    return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                           random.randrange(256),
                                           random.randrange(256))


# i tre estremi, A � il vertice
A = ig.Point(-5, 0, width=8, color="#00ff00")
B = ig.Point(5, 5, width=8)
C = ig.Point(5, -5, width=8)

# i segmenti
AB = ig.Segment(A, B)
AC = ig.Segment(A, C)

# lista di punti sul segmento AB e sul segmento AC
puntiAB = []
puntiAC = []
for i in range(1, 1 + NUM):
    puntiAB.append(ig.PointOn(AB, float(i) / NUM))
    puntiAC.append(ig.PointOn(AC, float(i) / NUM))

# Inviluppo di segmenti tra il primo punto in puntiAB e l'ultimo di puntiAC
# il secondo e il penultimo, ...
l = len(puntiAC)
for i in range(l):
    ig.Segment(puntiAB[i], puntiAC[l - i - 1], color=coloreacaso())


piano.mainloop()
