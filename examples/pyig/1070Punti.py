#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#--------------------------------pykig--------------------1070Punti.py--#
#                                                                       #
#                    Punti realizzati in vario modo                     #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Uno degli elementi fondamentali della Geometria � costituito dai punti.
I punti hanno una loro posizione che pu� essere individuata da due numeri
che possono rappresentare le coordinate cartesiane.
Ci sono svariati modi per creare dei punti partendo dalle coordinate.
In geometria il punto si deve considerare come infinitamente piccolo o
come NON composto da parti. In pratica, per poterlo rappresentare, dobbiamo
utilizzare un disegno che non � un punto, ma che ci pu� dare l'idea del
punto: i VERI PUNTI esistono solo nella nostra immaginazione.
In Pyig � possibile disegnare punti diversificandoli per
colore e spessore.
Lo spessore � un numero intero.
Il colore � una stringa nel formato: "#RRGGBB" dove RR, GG, BB sono numeri 
esadecimali che rappresentano l'intensit� della componente rispettivamente: 
rossa, verde e blu.

Problema
Disegnare alcuni punti con coordinate intere, razionali, o con coordinate
collegate ad un oggetto Kig.
Disegnamoli sfruttando i diversi stili messi a disposizione da Kig.

Soluzione
Creare diversi oggetti Point fornendo loro come argomenti: numeri naturali,
numeri razionali, oggetti Kig di tipo Double. Modifichiamo l'aspetto dei
punti con i metodi setcolor(), setwidth().
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Punti che ricevono come argomenti oggetti Python (numeri)
###

a = ig.Point(-4, 1)                  # Un punto con coordinate intere
a.color = "#aa0000"               # gli assegno un colore
b = ig.Point(-3. / 2, 5. / 2)            # Altro punto
b.color = "#00aa00"               # gli assegno un colore

###
# Punti che ricevono come argomenti oggetti Kig
###

# Ceazione di variabili numeriche
x1 = 2
y1 = 4
x2 = 6
y2 = -3

# Creazione dei punti
c = ig.Point(x1, y1, color="#0000aa", width=10)
e = ig.Point(x2, y2, color="#aaaa00", width=20)
d = ig.Point(x1, y2, color="#aaaa00", width=10)
f = ig.Point(x2, y1, color="#0000aa", width=20)

###
# Messaggio finale
###

s = """Punti di diverso colore e dimensione
Muovi i vari punti."""
ig.Text(-7, -5.5, s)

###
# Attivazione della finestra interattiva
###

piano.mainloop()
