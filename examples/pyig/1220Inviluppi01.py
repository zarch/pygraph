#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------1220Inviluppi01.py--#
#                                                                       #
#                       inviluppi di segmenti                           #
#                                                                       #
#--Bruno Stecca-Daniele Zambelli--GPL----------------------------2006---#

"""
Problema
Disegnare un inviluppo di segmenti con un estremo che si allontana
dall'origine sull'asse x e l'alto estremo che si avvicina all'origine
sull'assey.

Soluzione
Se gli estremi servono solo per la costruzione di un segmento, possiamo
fare a meno di usare variabili, e crearli direttamente all'interno del
costruttore del segmento.
"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

piano.defwidth = 2

# le righe seguenti, fino al prossimo commento,
# vengono copiate dal file colori.py
# servono a generare colori casuali
import random


def coloreacaso():
    return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                           random.randrange(256),
                                           random.randrange(256))


# le righe seguenti tracciano una serie di segmenti
ig.Segment(ig.Point(0, 9),   ig.Point(0.5, 0), color=coloreacaso())
ig.Segment(ig.Point(0, 8.5), ig.Point(1, 0),   color=coloreacaso())
ig.Segment(ig.Point(0, 8),   ig.Point(1.5, 0), color=coloreacaso())
ig.Segment(ig.Point(0, 7.5), ig.Point(2, 0),   color=coloreacaso())
ig.Segment(ig.Point(0, 7),   ig.Point(2.5, 0), color=coloreacaso())
ig.Segment(ig.Point(0, 6.5), ig.Point(3, 0),   color=coloreacaso())

# ....
# prosegui inserendo nuovi segmenti, scalati come i precedenti

###
# Attivazione della finestra interattiva
###

piano.mainloop()
