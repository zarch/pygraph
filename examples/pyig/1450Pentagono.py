# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1450Pentagono.py--#
#                                                                       #
#                               Pentagono                               #
#                                                                       #
#--Francesco Zambelli--------------GPL---------------------------2010---#

"""
Disegno di un pentagono regolare dati due vertici consecutivi.
"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

# i due vertici:
a = ig.Point(2, -2, width=5, name='A')
b = ig.Point(7, -1, width=5, name='B')
piano.defwidth = 1

# retta per A e B
r = ig.Line(a, b)
s0 = ig.Segment(a, b)
# circonferenze di centro A e B
c0 = ig.Circle(a, b)
t = ig.Orthogonal(r, a)

piano.mainloop()
