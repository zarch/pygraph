#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------1150Poligoni_Prop.py--#
#                                                                       #
#                         ProprietÓ dei Poligoni                        #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
I poligoni hanno le seguenti proprietÓ:
- Type: il tipo dell'oggetto,
- NumOfSides: il numero di lati,
- Perimeter: la lunghezza del perimetro,
- Surface: l'area della superficie,

Problema
Disegnare:
- un poligono,
- dei testi variabili con le informazioni sulle proprietÓ dell'oggetto,

Soluzione
Analoga a quella dei problemi precedenti.
"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

# Primo metodo

p = ig.Polygon((ig.Point(-7, 2), ig.Point(-6, -1,),
                ig.Point(-5, 1), ig.Point(-4, -3)))

ig.Text(-7, 8, "Tipo: {0}".format(p.type()))

ig.VarText(-7, 7, "Perimetro: {0} suddiviso in {1} lati.",
          (p.perimeter(), p.numofsides()))

ig.VarText(-7, 6, "Area: {0}", p.surface())

# Secondo metodo

p = ig.Polygon((ig.Point(5, 2), ig.Point(6, 0,),
                ig.Point(7, 1), ig.Point(5, -3)))

ig.Text(7, 9, "Tipo: {0}".format(p.type()))

ig.VarText(7, 8, "Perimetro: {0} suddiviso in {1} lati.",
        (p.perimeter(), p.numofsides()))

ig.VarText(7, 7, "Area: {0}", p.surface())

###
# Messaggio finale
###

ig.Text(-2, -5, """Sopra al poligono vengono scritte delle etichette
che contengono le proprietÓ del poligono.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
