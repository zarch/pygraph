#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1330Simmetria.py--#
#                                                                       #
#                              Simmetria                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Simmetrie

Problema
- Disegna un poligono.
- Disegna una retta
- Disegna il poligono simmetrico rispetto alla retta.

Soluzione
- Traccia le retta perpendicolari alla retta passanti per i vertici
  del poligono
- Usa i punti vincolati.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Poligono0
###
a0 = ig.Point(-7, 5, color="#00f050", width=6, name="A")
b0 = ig.Point(-2, -2, color="#00f050", width=6, name="B")
c0 = ig.Point(-3, 6, color="#00f050", width=6, name="C")
d0 = ig.Point(-4, 2, color="#00f050", width=6, name="D")
ig.Polygon((a0, b0, c0, d0), color="#a87234")

# Asse di simmetria
l = ig.Line(ig.Point(-1, -7, color="#00f050", width=6),
            ig.Point(2, 8, color="#00f050", width=6))

# Rette perpendicolari all'asse di simmetria
ra = ig.Orthogonal(l, a0, width=1)
rb = ig.Orthogonal(l, b0, width=1)
rc = ig.Orthogonal(l, c0, width=1)
rd = ig.Orthogonal(l, d0, width=1)

# Vertici del poligono simmetrico
a1 = ig.PointOn(ra, -1, name="A'")
b1 = ig.PointOn(rb, -1, name="B'")
c1 = ig.PointOn(rc, -1, name="C'")
d1 = ig.PointOn(rd, -1, name="D'")

# Poligono simmetrico
ig.Polygon((a1, b1, c1, d1), color="#72a834")

ig.Text(-5, -6, """Simmetria assiale di un poligono.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
