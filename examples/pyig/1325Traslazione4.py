#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------pyig-------------1325Traslazione4.py--#
#                                                                       #
#                             Traslazione                               #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2015---#

"""
Traslazioni

Problema
- Disegna un segmento AB
- Disegna un poligono.
- Disegna il poligono traslato di AB.

Soluzione
- Disegnare il fascio di rette parallele a AB, passanti
  per i vertici del poligono.
- Riporta su queste rette il segmento AB.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Trasla
###


def traslapunto(punto, traslazione):
    return punto + traslazione


def traslapoligono(poligono, traslazione, **kargs):
    poli1 = [traslapunto(vertice, traslazione) for vertice in poligono]
    return ig.Polygon(poli1, **kargs)


###
# Vettore
###
p = ig.Point(-7, 8, color="#00f050", width=6, name="P")
q = ig.Point(-1, 6, color="#00f050", width=6, name="Q")
vettore = ig.Vector(p, q)

###
# Poligono0
###
pol = (ig.Point(-7, 5, color="#00f050", width=6, name="A"),
       ig.Point(-2, -2, color="#00f050", width=6, name="B"),
       ig.Point(-3, 6, color="#00f050", width=6, name="C"),
       ig.Point(-4, 2, color="#00f050", width=6, name="C"))
ig.Polygon(pol, color="#a87234", width=4)

traslapoligono(pol, vettore, color="#7934b3", width=4)

ig.Text(-5, -6, """Traslazione di un poligono.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
