#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1430CircGonio.py--#
#                                                                       #
#                       Funzioni goniometriche                          #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2008---#

"""
Seno, coseno tangente

Problema
- Disegna una circonferenza non modificabile.
- Disegna un angolo al centro con un lato fisso sull'asse x.
- Disegna i segmenti corrispondenti al sen, cos, tg.

Soluzione
- I punti base della circonferenza non devono essere visibili.
- Disegna dei punti sulla circonferenza come riferimento (PointOn).
- Disegna un punto mobile sulla circ. che serva per modificare l'angolo.
- Visualizza dei testi con i valori di sen cos e tg.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Circonferenza
###
raggio = 6.
o = ig.Point(0, 0, False)
x = ig.Point(raggio, 0, False)
c0 = ig.Circle(o, x)
for ii in range(24):
    ig.PointOn(c0, ii / 12., color="red")

###
# Angolo
###
p = ig.Point(raggio + 2, 0, width=6, name="P")
r0 = ig.Ray(o, x)
r1 = ig.Ray(o, p)
a0 = ig.Angle(x, o, p)
i = ig.Intersection(r1, c0, 1)

###
# Rette di riferimento
###
assex = ig.Line(o, x, False)
assey = ig.Line(o, ig.Point(0, raggio, False), False)
asset = ig.Line(x, ig.Point(raggio, raggio, False), width=1, color="black")
parx = ig.Parallel(assex, i, width=1, color="blue")
pary = ig.Parallel(assey, i, width=1, color="red")

###
# Intersezioni con i riferimenti
###
ix = ig.Intersection(assex, pary, color="red")
iy = ig.Intersection(assey, parx, color="blue")
it = ig.Intersection(asset, r1, color="green")

###
# Segmenti goniometrici
###
sen = ig.Segment(o, iy, width=3, color="blue")
cos = ig.Segment(o, ix, width=3, color="red")
tan = ig.Segment(x, it, width=3, color="yellow")

pos = -8
ig.Text(-9, pos, """Funzioni goniometriche.""")
s = ig.Calc(lambda x: x / raggio, sen.length())
c = ig.Calc(lambda x: x / raggio, cos.length())
t = ig.Calc(lambda x: x / raggio, tan.length())
ig.VarText(-9, pos - 1, """ ang. = {0}.""", a0.extent(), color="DarkGray")
ig.VarText(-9, pos - 2, """|sen| = {0}.""", s, color="blue")
ig.VarText(-9, pos - 3, """|cos| = {0}.""", c, color="red")
ig.VarText(-9, pos - 4, """|tan| = {0}.""", t, color="green")

piano.mainloop()
