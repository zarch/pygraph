#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------1160Baricentro.kpy--#
#                                                                       #
#                      Baricentro di un triangolo                       #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Le tre mediane di un triangolo si intersecano in *un* punto, questo punto 
� il centro di massa del triangolo: il "baricentro".

Problema
Disegnare:
- un triangolo,
- le tre mediane,
- il baricentro.

Soluzione
- disegnare i tre vertici,
- disegnare il triangolo,
- creare i punti medi dei tre lati,
- disegnare le tre mediane,
- disegnare il punto di intersezione di due mediane: il baricentro.
"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

# tre punti
a = ig.Point(-5, 1, width=6)
b = ig.Point(-3, -3, width=6)
c = ig.Point(4, 5, width=6)

# triangolo
l1 = ig.Segment(a, b)
l2 = ig.Segment(b, c)
l3 = ig.Segment(c, a)

# punti medi dei lati
# primo metodo:
p1 = ig.MidPoint(l1)
# altro metodo:
p2 = l2.midpoint()
p3 = l3.midpoint()

# mediane
# primo metodo
m1 = ig.Line(c, p1, width=1)
m2 = ig.Line(a, p2, width=1)
m3 = ig.Line(b, p3, width=1)

# baricentro
baricentro = ig.Intersection(m1, m2, width=10, color="#ff66aa")

###
# Messaggio finale
###

ig.Text(-2, -5, """Triangolo e Baricentro.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
