# -*- coding: utf_8 -*-
#---------------------------------pyig-----------1490VersieraAgnesi.py--#
#                                                                       #
#                          Versiera di Agnesi                           #
#                                                                       #
#--Daniele Zambelli----------------GPL---------------------------2017---#

"""
Versiera di Maria Gaetana Agnesi.

Data una circonferenza di centro C(0; a) e di raggio a e
una retta t parallela all'asse x di equazione y = 2a
tangente al cerchio nel punto A(0; 2a ) , e
un fascio di rette passanti per l'origine degli assi,
la versiera è il luogo dei punti M che hanno:
- come ascissa, l'ascissa del punto L di intersezione
  di una generica retta del fascio con la tangente t;
- come ordinata, l'ordinata del punto C di intersezione
  della stessa retta del fascio con la circonferenza.
"""

# lettura delle librerie
import pygraph.pyig as ig

# funzioni


def puntoagnesi(p_b, construction=True, **kargs):
    """Restituisce un punto della versiera di Agnesi."""
    r_b = ig.Line(p_o, p_b, visible=construction, width=1)
    p_a = ig.Intersection(r_b, r_a, visible=construction)
    r_ap = ig.Orthogonal(r_a, p_a, visible=construction, width=1)
    r_bp = ig.Orthogonal(r_y, p_b, visible=construction, width=1)
    return ig.Intersection(r_ap, r_bp, **kargs)


# programma principale
piano = ig.InteractivePlane("Versiera di Agnesi", w=800, h=210, oy=200, sx=50)
p_o = ig.Point(0, 0, visible=False)
p_r = ig.Point(0, 1, visible=False)
r_y = ig.Line(p_o, p_r, visible=False)
p_c = ig.ConstrainedPoint(r_y, 1, width=4)
c_0 = ig.Circle(p_c, p_o, width=1)
p_c = ig.Intersection(c_0, r_y, 1, visible=False)
r_a = ig.Orthogonal(r_y, p_c, width=1)
num = 20
for par in range(1, 2 * num):
    puntoagnesi(ig.ConstrainedPoint(c_0, par / num, visible=False),
                construction=False)
p_b = ig.ConstrainedPoint(c_0, .45, width=4)
p_p = puntoagnesi(p_b, color='red')

# attivazione della finestra grafica
piano.mainloop()
