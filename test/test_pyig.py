#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------python--------------------test_pyig.py--#
#                                                                       #
#               Tests for Python Interactive Geometry                   #
#                                                                       #
#--Daniele Zambelli-----------Licence-GPL------------------------2009---#

"""
pyig tests
"""

from __future__ import division, print_function
#from pyig import *
#from math import sqrt
#from libtest import end,  alltests
import pygraph.pyig as ig
from libtest import alltests, end
from random import randrange


def intlc(xa, ya, xb, yb, xc, yc, xd, yd, ip=None, **args):
    """Return an intersection from a line and a cirle."""
    return ig.Intersection(ig.Line(ig.Point(xa, ya, width=10, iplane=ip),
                                   ig.Point(xb, yb, width=10, iplane=ip),
                                   width=1, color='magenta'),
                           ig.Circle(ig.Point(xc, yc, width=10, iplane=ip),
                                     ig.Point(xd, yd, width=10, iplane=ip),
                                     width=1, color='magenta'),
                           1, **args)


def viewdata_view(x, y, obj, color, ip=None):
    ig.VarText(x, y, "nome dell'oggetto: {0}",
               obj.name, color=color, iplane=ip)
    ig.VarText(
        x, y - 1, "tipo dell'oggetto: {0}", obj.type(), color=color, iplane=ip)
    ig.VarText(x, y - 2, "ascissa: {0}", obj.xcoord(), color=color, iplane=ip)
    ig.VarText(x, y - 3, "ordinata: {0}", obj.ycoord(), color=color, iplane=ip)
    ig.VarText(x, y - 4, "posizione: {0}",
               obj.coords(), color=color, iplane=ip)


def viewdata_straight(x, y, obj, color, ip=None):
    viewdata_view(x, y, obj, color, ip=ip)
    ig.VarText(x, y - 5, 'fst point: {0}', obj.point0().coords(),
               color=color, iplane=ip)
    ig.VarText(x, y - 6, 'lst point: {0}', obj.point1().coords(),
               color=color, iplane=ip)
    ig.VarText(x, y - 7, 'equ.: {0}', obj.equation(), color=color, iplane=ip)
    ig.VarText(x, y - 8, 'slope: {0}', obj.slope(), color=color, iplane=ip)


def viewdata_segment(x, y, obj, color, ip=None):
    viewdata_straight(x, y, obj, color, ip=ip)
    ig.VarText(x, y - 9, 'length: {0}', obj.length(), color=color, iplane=ip)


def viewdata_vector(x, y, obj, color, ip=None):
    viewdata_segment(x, y, obj, color, ip=ip)
    ig.VarText(x, y - 10, 'components: {0}',
               obj.components(), color=color, iplane=ip)


def viewdata_figure(x, y, obj, color, ip=None):
    viewdata_view(x, y, obj, color, ip=ip)
    ig.VarText(x, y - 5, 'perimeter: {0}',
               obj.perimeter(), color=color, iplane=ip)
    ig.VarText(x, y - 6, 'surface: {0}', obj.surface(), color=color, iplane=ip)


def viewdata_circle(x, y, obj, color, ip=None):
    viewdata_figure(x, y, obj, color, ip=ip)
    ig.VarText(x, y - 7, 'center: {0}',
               obj.center().coords(), color=color, iplane=ip)
    ig.VarText(x, y - 8, 'radius: {0}', obj.radius(), color=color, iplane=ip)


def viewdata_polygonal(x, y, obj, color, ip=None):
    viewdata_figure(x, y, obj, color, ip=ip)
    ig.VarText(x, y - 7, 'num of sides: {0}',
               obj.numofsides(), color=color, iplane=ip)


def test_00():
    """00: Costruzione di due piani interattivi contenenti due punti."""
    ip0 = ig.InteractivePlane()
    ip1 = ig.InteractivePlane(name="Geometria interattiva 1",
                              w=800, h=500,
                              sx=20, sy=20,
                              ox=200, oy=400,
                              axes=True, grid=True,
                              axescolor='green', gridcolor='red')
    print(ig.version())
    pbase = intlc(0, 0, 0, 14, 5, 6, 5, 9, ip0, width=5, color='red', name='A')
    p1 = ip1.newPoint(-3, -2)
    p1.color = 'blue'
    p1.width = 18
    p1.name = 'P'
    ig.Text(ip0, 7, -3, "Muovi un punto base della retta in modo",
            color='blue violet', width=12)
    ig.Text(ip0, 7, -4, "che intersechi la circonferenza.",
            color='blue violet', width=12)
    ig.Text(ip0, 7, -5, "Fare la stessa manovra",
            color='blue', width=12)
    ig.Text(ip0, 7, -6, "anche nei prossimi test",
            color='blue', width=12)
    viewdata_view(-7, 10, pbase, 'red', ip0)
    viewdata_view(7, 10, pbase, 'red', ip1)
    viewdata_view(-7, -2, p1, 'blue', ip0)
    viewdata_view(20, 10, p1, 'blue', ip1)
#  ip0.mainloop()    Basta un unico mainloop()!!!?
    end(ip0)


def test_01():
    """01: Test for ig.Text, VarText, Label, VarLabel."""

    def setcw(obj, color, width):
        obj.color = color
        obj.width = width

    ip = ig.InteractivePlane(test_01.__doc__)
    point0 = intlc(0, 0, 0, 14, 5, 6, 5, 9,
                   width=5, color='red', name="A")
    titolo = ig.Text(-7, 13, "Prove di testo ('blue violet')")
    setcw(titolo, 'blue violet', 20)
    titolo.name = '(titolo)'
    vt0 = ip.newVarText(-4, 3, "Posizione del titolo: ({0}; {1})",
                        (titolo.xcoord(), titolo.ycoord()))
    setcw(vt0, 'blue violet', 12)
    la0 = ig.Label(point0, 0, 20, "Etichetta del punto A (color='navy')")
    setcw(la0, 'navy', 15)
    vl0 = ig.VarLabel(point0, -40, -30,
                      "Posizione del punto A (color='green'): ({0}; {1})",
                      (point0.xcoord(), point0.ycoord()))
    setcw(vl0, 'green', 12)
    ig.Text(-1, -6,
            """- Muovi il titolo e controlla che le coordinate siano giuste,
- Muovi i punti base della retta, controlla le etichette e le coordinate.""",
            color=(.8, .4, .2))
    ig.Text(-1, -8,
            """Il testo può contenere anche lettere accentate: àèéìòù
e i simboli €@""",
            color=(.8, .4, .2))
    end(ip)


def test_02():
    """02: Test for Line, Ray and Segment."""
    ip = ig.InteractivePlane(test_02.__doc__)
    ig.Text(-7, 13, """Retta, semiretta, segmento""",
            color='#408040', width=12)
    p0 = intlc(0, 0, 0, 14, 5, 6, 5, 9,
               width=5, color='red', name="A")
    p1 = ig.Point(-3, 8, name="B")
    r0 = ig.Line(p0, p1, width=3, color=(.8, .2, .4), name='r')
    viewdata_straight(-7, 9, r0, (.8, .2, .4))
    p2 = ig.Point(-7, -5, name="C")
    r1 = ig.Ray(p0, p2, width=4, color=(.4, .8, .2), name='sr')
    viewdata_straight(-7, -2, r1, (.4, .8, .2))
    p3 = ig.Point(2, -1, name="D")
    seg = ig.Segment(p0, p3, width=4, color=(.2, .4, .8), name='s')
    viewdata_segment(7, -2, seg, (.2, .4, .8))
    end(ip)


def test_03():
    """03: Test for MidPoints and MidPoint."""
    ip = ig.InteractivePlane(test_03.__doc__)
    ig.Text(-7, 13, """Punto medio tra due punti""",
            color='#408040', width=12)
    point0 = intlc(0, 0, 0, 14, 5, 6, 5, 9,
                   width=5, color='red', name="A")
    point1 = ig.Point(11, 2, name="B")
    m = ig.MidPoints(point0, point1, width=6, color='violet', name='M')
    point2 = ig.Point(-6, 1, name="C")
    seg = ig.Segment(point0, point2, width=4, color='pink')
    n = ig.MidPoint(seg, width=6, color='orange', name='N')
    viewdata_view(-7, -3, m, 'violet')
    viewdata_view(-7, -8, n, 'orange')
    end(ip)


def test_04():
    """04: Test for Angle, AngleRA and AngleSide Bisector."""
    ip = ig.InteractivePlane(test_04.__doc__)
    ig.Text(-7, 13, """Angoli""",
            color='#408040', width=12)
    # without sides
    point0 = intlc(0, 0, 0, 14, 5, 6, 9, 8,
                   width=5, color='red', name="V")
    a0 = ig.Point(2, 2, name="A")
    b0 = ig.Point(9, 3, name="B")
    ang0 = ig.Angle(a0, point0, b0, width=3, color=(.8, .2, .4), name='alpha')
    ig.VarText(-7, -10, "alpha = {0}", ang0.extent(), color=(.8, .2, .4))
    a1 = ig.Point(9, -3, name="A1")
    v1 = ig.Point(1, -4, name="V1")
    ang1 = ig.Angle(v1, a1, ang0, width=3, color=(.8, .2, .4), name='beta')
    ig.VarText(-7, -11, "beta = {0}", ang1.extent(), color=(.8, .2, .4))
    # with sides
    a2 = ig.Point(-6, 2, name="A2")
    b2 = ig.Point(-4, 11, name="B2")
    ang2 = ig.Angle(b2, point0, a2, sides=(0, 1), width=3, color='green',
                    name='gamma')
    ig.VarText(-7, -12, "gamma = {0}", ang2.extent(), color='green')
    a3 = ig.Point(-9, -3, name="A3")
    v3 = ig.Point(-1, -6, name="V3")
    ang3 = ig.Angle(v3, a3, ang2, sides=(0, 1), width=3,
                    color='green', name='delta')
    ig.VarText(-7, -13, "delta = {0}", ang3.extent(), color='green')
    ig.Bisector(ang2, width=5, color='red')
    ang3.bisector(width=5, color='violet')
    ig.VarText(7, -10, "v3 = {0}", ang3.vertex().coords(), color='green')
    ig.VarText(7, -11, "a3 = {0}", ang3.point0().coords(), color='green')
    ig.VarText(7, -12, "b3 = {0}", ang3.point1().coords(), color='green')
    ig.VarText(7, -13, "m lato0 = {0}", ang3.side0().slope(), color='green')
    ig.VarText(7, -14, "m lato1 = {0}", ang3.side1().slope(), color='green')
    end(ip)


def test_05():
    """05: Test for Circle, CircleRC."""
# aggiungere i controlli per tutti i metodi di _CircleObject
    ip = ig.InteractivePlane(test_05.__doc__)
    ig.Text(-7, 13, """Circonferenze""",
            color='#408040', width=12)
    point0 = intlc(0, 0, 0, 14, 5, 6, 9, 8,
                   width=5, color='red', name="P")
    c0 = ig.Point(6, 2, name="C")
    circ0 = ig.Circle(c0, point0, color='chocolate')
#  c0c = circ0.center()
    a1 = ig.Point(-12, 8, width=6, name="A")
    b1 = ig.Point(-9, 7, width=6, name="B")
    r1 = ig.Segment(a1, b1)
    circ1 = ig.Circle(point0, r1, color='deep pink')
    ig.VarText(-7, -8, "C = {0},   r = {1}",
               (circ0.center().coords(), circ0.radius()),
               color='chocolate')
    ig.VarText(-7, -9, "circ = {0},   Surf = {1}",
               (circ0.perimeter(), circ0.surface()),
               color='chocolate')
    ig.VarText(-7, -11, "C = {0},   r = {1}",
               (circ1.center().coords(), circ1.radius()),
               color='deep pink')
    ig.VarText(-7, -12, "circ = {0},   surf = {1}",
               (circ1.perimeter(), circ1.surface()),
               color='deep pink')
    end(ip)


def test_07():
    """07: Test for Intersection."""
    ip = ig.InteractivePlane(test_07.__doc__)
    ig.Text(-7, 13, """Intersezioni""",
            color='#408040', width=12)
    a0 = ig.Point(-12, -5, name="A", width=7)
    b0 = ig.Point(-2, -3, name="B", width=7)
    line0 = ig.Line(a0, b0, color='dark turquoise')
    c0 = ig.Point(-12, -8, name="C", width=7)
    d0 = ig.Point(-7, -7, name="D", width=7)
    line1 = ig.Line(c0, d0, color='magenta')
    i0 = ig.Intersection(line0, line1, color='green yellow', name='I')
    ig.VarText(-7, -10, "I = {0}", i0.coords(), color='green yellow')
    e0 = ig.Point(6, 5, name="E", width=7)
    f0 = ig.Point(3, 3, name="F", width=7)
    circle0 = ig.Circle(e0, f0, color='medium blue')
    ig.Intersection(line1, circle0, -1, color='orange red', name='K')
    i2 = ig.Intersection(circle0, line1, +1, color='olive drab', name='K1')
    ig.VarText(-7, -11, "K1 = {0}", i2.coords(), color='olive drab')
    g0 = ig.Point(-3, 7, name="G", width=7)
    h0 = ig.Point(-3, 3, name="H", width=7)
    circle1 = ig.Circle(g0, h0, color='khaki')
    i3 = ig.Intersection(circle0, circle1, -1, color='orange red', name='N')
    ig.Intersection(circle0, circle1, +1, color='olive drab', name='N1')
    ig.VarText(-7, -12, "N = {0}", i3.coords(), color='orange red')
    end(ip)


def test_08():
    """08: Test for PointOn and ConstrainedPoint."""
    ip = ig.InteractivePlane(test_08.__doc__)
    ig.Text(-7, 13, """PointOn e ConstrainedPoint""",
            color='#408040', width=12)
    point0 = intlc(0, 0, 0, 14, 5, 6, 9, 8,
                   width=7, color='red', name="P")
    a0 = ig.Point(-12, -5, name="A", width=7)
    line0 = ig.Line(point0, a0, color='turquoise')
    b0 = ig.Point(-4, 8, name="B", width=7, )
    circle0 = ig.Circle(b0, point0, color='medium blue')
    q = ig.ConstrainedPoint(line0, 0.5, color='lime green', width=7, name='Q')
    r = ig.ConstrainedPoint(
        circle0, 0.5, color='lime green', width=7, name='R')
    line1 = ig.Line(point0, ig.Point(
        4, 3, name="C", width=7), color='turquoise')
    circle1 = ig.Circle(ig.Point(4, 1, name="D", width=7),
                        point0, color='medium blue')
    for cnt in range(7):
        if cnt == 6:
            s = ig.PointOn(line1, cnt / 2., width=5, color='red', name='S')
            t = ig.PointOn(circle1, 2 * cnt / 7.,
                           width=5, color='red', name='T')
        else:
            ig.PointOn(line1, cnt / 2., width=5, color='pink')
            ig.PointOn(circle1, 2 * cnt / 7., width=5, color='pink')
    seg0 = ig.Segment(point0, ig.Point(-12, 5, name="E",
                                       width=7), color='turquoise')
    s = ig.ConstrainedPoint(seg0, 0.5, color='lime green', width=7, name='R')
    ray0 = ig.Ray(point0, ig.Point(-8, 2, name="F",
                                   width=7), color='turquoise')
    t = ig.ConstrainedPoint(ray0, 0.5, color='lime green', width=7, name='R')
    ig.VarText(-7, -8, "parameter(Q) = {0}", q.parameter(), color='lime green')
    ig.VarText(-7, -9, "parameter(R) = {0}", r.parameter(), color='lime green')
    ig.VarText(-7, -10, "parameter(S) = {0}", s.parameter(), color='red')
    ig.VarText(-7, -11, "parameter(T) = {0}", t.parameter(), color='red')
    end(ip)


def test_09():
    """09: Test for Orthogonal and Parallel."""
    ip = ig.InteractivePlane(test_09.__doc__)
    ig.Text(-7, 13, """Orthogonal e Parallel 1""",
            color='#408040', width=12)
    point0 = intlc(0, 0, 0, 14, 5, 6, 9, 8,
                   width=7, color='red', name="P")
    a0 = ig.Point(-12, -5, name="A")
    b0 = ig.Point(-7, -4, name="B")
    line0 = ig.Line(a0, b0, color='dark turquoise')
    line1 = ig.Orthogonal(line0, point0, width=5, color='peru')
    line2 = ig.Parallel(line0, point0, width=5, color='plum')
    viewdata_straight(-9, -6, line0, 'dark turquoise')
    viewdata_straight(0, -6, line1, 'peru')
    viewdata_straight(9, -6, line2, 'plum')
    end(ip)


def test_09a():
    """09a: Test for Parallel and Orthogonal when not exist line."""
    ip = ig.InteractivePlane(test_09a.__doc__)
    "TODO when not exist line"
    ig.Text(-7, 13, """Orthogonal e Parallel 2""",
            color='#408040', width=12)
    point0 = intlc(0, 0, 0, 14, 5, 6, 9, 8,
                   width=7, color='red', name="P")
    a0 = ig.Point(-12, -5, name="A")
    b0 = ig.Point(-7, -4, name="B")
    line0 = ig.Line(a0, point0, color='dark turquoise')
    line1 = ig.Orthogonal(line0, b0, width=5, color='peru')
    line2 = ig.Parallel(line0, b0, width=5, color='plum')
    viewdata_straight(-9, -6, line0, 'dark turquoise')
    viewdata_straight(0, -6, line1, 'peru')
    viewdata_straight(9, -6, line2, 'plum')
    end(ip)


def test_10():
    """10: Test for Polygonal, CurviLine and Polygon."""
    ip = ig.InteractivePlane(test_10.__doc__)
    ig.Text(-7, 13, """Polygonal, CurviLine e Polygon""",
            color='#408040', width=12)
    point0 = intlc(0, 0, 0, 14, 5, 6, 9, 8,
                   width=7, color='red', name="P")
    pts0 = [ig.Point(randrange(-14, -1), randrange(1, 14), width=5)
            for c in range(10)] + [point0]
    pts1 = [ig.Point(randrange(-14, -1), randrange(-14, -1), width=5)
            for c in range(10)] + [point0]
    pts2 = [ig.Point(randrange(1, 14), randrange(-14, -1), width=5)
            for c in range(10)] + [point0]
    ig.Polygonal(pts0, name="Polygonal", width=5, color='dark turquoise')
    ig.Polygon(pts1, name="Polygon", width=5, color='peru')
    ig.CurviLine(pts2, name="Curviline", width=5, color='plum')
    end(ip)


def test_11():
    """11: Test for Calc."""
    ip = ig.InteractivePlane(test_11.__doc__)
    ig.Text(-7, 13, """Calc""",
            color='#408040', width=12)
    b0 = intlc(0, 0, 0, 14, 5, 6, 9, 8,
               width=7, color='red', name="B")
    a0 = ig.Point(-11, 2, name="A")
    c0 = ig.Point(-8, 6, name="C")
    line0 = ig.Segment(a0, b0, color='dark turquoise')
    line1 = ig.Segment(b0, c0, color='dark turquoise')
    line2 = ig.Segment(c0, a0, color='dark turquoise')
    quad = ig.Calc(lambda l2: l2 * l2, line0.length())
    sommaquad = ig.Calc(lambda l0, l1: l0 * l0 + l1 * l1,
                        (line1.length(), line2.length()))
    ig.VarText(-7, -10, "AB*AB = {0}", quad, color='olive drab')
    ig.VarText(-7, -11, "BC*BC+CA*CA = {0}", sommaquad,
               color='olive drab')
    end(ip)


def test_12():
    """12: Test for Point."""
    ip = ig.InteractivePlane(test_12.__doc__, sx=40, sy=40)
    ig.Text(-7, 13, """PointD""", color='#408040', width=12)
    a0 = ig.Point(0, -2, visible=False, name="0")
    b0 = ig.Point(1, -2, visible=False, name="1")
    line0 = ig.Line(a0, b0)
    cursor = ig.ConstrainedPoint(line0, 1, name='cursor', width=5)
    par = ig.Calc(lambda x: 0.5 * x * x, cursor.parameter())
    ig.Point(cursor.parameter(), par, width=5, color='dark turquoise')
    ig.VarText(-5, -4, "x = {0}", cursor.parameter(),
               color='olive drab')
    ig.VarText(-5, -5, "y = 0,5*x**2 = {0}", par, color='olive drab')
    end(ip)


def test_13():
    """13: newPoint:
    Point with object data as argouments."""
    ip = ig.InteractivePlane('13: newVarText')
    ip.newText(0, 13, 'Finestra con due punti e le loro coordinate',
               width=20, color='DarkOrchid3')
    p0 = ip.newPoint(-2, 7, width=8, name="P")
    xp = ip.newPoint(p0.xcoord(), 0,
                     width=8, color='dark turquoise', name='X_P')
    ip.newVarText(-5, -4, '{0}', p0.coords())
    ip.newVarText(+5, -4, '{0}', xp.coords(), color='dark turquoise')
    end(ip)


def test_14():
    """14: Test for object data."""
    ip = ig.InteractivePlane(test_14.__doc__)
    p = intlc(0, 0, 0, 14, 5, 6, 8, 9,
              width=7, color='red', name="P")
    a = ig.Point(-2, 5, name="A")
    segment = ig.Segment(a, p, width=6, color='navy', name='Seg')
    c = ig.Point(1, 9, name="C")
    circle = ig.Circle(c, p, width=2, color='lime green', name='Circ')
    triangle = ig.Polygon((a, c, p), width=2, color='magenta', name='Tri')
    viewdata_view(-8, -1, p, 'red')
    viewdata_straight(-7, -6, segment, 'navy')
    viewdata_circle(0, -1, circle, 'lime green')
    viewdata_polygonal(8, -1, triangle, 'magenta')
    end(ip)


def test_15():
    """15: Test for Vector and Vector algebra."""
    ip = ig.InteractivePlane(test_15.__doc__)
    ig.Text(-7, 14, """Vector""", color='#408040', width=12)
    ig.Text(-7, 13, """Object data""", color='#408040', width=12)
    p = intlc(0, 0, 0, 14, 5, 6, 8, 9,
              width=7, color='red', name="P")
    a = ig.Point(-2, 5, name="A", width=6)
    b = ig.Point(-4, -8, name="B", width=6)
    c = ig.Point(2, -3, name="C", width=6)
    d = ig.Point(-14, 3, name="D", width=6)
    vect0 = ig.Vector(a, p, width=6, color='navy', name='V0')
    vect1 = ig.Vector(b, c, width=6, color='red', name='V1')
    vect2 = ig.Vector(p, vect1, color='red', name='V2')
    vect3 = ig.Vector(d, vect0, color='navy', name='V3')
#  vect4 = VectorVsumV(vect0, vect1, color='green', name='V4')
    vect5 = vect0 + vect1
    vect6 = vect0 - vect1
    vect7 = vect0 * 3
    vect8 = -vect0 / 3
    vect8.color = 'red'
#  vect2.color = 'red'
#  vect3 = VectorBV(d, vect0, width=6, color='navy', name='V1')
    viewdata_vector(7, -2, vect0, (.2, .4, .8))
    end(ip)


def test_16():
    """16: Test for Point algebra."""
    ip = ig.InteractivePlane(test_16.__doc__)
    ig.Text(-7, 13, """Point algebra""", color='#408040', width=12)
    p = intlc(0, 0, 0, 14, 5, 6, 8, 9,
              width=7, color='red', name="P")
    a = ig.Point(-2, 5, name="A", width=6)
    b = ig.Point(-5, -2, name="B", width=6, color='pink')
    c = ig.Point(-9, 2, name="C", width=6, color='orange')
    d = ig.Point(1, -1, name="D", width=6, color='violet')
    r0 = b + ig.Vector(a, p, width=6, color='navy', name='v0')
    r1 = b - ig.Vector(a, p, width=6, color='navy', name='v0')
    r2 = c + p
    r3 = c - p
    r4 = d * p
    r5 = p * 1.2
    viewdata_view(7, -2, r0, (.2, .4, .8))
    end(ip)


def test_16a():
    """16a: Test for Point algebra."""
    ip = ig.InteractivePlane(test_16a.__doc__)
    ig.Text(-7, 13, """Point algebra""", color='#408040', width=12)
    p = intlc(0, 0, 0, 14, 5, 6, 8, 9,
              width=7, color='red', name="P")
    a = ig.Point(-2, 5, name="A", width=6)
    b = ig.Point(-5, -2, name="B", width=6)
    c = ig.Point(-9, 2, name="C", width=6)
    d = ig.Point(1, -1, name="D", width=6)
    r0 = b + ig.Vector(a, p, width=6, color='navy', name='v0')
    r0.name = 'B+v0'
    r1 = b - ig.Vector(a, p, width=6, color='navy', name='v0')
    r1.name = 'B-v0'
    r2 = c + p
    r2.name = 'C+P'
    r3 = c - p
    r3.name = 'C-P'
    r4 = d * p
    r4.name = 'D*P'
    r5 = p * 1.2
    r5.name = 'P*1.2'
    viewdata_view(7, -2, r0, (.2, .4, .8))
    end(ip)


def test_17():
    """17: Test for Segment Intersection."""
    ip = ig.InteractivePlane(test_17.__doc__)
    ig.Text(-7, 13, """Segment Intersection""", color='#408040', width=12)
    p = intlc(0, 0, 0, 14, 5, 6, 8, 9,
              width=7, color='red', name="P")
    a = ig.Point(-5, -10, name="A", width=6)
    b = ig.Point(-5, -2, name="B", width=6)
    c = ig.Point(-9, 2, name="C", width=6)
    d = ig.Point(1, -1, name="D", width=6)
    s0 = ig.Segment(a, p)
    s1 = ig.Segment(b, c)
    c0 = ig.Circle(d, b)
    i0 = ig.Intersection(s0, s1, color='red')
    i1 = ig.Intersection(s0, c0, 1, color='red')
    viewdata_view(7, -2, i0, (.2, .4, .8))
    end(ip)


def test_18():
    """18: Test for not visible objects name."""
    ip = ig.InteractivePlane(test_18.__doc__)
    ig.Text(-7, 13, """Not visible objects name""", color='#408040', width=12)
    p = intlc(0, 0, 0, 14, 5, 6, 8, 9,
              width=7, color='red', name="P")
    a = ig.Point(-5, -10, visible=False, name="A", width=6)
    b = ig.Point(-5, -2, name="B", width=6)
    c = ig.Point(-9, 2, name="C", width=6)
    d = ig.Point(1, -1, name="D", width=6)
    s0 = ig.Segment(a, p)
    s1 = ig.Segment(b, c)
    c0 = ig.Circle(d, b, visible=False, name="Circle")
    i0 = ig.Intersection(s0, s1, visible=False, color='red', name='I0')
    i1 = ig.Intersection(s0, c0, 1, color='red', name='I1')
    viewdata_view(7, -2, i0, (.2, .4, .8))
    end(ip)


def test_19():
    """19: Test for ig.Angle(Point, Point, Angle), sum."""
    ip = ig.InteractivePlane(test_19.__doc__)
    # i 2 angoli di partenza
    a = ig.Angle(ig.Point(-3, 7, width=6),
                 ig.Point(-7, 5, width=6),
                 ig.Point(-6, 8, width=6),
                 sides=(0, 1), color="#f09000", name='alfa')
    b = ig.Angle(ig.Point(9, 2, width=6),
                 ig.Point(2, 3, width=6),
                 ig.Point(6, 4, width=6),
                 sides=(0, 1), color="#0090f0", name='beta')
    # Punti di base dell'angolo somma di a b
    v = ig.Point(-11, -8, width=6)
    p0 = ig.Point(3, -10, width=6)
    # la somma degli angoli
    b1 = ig.Angle(p0, v, b, (0, 1), color="#0090f0")
    p1 = b1.point1()
    a1 = ig.Angle(p1, v, a, sides=True, color="#f09000")
    ig.Text(-4, -12, "Somma di due angoli")
    end(ip)


def test_20():
    """20: Test for ig.Angle(Point, Point, Angle), rotation."""
    ip = ig.InteractivePlane(test_20.__doc__)
    ###
    # Punto
    ###
    a0 = ig.Point(7, 5, color="#00f050", name="A")
    # Angolo di rotazione
    v = ig.Point(-1, 10, color="#00f050")
    p0 = ig.Point(1, 10, color="#00f050")
    p1 = ig.Point(1, 11, color="#00f050")
    a = ig.Angle(p0, v, p1)
    la0 = a.side0(width=1)
    la1 = a.side1(width=1)
    # Centro di rotazione
    o = ig.Point(-1, -6, color="#00f050", name="O")
    # Vertici del poligono ruotato

    def ruotapunto(punto, centro, angolo):
        aa = ig.Angle(punto, centro, angolo)
        la0 = aa.side0(width=1)
        la1 = aa.side1(width=1)
        ca = ig.Circle(centro, ig.Segment(
            centro, punto, visible=False), width=1)
        return ig.Intersection(la1, ca, 1)
    # Punto ruotato
    a1 = ruotapunto(a0, o, a)
    ig.Text(-5, -12, """Rotazione di un punto.""")
    end(ip)


def test_20a():
    """20a: Test for ig.Angle(Point, Point, Angle), rotation."""
    ip = ig.InteractivePlane(test_20a.__doc__)
    ###
    # Poligono0
    ###
    a0 = ig.Point(7, 5, color="#00f050", name="A")
    b0 = ig.Point(2, -2, color="#00f050", name="B")
    c0 = ig.Point(2, 5, color="#00f050", name="C")
    d0 = ig.Point(4, 2, color="#00f050", name="D")
    ig.Polygon((a0, b0, c0, d0), color="#a87234")
    # Angolo di rotazione
    v = ig.Point(-1, 10, color="#00f050")
    p0 = ig.Point(1, 10, color="#00f050")
    p1 = ig.Point(1, 11, color="#00f050")
    a = ig.Angle(p0, v, p1)
    la0 = a.side0(width=1)
    la1 = a.side1(width=1)
    # Centro di rotazione
    o = ig.Point(-1, -6, color="#00f050", name="O")
    # Vertici del poligono ruotato

    def ruotapunto(punto, centro, angolo):
        aa = ig.Angle(punto, centro, angolo)
        la = aa.side1(width=1)
        ca = ig.Circle(centro, ig.Segment(
            centro, punto, visible=False), width=1)
        return ig.Intersection(la, ca, 1)
    a1 = ruotapunto(a0, o, a)
    b1 = ruotapunto(b0, o, a)
    c1 = ruotapunto(c0, o, a)
    d1 = ruotapunto(d0, o, a)
    # Poligono ruotato
    ig.Polygon((a1, b1, c1, d1), color="#72a834")
    ig.Text(-5, -12, """Rotazione di un poligono.""")
    end(ip)


def test_21():
    """04: Test for AngleRA and AngleSide.
  non appare il lato1 quando lato1 di a0 è verticale"""
    ip = ig.InteractivePlane(test_21.__doc__)
    a0 = ig.Angle(ig.Point(5, 6), ig.Point(-5, 6), ig.Point(-5, 8), (0, 1))
    a1 = ig.Angle(ig.Point(5, -6), ig.Point(-8, -6), a0, (0, 1))
    a2 = ig.Angle(ig.Point(5, -8), ig.Point(-8, -10), a0, (0, 1))
    end(ip)


loc = locals()
if __name__ == '__main__':
    #  test_08()
    #  test_15()
    #  test_16()
    #  test_21()
    alltests(locals(), 0)
