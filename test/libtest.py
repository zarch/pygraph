#!/usr/bin/env python
#-------------------------------python----------------------libtest.py--#
#                                                                       #
#                         Function for tests                            #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2009---#

from __future__ import print_function


def end(p):
    print('fine')
    p.mainloop()


def alltests(locdict, first=0):
    tests = [n for n in locdict.keys() if n[:5] == 'test_']
    tests.sort()
    tot = len(tests)
    for n, t in enumerate(tests):
        if n < first:
            continue
        print('\n', t, n + 1, 'of', tot, ':')
        print(locdict[t].__doc__)
        locdict[t]()
