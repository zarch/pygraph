#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------python------------------test_pyplot.py--#
#                                                                       #
#                          Test for pyplot                              #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2010---#

'''
pyplot tests.
'''

from __future__ import division, print_function
from libtest import end,  alltests
import pygraph.pyplot as pp
import pygraph.pycart as cg
import math


def test_00():
    def fun(x):
        return math.sin(1 / x)
    piano = pp.PlotPlane('test_00: sin(1/x)', w=600, h=250, sx=100, sy=100,
                         axescolor='green', gridcolor='gray', axes=True, grid=True)
    p = pp.Plot(color='brown', width=1)
    p.xy(fun)
    end(piano)


def test_01():
    """Test del metodo xy"""
    piano = pp.PlotPlane('test_01: xy')
    p = piano.newPlot(width=2)
    print
    p.xy(lambda x: .1 * x * x * x - 2.5 * x - 2.4, color='navy')
    print
    p.xy(lambda p: 1 / (.1 * p * p * p - 2.5 * p - 2.4), color='maroon')
    print
    p.xy(lambda m: (.1 * m * m * m - 2.5 * m - 2.4)**0.5, color='gold')
    end(piano)


def test_02():
    """Test del metodo yx"""
    piano = pp.PlotPlane('test_02: yx')
    p = pp.Plot(width=2)
    p.yx(lambda y: .1 * y * y * y - 2.5 * y - 2.4, color='gold', width=6)
    p.yx(lambda y: 1 / (.1 * y * y * y - 2.5 * y - 2.4), color='maroon', width=4)
    p.yx(lambda y: (.1 * y * y * y - 2.5 * y - 2.4)**0.5, color='red')
    end(piano)


def test_03():
    """Test del metodo polar"""
    piano = pp.PlotPlane("test_03: polar")
    p = piano.newPlot(width=2)
    p.polar(lambda th: 5 * math.cos(2 * th), color='navy')
    p.polar(lambda th: 8 * math.sin(th / 2), color='maroon')
    p.polar(lambda th: th, 720, color='gold')
    end(piano)


def test_05():
    """Test del metodo privato: _succ()"""
    piano = pp.PlotPlane("test_05: _succ()", sx=10, ox=10)
    p = pp.Plot()
    p._succ([3, 5, 2, '+infinity', 9, 4, 'NaN', 7, 8, 10, 13, 17, 22, 28],
            color='green', width=4)
    p._succ([-3, -5, -2, '+infinity', -9, -4, 'NaN', -7, -8, -10, -13, -17,
             -22, -28], trace=True, color='blue', width=2)
    end(piano)


def test_06():
    """Test del metodo: ny()"""
    piano = pp.PlotPlane("test_06: ny", sx=10, sy=10, ox=10)
    p = piano.newPlot(width=3)
    p.ny(lambda n: n / 2 + 3, color='blue')
    p.ny(lambda n: 3 * math.sin(3 * n), trace=True, values=True, color='gold')
    p.ny(lambda n: (-10 * n - 6) / (n), trace=True, color='maroon')
#  p.save('succ_limiti01')
    end(piano)


def grandine(an):
    if an & 1:
        return an * 3 + 1
    else:
        return an // 2


def test_07():
    """Test del metodo: succ()"""
    piano = pp.PlotPlane("test_07: succ", sx=10, sy=10, ox=10)
    p = pp.Plot()
    p.succ(7, grandine, trace=True, values=True, color='green')
    p.succ(2, lambda an, n: an / 10 - an,
           trace=True, color='blue', shape=cg.ICS)
    p.succ(-2, lambda an, n: an - (n * 2 - 1), color='gold', shape=cg.SQUARE)
#  p.succ(-1,"an+(-1)**(n+1)*3*2**n", color='maroon')
    p.succ(18, lambda an, n: an**0.5 - (-1)**(n + 1) * 2. / 3 + 3,
           trace=True, values=True, color='maroon', shape=cg.EROUND)
    p.succ(50, lambda an, n, a0: (an + a0 / an) /
           2, color='pink', shape=cg.ESQUARE)
#  p.save('succ_limiti02')
    end(piano)


def test_08():
    """Test logaritmi"""
    piano = pp.PlotPlane("test_08: logaritmi", sx=10, sy=10, ox=10)
    p = piano.newPlot(width=3)
    p.xy(lambda x: math.log(x), color=(.8, .5, 0))
    p.xy(lambda x: math.log(x - 1), color=(.8, .4, 0))
    p.xy(lambda x: math.log(x - 1) + 2, color=(.8, .3, 0))
    p.xy(lambda x: -math.log(x - 1) - 2, color=(.8, .2, 0))
    p.xy(lambda x: abs(-math.log(x - 1) - 2), color=(.8, 0, .8))
    end(piano)


def test_09():
    """Test del metodo: param()"""
    piano = pp.PlotPlane("test_09: param", w=500, h=500)
    p = pp.Plot(color='blue', width=3)
    p.param(lambda t: 7 * math.cos(3 * t / 180.) + 3 * math.cos(3 * t / 180. + 8 * t / 180.),
            lambda t: 7 * math.sin(3 * t / 180.) - 3 *
            math.sin(3 * t / 180. + 8 * t / 180.),
            0, math.pi * 180 * 2)
#  p.save('succ_limiti02')
    end(piano)


def test_10():
    """Test del metodo: drawtext()"""
    piano = pp.PlotPlane("test_10: drawtext")
    p = piano.newPlot(width=2)
    p.xy(lambda x: 2**(x), width=1)
    p.drawtext("a", (4, 9))
    p.xy(lambda x: 2**(-x), width=2, color="#404040")
    p.drawtext("b", (-4, 9))
    p.xy(lambda x: 2**(-x + 4), width=3, color="#808080")
    p.drawtext("c", (5, 1))
    p.xy(lambda x: 2**(-x + 4) - 3, width=4, color="#c0c0c0")
    p.drawtext("d", (8, -4))
    end(piano)


def test_11():
    """Test del metodo: xpoints()"""
    def logistica(a):
        """Restituisce la funzione di Verhulst di parametro r."""
        def f(x):
            return -a * x * x + a * x
        return f
    a_mi = 2.9
    a_ma = 4
    width = 800
    height = 500
    margin = 20
    piano = pp.PlotPlane(w=width, h=height,
                         sx=1, sy=height - 2 * margin,
                         ox=margin, oy=height - margin,
                         axes=True, grid=False,
                         axescolor='black', gridcolor='black',
                         parent=None,
                         x_min=a_mi, x_max=a_ma, y_min=0, y_max=1,
                         margin=20, numxlab=10, numylab=10)
    p = pp.Plot()
    attrattori = set()
    x = 0
    a = a_mi
    delta = (a_ma - a_mi) / (width - margin * 1.5)
    while a <= a_ma:
        x += 1
        a += delta
        f = logistica(a)
        n = 0.8
        attrattori.clear()
        for _ in range(1000):
            n = f(n)
        for _ in range(200):
            n = f(n)
            attrattori.add(round(n, 4))
        p.xpoints(x, attrattori)
    end(piano)


def test_12():
    """Test di from-to nel grafico di una funzione xy.
       Grafico di una funzione a tratti."""
    piano = pp.PlotPlane("test_12: funzione a tratti xy", sx=10)
    p = piano.newPlot(width=3, color='navy')
    p.xy(lambda x: -4, d_to=-6)
    p.xy(lambda x: -x * x - 6 * x - 4, d_from=-6, d_to=-2)
    p.xy(lambda x: -2 * x, d_from=-2, d_to=1)
    p.xy(lambda x: x * x - 6 * x + 3, d_from=1, d_to=4)
    p.xy(lambda x: 1 / 2 * x - 3, d_from=4)
    end(piano)


def test_13():
    """Test di from-to nel grafico di una funzione yx."""
    piano = pp.PlotPlane('test_13: funzione a tratti yx')
    p = pp.Plot(width=2)
    p.yx(lambda y: 1 / 2 * y + 2, color='gold', width=6,
         d_to=-3.5)
    p.yx(lambda y: -1 / 2 * y * y, color='maroon', width=4,
         d_from=-3.5, d_to=2.5)
    p.yx(lambda y: 1 / 2 * y + 2, color='red',
         d_from=2.5)
    end(piano)


def test_14():
    """Test di from-to del metodo polar"""
    piano = pp.PlotPlane("test_14: from-to polar")
    p = piano.newPlot(width=2)
    p.polar(lambda th: 5 * math.cos(th), d_from=90, d_to=180, color='navy')
    p.polar(lambda th: 8 * math.sin(th), d_from=270, d_to=360, color='maroon')
    end(piano)


def test_15():
    """Test di from-to nel metodo: param()"""
    piano = pp.PlotPlane("test_15: from-to param", w=500, h=500)
    p = pp.Plot(color='blue', width=3)
    p.param(lambda t: 7 * math.cos(3 * t / 180.) + 3 * math.cos(3 * t / 180. + 8 * t / 180.),
            lambda t: 7 * math.sin(3 * t / 180.) - 3 *
            math.sin(3 * t / 180. + 8 * t / 180.),
            d_from=0, d_to=math.pi * 90)
#  p.save('succ_limiti02')
    end(piano)


def test_16():
    """Test di from-to del metodo: ny()"""
    piano = pp.PlotPlane("test_16: from-to ny", sx=10, sy=10, ox=10)
    p = piano.newPlot(width=3)
    p.ny(lambda n: n / 2 + 3, color='blue', d_from=0, d_to=10)
    p.ny(lambda n: 3 * math.sin(3 * n), d_from=10, d_to=20,
         trace=True, values=True, color='gold')
    p.ny(lambda n: (-10 * n - 6) / (n), d_from=20,
         trace=True, color='maroon')
#  p.save('succ_limiti01')
    end(piano)


loc = locals()

if __name__ == '__main__':
    #    test_06()
    #    test_07()
    #    test_16()
    alltests(locals())
