#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------python-------------------pianoyturtle.py--#
#                                                                      #
#                         Test for pyturtle                            #
#                                                                      #
#--Daniele Zambelli---------------GPL----------------------------2009--#

'''
pyturtle tests.
'''

from __future__ import division, print_function
from libtest import end,  alltests
from random import random, randrange
from math import pi, sin
import pygraph.pyturtle as tg

##############
# Test dei metodi di TurtlePlane presenti nella libreria PyTurtle
#########

# version


def test_00():
    """version:
    visualizza la versione corrente della libreria pyturtle"""
    print("Versione corrente", tg.version())

# newTurtle, after, reset


def test_01():
    """newTurtle, after, reset:
    Crea una tartaruga con tutti i parametri predefiniti modificati,
    disegna un segmento, cancella tutto con reset, disegna un altro segmento."""

    piano = tg.TurtlePlane(name="Geometria della tartaruga",
                           w=800, h=500,
                           sx=20, sy=20,
                           ox=200, oy=400,
                           axes=True, grid=True,
                           axescolor='grey', gridcolor='green')
    tina = piano.newTurtle(x=2, y=-2, d=45, color='red', width=10)
    tina.left(90)
    tina.color = 'green'
    tina.forward(8 * 2**.5)
    piano.after(1000)
    piano.reset()
    tina.forward(10 * 2**.5)
    end(piano)

# turtles


def test_02():
    """newTurtle:
    Crea 3 tartarughe e le fa muovere."""
    piano = tg.TurtlePlane("Piano con tre tartarughe",
                           sx=20, sy=20, grid=True,
                           axescolor="grey", gridcolor="pink")
    tina = tg.Turtle(x=-10, y=-10, d=90, color='red')
    gina = tg.Turtle(y=-10, d=90, color='green')
    pina = tg.Turtle(x=10, y=-10, d=90, color='blue')
    for tarta in piano.turtles():
        tarta.forward(10)
    end(piano)

# turtles, clean, reset


def test_03():
    """right:
    Disegna 4 tartarughe, le muove, le modifica, applica clean e dopo reset."""
    piano = tg.TurtlePlane(name='4 Tartarughe', w=400, h=400,
                           sx=20, sy=20, grid=True)
    tg.Turtle(x=-3, y=-3, d=225, color='orange red')
    tg.Turtle(x=+3, y=-3, d=315, color='orange')
    tg.Turtle(x=+3, y=+3, d=45, color='pale green')
    tg.Turtle(x=-3, y=+3, d=135, color='pale turquoise')
    piano.after(500)
    for tarta in piano.turtles():
        tarta.width = 10
        tarta.forward(5)
    piano.after(500)
    piano.clean()
    piano.after(500)
    piano.reset()
    end(piano)

##############
# Test dei metodi di Turtle presenti nella libreria pyturtle
#########

# forward, back, color


def test_04():
    """forward, back, right, color:
    Disegna una stella con 100 raggi lungli 150"""
    piano = tg.TurtlePlane()
    tina = tg.Turtle()
    n = 200
    for i in range(n):
        tina.color = '#ff{0:02x}00'.format(i * 256 // n)
        tina.forward(150)
        tina.back(150)
        tina.right(360. / n)
    end(piano)

# forward, back, left, right


def test_05(): # chiamato singolarmente funziona, chiamato da alltest, no?????
    """forward, back, left, right:
    Disegna un albero ricorsivo."""

    def albero(lung=100):
        """Albero binario"""
        if lung > 2:
            tina.forward(lung)
            tina.left(45)
            albero(lung * 0.7)
            tina.right(90)
            albero(lung * 0.6)
            tina.left(45)
            tina.back(lung)

    piano = tg.TurtlePlane()
    tina = tg.Turtle(y=-100, d=90)
    albero()
    end(piano)

# up, down


def test_06():
    """up, down:
    Traccia una linea tratteggiata e disegna un pentagono"""

    class MyTurtle(tg.Turtle):
        def tratteggia(self,  lung, pieno=5, vuoto=5):
            q, r = divmod(lung, pieno + vuoto)
            for cont in range(q):
                self.forward(pieno)
                self.up()
                self.forward(vuoto)
                self.down()
            self.forward(r)

    piano = tg.TurtlePlane(name="Pentagono tratteggiato")
    tina = MyTurtle(color='navy', width=3)
    for cont in range(5):
        tina.tratteggia(87)
        tina.left(72)
    end(piano)

# write


def test_07():
    """write:
    Scrive stringhe nella finestra grafica"""
    piano = tg.TurtlePlane(name='Testo nella finestra grafica', w=400, h=200)
    tina = tg.Turtle()
    position = tina.position
    tina.up()
    tina.position = (-140, 80)
    tina.color = 'green'
    tina.write('Parole')
    tina.color = 'pink'
    tina.write('sovrapposte!')
    tina.position = (-140, -80)
    tina.color = 'red'
    tina.write('Parole ', move=True)
    tina.color = 'orange'
    tina.write('scritte di seguito!')
    tina.position = position
    tina.down()
    end(piano)

# fill


def test_08():
    """fill:
    Disegna un quadrato con l'interno colorato"""
    def sposta(x, y):
        """Trasla Tartaruga delle componenti x e y"""
        tina.up()
        tina.forward(x)
        tina.left(90)
        tina.forward(y)
        tina.right(90)
        tina.down()

    def quadrato(lato, colore=None):
        if colore:
            tina.fill(True)
        for cont in range(4):
            tina.forward(lato)
            tina.left(90)
        if colore:
            c = tina.color
            tina.color = colore
            tina.fill(False)
            tina.color = c

    piano = tg.TurtlePlane()
    tina = tg.Turtle(d=30, width=4, color='peru')
    quadrato(100)
    sposta(30, 30)
    quadrato(40, 'brown')
    sposta(-30, -30)
    end(piano)

# tracer


def test_09():
    """tracer:
    Disegna una traccia circolare variando
    la velocita' e lo spessore."""
    piano = tg.TurtlePlane(name="Giro della morte")
    tina = tg.Turtle(y=-180)
    tina.color = (random(), random(), random())
    passi = 72
    angolo = 180. / 72
    for i in range(passi):
        altezza = (tina.position[1] + 200) / 20
        tina.width = altezza
        tina.tracer(altezza)
        tina.left(angolo)
        tina.forward(15)
        tina.left(angolo)
    end(piano)

# position, color


def test_10():
    """position, color:
    Sposta casualmente Tartaruga all'interno del rettangolo
    di vertici (0, 0) e (600, 400)"""
    def spostaacaso():
        tina.up()
        tina.position = (randrange(600) - 300, randrange(400) - 200)
        tina.down()

    def palle(n):
        """Disegna n punti"""
        for i in range(n):
            spostaacaso()
            tina.color = (random(), random(), random())
            tina.fill(1)
            tina.ccircle(20)
            tina.fill(0)

    piano = tg.TurtlePlane()
    tina = tg.Turtle()
    palle(20)
    end(piano)

# direction, color


def test_11():
    """direction, color:
    Ruota a caso Tartaruga e la sposta all'interno di un
    cerchio di dato raggio"""

    def spostaacaso(raggio):
        tina.up()
        tina.position = (0, 0)
        tina.direction = randrange(360)
        tina.forward(randrange(raggio))
        tina.down()

    def stella(lung, n):
        for cont in range(n):
            tina.forward(lung)
            tina.back(lung)
            tina.left(360. / n)

    def cielo():
        """Disegna un cielo stellato"""
        tina.fill(1)
        tina.ccircle(200)
        tina.fill(0)
        colori = ['ivory', 'medium violet red', 'seashell', 'dark orange',
                  'cadet blue', 'pale turquoise', 'peach puff',
                  'misty rose', 'white', 'medium slate blue', 'magenta',
                  'sea green', 'dark khaki', 'ghost white', 'antique white',
                  'medium sea green', 'orange', 'green', 'saddle brown',
                  'cyan', 'firebrick', 'white smoke', 'violet red', 'dim gray',
                  'slate gray', 'brown', 'midnight blue', 'DebianRed',
                  'pale green', 'navajo white', 'lime green', 'medium purple',
                  'light pink', 'moccasin', 'dark goldenrod', 'floral white',
                  'hot pink', 'snow', 'dark sea green', 'turquoise', 'coral',
                  'mint cream', 'blue', 'forest green', 'olive drab',
                  'old lace', 'dark grey', 'sienna', 'maroon',
                  'blanched almond', 'red', 'light slate blue',
                  'light steel blue', 'slate blue', 'dark salmon', 'orchid',
                  'pale goldenrod', 'peru', 'pale violet red', 'lavender',
                  'dark green', 'deep sky blue', 'bisque', 'medium turquoise',
                  'light coral', 'gray', 'light blue', 'dodger blue',
                  'chartreuse', 'medium blue', 'navy', 'violet', 'green yellow',
                  'tomato', 'spring green', 'papaya whip', 'light salmon',
                  'light slate gray', 'lavender blush', 'cornsilk',
                  'light goldenrod', 'thistle', 'beige', 'wheat',
                  'medium aquamarine', 'lemon chiffon', 'orange red', 'yellow',
                  'linen', 'light cyan', 'dark slate blue', 'light sea green',
                  'sky blue', 'blue violet', 'gainsboro', 'burlywood',
                  'lawn green', 'dark violet', 'steel blue',
                  'medium spring green', 'dark slate gray', 'black',
                  'aquamarine', 'cornflower blue', 'royal blue',
                  'dark turquoise', 'powder blue', 'light sky blue', 'gold',
                  'tan', 'indian red', 'light goldenrod yellow', 'sandy brown',
                  'dark orchid', 'plum', 'honeydew', 'pink', 'khaki', 'azure',
                  'alice blue', 'goldenrod', 'salmon', 'deep pink',
                  'medium orchid', 'dark olive green', 'light yellow',
                  'rosy brown', 'chocolate', 'light grey', 'yellow green',
                  'purple']
        for colore in colori:
            spostaacaso(190)
            tina.color = colore
            stella(10, 5)

    piano = tg.TurtlePlane()
    tina = tg.Turtle()
    cielo()
    end(piano)

# direction, color


def test_12():
    """color:
    Frecce tricolori"""

    piano = tg.TurtlePlane()
    fina = tg.Turtle()
    fina.color = 'light blue'
    fina.fill(True)
    fina.ccircle(200)
    fina.fill(False)
    fina.delete()
    tina = tg.Turtle(y=-200, d=100)
    tina.color = (1, 0, 0)
    gina = tg.Turtle(y=-200, d=120)
    gina.color = (1, 1, 1)
    pina = tg.Turtle(y=-200, d=140)
    pina.color = '#00ff00'
    for cont in range(80):
        for tarta in piano.turtles():
            tarta.width = tarta.width + 1
            tarta.forward(4)
            tarta.right(1)
    end(piano)

# width


def test_13():
    """width:
    Disegna una fila di 8 tra quadrati e cerchi alternati"""

    def quadrato(lato):
        for cont in range(4):
            tina.forward(lato)
            tina.left(90)

    piano = tg.TurtlePlane()
    tina = tg.Turtle()
    tina.up()
    tina.back(290)
    for i in range(8):
        tina.down()
        if i % 2:
            tina.width = 5
            tina.color = 'orange'
            tina.circle(20)
        else:
            tina.width = 10
            tina.color = 'maroon'
            quadrato(40)
        tina.up()
        tina.forward(80)
    tina.back(350)
    tina.down()
    end(piano)

# position


def test_14():
    """position:
    Disegna un inviluppo di triangoli rettangoli"""

    def trirett(cateto1, cateto2):
        """Disegna un triangolo rettangolo dati i due cateti"""
        tina.forward(cateto1)
        a = tina.position
        tina.back(cateto1)
        tina.left(90)
        tina.forward(cateto2)
        tina.position = a
        tina.right(90)
        tina.back(cateto1)

    def inviluppo(l1, l2, inc=10):
        if l1 < 0:
            return
        trirett(l1, l2)
        inviluppo(l1 - inc, l2 + inc)

    piano = tg.TurtlePlane(name="Inviluppo di triangoli rettangoli")
    tina = tg.Turtle(color='turquoise')
    for i in range(4):
        inviluppo(200, 0)
        tina.left(90)
    end(piano)

# direction, color, width


def test_15():
    """direction, color, width:
    Modifica a caso gli attributi di Tartaruga
    e li scrive sullo schermo."""

    def scrividati(tarta):
        """Scrive, nel piano, gli attributi della tartaruga."""
        tarta.up()
        p = tarta.position
        tarta.write("La posizione della tartaruga e': ({0}; {1})".format(
            *tarta.position))
        tarta.position = (p[0], p[1] - 1)
        tarta.write("La direzione e': {0}".format(tarta.direction))
        tarta.position = (p[0], p[1] - 2)
        tarta.write("Il colore e': {0}".format(str(tarta.color)))
        tarta.position = (p[0], p[1] - 3)
        tarta.write("La larghezza della penna e': {0}".format(tarta.width))
        tarta.position = p
        tarta.down()

    piano = tg.TurtlePlane(name='Stato della tartaruga',
                           sx=20, sy=20, axes=True, grid=True)
    tina = tg.Turtle()
    tina.left(randrange(180))
    tina.width = randrange(10) + 1
    tina.color = (random(), random(), random())
    tina.forward(randrange(10))
    scrividati(tina)
    gina = tg.Turtle()
    gina.radians()
    gina.left(random() * pi + pi)
    gina.width = randrange(10) + 1
    gina.color = (random(), random(), random())
    gina.forward(randrange(10))
    scrividati(gina)
    end(piano)

# reset


def test_16():
    """reset:
    Disegna un pentagono, applica reset e ridisegna il pentagono"""

    def pentagono(lato):
        for cont in range(5):
            tina.forward(lato)
            tina.left(72)

    piano = tg.TurtlePlane(sx=10, grid=True)
    tina = tg.Turtle(color='pink', width=3)
    tina.tracer(5)
    pentagono(5)
    tina.color = (random(), random(), random())
    tina.width = randrange(2, 20, 2)
    tina.right(randrange(360))
    tina.forward(randrange(5) + 5)
    pentagono(5)
    piano.after(700)
    piano.reset()
    pentagono(5)
    end(piano)

# clean


def test_17():
    """clean:
    Disegna un pentagono, applica clean e ridisegna il pentagono"""

    def pentagono(lato):
        for cont in range(5):
            tina.forward(lato)
            tina.left(72)

    piano = tg.TurtlePlane(sx=10, grid=True)
    tina = tg.Turtle(color='pink', width=3)
    tina.tracer(5)
    pentagono(5)
    tina.color = (random(), random(), random())
    tina.width = randrange(2, 20, 2)
    tina.right(randrange(360))
    tina.forward(randrange(5) + 5)
    pentagono(5)
    piano.after(700)
    piano.clean()
    pentagono(5)
    end(piano)

# circle


def test_18():
    """circle:
    Disegna 25 archi e 25 segmenti circolari alternati."""
    piano = tg.TurtlePlane(w=600, h=600)
    tina = tg.Turtle(width=5)
    for j in range(50):
        tina.color = (random(), random(), random())
        tina.up()
        tina.position = (randrange(-250, 250), randrange(-250, 250))
        tina.down()
        tina.fill(j % 2 == 1)
        tina.circle(randrange(25) + 25, randrange(360))
        tina.color = (random(), random(), random())
        tina.fill(0)
    end(piano)

# ccircle


def test_19():
    """ccircle:
    Disegna 25 archi e 25 settori circolari alternati."""
    piano = tg.TurtlePlane(w=600, h=600)
    tina = tg.Turtle(width=5)
    for j in range(50):
        tina.color = (random(), random(), random())
        tina.up()
        tina.position = (randrange(-250, 250), randrange(-250, 250))
        tina.down()
        tina.fill(j % 2 == 1)
        tina.ccircle(randrange(25) + 25, randrange(360))
        tina.color = (random(), random(), random())
        tina.fill(0)
    end(piano)

# tracer, circle


def test_20():
    """tracer, circle:
    Produce uno strano disegno"""
    piano = tg.TurtlePlane()
    tina = tg.Turtle()
    tina.tracer(5)
    tina.width = 10
    tina.fill(1)
    for i in range(3):
        tina.forward(100)
        tina.right(90)
        tina.color = "green"
        tina.circle(30)
        tina.color = "black"
        tina.left(90)
        tina.back(80)
        tina.left(120)
    tina.color = "maroon"
    tina.fill(0)
    end(piano)

# ccircle


def test_21():
    """ccircle:
    Disegna un bersaglio"""
    piano = tg.TurtlePlane()
    tina = tg.Turtle()
    tina.width = 4
    for dim in range(170, 0, -40):
        colore = '#ff{0:02x}00'.format(dim)
        tina.color = colore
        tina.fill(1)
        tina.ccircle(dim)
        tina.color = '#00ff{0:02x}'.format(dim)
        tina.fill(0)
    end(piano)

# radians, degree


def test_22():
    """radians:
    Disegna un orologio analogico che riporta l'ora del sistema."""
    from time import time, localtime

    def lancette(d, tempo):
        """Disegna le lancette di un orologio che segna ore:minuti:secondi"""
        ore, minuti, secondi = tempo
        tina.degrees()
        aore = round(90 - ore * 30 - minuti * 0.5)
        aminuti = round(90 - minuti * 6 - secondi * 0.1)
        asecondi = round(90 - secondi * 6)
        tina.direction = aore
        tina.width = d * 0.1
        tina.forward(d * 0.6)
        tina.back(d * 0.6)
        tina.direction = aminuti
        tina.width = d * 0.05
        tina.forward(d * 0.8)
        tina.back(d * 0.8)
        tina.direction = asecondi
        tina.width = d * 0.02
        tina.forward(d * 0.9)
        tina.back(d * 0.9)

    def quadrante(dimensione):
        """Il quadrante di un orologio"""
        tina.radians()
        tina.ccircle(dimensione)
        d1 = dimensione * 0.9
        d2 = dimensione - d1
        for i in range(12):
            tina.up()
            tina.forward(d1)
            tina.down()
            tina.forward(d2)
            tina.up()
            tina.back(dimensione)
            tina.down()
            tina.left(pi / 6)
        tina.degrees()

    def orologio(dimensione):
        quadrante(dimensione)
        lancette(dimensione, localtime(time())[3:6])

    piano = tg.TurtlePlane(name="Ora esatta")
    tina = tg.Turtle()
    orologio(100)
    end(piano)

# distance


def test_23():
    """distance:
    Dirige Tartaruga verso un bersaglio emulando l'olfatto."""

    def saltaacaso():
        tina.up()
        tina.position = (randrange(-280, 280), randrange(-180, 180))
        tina.down()

    def bersaglio():
        """Disegna un bersaglio"""
        saltaacaso()
        for cont in range(4):
            tina.forward(5)
            tina.back(5)
            tina.left(90)
        larghezza = tina.width
        tina.width = 2
        tina.ccircle(20)
        tina.width = larghezza
        tina.ccircle(10)
        return(tina.position)

    def odore(p):
        """Riporta un numero inversamente proporzionale
        al quadrato della distanza da p"""
        return 1. / (tina.distance(p)**2)

    def ricerca_per_odore(bersaglio):
        """Muove Tartaruga in base ad una regola olfattiva"""
        tina.tracer(10)
        ricordo_odore = odore(bersaglio)
        while tina.distance(bersaglio) > 10:
            tina.forward(randrange(10))
            nuovo_odore = odore(bersaglio)
            if nuovo_odore < ricordo_odore:
                #              tina.right(randrange(80))
                tina.right(randrange(20) + 80)
            ricordo_odore = nuovo_odore

    piano = tg.TurtlePlane("Ricerca in base all'odore")
    tina = tg.Turtle()
    b = bersaglio()
    saltaacaso()
    ricerca_per_odore(b)
    end(piano)

# dirto


def test_24():
    """dirto:
    Simula l'inseguimento tra un gatto e un topo."""

    class Topo(tg.Turtle):
        """Classe che simula il comportamento di un topo"""

        def __init__(self, **args):
            tg.Turtle.__init__(self, **args)
            piano = self._plane
            self.up()
            self.position = (piano._s2x(20), piano._s2y(20))
            self.write("inseguimento in base alla vista")
            self.position = (randrange(120) - 60, randrange(80) - 40)
            self.down()

        def scappa(self, da):
            """Fa muovere il topo di un passo cercando di scappare da da"""
            dir_gatto = self.dirto(da.position)
            if dir_gatto < 170:
                self.right(randrange(20) - 10)
            elif dir_gatto > -170:
                self.left(randrange(20) - 10)
            else:
                self.left(randrange(80) - 40)
            self.forward(1)

    class Gatto(tg.Turtle):
        """Classe che simula il comportamento di un gatto"""

        def __init__(self, **args):
            tg.Turtle.__init__(self, **args)
            self.color = "red"
            self.width = 3

        def insegui(self, chi):
            """Fa muovere il gatto di un passo come se inseguisse chi
            ritorna 0 se ha raggiunto chi"""
            if self.distance(chi.position) < 3:
                self.position = chi.position
                return 0
            dir_topo = self.dirto(chi.position)
            if dir_topo < 180:
                self.left(randrange(5))
            else:
                self.right(randrange(5))
            self.forward(2)
            return 1

    piano = tg.TurtlePlane()
    topo = Topo()
    gatto = Gatto()  # plane=piano)
    while gatto.insegui(topo):
        topo.scappa(gatto)
    end(piano)

# whereis


def test_25():
    """whereis:
    Simula la ricerca in base all'udito."""

    def saltaacaso():
        tina.up()
        tina.position = (randrange(-280, 280), randrange(-180, 180))
        tina.down()

    def bersaglio():
        """Disegna un bersaglio"""
        saltaacaso()
        for cont in range(4):
            tina.forward(5)
            tina.back(5)
            tina.left(90)
        larghezza = tina.width
        tina.width = 2
        tina.ccircle(20)
        tina.width = larghezza
        tina.ccircle(10)
        return(tina.position)

    def intensita_destra(distanza, angolo):
        """Restituisce un numero che indica l'intensita' con cui
        l'orecchio destro ode un suono proveniente da un bersaglio
        posto ad una certa distanza e con un certo angolo"""
        senangolo = sin((angolo % 360) * pi / 180)
        if senangolo < 0:
            return -10. * senangolo / (distanza**2)
        else:
            return 5. * senangolo / (distanza**2)

    def intensita_sinistra(distanza, angolo):
        """Restituisce un numero che indica l'intensita' con cui
        l'orecchio sinistro ode un suono proveniente da un bersaglio
        posto ad una certa distanza e con un certo angolo"""
        senangolo = sin((angolo % 360) * pi / 180)
        if senangolo > 0:
            return 10. * senangolo / (distanza**2)
        else:
            return -5. * senangolo / (distanza**2)

    def ricerca_per_udito(bersaglio):
        """Simula la ricerca di un bersaglio utilizzando l'udito"""
        tina.tracer(10)
        while tina.distance(bersaglio) > 10:
            d, a = tina.whereis(bersaglio)
            tina.forward(randrange(5))
            if intensita_destra(d, a) > intensita_sinistra(d, a):
                tina.right(randrange(30))
            else:
                tina.left(randrange(30))

    piano = tg.TurtlePlane("Ricerca in base all'udito")
    tina = tg.Turtle()
    b = bersaglio()
    saltaacaso()
    ricerca_per_udito(b)
    end(piano)

# lookat


def test_26():
    """lookat:
    Disegna una parabola per mezzo dell'inviluppo di rette."""

    def asse(punto):
        """Disegna l'asse del segmnento che ha per estremi Tartaruga e punto"""
        direction = tina.direction
        position = tina.position
        tina.up()
        tina.lookat(punto)
        tina.forward(tina.distance(punto) / 2.)
        tina.left(90)
        tina.back(1000)
        tina.down()
        tina.forward(2000)
        tina.up()
        tina.position = position
        tina.direction = direction
        tina.down()

    piano = tg.TurtlePlane(name="Parabola formata da un inviluppo di rette")
    tina = tg.Turtle()
    fuoco = (0, 50)
    numpassi = 80
    lato = 5
    tina.up()
    tina.back(200)
    tina.down()
    for i in range(numpassi):
        asse(fuoco)
        tina.forward(lato)
    end(piano)

# hide e show


def test_27():
    """hide, show:
    Traccia una linea mostrando e nascondendo Tartaruga."""
    piano = tg.TurtlePlane()
    tina = tg.Turtle()
    tina.up()
    tina.back(295)
    tina.down()
    tina.tracer(10)
    for i in range(15):
        tina.forward(19)
        tina.hide()
        tina.forward(19)
        tina.show()
    end(piano)

# save


def test_28():
    """save:
    Produce un disegno e lo salva nel file immagine.png.
    Funziona solo con Linux perche' utilizza il programma convert"""

    def spirin(lato, angolo, inc, cnt):
        if cnt == 0:
            return
        tina.forward(lato)
        tina.right(angolo)
        spirin(lato, angolo + inc, inc, cnt - 1)

    piano = tg.TurtlePlane(name="spirin")
    tina = tg.Turtle(color='violet', width=3)
    spirin(40, 2, 20, 90)
    piano.save('immagine')
    end(piano)

# <TurtlePlane>.newTurtle, <TurtlePlane>.turtles
# <TurtlePlane>.clean, <TurtlePlane>.reset
# <turtle>.delete


def test_29():
    """newTurtle, turtles, clean, reset, delete:
    Crea 36 tartarughe all'interno dello stesso piano
    mettendole in una lista, poi le fa muovere"""
    piano = tg.TurtlePlane("36 tartarughe che eseguono gli stessi comandi",
                           w=500, h=500,
                           sx=20, sy=20,
                           grid=True)
    n = 36
    l = 4.
    for i in range(n):
        tg.Turtle(color='#ff{0:02x}00'.format(i * 256 // n),
                  d=(360. / n * i), width=5)
    for tarta in piano.turtles():
        tarta.forward(l)
    for tarta in piano.turtles():
        tarta.left(90)
    for tarta in piano.turtles():
        tarta.forward(l / 4)
    for tarta in piano.turtles():
        tarta.right(45)
    for tarta in piano.turtles():
        tarta.forward(l)
    for tarta in piano.turtles():
        tarta.right(120)
    for tarta in piano.turtles():
        tarta.forward(l / 2)
    for tarta in piano.turtles():
        tarta.left(60)
    for tarta in piano.turtles():
        tarta.forward(l / 2)
    piano.after(500)
    piano.clean()
    for tarta in piano.turtles():
        tarta.forward(2)
    piano.after(500)
    piano.reset()
    for tarta in piano.turtles():
        tarta.forward(5)
    for tarta in piano.turtles():
        piano.after(100)
        tarta.delete()
    piano.after(500)
    piano.clean()
    end(piano)

# clone


def test_30():
    """clone:
    Fa collaborare alcune tartarughe per disegnare un albero"""
    piano = tg.TurtlePlane("Albero realizzato da una famiglia di tartarughe",
                           w=500, h=500)
    tina = tg.Turtle(y=-200, d=90, color='olive drab')
    dim = 100
    angl = 25
    angr = 35
    while dim > 5:
        for tarta in piano.turtles():
            tarta.width = dim // 5
            tarta.forward(dim)
            t1 = tarta.clone()
            tarta.left(angl)
            t1.right(angr)
        piano.after(200)
        dim *= 0.7
        print(len(piano.turtles()))
    for tarta in piano.turtles():
        tarta.color = 'lime green'
    end(piano)

# circle


def test_31():
    """circle:
    Bug in circle."""
    piano = tg.TurtlePlane("Circonferenza realizzata da una sequenza di archi",
                           w=500, h=500)
    tina = tg.Turtle()
    n = 20
    r = 100.
    print(tina.position)
    for cont in range(n):
        tina.circle(r, 360. / n)
    print(tina.position)
    end(piano)


def test_32():
    """square:
    Bug in left."""
    piano = tg.TurtlePlane("Quadrato", w=500, h=500)
    tina = piano.newTurtle()
    n = 10
    lato = 100.
    print(tina.position)
    for cont in range(n):
        for n in range(4):
            tina.forward(lato)
            tina.left(360. / 4)
            print(tina.position)
    end(piano)


loc = locals()

if __name__ == '__main__':
##    test_04()
##    test_05()
##    test_06()
##    test_31()
##    test_32()
    alltests(locals())
