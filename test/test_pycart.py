#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------python------------------test_pycart.py--#
#                                                                       #
#                           Test for pycart                             #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2018---#

"""
pycart tests
"""

from __future__ import division, print_function
from libtest import end,  alltests
import pygraph.pycart as cg
import random


def randcolor():
    """Return a random RGB color."""
    return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                           random.randrange(256),
                                           random.randrange(256))


def test_00():
    """Controllare la versione della libreria.
       uso di tutti i valori predefiniti."""
    if cg.version() < '2.9.00':
        print("versione un po' vecchiotta")
        exit(1)
    else:
        print("versione ", cg.version(), sep=' ')


def test_01():
    """Piano con tutti i valori predefiniti."""
    piano = cg.Plane('01. Plane vanilla')
    end(piano)


def test_02():
    """Modifica alcuni valori predefiniti."""
    piano = cg.Plane('02. sx=30; sy=20',
                     w=400, h=600,
                     sx=30,  sy=20,
                     ox=100,  oy=200,
                     axescolor='green', gridcolor='red', color='yellow')
    end(piano)


def test_03():
    """scale:
    Crea un piano, modifica la scala e l'origine ridisegna assi e griglia."""
    piano = cg.Plane('03. scale', sx=15, axescolor='green',
                     color='AntiqueWhite3')
    piano.after(500)
    x_o, y_o = piano.origin
    s_x, s_y = piano.scale
    piano.origin = (x_o - 4 * s_x, y_o + 6 * s_y)
    piano.scale = (30, 20)
    prevcolor = piano.color
    piano.color = 'yellow'
    piano.axes(color='blue')
    piano.grid(color='red')
    piano.after(1000)
    piano.color = prevcolor
    end(piano)


def test_04():
    """Reactive Plane."""
    def onkeypress(event):
        print("Key pressed:", event.char)

    def onkeyrelease(event):
        print("Key released:", event.char)

    def onenter(event):
        print("Enter at", event)

    def onleave(event):
        print("Leave at", event)

    def onpress1(event):
        print("Left clicked at", event.x, event.y)

    def onpress2(event):
        print("Center clicked at", event.x, event.y)

    def onpress3(event):
        print("Right clicked at", event.x, event.y)

    def onmotion1(event):
        print("Left motion at", event.x, event.y)

    def onmotion2(event):
        print("Center motion at", event.x, event.y)

    def onmotion3(event):
        print("Right motion at", event.x, event.y)

    def onrelease1(event):
        print("Left released at", event.x, event.y)

    def onrelease2(event):
        print("Center released at", event.x, event.y)

    def onrelease3(event):
        print("Right released at", event.x, event.y)
    piano = cg.Plane('04. events', color='pink')
    piano.onkeypress(onkeypress)
    piano.onkeyrelease(onkeyrelease)
    piano.onenter(onenter)
    piano.onleave(onleave)
    piano.onpress1(onpress1)
    piano.onpress2(onpress2)
    piano.onpress3(onpress3)
    piano.onmotion1(onmotion1)
    piano.onmotion2(onmotion2)
    piano.onmotion3(onmotion3)
    piano.onrelease1(onrelease1)
    piano.onrelease2(onrelease2)
    piano.onrelease3(onrelease3)
    biro = cg.Pen()
    biro.drawtext('Clicca in vari punti del piano', (0, 9), 'navy', 2)
    biro.drawtext('con i tre tasti del mouse', (0, 7), 'navy', 2)
    end(piano)


def test_10():
    """Disegnare un quadrato con spessore di 4 pixel."""
    piano = cg.Plane('10. Poligono', sx=5, gridcolor='grey', color='AliceBlue')
    piano.axes()
    biro = cg.Pen(color='magenta', width=4)
    a = 25
    b = 15
    square = ((-a, -b), (b, -a), (a, b), (-b, a))
    biro.drawpoly(square)
    end(piano)


def test_11():
    """Disegnare tre figure di diverso colore."""
    piano = cg.Plane('11. Poligoni colorati', sx=1, color='#f0f0f0')
    biro = piano.newPen(width=4)
    square = ((-50 - 200, -30), (30 - 200, -50),
              (50 - 200, 30), (-30 - 200, 50))
    biro.drawpoly(square, color='red')
    square = ((-50 - 150, -30), (30 - 150, -50),
              (50 - 150, 30), (-30 - 150, 50))
    biro.drawpoly(square, color='#fabdc3')
    square = ((-50 - 100, -30), (30 - 100, -50),
              (50 - 100, 30), (-30 - 100, 50))
    biro.drawpoly(square, color=(0.8, 0.75, 0.56))
    end(piano)


def test_12():
    """Disegnare un quadrato in movimento."""
    piano = cg.Plane('12. Quadrato in movimento',
                     sx=1, sy=1, axes=False, grid=False)
    biro = cg.Pen(width=4)
    for i in range(0, 500, 2):
        color = '#ff{0:02x}00'.format(i // 2)
        verts = ((-300 + i, -30), (-220 + i, -50),
                 (-200 + i, 30), (-280 + i, 50))
        poly = biro.drawpoly(verts, color)
        piano.after(10)
        piano.delete(poly)
    biro.drawpoly(verts, 'green')
    end(piano)


def test_13():
    """Disegnare un segmento che scivola sugli assi."""
    piano = cg.Plane('13. Segmento che scivola sugli assi', sx=1)
    biro = cg.Pen(width=4, color='navy')
    lung = 200
    for y in range(200, 0, -1):
        x = (lung * lung - y * y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        piano.after(4)
        piano.delete(seg)
    for y in range(0, -200, -1):
        x = (lung * lung - y * y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        piano.after(4)
        piano.delete(seg)
    for y in range(-200, 0):
        x = -(lung * lung - y * y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        piano.after(4)
        piano.delete(seg)
    for y in range(200):
        x = -(lung * lung - y * y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        piano.after(4)
        piano.delete(seg)
    biro.drawsegment((0, 0), (0, lung))
    end(piano)


def test_14():
    """Disegnare un rettangolo che circonda la finestra grafica."""
    piano = cg.Plane('14. Cornice', sx=1, axes=False, grid=False)
    biro = cg.Pen(width=4, color='green')
    bordo = 10
    w_2 = piano.getcanvaswidth() // 2 - bordo
    h_2 = piano.getcanvasheight() // 2 - bordo
    biro.drawpoly(((-w_2, -h_2), (w_2, -h_2), (w_2, h_2), (-w_2, h_2)))
    end(piano)


def test_15():
    """Disegnare un poligono, dopo mezzo secondo cancellare tutto e
       ridisegnarlo con un altro colore."""
    piano = cg.Plane('15. Poligono')
    vertici = ((-2, -3), (4, -1), (6, 4), (-5, 6))
    biro = cg.Pen(color="red", width=20)
    biro.drawpoly(vertici)
    piano.after(500)
    piano.reset()
    biro.drawpoly(vertici, color="green")
    end(piano)


def test_16():
    """Tracciare una linea tratteggiata in diagonale sullo schermo."""
    piano = cg.Plane('16. Linea tratteggiata', sx=1, axes=False, grid=False)
    x_i = -200
    y_i = -100
    biro = cg.Pen(x=x_i, y=y_i, color='blue', width=4)
    for _ in range(25):
        x_i += 10
        y_i += 5
        biro.drawto((x_i, y_i))
        x_i += 6
        y_i += 3
        biro.position = (x_i, y_i)
    end(piano)


def test_17():
    """Produrre un file che contiene il disegno di un esagono."""
    piano = cg.Plane("17. Salva l'immagine", color=randcolor())
    biro = cg.Pen(color=randcolor(), width=4)
    exa = []
    for _ in range(6):
        exa.append((10 - random.randrange(20), 10 - random.randrange(20)))
    biro.drawpoly(exa)
##    piano.save(str(b'esagono', encoding='utf-8'))
    piano.save('esagono')
    end(piano)


def test_18():
    """Disegnare un pentagono casuale."""
    piano = cg.Plane('18. Pentagono casuale', axes=True)
    biro = cg.Pen(color='blue', width=4)
    penta = []
    for _ in range(5):
        penta.append((10 - random.random() * 20, 10 - random.random() * 20))
    biro.drawpoly(penta)
    end(piano)


def test_19():
    """Disegna un pentagono casuale con i vertici sulla griglia."""
    piano = cg.Plane('19. Pentagono casuale con i vertici sulla griglia',
                     axes=False)
    biro = cg.Pen(color='red', width=4)
    penta = []
    for _ in range(5):
        penta.append((10 - random.randrange(20), 10 - random.randrange(20)))
    biro.drawpoly(penta)
    end(piano)


def test_20():
    """Disegna elementi geometrici in diversi piani."""
    def drawpoints(pen):
        pen.position = (6, -2)
        pen.drawpoint(shape=cg.SQUARE)
        pen.drawpoint((-6, 9), color=(0.2, 0.8, 0.55))

    def drawcircles(pen):
        pen.drawcircle(6, (-6, -2),
                       incolor='DarkOrchid4')
        pen.drawcircle(2, (6, 7),
                       color='DarkOrchid4', incolor='dark khaki')

    def drawsegments(pen):
        pen.drawsegment((-4, 5), color='#228b22', width=5)
        pen.drawsegment((-4, 5), (7, 2), color='orange', width=8)

    def drawpolys(pen):
        pen.drawpoly(((-4, -1), (-1, -5), (3, -3), (1, -1)),
                     color=(0.5, 0.6, 0.7), width=10, incolor='grey84')
        pen.drawpoly(((4, -6), (7, -9), (9, -5), (5, -3)),
                     color=(0.5, 0.6, 0.7), width=6, incolor='DarkOrange')

    piano = cg.Plane('20.0. Punti, segmenti, circonferenze',
                     sx=20, axes=True, grid=True,
                     axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro0 = cg.Pen(color='dark khaki', width=5)
    cg.Plane('20.1. Punti',
             sx=20, axes=True, grid=True,
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = cg.Pen(color='dark khaki', width=5)
    drawpoints(biro0)
    drawpoints(biro1)
    cg.Plane('20.2. Circonferenze',
             sx=20, axes=True, grid=True,
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = cg.Pen(color='dark khaki', width=5)
    drawcircles(biro0)
    drawcircles(biro1)
    cg.Plane('20.3. Segmenti',
             sx=20, axes=True, grid=True,
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = cg.Pen(color='dark khaki', width=5)
    drawsegments(biro0)
    drawsegments(biro1)
    cg.Plane('20.4. Poligono',
             sx=20, axes=True, grid=True,
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = cg.Pen(color='dark khaki', width=5)
    drawpolys(biro0)
    drawpolys(biro1)
    end(piano)


def test_21():
    """Verifica del funzionamento di assi e griglia.
    Attendere, impiega 4 secondi."""
    piano = cg.Plane('21. Assi e griglie',  sx=10,  sy=20,
                     axescolor='grey', gridcolor='tomato')
    pen = piano.newPen(x=-10, y=3, color='red', width=2)
    pen.drawtext('attendere qualche secondo... 4')
    ritardo = 1000
    piano.origin = (342, 218)
    piano.scale = (20, 40)
    piano.after(ritardo)
    piano.axes('purple')
    piano.after(ritardo)
    piano.grid('#22aa55')
    piano.after(ritardo)
    piano.reset()
    pen.drawtext('attendere qualche secondo... 3')
    piano.after(ritardo)
    piano.clean()
    pen.drawtext('attendere qualche secondo... 2')
    piano.grid('#aa5522')
    piano.after(ritardo)
    piano.clean()
    pen.drawtext('attendere qualche secondo... 1')
    piano.after(ritardo)
    piano.axes('#55bb22')
    piano.after(ritardo)
    piano.reset()
    pen.drawtext('finito!')
    end(piano)


def test_22():
    """Verifica del funzionamento di penwidth."""
    piano = cg.Plane('22. penwidth', sx=20,  sy=20)
    biro = cg.Pen()
    for i in range(10):
        biro.width = i * 2
        biro.drawsegment((-5, i - 4), (5, i - 4))
    end(piano)


def test_23():
    """Verifica del funzionamento di color."""
    piano = cg.Plane('23. color', w=620, h=700, sx=1,
                     axescolor='#4000a0', gridcolor='#a00040')
    colors = ['AliceBlue', 'AntiqueWhite', 'AntiqueWhite1', 'AntiqueWhite2',
              'AntiqueWhite3', 'AntiqueWhite4', 'BlanchedAlmond', 'BlueViolet',
              'CadetBlue', 'CadetBlue1', 'CadetBlue2', 'CadetBlue3',
              'CadetBlue4', 'CornflowerBlue', 'DarkBlue', 'DarkCyan',
              'DarkGoldenrod', 'DarkGoldenrod1', 'DarkGoldenrod2',
              'DarkGoldenrod3', 'DarkGoldenrod4', 'DarkGray', 'DarkGreen',
              'DarkGrey', 'DarkKhaki', 'DarkMagenta', 'DarkOliveGreen',
              'DarkOliveGreen1', 'DarkOliveGreen2', 'DarkOliveGreen3',
              'DarkOliveGreen4', 'DarkOrange', 'DarkOrange1', 'DarkOrange2',
              'DarkOrange3', 'DarkOrange4', 'DarkOrchid', 'DarkOrchid1',
              'DarkOrchid2', 'DarkOrchid3', 'DarkOrchid4', 'DarkRed',
              'DarkSalmon', 'DarkSeaGreen', 'DarkSeaGreen1', 'DarkSeaGreen2',
              'DarkSeaGreen3', 'DarkSeaGreen4', 'DarkSlateBlue',
              'DarkSlateGray', 'DarkSlateGray1', 'DarkSlateGray2',
              'DarkSlateGray3', 'DarkSlateGray4', 'DarkSlateGrey',
              'DarkTurquoise', 'DarkViolet', 'DebianRed', 'DeepPink',
              'DeepPink1', 'DeepPink2', 'DeepPink3', 'DeepPink4', 'DeepSkyBlue',
              'DeepSkyBlue1', 'DeepSkyBlue2', 'DeepSkyBlue3', 'DeepSkyBlue4',
              'DimGray', 'DimGrey', 'DodgerBlue', 'DodgerBlue1', 'DodgerBlue2',
              'DodgerBlue3', 'DodgerBlue4', 'FloralWhite', 'ForestGreen',
              'GhostWhite', 'GreenYellow', 'HotPink', 'HotPink1', 'HotPink2',
              'HotPink3', 'HotPink4', 'IndianRed', 'IndianRed1', 'IndianRed2',
              'IndianRed3', 'IndianRed4', 'LavenderBlush', 'LavenderBlush1',
              'LavenderBlush2', 'LavenderBlush3', 'LavenderBlush4', 'LawnGreen',
              'LemonChiffon', 'LemonChiffon1', 'LemonChiffon2', 'LemonChiffon3',
              'LemonChiffon4', 'LightBlue', 'LightBlue1', 'LightBlue2',
              'LightBlue3', 'LightBlue4', 'LightCoral', 'LightCyan',
              'LightCyan1', 'LightCyan2', 'LightCyan3', 'LightCyan4',
              'LightGoldenrod', 'LightGoldenrod1']
    biro = cg.Pen(width=2)
    num = 100
    for i in range(num):
        biro.color = (i / num, 1 - i / num, 0)
        biro.drawsegment((1, i * 3 + 1), (300, i * 3 + 1))
    for i in range(num):
        biro.color = (i / num, 0, 1 - i / num)
        biro.drawsegment((-(i * 3 + 1), 1), (-(i * 3 + 1), 300))
    for i in range(num):
        biro.color = (0, i / num, 1 - i / num)
        biro.drawsegment((-1, -(i * 3 + 1)), (-300, -(i * 3 + 1)))
    print(len(colors))
    for i, color in enumerate(colors):
        biro.color = color
        biro.drawsegment((1, -(i * 3 + 1)), (300, -(i * 3 + 1)))
    end(piano)


def test_24():
    """Verifica il funzionamento di position."""
    points = ((200, 0), (100, 173), (-100, 173),
              (-200, 0), (-100, -173), (100, -173), (200, 0))
    piano = cg.Plane('24. Esagono', sx=1, axes=False)
    biro = cg.Pen(color='red', width=8)
    biro.drawpoint()
    biro.penwidth = 4
    for point in points:
        biro.drawsegment(point, color='blue')
        biro.position = point
        biro.drawpoint(color='red')
    end(piano)


def test_25():
    """Verifica il funzionamento di drawto."""
    points0 = ((200, 0), (100, 173), (-100, 173),
               (-200, 0), (-100, -173), (100, -173), (200, 0))
    piano = cg.Plane('25. Esagono', sx=1, axes=False)
    biro = cg.Pen(color='red', width=8)
    for point in points0:
        biro.color = 'blue'
        biro.drawto(point)
        biro.color = 'red'
        biro.drawpoint()
    for point in points0:
        biro.drawto((point[0] / 2, point[1] / 2), color='green', width=4)
        biro.drawpoint()
    end(piano)


def test_26():
    """Verifica il funzionamento di drawsegment."""
    points = ((200, 0), (100, 173), (-100, 173),
              (-200, 0), (-100, -173), (100, -173), (200, 0))
    piano = cg.Plane('26. Esagono', sx=1, axes=False)
    biro = cg.Pen(color='red', width=8)
    biro.drawpoint()
    for point in points:
        biro.drawpoint(point)
    biro.color = 'green'
    for point in points:
        biro.drawsegment(point)
    for point in points:
        biro.drawsegment((point[0] / 2, point[1] / 2), color='blue', width=4)
    for point in points:
        biro.drawpoint((point[0] / 2, point[1] / 2), color='red', width=4)
    end(piano)


def test_27():
    """Verifica il funzionamento di drawpoint."""
    piano = cg.Plane('27. punti', w=420, h=420, sx=1,
                     axes=False, grid=False)
    biro = cg.Pen(color='red', width=200)
    biro.drawpoint(shape=cg.BSQUARE)
    for _ in range(100):
        biro.drawpoint((random.randrange(-160, 160),
                        random.randrange(-160, 160)),
                       color=randcolor(),
                       width=random.randrange(30) + 1,
                       shape=random.choice(cg.SHAPES))
    end(piano)


def test_28():
    """Verifica il funzionamento di drawcircle."""
    piano = cg.Plane('28. cerchi', w=420, h=420, sx=1,
                     axes=False, grid=False)
    biro = cg.Pen(color='blue', width=4)
    biro.drawcircle(200, incolor='pink')
    for _ in range(100):
        biro.drawcircle(radius=random.randrange(80),
                        center=(random.randrange(-100, 100),
                                random.randrange(-100, 100)),
                        width=random.randrange(10),
                        color=randcolor(), incolor=randcolor())
    end(piano)


def test_29():
    """Verifica il funzionamento di drawpoly."""
    piano = cg.Plane('29. poligoni', sx=1, axes=False)
    biro = cg.Pen(color='red', width=6)
    polygon = [(random.randrange(-300, 300, 20),
                random.randrange(-200, 200, 20)) for
               _ in range(7)]
    biro.drawpoly(polygon)
    polygon = [(random.randrange(-300, 300, 20),
                random.randrange(-200, 200, 20)) for
               _ in range(7)]
    biro.drawpoly(polygon,
                  color=randcolor(), width=random.randrange(10),
                  incolor=randcolor())
    end(piano)


def test_30():
    """Verifica il funzionamento di drawtext."""
    # Diverse cose che non vanno: da sistemare!
    piano = cg.Plane('30. drawtext', w=400, h=400)
    biro = piano.newPen(color='red')
    biro.drawtext("a", (-8, 5))
    biro.width = 2
#    biro.drawtext(u"ambarabà cici cocò", (0, 3)) # in Python2.x
    biro.drawtext(u"ambarabà cici cocò", (0, 3))
    biro.width = 3
    biro.drawtext("c", (-2, 1))
    biro.width = 4
    biro.drawtext("d", (1, -2))
    end(piano)


def test_31():
    """Verifica il funzionamento di drawpoint shape ."""
    piano = cg.Plane('31. drawpoint shape', h=540, axes=False, grid=False)
    px1, px2, py1, py2 = -13, -10, +11, +9
    biro = cg.Pen()
    colors = ('red', 'gold', 'blue', 'green', 'pink', 'violet',
              'turquoise', 'light cyan')
    for num, forma in enumerate(cg.SHAPES):
        biro.drawtext(forma, (px1, py2 - 2.7 * num), width=1.5)
    for dim in range(8):
        biro.drawtext(3 * dim, (px2 + 3 * dim, py1), width=1.5)
        for num, (color, shape) in enumerate(zip(colors, cg.SHAPES)):
            biro.drawpoint((px2 + 3 * dim, py2 - 2.7 * num), width=3 * dim,
                           color=color, shape=shape)
    end(piano)


def test_32():
    """Verifica il funzionamento di Point.shape ."""
    piano = cg.Plane('31. Point shape',
                     w=400, h=400, sx=10, axes=False, grid=False)
    biro = piano.newPen(width=15, shape=cg.ICS)
    colors = ('red', 'gold', 'blue', 'green', 'pink', 'violet',
              'turquoise', 'light cyan')
    for forma, colore in zip(cg.SHAPES, colors):
        posizione = (random.randrange(-10, 10),
                     random.randrange(-10, 10))
        biro.position = posizione
        biro.color = colore
        biro.shape = forma
        print(biro.position, biro.color, biro.shape)
        biro.drawpoint()
    end(piano)


def test_33():
    """Verifica il funzionamento di newPen."""
    piano = cg.Plane("33. funzionamento di newPen",
                     w=400, h=400, sx=10, axes=False)
#    penna = cg.Pen(width=10)
    penna = piano.newPen(width=10, color='red', shape=cg.BROUND)
    print(penna)
    penna.drawpoint(color='red')
    penna.drawsegment((5, -3), color='blue')
    penna.drawpoint((5, -3), shape=cg.BSQUARE)
    end(piano)


loc = locals()

if __name__ == '__main__':
    #    test_04()
    #    test_11()
    #    test_12()
    #    test_13()
    #    test_27()
    #    test_32()
    alltests(locals())
