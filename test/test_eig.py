# -*- coding: utf-8 -*-
#-------------------------------python---------------------test_eig.py--#
#                                                                       #
#                            Test for eig                               #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2010---#

from eig import *

"""
p0=Point(interactiveplane, 7, 2, width=7)
p1=Point(interactiveplane, -3, 8, width=7)
Segment(p0, p1, color='gold', width=5)
interactiveplane.mainloop()

"""

# Altro modo
p0 = Point(ip, 7, 2, width=7)
p1 = Point(ip, -3, 8, width=7)
Segment(p0, p1, color='gold', width=5)
ip.mainloop()
