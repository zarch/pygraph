#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------python-------------test_pyig_errors.py--#
#                                                                       #
#            Tests for Python Interactive Geometry Errors               #
#                                                                       #
#--Daniele Zambelli-----------Licence-GPL------------------------2012---#

"""
pyig tests
"""

from __future__ import division, print_function
from pygraph.pyig import *
#from libtest import alltests, end
from doctest import *


def test_00():
    """00: Costruzione di un oggetto senza un piano interattivoo.
  >>> test_00()
  Traceback (most recent call last):
    ...
  PlaneError: Ther are no Planes
  """
    p0 = Point(3, 5)


def test_01():
    """01: Test for Text, VarText, Label, VarLabel.
  >>> test_01()
  Traceback (most recent call last):
    ...
  TypeError: __init__() takes at least 4 arguments (2 given)

  """
    ip = InteractivePlane(test_01.__doc__)
    point0 = Point(5, 6, width=5, color='red', name="A")
    titolo = Text("Prove di testo ('blue violet')")
#  ip.mainloop()


def test_02():
    """02: Test for Text, VarText, Label, VarLabel.
  >>> test_02()
  Traceback (most recent call last):
    ...
  IndexError: VarText needs more objects to complete the string
  """
    ip = InteractivePlane(test_01.__doc__)
    point0 = Point(5, 6, width=5, color='red', name="A")
    titolo = Text(-5, 8, "Prove di testo ('blue violet')")
    titolo.name = '(titolo)'
    vt0 = VarText(-4, 3,
                  "Posizione del titolo: ({0}; {1})", (titolo.xcoord(),))
#  ip.mainloop()


def test_03():
    """03: Test for Intersection.
  >>> test_03()
  Traceback (most recent call last):
    ...
  IgError: Intersection need which point: -1 or +1
  """
    ip = InteractivePlane(test_01.__doc__)
    lin = Line(Point(3, -2), Point(-5, 6))
    cir = Circle(Point(5, -6), Point(-2, -6))
    i = Intersection(lin, cir)
    ip.mainloop()


'''
def test_03():
  """03: Test for Angle, AngleRA and AngleSide Bisector.
>>> test_03()
Traceback (most recent call last):
  ...
IndexError: VarText needs more objects to complete the string
"""
  ip = InteractivePlane(test_01.__doc__)
  a0 = Angle(Point(5, 6), Point(-5, 6), Point(-5, 8), (0, 1))
  a1 = Angle(Point(5, -6), Point(-5, -6), a0, (0, 1))
  ip.mainloop()

def test_03():
  """02: Test for Line, Ray and Segment.
>>> test_03()
Traceback (most recent call last):
  ...
IndexError: VarText needs more objects to complete the string
"""
  ip = InteractivePlane(test_01.__doc__)
  Line(Point(5, 6), Point(-5, 6))


def test_03():
  """03: Test for MidPoints and MidPoint."""

def test_05():
  """05: Test for Circle."""

def test_08():
  """08: Test for PointOn and ConstrainedPoint."""

def test_09():
  """09: Test for Orthogonal and Parallel."""

def test_09a():
  """09a: Test for Parallel and Orthogonal when not exist line."""

def test_10():
  """10: Test for Polygonal, CurviLine and Polygon."""

def test_11():
  """11: Test for Calc."""

def test_12():
  """12: Test for Point."""

def test_13():
  """13: newPoint:

def test_14():
  """14: Test for object data."""

def test_15():
  """15: Test for Vector and Vector algebra."""

def test_16():
  """16: Test for Point algebra."""

def test_17():
  """17: Test for Segment Intersection."""

def test_18():
  """18: Test for not visible objects name."""

def test_19():
  """19: Test for Angle(Point, Point, Angle), sum."""

def test_20():
  """20: Test for Angle(Point, Point, Angle), rotation."""

def test_20a():
  """20a: Test for Angle(Point, Point, Angle), rotation."""

'''
loc = locals()

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True,
                    optionflags=doctest.ELLIPSIS |
                    doctest.NORMALIZE_WHITESPACE)
#  test_04()
#  test_09a()
#  test_20()
#  alltests(locals(), 0)
