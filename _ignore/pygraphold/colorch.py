#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
#-------------------------------python----------------------colorch.py--#
#                                                                       #
#                          Scelta del colore                            #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""Palette for color choice"""

try:
#  pyversion = 'py2'
  import Tkinter as tk
  input = raw_input
except ImportError:
#  pyversion = 'py3'
  import tkinter as tk
from pyplot import Plot

####
# Costanti relative a dimensioni e posizioni
###
DIMCOLOR=16
RANGE=("00", "20", "40", "60", "80", "a0", "c0", "e0", "ff")
ICOLORI=("R", "G", "B")
STRINGCOLOR=("#{}0000", "#00{}00", "#0000{}")
##R=0
##G=1
##B=2

class Palette(tk.Frame):
  """Classe che realizza l'applicazione."""
  
  def __init__(self, master):
    """Inizializza i vari attributi, crea i widgets e collega gli eventi."""
    tk.Frame.__init__(self, master, relief=tk.RIDGE, bd=4)
    self.rgb=["00", "00", "00"]
    self.color="#000000"
    self.cursori = self.createWidgets()
    self.bindhandlers()
##    self.p.move(self.cr, 3*DIMCOLOR, 3*DIMCOLOR)

  def createWidgets(self):
    """Crea i widgets."""
    top=self.master
    self.p=tk.Canvas(top, width=DIMCOLOR*3, height=DIMCOLOR*12)
    self.p.grid(row=0, column=0)
    # tavolozza
    cursori=[]
    for j in range(3):
      sc=STRINGCOLOR[j]
      x1=j*DIMCOLOR; x2=(j+1)*DIMCOLOR
      self.p.create_text((x1+x2)/2, DIMCOLOR/2, text=ICOLORI[j])
      for i, c in enumerate(RANGE):
        self.p.create_rectangle(x1, (i+1)*DIMCOLOR, x2, (i+2)*DIMCOLOR,
                                fill=sc.format(c))
      cursori.append(self.p.create_rectangle(x1+2, DIMCOLOR+2,
                                             x2-1, 2*DIMCOLOR-1,
                                             outline="white"))
    # risultato
    self.rcolor=self.p.create_rectangle(0, 10*DIMCOLOR,
                                         3*DIMCOLOR, 12*DIMCOLOR,
                                         fill=self.color)
    return cursori


  def bindhandlers(self):
    """Collega gli eventi ai metodi."""
    self.p.bind("<1>", self.mouseDown)

  def mouseDown(self, event):
    """Chiamato quando viene premuto il mouse sul cursore x.
    Gestiscono la scelta della componente del colore."""
    color=int(event.x/DIMCOLOR)
    gradazione=int(event.y/DIMCOLOR-1)
    if 0<=color<3 and 0<=gradazione<9:
      self.p.coords(self.cursori[color],
                    color*DIMCOLOR+2, (gradazione+1)*DIMCOLOR+2,
                    (color+1)*DIMCOLOR-1, (gradazione+2)*DIMCOLOR-1)
      self.rgb[color]=RANGE[gradazione]
      self.color="#{}{}{}".format(*self.rgb)
      self.p.itemconfig(self.rcolor, fill=self.color)
      self.event_generate("<Pause>")

  def getcolor(self):
    return self.color

def main():
  root=tk.Tk()
  a=Palette(root)
  root.mainloop()

if __name__ == "__main__" : main ()
