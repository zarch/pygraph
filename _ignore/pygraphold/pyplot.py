#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------python-----------------------pyplot.py--#
#                                                                       #
#                    traccia grafici di funzioni                        #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Plot functions
"""

from __future__ import division, print_function
#from pygrapherror import PlotError
# import pygrapherror as pyerr
import math
#import Tkinter as tk
import colors
from pycart import Plane, Pen, NaN, INF

def version():
    """Return pyplot version."""
    return '3.1.00'

class PlotPlane(Plane):
    """Cartesian plane for plotting function."""
    def __init__(self, name="Functions Plotter",
                 w=400, h=400,
                 sx=20, sy=None,
                 ox=None, oy=None,
                 axes=True, grid=True,
                 axescolor='black', gridcolor='black',
                 color='white',
                 parent=None,
                 x_min=0, x_max=1, y_min=0, y_max=1,
                 margin=10, numxlab=0, numylab=0):
        Plane.__init__(self, name,
                       w, h, sx, sy, ox, oy,
                       axes, grid, axescolor, gridcolor, color, parent)
        p = Plot()
        if numylab:
            posx = -margin / 2
            incposy = (y_max - y_min) / numylab
            for count in range(numylab + 1):
                p.drawtext("{0}".format(round(y_min + count / numylab, 4)), 
                           (posx, count * incposy))
        if numxlab:
            posy = -float(margin) / h / 2
            incposx = (w - 2 * margin) / numxlab
            deltax = (x_max - x_min) / numxlab
            for count in range(numxlab + 1):
                p.drawtext("{0}".format(round(x_min + count * deltax, 4)), 
                           (count * incposx, posy))
        self._plotters = []

    def newPlot(self, color='black', width=1):
        """Return a new plot in this plane."""
        return Plot(color, width, self)

    def plotters(self):
        """Return all plotters."""
        return self._plotters[:]

    def delplotters(self):
        """Delete all plotters."""
        self._plotters = []

def _eval(f, *var):
    """Eval function f."""
    try:
        return f(*var)
    except ZeroDivisionError:
        return INF
    except (ValueError, OverflowError):
        return NaN

class Plot(Pen):
    """Class for plotting functions."""
    def __init__(self, color='black', width=1, plane=None):
        Pen.__init__(self, color=color, width=width, plane=plane)
#    self._init()
#
#  def _init(self):
#    """Initialize all attributes."""
#    self._width = self._defwidth
#    self._color = self._defcolor

    def _drawto(self, pos, color, width):
        """Set pen position to pos and draw line from last position to pos."""
        poss0 = self._poss
        self._poss = self._plane._p2s(pos)
        try:
            self._canvas.create_line(poss0, self._poss,
                                   width=width, capstyle='round',
                                   fill=color)
            self._canvas.update()
    #    except tk.TclError:
        except:
            pass

    def xy(self, f, color=None, width=None):
        """y=f(x).
    
        example:
          >>> p=Plot()
          >>> p.xy(".2*x*x+2*x-3")
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        inf_dom = self._plane._s2x(0)
        self._poss = (NaN, NaN)
        for x1 in range(int(self._plane.getcanvaswidth())):
            x = self._plane._s2x(x1)
            y = _eval(f, x)
            self._drawto((x, y), color, width)
            if type(y) == str:
                if defined:
                    if y == INF:
                        print("Probable asymptote for x={0}".format(x))
                    if inf_dom != x:
                        print("Defined from {0} to {1}".format(inf_dom, 
                                                         self._plane._s2x(x1-1)))
                    defined = False
            else:
                if not defined:
                    defined = True
                    inf_dom = x
        print("Defined from {0} to {1}".format(inf_dom, x))

    def yx(self, f, color=None, width=None):
        """y=f(x).
    
        example:
          >>> p=Plot()
          >>> p.xy(".2*x*x+2*x-3")
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        inf_dom = self._plane._s2y(0)
        self._poss = (NaN, NaN)
        for y1 in range(int(self._plane.getcanvasheight())):
            y = self._plane._s2y(y1)
            x = _eval(f, y)
            self._drawto((x, y), color, width)
            if type(x) == str:
                if defined:
                    if x == INF:
                        print("Probable asymptote for y={0}".format(y))
                    if inf_dom != y:
                        print("Defined from {0} to {1}".format(inf_dom, 
                                                         self._plane._s2y(y1-1)))
                    defined = False
            else:
                if not defined:
                    defined = True
                    inf_dom = y
        print("Defined from {0} to {1}".format(inf_dom, y))

    def polar(self, f, ma=360, color=None, width=None):
        """ro=f(th).
    
        example:
          >>> p=Plot()
          >>> p.polar("th", 720)
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        inf_dom = 0
        conv = math.pi/180
        self._poss = (NaN, NaN)
        for i in range(0, ma):
            th = i*conv
            ro = _eval(f, th)
            self._drawto((ro*math.cos(th), ro*math.sin(th)), color, width)
            if type(ro) == str:
                if defined:
                    if ro == INF:
                        print("Probable point to the infinite for th={0}".format(th))
                    if inf_dom != th:
                        print("Defined from {0} to {1}".format(inf_dom, th-1))
                    defined = False
            else:
                if not defined:
                    defined = True
                    inf_dom = th
        print("Defined from {0} to {1}".format(inf_dom, th))

    def param(self, fx, fy, mi=0, ma=100, color=None, width=None):
        """x=fx(t); y=fy(t).
    
        example:
          >>> p=Plot()
          >>> p.param("5+2*t", "1+3t", -50, 50)
        """
        color = self._colordefault(color)
        width = width or self._width
        defined = True
        inf_dom = mi
        self._poss = (NaN, NaN)
        for t in range(int(mi), int(ma)):
            x = _eval(fx, t)
            y = _eval(fy, t)
            self._drawto((x, y), color, width)
            if type(x) == str or type(x) == str:
                if defined:
                    if x == INF or y == INF:
                        print("Probable point to the infinite for t={0}".format(t))
                    if inf_dom != t:
                        print("Defined from {0} to {1}".format(inf_dom, t-1))
                    defined = False
            else:
                if not defined:
                    defined = True
                    inf_dom = t
        print("Defined from {0} to {1}".format(inf_dom, t))

    def ny(self, f, trace=False, values=False, color=None, width=None):
        """y=f(n).
    
        example:
          >>> p=Plot()
          >>> p.ny("3*sin(3*n)")
        """
        numbers = [_eval(f, float(ni)) for ni in 
                    range(0, int(self._plane._s2x(self._plane.getcanvaswidth())))]
        self._succ(numbers, trace, values, color, width)

    def succ(self, a0, fan1, trace=False, values=False,
             color=None, width=None):
        """y=f(n).
    
        example:
          >>> p=Plot()
          >>> p.succ(50, lambda an, n, a0: (an+a0/an)/2, values=True)
        """
        numbers = [a0]
        an = a0
        for ni in range(1, int(self._plane._s2x(self._plane.getcanvaswidth()))):
            n = float(ni)
            try:
                an = _eval(fan1, an)
            except TypeError:
                try:
                  an = _eval(fan1, an, n)
                except TypeError:
                  try:
                    an = _eval(fan1, an, n, a0)
                  except Exception:
                    raise Exception('error in function evaluation')
            numbers.append(an)
        self._succ(numbers, trace, values, color, width)

    def _succ(self, numlist, trace=False, values=False,
              color=None, width=None):
        """a(n+1)=fa1(an).
    
        example:
          >>> p=Plot()
          >>> p.succ([3, 5, 2, 'infinity', 9, 4, 'NaN', 7])
        """
        color = self._colordefault(color)
        width = width or self._width
        if trace:
            self._poss = (NaN, NaN)
            for n, a in enumerate(numlist):
                self._drawto((n, a), color='#909090', width=1)
            for n, a in enumerate(numlist):
                if values:
                    print(a)
                self.drawpoint((n, a), color, width)

    def xpoints(self, x_coord, y_list,
                color=None, width=None):
        """a(n+1)=fa1(an).
    
        example:
          >>> p=Plot()
          >>> p.succ(3, [3, 5, 2, 'infinity', 9, 4, 'NaN', 7])
        """
        color = self._colordefault(color)
        width = width or self._width
        for  y in y_list:
            a_x, a_y = self._plane._p2s((x_coord, y))
            if a_x != 'NaN':
                b_x, b_y = a_x, a_y + 1
                self._canvas.create_line((a_x, a_y), (b_x, b_y),
                                         width=width, capstyle='round',
                                         fill=color)
        self._canvas.update()


if __name__ == "__main__":
  from sys import path
  from os.path import join
  path.append(join('..','test'))
  import test_pyplot
  test_pyplot.alltests(test_pyplot.loc)
