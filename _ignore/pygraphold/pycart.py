#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python-----------------------pycart.py--#
#                                                                       #
#                          Cartesian plane                             #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Classes Plane and Pen for pygraph
"""

try:
#  pyversion = 'py2'
    import Tkinter as tk
    input = raw_input
except ImportError:
#  pyversion = 'py3'
    import tkinter as tk
import os
#from pil.Image import open as imopen
#from PIL.Image import open as imopen
#from pillow import open as imopen
from io import OpenWrapper

from pygrapherror import PlaneError
import colors

NaN = 'NaN'          # Not a Number
NaP = ('NaN', 'NaN') # Not a Point
OoB = 'OoB'          # Out of Bounds
INF = 'Inf'          # Infinity

def version():
    """Return version of pycart."""
    return '3.1.00'

def _defaultvalue(value, default):
    """Return value if not none else default."""
    if value == None:
        return default
    else:
        return value
# callbacks
def onkeypress(event):
    pass

def onkeyrelease(event):
    pass

def onenter(event):
    pass

def onleave(event):
    pass

def onpress1(event):
    pass

def onpress2(event):
    pass

def onpress3(event):
    pass

def onmotion1(event):
    pass

def onmotion2(event):
    pass

def onmotion3(event):
    pass

def onrelease1(event):
    pass

def onrelease2(event):
    pass

def onrelease3(event):
    pass

class Plane(object):
    """Class for cartesian plane.

    Use sample:
    >>> from pycart import Plane
    >>> piano = Plane()

    """
    _planes = []
    def __init__(self, name="Cartesian Plane",
                 w=600, h=400,
                 sx=20, sy=None,
                 ox=None, oy=None,
                 axes=True, grid=True,
                 axescolor='black', gridcolor=None,
                 color='white',
                 parent=None):
        Plane._planes.append(self)
        if parent:
            self._parent = parent
        else:
            self._parent = tk.Tk()
    #        self._parent.geometry(str(w+2)+'x'+str(h+2)+'+0+0')
            self._parent.geometry(str(w+2)+'x'+str(h+2))
            self._parent.title(name)
        self._canvas = tk.Canvas(self._parent, bg=color, width=w, height=h)
        if not parent:
            self._canvas.pack(side=tk.TOP, expand=1, fill=tk.BOTH)
        self._canvaswidth = w
        self._canvasheight = h
        if ox == None:
            ox = w//2
        if oy == None:
            oy = h//2
        self._deforigin = (ox, oy)
        if sy == None:
            sy = sx
        self._defscale = (sx, sy)
        self._defaxes = axes
        self._defgrid = grid
        self._defaxescolor = axescolor
        if gridcolor == None:
            self._defgridcolor = axescolor
        else:
            self._defgridcolor = gridcolor
        self._pens = []
        self._init()
        self._canvas.bind("<KeyPress>", onkeypress)
        self._canvas.bind("<KeyRelease>", onkeyrelease)
        self._canvas.bind("<Enter>", onenter)
        self._canvas.bind("<Leave>", onleave)
        self._canvas.bind("<Button-1>", onpress1)
        self._canvas.bind("<Button-2>", onpress2)
        self._canvas.bind("<Button-3>", onpress3)
        self._canvas.bind("<B1-Motion>", onmotion1)
        self._canvas.bind("<B2-Motion>", onmotion2)
        self._canvas.bind("<B3-Motion>", onmotion3)
        self._canvas.bind("<ButtonRelease-1>", onrelease1)
        self._canvas.bind("<ButtonRelease-2>", onrelease2)
        self._canvas.bind("<ButtonRelease-3>", onrelease3)
        self._canvas.focus_force()
        self._canvas.update()
    #    self._canvas.bind("<Any-Enter>", self._mouseEnter)
    #    self._canvas.bind("<Key>", key)

    def _init(self):
        """(Re)initialize some arguments."""
        self._origin = self._deforigin
        self._scale = self._defscale
        self._setgxgy()
        self._withaxes = self._defaxes
        self._withgrid = self._defgrid
        self._axescolor = self._defaxescolor
        self._gridcolor = self._defgridcolor
        self._setgxgy()
        if self._withaxes:
            self.axes()
        if self._withgrid:
            self.grid()

    def _setorigin(self, point):
        """Set origin."""
        self._origin = point
        self._setgxgy()
    def _getorigin(self):
        """Gets origin."""
        return self._origin
    origin = property(_getorigin, _setorigin)

    def _setscale(self, scale):
        """Sets scale."""
        self._scale = scale
        self._setgxgy()
    def _getscale(self):
        """Gets scale."""
        return self._scale
    scale = property(_getscale, _setscale)

    def _setcolor(self, color):
        """Sets color."""
        self._color = color
        self._canvas['bg'] = color
    def _getcolor(self):
        """Gets color."""
        return self._canvas['bg']
    color = property(_getcolor, _setcolor)

    def _inside(self, point):
        """point is inside the window."""
        x_p, y_p = point
        return (0 <= x_p <= self._canvaswidth and 
                0 <= y_p <= self._canvasheight)

    def _p2s(self, point):
        """Translate point coords to pixel coords."""
        x_p, y_p = point
        x_o, y_o = self.origin
        x_s, y_s = self.scale
        try:
            return int(round(x_p, 8)*x_s+x_o), int(-round(y_p, 8)*y_s+y_o)
        except:
            return NaN, NaN

    def _r2s(self, rect):
        """Translate rectangular coords to screen region coords."""
        x_a, y_a, x_b, y_b = rect
        x_o, y_o = self.origin
        x_s, y_s = self.scale
        try:
            return (int(round(x_a, 8)*x_s+x_o), int(round(-y_a, 8)*y_s+y_o),
                    int(round(x_b, 8)*x_s+x_o), int(round(-y_b, 8)*y_s+y_o))
        except:
            return NaN, NaN, NaN, NaN

    def _s2p(self, ps):
        """Translate pixel coords to point coords."""
        x_ps, y_ps = ps
        x_o, y_o = self.origin
        x_s, y_s = self.scale
        return (float(x_ps)-x_o)/x_s, (float(-y_ps)+y_o)/y_s

    def _s2r(self, rs):
        """Translate screen region coords to rectangular coords."""
        x_as, y_as, x_bs, y_bs = rs
        x_o, y_o = self.origin
        x_s, y_s = self.scale
        return ((float(x_as)-x_o)/x_s,
                (float(-y_as)+y_o)/y_s,
                (float(x_bs)-x_o)/x_s,
                (float(-y_bs)+y_o)/y_s)

    def _s2x(self, x_ps):
        """Translate screen xcoord to point xcoord."""
        return (float(x_ps) - self._origin[0]) / self._scale[0]

    def _s2y(self, y_ps):
        """Translate screen ycoord to point ycoord."""
        return (float(-y_ps) + self._origin[1]) / self._scale[1]

    def _x2s(self, x):
        """Translate point xcoord to screen xcoord."""
        return int(round(x * self._scale[0] + self._origin[0]))

    def _y2s(self, y):
        """Translate point ycoord to screen ycoord."""
        return int(round(-y * self._scale[1] + self._origin[1]))

    def _setgxgy(self):
        """Set self.gx and self.gy."""
        w = self._canvaswidth
        h = self._canvasheight
        x_o, y_o = self._origin
        x_s, y_s = self._scale
        if x_s > 2:
            self._gx = tuple(range(x_o, 0, -x_s)) + tuple(range(x_o, w, x_s))
        else:
            self._gx = []
        if y_s > 2:
            self._gy = tuple(range(y_o, 0, -y_s)) + tuple(range(y_o, h, y_s))
        else:
            self._gy = []

    def mainloop(self):
        """Expose mainloop()."""
        self._canvas.mainloop()
    #    Plane._planes.remove(self)
        Plane._planes = []

    def after(self, *args):
        """Expose after()."""
        self._canvas.after(*args)

    def axes(self, color=None, ax=True, ay=True):
        """Draw cartesian plane axes."""
        color = _defaultvalue(colors.codecolor(color), self._axescolor)
        canvas = self._canvas
        w = self._canvaswidth
        h = self._canvasheight
        x_o, y_o = self.origin
        if ax:
          canvas.create_line(0, y_o, w, y_o, fill=color)
          canvas.create_line(w - 10, y_o-5, w, y_o, fill=color)
          canvas.create_line(w - 10, y_o+5, w, y_o, fill=color)
          ymi, yma = y_o - 1, y_o + 2
          for i in self._gx:
            canvas.create_line(i, ymi, i, yma, fill=color)
        if ay:
          canvas.create_line(x_o, h, x_o, 0, fill=color)
          canvas.create_line(x_o - 5, 10, x_o, 0, fill=color)
          canvas.create_line(x_o + 5, 10, x_o, 0, fill=color)
          xmi, xma = x_o - 1, x_o + 2
          for i in self._gy:
            canvas.create_line(xmi, i, xma, i, fill=color)
        canvas.update()

    def grid(self, color=None):
        """Draw dot grid."""
        color = _defaultvalue(colors.codecolor(color), self._gridcolor)
        canvas = self._canvas
        for i in self._gx:
          for j in self._gy:
            canvas.create_line(i, j, i+1, j+1, fill=color)
        canvas.update()

    def newPen(self, x=0, y=0, color='black', width=1):
        """Return a new pen in this plane."""
        return Pen(x, y, color, width, self)

    def delete(self, *args):
        """Delete *args from this plane."""
        self._canvas.delete(*args)

    def clean(self):
        """Clean grapic windows without reset pens."""
        for i in self._canvas.find_all():
          self._canvas.delete(self, i)
        self._canvas.update()

    def reset(self):
        """Reset graphic window."""
        for t in self._pens:
          t.reset()
        self.clean()
        self._init()

    def getcanvaswidth(self):
        """Get canvas width."""
        return self._canvaswidth

    def getcanvasheight(self):
        """Get canvas height."""
        return self._canvasheight

    def getcanvas(self):
        """Get canvas."""
        return self._canvas

    def save(self, filename):
        """Save graphic window to png or ps file."""
        nfps = filename+'.ps'
        nfpng = filename+'.png'
        self._canvas.postscript(file=nfps)
        try:
            from PIL.Image import open as imopen
            imopen(nfps).save(nfpng)
        except:
            pass
        os.remove(nfps)

    def _repr_png_(self):
        """For ipython notebook rapresentation."""
        pngfile = '_repr_png'
        self.save(pngfile)
        with OpenWrapper(pngfile, mode='rb') as data:
          result = data.read()
        os.remove(pngfile)
        return result

    """For ipython notebook tratto dal lavoro di Pietro, da sistemare

  def r_export(rast, output='', fmt='png', **kargs):
    from grass.pygrass.modules import Module
    if rast.exist():
      output = output if output else "%s_%s.%s" % (rast.name, rast.mapset,
                                                   fmt)
      Module('r.out.%s' % fmt, input=rast.fullname(), output=output,
             overwrite=True, **kargs)
      return output
    else:
      raise ValueError('Raster map does not exist.')
      
  def raw_figure(figpath):
    with io.OpenWrapper(figpath, mode='rb') as data:
      res = data.read()
      return res	

  def _repr_png_(self):
    return raw_figure(functions.r_export(self))


   fine del pezzo da sistemare"""

class Pen(object):
    """Class for a drawing pointer in the cartesian plane."""
    def __init__(self, x=0, y=0, color='black', width=1, plane=None):
      if plane == None:
          try:
              self._plane = Plane._planes[-1]
          except IndexError:
              raise PlaneError("Ther are no Planes")
      else:
          self._plane = plane
      self._plane._pens.append(self)
      self._defx = x
      self._defy = y
      self._defcolor = colors.codecolor(color)
      self._defwidth = width
      self._canvas = self._plane.getcanvas()
      self.reset()

    def reset(self):
        """Initialize all attributes."""
        self._pos = self._defx, self._defy
        self._poss = self._plane._p2s(self._pos)
        self._width = self._defwidth
        self._color = self._defcolor

    def _colordefault(self, color):
        """Return color or default color."""
        if color == None:
            return self._color
        else:
            return colors.codecolor(color)

    def _setcolor(self, color):
        """Set pen color."""
        self._color = colors.codecolor(color)
    def _getcolor(self):
        """Get pen color."""
        return colors.codecolor(self._color)
    color = property(_getcolor, _setcolor)

    def _setwidth(self, width):
        """Set pen width."""
        self._width = width
    def _getwidth(self):
        """Get pen width."""
        return self._width
    width = property(_getwidth, _setwidth)

    def _setpos(self, position):
        """Set pen position to position."""
        self._pos = position
        self._poss = self._plane._p2s(position)
    def _getpos(self):
        """Get pen _penposition."""
        return self._pos
    position = property(_getpos, _setpos)

    def drawto(self, position, color=None, width=None):
        """Set pen position to position and draw line from last position 
        to position."""
        color = self._colordefault(color)
        width = width or self._width
        self._pos = position
        poss0 = self._poss
        self._poss = self._plane._p2s(position)
        try:
            _id = self._canvas.create_line(poss0, self._poss,
                                           width=width, capstyle='round',
                                           fill=color)
            self._canvas.update()
            return _id
        except tk.TclError:
            return None

    def drawsegment(self, point0, point1=None, color=None, width=None):
        """Draw a segment from point0 to point1 or from pen position to point0
        has no effect on the pen position."""
        color = self._colordefault(color)
        width = width or self._width
        if point1 == None: point0, point1 = self._pos, point0
        pps0 = self._plane._p2s(point0)
        pps1 = self._plane._p2s(point1)
        _id = self._canvas.create_line(pps0, pps1,
                                       width=width, capstyle='round',
                                       fill=color)
        self._canvas.update()
        return _id

    def drawpoint(self, position=None, color=None, width=None):
        """Draw a point in position or in actual position."""
        color = self._colordefault(color)
        width = width or self._width
        if position == None: position = self.position
        x, y = self._plane._p2s(position)
        try:
            0 + x + y
        except TypeError:
            return
    #      raise ValueError, "{} or {} is not a number".format(x, y)
        _id = self._canvas.create_oval((x-width, y-width, x+width, y+width),
                                     fill=color)
        self._canvas.update()
        return _id

    def drawcircle(self, radius, 
                   center=None, color=None, width=None, incolor=''):
        """Draw a circle with centere in center or in pen position."""
        color = self._colordefault(color)
        width = width or self._width
    #    if incolor == None: incolor = self._color
        incolor = colors.codecolor(incolor)
        if center == None:  center = self._pos
        x, y = self._plane._p2s(center)
        radius *= self._plane.scale[0]
        _id = self._canvas.create_oval((x-radius, y-radius, 
                                        x+radius, y+radius),
                                       outline=color, width=width, 
                                       fill=incolor)
        self._canvas.update()
        return _id

    def _getv(self, vertices):
        """Return vertex translate tu screen coords."""
        vv = []
        for x, y in vertices:
            vv.append(self._plane._x2s(x))
            vv.append(self._plane._y2s(y))
        return tuple(vv)

    def drawpoly(self, vertices, color=None, width=None, incolor=''):
        """Draw polygon from data vertices, move pen."""
    #    print(vertices, color, width, incolor)
        color = self._colordefault(color)
        width = width or self._width
        incolor = colors.codecolor(incolor)
    #    print(vertices, color, width, incolor)
        _id = self._canvas.create_polygon(self._getv(vertices),
                                          outline=color,
                                          width=width, fill=incolor)
        self.position = vertices[0]
        self._canvas.update()
        return _id

    def drawtext(self, text, position=None, color=None, width=None):
        """Draw a text in position or in actual position."""
        color = self._colordefault(color)
        width = width or self._width
        if position == None: position = self.position
        _id = self._canvas.create_text(self._plane._p2s(position), 
                                       text=text, fill=color, 
                                       font=("Nimbus Sans L", int(width*8)))
        self._canvas.update()
        return _id

if __name__ == "__main__":
    from sys import path #, argv
    from os.path import join
    path.append(join('..','test'))
    import test_pycart 
    test_pycart.alltests(test_pycart.loc)
