#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
#-------------------------------python----------------------viewfun.py--#
#                                                                       #
#                     Visualizzatore di funzioni                        #
#                                                                       #
# Copyright (c) 2011 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""functions plotter."""

from __future__ import print_function

_viewfun_version='02.09.02'

try:
#  pyversion = 'py2'
  import Tkinter as tk
  input = raw_input
except ImportError:
#  pyversion = 'py3'
  import tkinter as tk
#import os

from pyplot import PlotPlane
from colorch import Palette
from math import *              # per le funzioni matematiche

####
# Costanti relative a dimensioni e posizioni
###
DIMX=DIMY=600
HEIGHT=30
GRID=True
POSX= DIMX/2
POSY= HEIGHT/4*3
DIMCUR = HEIGHT/2
COLORDES= "blue"
COLORSEL= "red"
P1X_CX, P1Y_CX, P2X_CX, P2Y_CX = POSX, POSY, POSX, POSY-DIMCUR
P1X_CY, P1Y_CY, P2X_CY, P2Y_CY = HEIGHT-POSY, POSX, HEIGHT-POSY+DIMCUR, POSX
D=3
P1X_XY, P1Y_XY, P2X_XY, P2Y_XY = POSX-D, POSX-D, POSX+D, POSX+D

####
# Costanti che definiscono nomi
###
CURSORE=0
ENTRY=1

def onrange(n, mi, ma):
  if n<mi:   return mi
  elif n>ma: return ma
  else:      return n

class App(tk.Frame):
  """Classe che realizza l'applicazione."""
  
  def __init__(self, master):
    """Inizializza i vari attributi, crea i widgets e collega gli eventi."""
    tk.Frame.__init__(self, master)
    self.x=0.
    self.y=0.
    self.f=""
    self.ox=DIMX/2
    self.oy=DIMY/2
    self.scala=20
    self.punti=[]
    self.colore="#000000"
    self.sx=tk.StringVar()
    self.sx.set(str(self.x))
    self.sy=tk.StringVar()
    self.sy.set(str(self.y))
    self.sox=tk.StringVar()
    self.sox.set(self.ox)
    self.soy=tk.StringVar()
    self.soy.set(self.oy)
    self.sscala=tk.StringVar()
    self.sscala.set(str(self.scala))
    self.createWidgets()
    self.bindhandlers()

  def createWidgets(self):
    """Crea i widgets."""

    def insor(m, r, c, t, s, w, tv=None):
      """Inserisce all'interno del master m nella riga r colonna c, un entry
      formato da un'etichetta t, seguita da un oggetto Entry collegato
      alla variabile tv."""
      tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
      e=tk.Entry(m, width=w, textvariable=tv)
      e.grid(row=r, column=c+1, sticky=s)
      return e

    def insve(m, r, c, t, s, w, tv=None):
      """Inserisce all'interno del master m nella riga r un entry
      formato da un'etichetta t, con sotto un oggetto Entry collegato
      alla variabile tv."""
      tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
      e=tk.Entry(m, width=w, textvariable=tv)
      e.grid(row=r+1, column=c, sticky=s)
      return e

    def visuaor(m, r, c, t, s, w, tv=None):
      """Visualizza due etichette affiancate,
      la seconda collegata alla variabile tv."""
      tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
      tk.Label(m, width=w, textvariable=tv).grid(row=r, column=c+1, sticky=s)

    def visuave(m, r, c, t, s, w, tv=None):
      """Visualizza due etichette sovrapposte,
      la seconda collegata alla variabile tv."""
      tk.Label(m, text=t).grid(row=r, column=c, sticky=s)
      tk.Label(m, width=w, textvariable=tv).grid(row=r+1, column=c, sticky=s)

    top=self.master
# Pannello principale (pp)
    pp=tk.Frame(top)
    pp.grid(row=0, column=0)
# Pannello comandi (pc)
    pc=tk.Frame(top, relief=tk.RIDGE, bd=4)
    pc.grid(row=0, column=1, sticky=tk.N+tk.S)
# Pannello Funzione (pf)
    pf=tk.Frame(pp)
    pf.grid(row=0, column=0)
# Pannello input-output (pio)
    pio=tk.Frame(pp)
    pio.grid(row=1, column=0)
# Pannello y (py)
    py=tk.Frame(pio, relief=tk.RIDGE, bd=4)
    py.grid(row=0, column=0)
# Pannello x (px)
    px=tk.Frame(pio, relief=tk.RIDGE, bd=4)
    px.grid(row=2, column=2)
# Contenuto di pf
    self.function=insor(pf, 0, 0, "Funzione:  y=", tk.W, 80, self.f)
    self.function.focus()
# Contenuto di pc
  # Pannello modifica (pcm)
    pcm=tk.Frame(pc, relief=tk.RIDGE, bd=4)
    pcm.grid(row=0, column=0, sticky=tk.N)
    l=tk.Label(pcm, text="Modifica parametri")
    l.grid(row=0, column=0, columnspan=2)
    self.eox=insor(pcm, 1, 0, "Ox= (1-{})".format(DIMX), tk.N, 8, self.sox)
    self.eoy=insor(pcm, 2, 0, "Oy= (1-{})".format(DIMY,), tk.N, 8, self.soy)
    self.escala=insor(pcm, 3, 0, "Scala=", tk.N, 8, self.sscala)
  # Pannello colori (pcc)
    pcc=tk.Frame(pc, relief=tk.RIDGE, bd=4)
    pcc.grid(row=1, column=0, sticky=tk.N)
    self.tavolozza=Palette(pcc)
    self.tavolozza.grid(row=0, column=0)
  # Pannello bottoni (pcb)
    pcb=tk.Frame(pc, relief=tk.RIDGE, bd=4)
    pcb.grid(row=3, column=0, sticky=tk.N)
    d=tk.Button(pcb, text='Disegna', foreground='blue', command=self.draw)
    d.grid(row=0, column=0)
    d=tk.Button(pcb, text='Cancella', foreground='blue', command=self.clean)
    d.grid(row=1, column=0)
    q=tk.Button(pcb, text='Esci', foreground='red', command=self.quit)
    q.grid(row=2, column=0)

# Contenuto di pio
  # Piano cartesiano
    self.pp=PlotPlane(name="", w=DIMX, h=DIMY, sx=self.scala, sy=self.scala,
                      axes=True, grid=GRID, parent=pio)
    self.canvas=self.pp.getcanvas()
    self.canvas.grid(row=0, column=2)
    self.plot=self.pp.newPlot()
  # Cursore XY
    self.curxy=self.canvas.create_oval(P1X_XY, P1Y_XY, P2X_XY, P2Y_XY,
                                     outline=self.colore)
  # Cursore Y
    self.pcury=tk.Canvas(pio, width=HEIGHT, height=DIMY)
    self.pcury.grid(row=0, column=1)
    self.cury=self.pcury.create_line(P1X_CY, P1Y_CY, P2X_CY, P2Y_CY,
                                     fill=COLORDES, arrow='last')
  # Cursore X
    self.pcurx=tk.Canvas(pio, width=DIMX, height=HEIGHT)
    self.pcurx.grid(row=1, column=2)
    self.curx=self.pcurx.create_line(P1X_CX, P1Y_CX, P2X_CX, P2Y_CX,
                                     fill=COLORDES, arrow='last')
  # x e y
    self.ey=visuaor(py, 0, 0, "y=", None, 20, self.sy )
    self.ex=insve(px, 0, 0, "x", None, 12, self.sx)
  # Messaggio (pcm)
    self.mess=tk.Text(pio, width=30, height=7, fg="red", state=tk.DISABLED)
    self.mess.grid(row=1, column=0, rowspan=2, columnspan=2)

  def bindhandlers(self):
    """Collega gli eventi ai metodi."""
    self.pcurx.tag_bind(self.curx, "<Any-Enter>", self.mouseEnter)
    self.pcurx.tag_bind(self.curx, "<Any-Leave>", self.mouseLeave)
    self.pcurx.tag_bind(self.curx, "<1>", self.mouseDown)
    self.pcurx.tag_bind(self.curx, "<B1-Motion>", self.movex)
    self.bind_all("<Return>", self.aggiorna) # aggiungere KP_Enter
    self.bind_all("<FocusOut>", self.aggiorna)
    self.bind_all("<Pause>", self.aggiornacolore)

####
# Metodi che gestiscono il trascinamento del cursore x
###
  def mouseDown(self, event):
    """Chiamato quando viene premuto il mouse sul cursore x.
    Ricorda dove il mouse � stato premuto."""
    self.lastx = event.x
    e=tk.Event()
    e.widget=self.focus_get()
    self.aggiorna(e)

  def movex(self, event):
    """Chiamato quando il mouse trascina il cursore x."""
    self.pcurx.move(tk.CURRENT, event.x - self.lastx, 0)
    self.lastx = event.x
    self.x=self.pp._s2x(self.pcurx.coords(self.curx)[0])
    self.aggiornax(CURSORE)

  def mouseEnter(self, event):
    """Chiamato quando il mouse sormonta il cursore x."""
    self.pcurx.itemconfig(tk.CURRENT, fill=COLORSEL)
	
  def mouseLeave(self, event):
    """Chiamato quando il mouse esce dal cursore x."""
    self.pcurx.itemconfig(tk.CURRENT, fill=COLORDES)

  def quit(self):
    """Chiamato quando viene premuto il pulsante Esci."""
    self.master.quit()
    self.master.destroy()

  def aggiornax(self, da):
    """Chiamato quando viene modificato il valore di x.
    da indica chi ha provocato il cambiamento,
    se il cursore x o la casella di inserimento."""
    self.f=self.function.get()
    self.y=self.calcola(self.x, self.f)
    y=self.pp._y2s(self.y)
    x=self.pp._x2s(self.x)
    self.pcury.coords(self.cury, (P1X_CY, y, P2X_CY, y))
    self.canvas.coords(self.curxy, (x-D, y-D, x+D, y+D))
    if not (x, y) in self.punti:
      self.canvas.create_line(x, y, x+1, y+1, fill=self.colore)
      self.punti.append((x, y))
    self.lasty=y
    self.sx.set("{:7.2f}".format(self.x))
    self.sy.set("{:7.2f}".format(self.y))
    if da!=CURSORE:
      x=self.pp._x2s(self.x)
      self.pcurx.coords(self.curx, (x, P1Y_CX, x, P2Y_CX))

  def aggiorna(self, event):
    """Modifica x o il piano in base a quanto inserito dall'utente."""
    if event.widget == self.ex:
      self.x = float(self.ex.get())
      self.aggiornax(ENTRY)    
    else:
      if event.widget == self.eox:
        v = onrange(int(self.eox.get()), 1, DIMX)
        if v != self.ox:
          self.ox = v
          self.sox.set(str(self.ox))
          self.redraw()
      elif event.widget == self.eoy:
        v = DIMY-onrange(int(self.eoy.get()), 1, DIMY)+1
        if v != self.oy:
          print(v, self.oy)
          self.oy = v
          self.soy.set(str(DIMY-self.oy+1))
          self.redraw()
      elif event.widget == self.escala:
        v=int(self.escala.get())
        if v != self.scala:
          self.scala = v
          self.redraw()

  def aggiornacolore(self, event):
    """Modifica x o il piano in base a quanto inserito dall'utente."""
    self.colore=self.tavolozza.getcolor()
    self.canvas.itemconfig(self.curxy, outline=self.colore)

  def redraw(self):
    self.pp.clean()
    self.pp.origin = (self.ox, self.oy)
    self.pp.scale = (self.scala, self.scala)
    self.pp.axes()
    self.pp.grid()
    self.x = 0.
    self.aggiornax(ENTRY)    
    sx = self.pp._x2s(self.x)
    sy = self.pp._y2s(self.y)
    self.pcurx.coords(self.curx, (sx, P1Y_CX, sx, P2Y_CX))
    self.pcury.coords(self.cury, (P1X_CY, sy, P2X_CY, sy))
    self.curxy = self.canvas.create_oval(sx-D, sy-D, sx+D, sy+D,
                                         outline=self.colore)
    self.punti = []

  def reset(self):
    self.pp.reset()
    self.aggiornax(ENTRY)
    self.curxy=self.canvas.create_oval(self.pp._x2s(self.x)-D,
                                       self.pp._y2s(self.y)-D,
                                       self.pp._x2s(self.x)+D,
                                       self.pp._y2s(self.y)+D,
                                       outline=self.colore)
    self.punti=[]

  def clean(self):
    self.x=self.y=0
    self.reset()

  def draw(self):
    """Disegna il grafico della funzione."""
    e=tk.Event()
    e.widget=self.focus_get()
    self.aggiorna(e)
    self.f=self.function.get()
    if self.f:
      self.plot.xy(lambda x: eval(self.f), self.colore)
      self.y=self.calcola(self.x, self.f)
      y=self.pp._y2s(self.y)
      x=self.pp._x2s(self.x)
      self.canvas.coords(self.curxy, (x-D, y-D, x+D, y+D))

  def setmess(self, m):
    self.mess.config(state=tk.NORMAL)
    self.mess.insert(tk.END, m)
    self.mess.config(state=tk.DISABLED)
    self.after(3000, self.resetmess)

  def resetmess(self):
    self.mess.config(state=tk.NORMAL)
    self.mess.delete(1.0, tk.END)
    self.mess.config(state=tk.DISABLED)

  def calcola(self, x, f):
    y=0
    if f:
      try:
        y=eval(f)
        return y
      except(ZeroDivisionError, e):
        self.setmess("Divisione per zero se x={}\n".format(x))
      except(ValueError, e):
        self.setmess("Errore nel calcolo se x={}\n".format(x))
      except(OverflowError, e):
        self.setmess("Errore di Overflow per x={}\n".format(x))
      except(NameError, e):
        self.setmess("Parola sconosciuta in {}\n".format(f))
      except(SyntaxError, e):
        self.setmess("Errore di sintassi in {}\n".format(f))
    return y

def main():
  root=tk.Tk()
  a=App(root)
  root.mainloop()


if __name__ == "__main__" : main ()
