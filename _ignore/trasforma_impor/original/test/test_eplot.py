# -*- coding: utf-8 -*-
#-------------------------------python-------------------test_eplot.py--#
#                                                                       #
#                           Test for eplot                              #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2010---#

from eplot import *

def test_00():
  plot.xy(lambda x: 0.3*x*x*x, color='brown', width=4)
  plot.yx(lambda y: 0.3*y*y*y, color='navy', width=4)

test_00()
plotplane.mainloop()

"""
# Altro modo

from eplot import plot as p, plotplane as pp

def test_01():
  p.xy(lambda x: 0.3*x*x, color='brown', width=4)
  p.yx(lambda y: 0.3*y*y, color='navy', width=4)

test_01()
pp.mainloop()

"""
