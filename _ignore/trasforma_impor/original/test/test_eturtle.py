# -*- coding: utf-8 -*-
#-------------------------------python-----------------test_eturtle.py--#
#                                                                       #
#                           Test for eplot                              #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2010---#

from eturtle import *

def spi(dim):
  if dim>300: return
  turtle.forward(dim)
  turtle.left(145)
  spi(dim+1)

turtle.color='blue'
spi(1)

turtleplane.mainloop()

"""

# Altro modo

from eturtle import *

def spi(dim):
  if dim>300: return
  t.forward(dim)
  t.left(145)
  spi(dim+1)

t.color='red'
spi(1)

tp.mainloop()

"""
"""

# Altro modo

from eturtle import turtleplane as tp, turtle as tina

def spi(dim):
  if dim>300: return
  tina.forward(dim)
  tina.left(145)
  spi(dim+1)

tina.color='green'
spi(1)

tp.mainloop()

"""
