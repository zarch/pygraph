#!/usr/bin/env python
#-------------------------------python------------------------demo.py--#
#                                                                       #
#                       Demo di turtlegraphics                          #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2002--#

from pyturtle import TurtlePlane, Turtle
tp = TurtlePlane()
t = Turtle()

def demo():
  t.tracer(1)
  t.up()
  t.back(100)
  t.down()
  # draw 3 squares; the last filled
  t.width = 3
  for i in range(3):
    if i == 2:
      t.fill(1)
    for j in range(4):
      t.forward(20)
      t.left(90)
    if i == 2:
      t.color = "maroon"
      t.fill(0)
    t.up()
    t.forward(30)
    t.down()
  t.up()
  t.forward(10)
  t.down()
  t.color = "black"
  t.circle(10)
  t.up()
#  t.fill(1)
  t.forward(30)
  t.fill(1)
  t.down()
  t.circle(10)
  t.color = "maroon"
  t.fill(0)
  t.width = 1
  t.color = "black"
  # move out of the way
  t.tracer(0)
  t.up()
  t.right(90)
  t.forward(100)
  t.right(90)
  t.forward(100)
  t.right(180)
  t.down()
  # some text
  t.up()
  t.write("startstart", 1)
  t.color = "green"
  t.write("start", 1)
  t.down()
  # staircase
  t.color = "red"
  for i in range(5):
    t.forward(20)
    t.left(90)
    t.forward(20)
    t.right(90)
  # filled staircase
  t.fill(1)
  for i in range(5):
    t.forward(20)
    t.left(90)
    t.forward(20)
    t.right(90)
  t.fill(0)
  # more text
  t.color = "green"
  t.write("end")
  
demo()
tp.mainloop()
