#!/usr/bin/env python
#--------------------------python-pyturtle----------------01casetta.py--#
#                                                                       #
#                              Casetta                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from pyturtle import TurtlePlane, Turtle

tp = TurtlePlane()
tina = Turtle()

tina.forward(100)
tina.left(90)
tina.forward(100)
tina.left(45)
tina.forward(71)
tina.left(90)
tina.forward(71)
tina.left(135)
tina.forward(100)
tina.right(135)
tina.forward(141)
tina.right(135)
tina.forward(100)
tina.right(135)
tina.forward(141)

tp.mainloop()
