#!/usr/bin/env python
#--------------------------python-pyturtle------------------2spirad.py--#
#                                                                       #
#             La spirale delle radici dei numeri interi                 #
#                                                                       #
#--Daniele Zambelli-------------GPL3-------------------------------2012-#

from __future__ import division, print_function
from pyturtle import TurtlePlane, Turtle

def spi():
    """Disegna una "spirale" poligonale."""
    for lato in range(200):
        tina.forward(lato)
        tina.left(121)

tp = TurtlePlane()
tina = Turtle()
spi()

tp.mainloop()
