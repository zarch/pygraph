#!/usr/bin/env python
#--------------------------python-pyturtle---------------05quadrati.py--#
#                                                                       #
#                             Quadrati                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from pyturtle import TurtlePlane, Turtle

def quadrato(lato):
  for i in range(4):
    tina.forward(lato)
    tina.left(90)

def quadrati(n):
  for i in range(10, n*10, 10):
    quadrato(i)

def griglia(n):
  for i in range(4):
    quadrati(n)
    tina.left(90)
'''
def fila(d):
  while d>2:
    quadrato(d)
    tina.forward(d)
    d=d/2.
'''

def fila(d):
  di=d
  while d>2:
    quadrato(d)
    tina.forward(d)
    d=d/2.
  while d<di:
    d=d*2.
    tina.back(d)

'''
def filar(d):
  if d<2: return
  quadrato(d)
  tina.forward(d)
  filar(d/2.)
'''

def filar(d):
  if d<2: return
  quadrato(d)
  tina.forward(d)
  filar(d/2.)
  tina.back(d)

  
def coda(d):
  di=d
  while d>2:
    quadrato(d)
    tina.forward(d)
    tina.right(20)
    d=d/2.
  while d<di:
    d=d*2.
    tina.left(20)
    tina.back(d)

def codar(d):
  if d<2: return
  quadrato(d)
  tina.forward(d)
  tina.right(20)
  codar(d/2.)
  tina.left(20)
  tina.back(d)

def tricoda(d):
  for i in range(3):
    codar(d)
    tina.left(120)

tp = TurtlePlane()
tina = Turtle(width=6, color='navy')
tricoda(100)

tp.mainloop()
