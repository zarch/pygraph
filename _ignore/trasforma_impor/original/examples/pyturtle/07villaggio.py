#!/usr/bin/env python
#--------------------------python-pyturtle----------------01casetta.py--#
#                                                                       #
#                              Casetta                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from random import random
from pyturtle import TurtlePlane, Turtle

def coloraacaso():
  '''Assegna a Tartaruga un colore con componenti RGB casuali'''
  tina.color = (random(), random(), random())

def casa():
  """Disegna l'intera casetta."""
  facciata()
  tina.left(90)
  tina.forward(100)
  tina.right(90)
  tetto()
  tina.left(90)
  tina.back(100)
  tina.right(90)

def facciata():
  """Disegna la facciata."""
  interno()
  muri()

def muri():
  """Disegna il controrno della facciata."""
  coloraacaso()
  tina.fill(1)
  for i in range(4):
    tina.forward(100)
    tina.left(90)
  tina.fill(0)

def interno():
  """Disegna l'interno della facciata."""
  tina.forward(40)
  porta()
  tina.up()
  tina.left(90)
  tina.forward(60)
  tina.right(90)
  tina.back(30)
  tina.down()
  finestre()
  tina.up()
  tina.back(10)
  tina.left(90)
  tina.back(60)
  tina.right(90)
  tina.down()

def porta():
  """Disegna un rettangolo."""
  coloraacaso()
  tina.fill(1)
  for i in range(2):
    tina.forward(20)
    tina.left(90)
    tina.forward(40)
    tina.left(90)
  tina.fill(0)

def finestre():
  """Disegna due finestre."""
  coloraacaso()
  finestra()
  tina.up()
  tina.forward(60)
  tina.down()
  finestra()
  tina.up()
  tina.back(60)
  tina.down()

def finestra():
  """Disegna un quadratino."""
  tina.fill(1)
  for i in range(4):
    tina.forward(20)
    tina.left(90)
  tina.fill(0)

def tetto():
  """Disegna il tetto."""
  coloraacaso()
  tina.fill(1)
  falde()
  tina.left(60)
  tina.forward(30)
  tina.left(30)
  camino()
  tina.right(30)
  tina.back(30)
  tina.right(60)
  tina.fill(0)

def falde():
  """Disegna un triangolo."""
  for i in range(3):
    tina.forward(100)
    tina.left(120)

def camino():
  """Disegna il camino."""
  tina.forward(30)
  tina.right(90)
  tina.forward(10)
  tina.right(90)
  tina.forward(12)
  tina.back(12)
  tina.left(90)
  tina.back(10)
  tina.left(90)
  tina.back(30)

tp = TurtlePlane()
tina = Turtle(x=-200)
for cont in range(4):
    casa()
    tina.forward(120)

tp.mainloop()
