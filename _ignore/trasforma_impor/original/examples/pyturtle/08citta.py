#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#--------------------------python-pyturtle--------------------citta.py--#
#                                                                       #
#                         Una citt� modulare                            #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

import random
from pyturtle import TurtlePlane, Turtle

def citta():
  """Disegna un'intera citt� di palazzoni."""
  for i in range(11):
    palazzo(random.randrange(1, 15))
    tina.forward(50)

def palazzo(n):
  """Disegna un palazzo a n piani."""
  pterra()
  for i in range(n): piano()
  tina.right(90)
  tetto()
  tina.left(90)
  tina.back(20*n+20)
  tina.right(90)

def pterra():
  """Disegna il piano terra e sposta Tartaruga."""
  tina.forward(20)
  porta()
  tina.forward(30)
  tina.left(90)
  tina.forward(20)
  tina.back(20)
  tina.right(90)
  tina.back(50)
  tina.left(90)
  tina.forward(20)

def piano():
  """Disegna un piano della casa e sposta Tartaruga"""
  tina.up()
  tina.right(90)
  tina.forward(50)
  tina.left(90)
  tina.down()
  tina.forward(20)
  tina.up()
  tina.back(15)
  tina.right(90)
  tina.back(40)
  tina.down()
  finestre()
  tina.up()
  tina.back(10)
  tina.left(90)
  tina.back(5)
  tina.down()
  tina.forward(20)

def porta():
  """Disegna un rettangolo."""
  for i in range(2):
    tina.forward(10)
    tina.left(90)
    tina.forward(20)
    tina.left(90)

def finestre():
  """Disegna due finestre."""
  finestra()
  tina.up()
  tina.forward(20)
  tina.down()
  finestra()
  tina.up()
  tina.back(20)
  tina.down()

def finestra():
  """Disegna un quadratino."""
  for i in range(4):
    tina.forward(10)
    tina.left(90)

def tetto():
  """Disegna il tetto."""
  falde()
  tina.left(60)
  tina.forward(10)
  tina.left(30)
  camino()
  tina.right(30)
  tina.back(10)
  tina.right(60)

def falde():
  """Disegna un triangolo."""
  for i in range(3):
    tina.forward(50)
    tina.left(120)

def camino():
  """Disegna il camino."""
  tina.forward(20)
  tina.right(90)
  tina.forward(5)
  tina.right(90)
  tina.forward(10)
  tina.back(10)
  tina.left(90)
  tina.back(5)
  tina.left(90)
  tina.back(20)

tp = TurtlePlane()
tina = Turtle(x=-280, y=-180, width=2)
#tina.tracer(1)
citta()
tp.mainloop()
