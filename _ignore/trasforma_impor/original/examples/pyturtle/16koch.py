#!/usr/bin/env python
#--------------------------python-pyturtle---------------------koch.py--#
#                                                                       #
#                         Isola di koch                             #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2004--#

from __future__ import print_function

from pyturtle import TurtlePlane, Turtle
from random import randrange
import math

def koch(lung, liv):
  if liv == 0: t.forward(lung); return
  koch(lung/3.0, liv-1)
  t.left(60)
  koch(lung/3.0, liv-1)
  t.right(120)
  koch(lung/3.0, liv-1)
  t.left(60)
  koch(lung/3.0, liv-1)
  
def fiocco(lato, liv):
  for i in range(3):
    koch(lato, liv)
    t.right(120)
    
def fioccodati(lato, liv):
##  for i in range(3):
##    koch(lato, liv)
##    t.right(120)
  p = lato*3.
  s = lato*lato*math.sqrt(3)/4
  print("livello = 0\t", "perimetro = ", p, "\tsuperficie = ", s)
  inc = s/3
  for i in range(liv):
    p = p*4/3
    s = s+inc
    if i == 0: inc = inc*4/9
    inc = inc*2/9
    print("livello =", i+1, "\t", "perimetro = ", p, "\tsuperficie = ", s)
    
def fioccol(lato, liv):
  for i in range(3):
    koch(lato, liv)
    t.left(120)

def kochcas(lung, liv):
  if liv == 0:
    t.forward(lung)
    return
  verso = randrange(-1, 2, 2)
  kochcas(lung/3.0, liv-1)
  t.left(60*verso)
  koch(lung/3.0, liv-1)
  t.right(120*verso)
  kochcas(lung/3.0, liv-1)
  t.left(60*verso)
  koch(lung/3.0, liv-1)
  
def fioccocas(lato, liv):
  for i in range(3):
    kochcas(lato, liv)
    t.right(120)

tp = TurtlePlane()  
t = Turtle()
fiocco(200, 4)
t.left(180)
fioccocas(200, 4)
t.left(180)
fioccodati(100, 15)
tp.mainloop()
