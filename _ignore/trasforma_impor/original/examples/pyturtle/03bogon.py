#!/usr/bin/env python
#--------------------------python-pyturtle------------------03bogon.py--#
#                                                                       #
#               Bogon Bogonela Maron di Mario Torneri                   #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from pyturtle import TurtlePlane, Turtle

def triangolo(lato):
  for i in range(3):
    tina.forward(lato)
    tina.left(120)

def bogonela():
  tina.color = (0.5, 0.10, 0.05)
  for n in range(120):
    triangolo(n*1.5)
    tina.back(3)
    tina.left(6)

tp = TurtlePlane()
tina = Turtle()
bogonela()
tp.mainloop()
