#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------python----------------------97snake.py--#
#                                                                       #
#                               Snake                                   #
#                                                                       #
# Copyright (c) 2013 Alessandro Canevaro, Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""
Snake

Snake è composto da un pallino verde indicato da self.testa e
da pallini di un altro colore contenuti nella lista self.corpo.
All'inizio del gioco nel campo sono sparsi degli altri pallini
dello stesso colore del corpo contenuti in self.cibo e
dei quadrati di un altro colore contenuti in self.muro.
Quando Testa raggiunge un cibo snake si allunga.
Il gioco termina quando:
- testa raggiunge un muro;
- testa raggiunge una parte del suo corpo;
- quando è stato raggiunto tutto il cibo.
"""

from __future__ import division, print_function
import pycart as pc
from random import randint

#####
# Classi

class SnakeGame(object):
    """SnakeGame class."""
    def __init__(self, dim, cella, tempo):
        self.dim = dim
        self.cella = cella
        self.tempo = tempo
        self.xmi, self.xma = -((self.dim/self.cella)//2)+1, ((self.dim/self.cella)//2)-1
        self.ymi, self.yma = self.xmi, self.xma
        self.width = cella // 2
        self.piano = pc.Plane(w=dim, h=dim, sx=cella, axes=False, grid=False, color='snow')
        self.penna = pc.Pen()
        self.canvas = self.piano._canvas
        self.piano.onright(self.onright)
        self.piano.onup(self.onup)
        self.piano.onleft(self.onleft)
        self.piano.ondown(self.ondown)

    def sottolatesta(self):
        """Restituisce la tupla degli id presenti
           sotto la testa di snake."""
        return self.canvas.find_enclosed(*self.canvas.bbox(self.testa))

    def cellaacaso(self):
        """Restituisce le coordinate di una cella a caso VUOTA."""
        while True:
            x = randint(self.xmi, self.xma)
            y = randint(self.ymi, self.yma)
            if not self.canvas.find_enclosed(x-self.width, y-self.width,
                                             x+self.width, y+self.width):
                return (x, y)

    def spargicibo(self, n):
        """Crea n bocconi di cibo nell'ambiente di snake e
           restituisce la lista degli identificatori creati da canvas.
           Non devono sovrapporsi."""
        cibo = []
        for i in range(n):
            x, y = self.cellaacaso()
            cibo.append(self.penna.drawpoint((x, y), width=self.width,
                                             color='light coral'))
        return cibo

    def creamuri(self, n):
        """Crea n blocchi di muro nell'ambiente di snake e
           restituisce la lista degli identificatori creati da canvas.
           Non devono sovrapporsi ne' sovrapporsi al cibo."""
        muri = []
        for i in range(n):
            x, y = self.cellaacaso()
            muri.append(self.penna.drawpoint((x, y), width=self.width,
                                             color='steel blue', shape=1))
            
        r = range(-self.dim//self.cella//2, self.dim//self.cella//2)
        dist = self.dim//self.cella//2 #distanza centro-bordo
        bordo = [(dist, i) for i in r]+[(-dist, i) for i in r]+\
                [(i, dist) for i in r]+[(i, -dist) for i in r]
        for i in bordo:
            muri.append(self.penna.drawpoint(i, width=self.width,
                                             color='steel blue', shape=1))
        return muri

    def newgame(self):
        """Prepara il gioco."""
        self.cibo = self.spargicibo(20)
        self.muri = self.creamuri(20)
        self.x, self.y = -5, -19
        self.testa = self.penna.drawpoint((self.x, self.y),
                                          width=self.cella//2, color='green yellow')
        self.corpo = [self.testa]
        self.movimenti = []

        self.canvas.update()
        self.piano.after(100)
        self.canvas.update()
        self.piano.after(2000)

    def run(self):
        """Avvia il gioco."""
        self.v_x, self.v_y = 0, -self.cella
        while True:
            if not self.muovi():
                print('Gioco finito')
                break
            self.piano.after(self.tempo)
        self.piano.mainloop()

    def scontro(self):
        """Gestisce lo scontro con un muro."""
        print('muro')
        return False

    def mangia(self):
        """Gestisce l'ingestione di cibo."""
        print('cibo')
        ##trovo le coordinate dell' ultima sezione di serpente
        x, y = self.x, self.y
        for v_x, v_y in self.movimenti:
            x -= v_x//self.cella
            y -= -v_y//self.cella
        ##aggiungo un pezzo di serpente sulla coda
        self.corpo.append(self.penna.drawpoint((x, y), width=self.cella//2,
                                     color={0:'goldenrod', 1:'olive drab'}[len(self.corpo)%2]))
        ##inizializzo i movimenti del nuovo pezzo
        self.movimenti.append((0, 0))

    def salta(self):
        """Fa muovere tutto il serpente."""
        for sezione, movimento in zip(self.corpo, self.movimenti):
            self.canvas.move(sezione, movimento[0], movimento[1])

    def muovi(self):
        """Muove snake,
           - se incontra un cibo si allunga;
           - se incontra un muro termina il gioco;
           - se incontra se stesso, termina il gioco; #TODO
           - se il cibo finisce, termina il gioco."""
        ##Aggiorno lista movimenti
        self.movimenti = [(self.v_x, self.v_y)] + self.movimenti
        ##muovo serpente
        self.salta()
        ##aggiorno la posizione della testa con riferimento Oxy di pycart
        self.x += self.v_x//self.cella
        self.y += -self.v_y//self.cella

        if len(self.cibo) == 0:
            print('Hai vinto!!!')
            return False
        
        sotto = self.sottolatesta()[0]
        if sotto in self.muri:
            return self.scontro()
        if sotto in self.cibo:
            self.mangia()
            ##rimuovo il cibo dalla lista e dal piano
            self.piano.delete(self.cibo.pop(self.cibo.index(sotto)))
        ##elimino l'ultimo movimento della lista
        del self.movimenti[-1]
        self.canvas.update()
        return True

########### altri metodi di canvas che potrebbero essere utili    
# invece che aggiornare esplicitamente le coordinate della testa
# usare i metodi di canvas:
# .bbox, .find_overlapping (o find_enclosed) o alla peggio .coords
# per trovare l'elemento su cui è arrivata la testa di snake
# e controllare se quell'elemento è nella lista cibo o muri
###################

    def onright(self, event):
        """Gestisce freccia a destra."""
        self.v_x, self.v_y = self.cella, 0
        
    def onup(self, event):
        """Gestisce freccia verso l'alto."""
        self.v_x, self.v_y = 0, -self.cella
        
    def onleft(self, event):
        """Gestisce freccia verso sinistra."""
        self.v_x, self.v_y = -self.cella, 0
        
    def ondown(self, event):
        """Gestisce freccia verso il basso."""
        self.v_x, self.v_y = 0, self.cella

#####
# Programma principale

gioco = SnakeGame(400, 10, 200)
gioco.newgame()
gioco.run()
