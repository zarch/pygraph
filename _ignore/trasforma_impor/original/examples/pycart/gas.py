#!/usr/bin/env python
#-------------------------------python--------------------------gas.py--#
#                                                                       #
#                                 Gas                                   #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2002---#

from __future__ import print_function
from math import pi, sin, cos
from random import randrange, random
from pycart import Plane, Pen

DIM_MOL = 2
ITER = 1000

class Bombola:
  def __init__(self, piano, w=50, h=100, px=0, py=0, nmol=50, dim_mol=2):
    self.penna = Pen(width=DIM_MOL)
    self.canvas=piano.getcanvas()
    self.dim_mol = dim_mol
    self.bxmi, self.bymi = -w/2+px, -h/2+py
    self.bxma, self.byma = w/2+px, +h/2+py
    self.draw()
    self.carica(nmol)

  def draw(self):
    self.penna.drawpoly(((self.bxmi, self.bymi), (self.bxma, self.bymi),
                         (self.bxma, self.byma), (self.bxmi, self.byma)))

  def carica(self, nmol):
    self.gas = [Molecola(bombola=self,
                         x=self.randomx(), y=self.randomy(), 
                         velocita=random()*2+1, colore='red')
                for cont in range(nmol)]

  def randomx(self):
    return randrange(self.bxmi+self.dim_mol*2, self.bxma-self.dim_mol*2)

  def randomy(self):
    return randrange(self.bymi+self.dim_mol*2, self.byma-self.dim_mol*2)

  def impattos(self, x, y):
    return x<=self.bxmi+self.dim_mol*2

  def impattod(self, x, y):
    return x>=self.bxma-self.dim_mol*2

  def impattoa(self, x, y):
    return y>=self.byma-self.dim_mol*2

  def impattob(self, x, y):
    return y<=self.bymi+self.dim_mol*2

  def anima(self):
    for i in range(ITER):
      self.muovi()
      self.canvas.update()
#      self.canvas.after(10)

  def muovi(self):
    for m in self.gas:
      m.muovi()

class Molecola:
  def __init__(self, bombola, x, y, velocita, colore):
    self.bombola = bombola
    self.x, self.y = x, y
    angolo = random()*2*pi
    self.vx = velocita*cos(angolo)
    self.vy = velocita*sin(angolo)
    self.o = bombola.penna.drawpoint((self.x, self.y), color=colore)

  def muovi(self):
    vx, vy = self.vx, self.vy
    x, y = self.x + vx, self.y + vy
    if (self.bombola.impattos(x, y) or
        self.bombola.impattod(x, y)):
      vx = -vx*(1+random()*.2-0.1)
      vy = +vy*(1+random()*.2-0.1)
##      print('impattods', vx)
      x += vx
      while self.bombola.impattos(x, y):
        x += vx
#        print('corregges', x, vx)
      while self.bombola.impattod(x, y):
        x += vx
#        print('corregged', x, vx)
    if (self.bombola.impattoa(x, y) or
        self.bombola.impattob(x, y)):
      vx = +vx*(1+random()*.2-0.1)
      vy = -vy*(1+random()*.2-0.1)
##      print('impattoab', vy)
      y += vy
      while self.bombola.impattoa(x, y):
        y += vy
#        print('correggea', y, vy)
      while self.bombola.impattob(x, y):
        y += vy
#        print('correggeb', y, self.vy)
    self.vx, self.vy = vx, vy
    self.x, self.y = x, y
#    print(self.x, self.y)
    self.bombola.canvas.move(self.o, vx, -vy)

if __name__ == '__main__':
  piano=Plane("Gas", w=400, h=400, sx=1, axes=False)
  b=Bombola(piano, w=100, h=200, nmol=100, dim_mol=DIM_MOL)
  b.anima()
  piano.mainloop()


