#!/usr/bin/env python
#-------------------------------python--------------------------gas.py--#
#                                                                       #
#                                 Gas                                   #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2002---#

from math import pi, sin, cos
from random import randrange, random
from pycart import Plane, Pen

DIM_MOL=2
#v=2.0

class Bombola:
  def __init__(self, w=50, h=100, px=0, py=0, nmol=50, dim_mol=2):
    self.dim_mol = dim_mol
    bxmi, bymi, bxma, byma = -w/2+px, -h/2+py, w/2+px, +h/2+py
    penna.drawpoly(((bxmi, bymi), (bxma, bymi), (bxma, byma), (bxmi, byma)))
    self.gas = [Molecola(bxmi+2*dim_mol, bymi+2*dim_mol,
                         bxma-2*dim_mol, byma-2*dim_mol,
                         random()*2+1, 'red')
                for cont in range(nmol)]

  def impattodx(self, x):
    return self.x <= self.bxmi+self.dim_mol

  def impattosx(self, x):
    return self.x >= self.bxmi+self.dim_mol

  def anima(self):
    for i in range(1000):
      for m in self.gas: m.muovi()
      canvas.update()
#      canvas.after(20)

class Molecola:
  def __init__(self, bxmi, bymi, bxma, byma, velocita=0, colore='black'):
    self.bxmi, self.bymi, self.bxma, self.byma = bxmi, bymi, bxma, byma
    self.x, self.y = randrange(bxmi, bxma), randrange(bymi, byma)
    self.vx = velocita*cos(random()*2*pi)
    self.vy = velocita*sin(random()*2*pi)
    self.colore = colore
    self.o = penna.drawpoint((self.x, self.y), color=self.colore)

  def muovi(self):
    if self.x < self.bxmi+DIM_MOL or self.x>self.bxma-DIM_MOL:
      self.vx = -self.vx+random()*.25-.125
      self.vy = self.vy+random()*.25-.125
    if self.y < self.bymi+DIM_MOL or self.y>self.byma-DIM_MOL:
      self.vx = self.vx+random()*.25-.125
      self.vy = -self.vy+random()*.25-.125
    self.x += self.vx
    self.y += self.vy
    canvas.move(self.o, self.vx, -self.vy)
    
piano = Plane("Gas", w=400, h=400, sx=1, axes=False)
penna = Pen(width=DIM_MOL)
canvas = piano.getcanvas()
b = Bombola(w=100, h=200, nmol=50)
b.anima()
piano.mainloop()


