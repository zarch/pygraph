#!/usr/bin/env python
#-------------------------------python------------------------05gas.py--#
#                                                                       #
#                                 Gas                                   #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2011---#

from __future__ import print_function
#from math import pi, sin, cos
from random import randrange #, random
from pycart import Plane
from gas import Bombola, DIM_MOL #, Molecola

class DueBombole(Bombola):

  def draw(self):
    self.bxce, self.byce = (self.bxmi + self.bxma) / 2, self.byma * 0.5
    self.penna.drawpoly(((self.bxmi, self.bymi), (self.bxce, self.bymi),
                         (self.bxce, self.byce), (self.bxce, self.bymi),
                         (self.bxma, self.bymi),
                         (self.bxma, self.byma), (self.bxmi, self.byma)))

  def randomx(self):
    return randrange(self.bxmi+self.dim_mol*2, self.bxce-self.dim_mol*2)

  def impattos(self, x, y):
    return (x <= self.bxmi+self.dim_mol or
            (self.bxce <= x < self.bxce+self.dim_mol and y<self.byce))

  def impattod(self, x, y):
    return (x > self.bxma-self.dim_mol or
            (self.bxce-self.dim_mol <= x < self.bxce and y<self.byce))

  def muovi(self):
    sin = des = 0
    for m in self.gas:
      m.muovi()
      if m.x < self.bxce:
        sin += 1
      else:
        des += 1
    print('sin; ', sin, ';\tdes: ', des)

if __name__ == '__main__':
  piano=Plane("Gas", w=400, h=400, sx=1, axes=False)
  b=DueBombole(piano, w=200, h=200, nmol=200, dim_mol=DIM_MOL)
  b.anima()
  print("fine")
  piano.mainloop()


