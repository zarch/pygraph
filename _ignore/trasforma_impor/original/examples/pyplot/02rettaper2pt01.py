#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python------------------rettaper2pt.py--#
#                                                                       #
#        Calcola l'equazione e disegna una retta per 2 punti            #
#                                                                       #
#--Daniele Zambelli----------------------------------------------2006---#

from pyplot import PlotPlane, Plot

def cango(p0, p1):
  """Calcola il coefficiente angolare."""
  x0, y0 = p0
  x1, y1 = p1
  if x0 == x1: return None
  else:      return (y0-y1)/(x0-x1)

def quota(p0, m):
  """Calcola il termine noto."""
  x0, y0 = p0
  if m == None: return x0
  else:       return y0-m*x0

def eq_retta(m, q):
  """Restituisce l'equazione di una retta dati m e q."""
  if m == None: return "x={0}".format(q)
  else:
    if q > 0:   return "y={0}*x+{1}".format(m, q)
    else:       return "y={0}*x{1}".format(m, q)

def f_retta(m, q):
  """Restituisce la funzione della retta per p0 e p1."""
  if m == None:
    def r(x):
      return q
  else:
    def r(x):
      return m*x+q
  return r

##def f_retta(m, q):
##  """Restituisce la funzione della retta per p0 e p1.
##     versione pi� compatta della precedente che utilizza lambda."""
##  return lambda x: m*x+q

def leggi(cosa, chi):
  """Legge in numero razionale."""
  return float(input("scrivi l'{0} di {1} ".format(cosa, chi)))

def leggi_x(chi):
  """Legge l'ascissa di un punto."""
  return leggi("ascissa", chi)

def leggi_y(chi):
  """Legge l'ordinata di un punto."""
  return leggi("ordinata", chi)

def leggi_coord(chi):
  """Legge le coordinate di un punto."""
  return leggi_x(chi), leggi_y(chi)

def demo():
  p0 = leggi_coord("p0")
  p1 = leggi_coord("p1")
  m = cango(p0, p1)
  q = quota(p0, m)
  er = eq_retta(m, q)
  piano = PlotPlane(er, w=600, h=400)
  plot = Plot(color='#a43598', width=3)
  plot.drawpoint(p0)
  plot.drawpoint(p1)
  fr = f_retta(m, q)
  if m!=None:
    plot.xy(fr, color='#57b498', width=1)
  else:
    plot.yx(fr, color='#57b498', width=1)
  piano.mainloop()
  
demo()
