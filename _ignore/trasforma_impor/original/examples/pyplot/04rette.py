#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python------------------------rette.py--#
#                                                                       #
#                 Fasci di rette nel piano cartesiano                   #
#                                                                       #
#--Daniele Zambelli--------------GPL------------------------------2009--#

from __future__ import division
from pyplot import PlotPlane, Plot

def retta(m, q):
  """Restituisce una funzione lineare in x con coeff angolare m e quota q."""
  def f(x):
    return m*x+q
  return f

def fascio1(k):
  """Restituisce una retta del fascio."""
  def f(x):
    return (k-2)*x+(k+4)
  return f


# Disegna rette parallele
pp = PlotPlane('rette parallele')
p = Plot(width=3)
p.color = 'blue'
for q in range(-5, 2):
  p.xy(retta(0.5, q))

# Disegna rette con uguale quota e diverse pendenze
pp = PlotPlane('rette con uguale quota')
p = Plot(width=3)
p.color = 'green'
for m in [1/3, 1/2, 1, 2, 3, -3, -2, -1, -1/2, -1/3]:
  p.xy(retta(m, 2))

# Disegna rette di un fascio
pp = PlotPlane('rette di un fascio')
p = Plot(width=3)
p.color = 'red'
for k in range(-5, 5):
  p.xy(fascio1(k))

pp.mainloop()
