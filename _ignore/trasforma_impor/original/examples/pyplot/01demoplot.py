#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------python---------------------demoplot.py--#
#                                                                       #
#                    traccia grafici di funzioni                        #
#                                                                       #
#--Daniele Zambelli----------------------------------------------2002---#

from __future__ import print_function

from pyplot import PlotPlane, Plot
from math import sin, cos, sqrt

def f0(x):
  return sin(1/x)

def f1(x):
  return sqrt((x**2-4)/(x**2-25))

def f2(th):
  return 2*th

def f3(th):
  return 1.1**th

def parabola(var):
  return .3*var**2+3*var+4

funzioni=((lambda x: 1, 11, '#f2f4a6'),
          (lambda x: 1-(x**2)/2, 10, '#e2e496'),
          (lambda x: 1-(x**2)/2+(x**4)/24, 9, '#d2d486'),
          (lambda x: 1-(x**2)/2+(x**4)/24-(x**6)/720, 8, '#c2c476'),
          (lambda x: 1-(x**2)/2+(x**4)/24-(x**6)/720+(x**8)/40320,
           7, '#b2b466'),
          (lambda x: 1-(x**2)/2+(x**4)/24-(x**6)/720+(x**8)/40320-
           (x**10)/3628800, 6, '#a2a456'),
          (lambda x: 1-(x**2)/2+(x**4)/24-(x**6)/720+(x**8)/40320-
           (x**10)/3628800+(x**12)/479001600, 5, '#929446'),
          (lambda x: 1-(x**2)/2+(x**4)/24-(x**6)/720+(x**8)/40320-
           (x**10)/3628800+(x**12)/479001600-(x**14)/87178291200, 4, '#828436'),
          (lambda x: 1-(x**2)/2+(x**4)/24-(x**6)/720+(x**8)/40320-
           (x**10)/3628800+(x**12)/479001600-(x**14)/87178291200+
           (x**16)/20922789888000, 3, '#727426'),
          (lambda x: cos(x), 2, '#525406'))
          
def demo():
  # funzioni definite all'esterno
  pp0 = PlotPlane('sin(1/x)', w=600, h=250, sx=100)
  p0 = Plot(width=2)
  p0.xy(f0)
  print()
  pp1 = PlotPlane('sqrt((x**2-4)/(x**2-25))', w=400, h=400, oy=150)
  p1 = Plot(color='red', width=2)
  p1.xy(f1)
  print()
  pp2 = PlotPlane('Spirali', w=600, h=600, sx=10)
  p2 = Plot(color='gold', width=2)
  p2.polar(f2, 720)
  p2.polar(f3, 2520, color='brown')
  print()
  # funzioni definite con lambda
  pp3 = PlotPlane('Coordinate polari', w=200, h=200, ox=30, sx=10)
  p3 = Plot(color='gold', width=2)
  p3.polar(lambda th: 10*cos(th)+5)
  print()
  pp4 = PlotPlane('Parabole')
  p4 = Plot(width=2)
  p4.xy(parabola, color='green')
  p4.yx(parabola, color='magenta')
  print()
  pp5 = PlotPlane('Coseno')
  p5 = Plot(width=2)
  for f, w, c in funzioni:
    p5.xy(f, color=c, width=w)
  print('fine')
  pp5.mainloop()

demo()
