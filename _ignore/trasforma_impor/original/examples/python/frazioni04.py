#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python-------------------frazioni04.py--#
#                                                                       #
#                          Classe Frazioni                              #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

def mcd(a, b):
  '''Massimo Comune Divisore tra a e b'''
  a, b = abs(a), abs(b)
  while a != b and a != 1 and b != 1:
    if a > b: a -= b
    else:     b -= a
  if b == 1:
    return b
  else:
    return a

def riduci(f):
  """Riduce una coppia di numeri dividendoli per il loro MCD."""
  num, den = f
  divisore = mcd(num, den)
  return (num//divisore, den//divisore)

class Frazione(object):

  OOo=True  # Se True visualizza le frazioni nel formato di OpenOffice.org:
            # "{n over d}"
            # se False visualizza nella forma testuale "n/d"

  def __init__(self, num, den):
    """Restituisce una frazione dati numeratore e denominatore.

    Solleva un'eccezione se il denominatore � nullo.
    Il denominatore � sempre positivo.
    La frazione � ridotta ai minimi termini."""
    if den == 0:
      raise ZeroDivisionError
    elif num == 0:
      self.num = 0
      self.den = 1
    else:
      if den > 0:
        self.num, self.den = riduci((num, den))
      else:
        self.num, self.den = riduci((-num, -den))

  def __add__(self, other):
    """Restituisce la somma di questa frazione con un'altra."""
    num = self.num*other.den+other.num*self.den
    den = self.den*other.den
    return Frazione(num, den)

  def __neg__(self):
    """Restituisce l'opposto di una frazione."""
    return Frazione(-self.num, self.den)
  
  def __sub__(self, other):
    """Restituisce la differenza di due frazioni."""
    num = self.num*other.den-other.num*self.den
    den = self.den*other.den
    return Frazione(num, den)

  def __mul__(self, other):
    """Restituisce il prodotto di due frazioni."""
    return Frazione(self.num*other.num, self.den*other.den)

  def reciprocof(self):
    """Restituisce il reciproco di una frazione."""
    return Frazione(self.den, self.num)

  def __div__(self, other):                        # for Python2.x
    """Restituisce il quoziente di due frazioni."""
    return Frazione(self.num*other.den, self.den*other.num)

  def __truediv__(self, other):                    # for Python3.x
    """Restituisce il quoziente di due frazioni."""
    return Frazione(self.num*other.den, self.den*other.num)

  def ooostr(self):
    Frazione.OOo = True

  def txtstr(self):
    Frazione.OOo = False

  def __str__(self):
    """Restituisce una stringa che rappresenta una frazione.

    La stringa pu� essere in formato OpenOffice.org o in un'altro formato."""
    if self.den == 1: return str(self.num)
    if self.num == 0: return '0'
    if Frazione.OOo:
      return "{{{} over {}}}".format(self.num, self.den)
    else:
      return "{}/{}".format(self.num, self.den)

def randf(num_mi, num_ma, den_mi, den_ma):
  """Restituisce una frazione casuale.

  Il numeratore � compreso tra num_mi e num_ma;
  Il denominatore � compreso tra den_mi e den_ma"""
  from random import randrange
  num = randrange(num_mi, num_ma)
  while True:
    den = randrange(den_mi, den_ma)
    if den: break
  return Frazione(num, den)

def test():
  a = Frazione(2,5)
  b = Frazione(3,4)
  print("{} + {} = {}".format(a, b, a+b))
  print("{} - {} = {}".format(a, b, a-b))
  print("{} * {} = {}".format(a, b, a*b))
  print("{} / {} = {}".format(a, b, a/b))
  c = randf(-10, 10, -10, 10)
  d = randf(-10, 10, -10, 10)
  print("Espressione (formato OOo)")
  print("{} / {} + {} * {} = {}".format(a, b, c, d, a/b+c*d))
  print("Espressione (formato txt)")
  a.txtstr()
  print("{} / {} + {} * {} = {}".format(a, b, c, d, a/b+c*d))
  print("Frazione con divisore nullo:")
  f = Frazione(5, 0)
 
if __name__ == "__main__": test()
