#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python-------------------frazioni03.py--#
#                                                                       #
#             Esercitatore di calcolo con le Frazioni                   #
#    Versione che utilizza le funzioni definite in frazioni02.py        #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

from __future__ import print_function

import frazioni02 as fraz

try:
#  pyversion = 'py2'
  input = raw_input
except:
  pass

NUM_ESERCIZI=3
MAX_VAL=11

def eser_fraz():
  """ Esercitatore di calcolo frazionario."""
  while True:
    menu()                                # Stampa il menu
    scelta=input("La tua scelta: ")   # legge la scelta dell'utente
    if scelta=="0": break                 # con zero termina il ciclo
    elif scelta=="1": addizione()         # le varie scelte...
    elif scelta=="2": sottrazione()
    elif scelta=="3": moltiplicazione()
    elif scelta=="4": divisione()

def menu():
  """Menu di scelta degli esercizi."""
  print(\
"""
Frazioni

Scegli il tipo di esercizi:
1: Addizione
2: Sottrazione
3: Moltiplicazione
4: Divisione

0: Fine
""")

def addizione():
  """Esercizi sull'addizione di frazioni."""
  giuste = 0
  sbagliate = 0
  for i in range(NUM_ESERCIZI):            # ciclo
    f1 = fraz.randf(1, MAX_VAL, 1, MAX_VAL)   # primo operando
    f2 = fraz.randf(1, MAX_VAL, 1, MAX_VAL)   # secondo operando
    fr = fraz.sommaf(f1, f2)                  # risultato
    risposta=input("{0[0]}/{0[1]} + {1[0]}/{1[1]} = ".format(f1, f2))
    if risposta == "{}/{}".format(*fr):
      giusto()
      giuste += 1
    else:
      sbagliato(fr)
      sbagliate+=1
  risultati("addizioni", giuste, sbagliate)

def sottrazione():
  """Esercizi sulla sottrazione di frazioni."""
  print("ecco le sottrazioni")

def moltiplicazione():
  """Esercizi sulla moltiplicazione di frazioni."""
  print("ecco le moltiplicazioni")

def divisione():
  """Esercizi sulla divisione di frazioni."""
  print("ecco le divisioni")

def giusto():
  """Azioni da svolgare quando la risposta � giusta."""
  print("Bravo, la risposta � giusta!")

def sbagliato(risultato):
  """Azioni da svolgare quando la risposta � sbagliata."""
  print("Sbagliato, la risposta era:{}/{}".format(*risultato))
  c = input("premi <invio>")

def risultati(tipo, g, s):
  """Visualizza i risultati ottenuti."""
  print("\n\nNelle {}, hai ottenuto {}% di risposte esatte!\n".format(
        tipo, 100*g//(g+s)))
  d=input("premi <invio> per continuare")
  

eser_fraz()
