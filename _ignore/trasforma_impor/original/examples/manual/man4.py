#!/usr/bin/env python
#----------------------python-pyturtle-----------------man4.py--#
#                                                               #
#                     Terremoto a colori                        #
#                                                               #
#--Daniele Zambelli------License: GPL---------------------2005--#

from man3 import Architetto, TurtlePlane
from random import randrange

class Artista(Architetto):

  def quadrato(self, lato):
    """Disegna un mattone colorato."""
    rosso=randrange(100)/100.
    verde=randrange(100)/100.
    blu=randrange(100)/100.
    self.fill(1)
    self.color = (rosso, verde, blu)
    Architetto.quadrato(self, lato)
    self.fill(0)

def main():
  tp = TurtlePlane()
  raffaello = Artista()
  raffaello.sposta(-250, -180)
  raffaello.muro(20, 15, 20)
  tp.mainloop()

if __name__ == "__main__": main()
