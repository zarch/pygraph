#!/usr/bin/env python
#----------------------python-pyturtle-----------------man3.py--#
#                                                               #
#                         Terremoto                             #
#                                                               #
#--Daniele Zambelli------License: GPL---------------------2005--#

from man2 import Ingegnere, TurtlePlane
from random import randrange

class Architetto(Ingegnere):
  def quadrato(self, lato):
    """Disegna un mattone spostato rispetto alla posizione
    attuale di Tartaruga."""
    angolo=randrange(30)
    spostamento=randrange(30)
    self.up()
    self.right(angolo); self.forward(spostamento)
    self.down()
    Ingegnere.quadrato(self, lato)
    self.up()
    self.back(spostamento); self.left(angolo)
    self.down()

def main():
  tp = TurtlePlane()
  michelangelo = Architetto()
  michelangelo.sposta(-250, -180)
  michelangelo.muro(20, 15, 20)
  tp.mainloop()

if __name__ == "__main__": main()
