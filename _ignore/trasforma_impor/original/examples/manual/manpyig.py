# -*- coding: iso-8859-15 -*-
#-------------------------------python----------------------manpyig.py--#
#                                                                       #
#        Esempi del manuale relativi alla geometria interattiva         #
#                                                                       #
#--Daniele Zambelli-----------Licence GPL------------------------2010---#

"""
pyig examples for manual.
"""

from __future__ import print_function, division
#import random
import libmanex
from pyig import *

######################################
#     ``class InteractivePlane``     #
#     --------------------------     #
######################################

def ex_000():
    """Primo esempio."""
    from pyig import *                 # Carica la libreria
    ip = InteractivePlane('Primo')     # Crea un piano
    ip.mainloop()                      # Rendi interattivo il piano

def ex_001():
    """Secondo esempio."""
    from pyig import *                 # Carica la libreria
    ip = InteractivePlane('Secondo')   # Crea un piano
    p0 = Point(-3, -5)                 # Disegna due punti
    p1 = Point(2, 3)
    Line(p0, p1)                       # Disegna la retta
    ip.mainloop()                      # Rendi interattivo il piano

def ex_00():
    """version:
    Controlla la versione della libreria."""
    import pyig                  # Carica la libreria
    ip = InteractivePlane('versione')
#    import pyig
    if pyig.version()<"2.9.00":
        print("versione un po' vecchiotta")
    else:
        print("versione:", pyig.version())
    libmanex.end(ip)
    
def ex_01():
    """defvisible, defwidth, defwidthtext, defcolor, defdragcolor, defintcolor.
    Disegna l'asse di un segmento 
    usando linee di costruzioni sottili e grige"""
    ip = InteractivePlane('default')
    ip.defwidth = 5
    a = Point(2, 1, name='A')
    b = Point(5, 3, name='B')
    ip.defcolor = 'turquoise'
    Segment(a, b)
    ip.defwidth = 1
    ip.defcolor = 'gray40'
    c0 = Circle(a, b)
    c1 = Circle(b, a)
    i0 = Intersection(c0, c1, -1)
    i1 = Intersection(c0, c1, 1)
    asse = Line(i0, i1, width=3, color='royal blue')
    libmanex.end(ip)

def ex_02():
    """newPoint:
    Crea un testo nel piano interattivo."""
    ip = InteractivePlane('newText')
    ip.newText(0, 13, 'Finestra (quasi) vuota', 
               width=20, color='DarkOrchid3')
    libmanex.end(ip)

def ex_03():
    """newPoint:
    Crea un punto in una data posizione."""
    ip = InteractivePlane('newPoint')
    ip.newText(0, 13, 'Finestra con un punto',
               width=20, color='DarkOrchid3')
    p0 = ip.newPoint(-2, 7, width=8, name="P")
    libmanex.end(ip)

def ex_04():
    """newPointD:
    Visualizza alcune informazioni che dipendono da altri oggetti."""
    ip = InteractivePlane('newVarText')
    ip.newText(0, 13, 'Finestra con un punto e le sue coordinate', 
               width=20, color='DarkOrchid3')
    p0 = ip.newPoint(-2, 7, width=8, name="P")
    ip.newVarText(-5, -4, 'P = {0}', p0.coords())
    libmanex.end(ip)

######################################
#        ``class _ViewObject``       #
#        ---------------------       #
######################################

def ex_10():
    """Point:
    Funzione definita in N ad andamento casuale."""
    import random
    ip = InteractivePlane('Point')
    y = 0
    for x in range(-14, 14):
        y += random.randrange(-1, 2)
        Point(x, y, color='red')
    libmanex.end(ip)

def ex_101():
    """10: visible, color, width, name.
    Disegna tre punti, uno con i valori di default,
    uno con colore dimensione e nome definiti quando viene creato,
    uno con valori cambiati dopo essere stato cerato."""
    ip = InteractivePlane('attributi')
    a = Point(-5, 3)
    b = Point(2, 3, color='indian red', width=8, name='B')
    c = Point(9, 3)
    c.color = 'dark orange'
    c.width = 8
    c.name = 'C'
    libmanex.end(ip)    

def ex_102():
    """11: coords, xcoord, ycoord.
    Scrivi ascissa, ordinata e posizione di un punto."""
    ip = InteractivePlane('coords, xcoord, ycoord')
    a = Point(-5, 8, name='A')
    VarText(-5, -1, 'ascissa di A: {0}', a.xcoord())
    VarText(-5, -2, 'ordinata di A: {0}', a.ycoord())
    VarText(-5, -3, 'posizione di A: {0}', a.coords())
    libmanex.end(ip)    

def ex_103():
    """Point operations:
    Crea due punti poi calcola la loro somma, differenza e prodotto."""
    ip = InteractivePlane('Point operations')
    a = Point(2, 1)
    b = Point(4, 4)
    s = a+b
    d = a-b
    t = a*3
    p = a*b
    VarLabel(a, 10, -10, 'A{0}', a.coords())
    VarLabel(b, 10, -10, 'B{0}', b.coords())
    VarLabel(s, 10, -10, 'A+B{0}', s.coords())
    VarLabel(d, 10, -10, 'A-B{0}', d.coords())
    VarLabel(t, 10, -10, '3A{0}', t.coords())
    VarLabel(p, 10, -10, 'A*B{0}', p.coords())
    libmanex.end(ip)    

def ex_11():
    """Segment:
    Disegna un triangolo con i lati colorati in modo differente."""
    ip = InteractivePlane('Segment')
    # creo i 3 vertici
    v0 = Point(-4, -3, width=5)
    v1 = Point( 5, -1, width=5)
    v2 = Point( 2,  6, width=5)
    # creo i 3 lati
    l0 = Segment(v0, v1, color='steel blue')
    l1 = Segment(v1, v2, color='sea green')
    l2 = Segment(v2, v0, color='saddle brown')
    libmanex.end(ip)

def ex_111():
    """length:
    Disegna un segmento e scrivi la sua lunghezza."""
    ip = InteractivePlane('length')
    p0 = Point(-4, 7, width=5, name='A')
    p1 = Point(8, 10, width=5, name='B')
    seg = Segment(p0, p1)
    VarText(-5, -5, 'lunghezza di AB = {0}', seg.length())
    libmanex.end(ip)

def ex_112():
    """midpoint:
    Disegna un segmento e il suo punto medio."""
    ip = InteractivePlane('35: midpoint')
    seg = Segment(Point(-4, 7, width=5, name='A'),
                  Point(8, 10, width=5, name='B'))
    seg.midpoint(color='gold', width=10)
    libmanex.end(ip)

def ex_113():
    """equation:
    Disegna una retta e scrivi la sua equazione ."""
    ip = InteractivePlane('equation')
    p0 = Point(-4, 7, width=5)
    p1 = Point(8, 10, width=5)
    retta = Line(p0, p1, name='r')
    VarText(-5, -5, 'equazione di r: {0}', retta.equation())
    libmanex.end(ip)

def ex_114():
    """slope:
    Disegna una semiretta e scrivi la sua pendenza ."""
    ip = InteractivePlane('slope')
    p0 = Point(-4, 7, width=5)
    p1 = Point(8, 10, width=5)
    semiretta = Ray(p0, p1, name='r')
    VarText(-5, -5, 'pendenza di r: {0}', semiretta.slope())
    libmanex.end(ip)

def ex_115():
    """point0 point1:
    Disegna un segmento e scrivi le coordinate dei suoi estremi."""
    ip = InteractivePlane('point0 point1')
    seg = Segment(Point(-4, 7, width=5, name='A'),
                  Point(8, 10, width=5, name='B'))
    VarText(-5, -5, 'A{0}', seg.point0().coords())
    VarText(-5, -6, 'B{0}', seg.point1().coords())
    libmanex.end(ip)

def ex_12():
    """MidPoints:
    Punto medio tra due punti."""
    ip = InteractivePlane('MidPoints')
    # creo due punti
    p0 = Point(-2, -5)
    p1 = Point(4, 7)
    # cambio i loro attributi
    p0.color = "#00a600"
    p0.width = 5
    p1.color = "#006a00"
    p1.width = 5
    # creao il punto medio tra p0 e p1
    m = MidPoints(p0, p1, name='M')
    # cambio gli attributi di m
    m.color = "#f0f000"
    m.width = 10
    libmanex.end(ip)

def ex_13():
    """MidPoint:
    Punto medio di un segmento."""
    ip = InteractivePlane('MidPoint')
    # creo un segmento
    s=Segment(Point(-2, -1, color="#a60000", width=5),
              Point(5, 7, color="#6a0000", width=5), 
              color="#a0a0a0")
    # creo il suo punto medio
    MidPoint(s, color="#6f6f00", width=10, name='M')
    libmanex.end(ip)

def ex_14():
    """Line
    Triangolo delimitato da rette."""
    ip = InteractivePlane('Line')
    # creo i 3 punti
    a=Point(0, 0)
    b=Point(1, 5)
    c=Point(5, 1)
    # creo i 3 lati
    Line(a, b, color="#dead34")
    Line(b, c, color="#dead34")
    Line(c, a, color="#dead34")
    libmanex.end(ip)

def ex_15():
    """Ray:
    Triangolo delimitato da semirette."""
    ip = InteractivePlane('Ray')
    # creo i 3 punti
    a=Point(0, 0)
    b=Point(1, 5)
    c=Point(5, 1)
    # creo i 3 lati
    Ray(a, b, color="#de34ad")
    Ray(b, c, color="#de34ad")
    Ray(c, a, color="#de34ad")
    libmanex.end(ip)

#def ex_18Segment():
#    """Ray:
#    Triangolo delimitato da segmenti."""
#    ip = InteractivePlane('18: Segment')
#    # creo i 3 punti
#    a=Point(0, 0)
#    b=Point(1, 5)
#    c=Point(5, 1)
#    # creo i 3 lati
#    Segment(a, b, color="#34dead")
#    Segment(b, c, color="#34dead")
#    Segment(c, a, color="#34dead")
#    libmanex.end(ip)

def ex_16():
    """Orthogonal:
    Disegna la perpendicolare ad una retta data passante per un punto."""
    ip = InteractivePlane('Orthogonal')
    retta = Line(Point(-4, -1, width=5), 
                 Point(6, 2, width=5), 
                 width=3, color='DarkOrange1', name='r')
    punto = Point(-3, 5, width=5, name='P')
    Orthogonal(retta, punto)
    libmanex.end(ip)

def ex_17():
    """Parallel:
    Disegna la parallela ad una retta data passante per un punto."""
    ip = InteractivePlane('Parallel')
    retta = Line(Point(-4, -1, width=5), 
                 Point(6, 2, width=5), 
                 width=3, color='DarkOrange1', name='r')
    punto = Point(-3, 5, width=5, name='P')
    Parallel(retta, punto)
    libmanex.end(ip)

#def ex_18_Vector():
#    """Segment:
#    Disegna un vettore dati i suoi estremi e visualizza le sue componenti."""
#    ip = InteractivePlane('Vector')
#    a = Point(-2, +3, width=5)
#    b = Point(5, 7, width=5)
#    v = Vector(a, b, color='saddle brown')
#    VarText(-5, -1, 'componenti: {0}', v.components())
#    libmanex.end(ip)

def ex_18():
    """Vector:
    Disegna due vettori, la loro somma e la loro differenza."""
    ip = InteractivePlane('Vector')
    Text(-7, 13, """Vector""", color='#408040', width=12)
    a = Point(-5, 10, name="A", width=6)
    b = Point(-1, 13, name="B", width=6)
    c = Point(-3, 5, name="C", width=6)
    d = Point(-10, 6, name="D", width=6)
    vect0 = Vector(a, b, width=6, color='navy', name='V0')
    vect1 = Vector(a, c, width=6, color='red', name='V1')
    vect2 = Vector(d, vect1, color='red', name='V2')
    vect3 = vect0 + vect1
    vect4 = vect0 - vect1
    vect5 = vect0 * 3
    vect6 = -vect0 / 3
    vect6.color = 'red'
    VarText(7, -2, '{0}', vect0.components(), (.2, .4, .8))
    libmanex.end(ip)

def ex_19():
    """Polygon:
    Disegna un poligono date le coordinate dei vertici."""
    ip = InteractivePlane('Polygon')
    # Lista di coordinate
    coords = ((-8, -3), (-6, -2), (-5, -2), (-4, 2), (-2, 3), (0, 4),
              (2, 3), (4, 2), (5, -2), (6, -2), (8, -3))
    # Costruzione di una lista di punti partendo da una lista di coordinate:
    # listcompreension
    ip.defwidth = 5
    points = [Point(x, y) for x,y in coords]
    Polygon(points, color='HotPink3')
    libmanex.end(ip)

def ex_191():
    """perimeter surface:
    Scrive alcune informazioni relative a un poligono."""
    ip = InteractivePlane('perimeter, surface')
    # I tre vertici del triangolo
    poli = Polygon((Point(-7, -3, width=5, name="A"),
                    Point(5, -5, width=5, name="B"),
                    Point(-3, 8, width=5, name="C")),
                    width=4, color='magenta', intcolor='olive drab')
    VarText(-3, -6, "perimetro={0}", poli.perimeter(), color='magenta')
    VarText(-3, -7, "area={0}", poli.surface(), color='olive drab')
    libmanex.end(ip)

def ex_20():
    """Circle:
    Circonferenze con centro nell'origine."""
    ip = InteractivePlane('Circle(Point, Point)')
    origine = Point(0, 0, visible=False, name="O")
    p0 = Point(-7, -3, width=5, name="P")
    Circle(origine, p0, color="#c0c0de", width=4)
    raggio = Segment(Point(-7, 9, width=5, name="A"),
                     Point(-4, 9, width=5, name="B"))
    Circle(origine, raggio, color="#c0c0de", width=4)
    libmanex.end(ip)

def ex_201():
    """radius:
    Visualizza il raggio di una circonferenza."""
    ip = InteractivePlane('radius')
    centro = Point(3, 4, name="C")
    p0 = Point(-5, 4, width=5, name="P")
    c = Circle(centro, p0, color="#c0c0de", width=4)
    VarText(-5, -1, 'raggio: {0}', c.radius())
    libmanex.end(ip)

def ex_211():
    """Intersection:
    Disegna una circonferenza tangente a una retta."""
    ip = InteractivePlane('Intersection line line')
    # Disegno retta e punto
    retta = Line(Point(-4, -1, width=5), 
                 Point(6, 2, width=5), 
                 width=3, color='DarkOrange1', name='r')
    punto = Point(-3, 5, width=5, name='P')
    # trovo il punto di tangenza
    perpendicolare = Orthogonal(retta, punto, width=1)
    p_tang = Intersection(retta, perpendicolare, width=5)
    # disegno la circonferenza
    Circle(punto, p_tang, width=4, color='IndianRed')
    libmanex.end(ip)

def ex_212():
    """Intersection:
    Disegna il simmetrico di un punto rispetto ad una retta."""
    ip = InteractivePlane('Intersection line circle')
    # disegno l'asse di simmetria e il punto
    asse = Line(Point(-4, -11, width=5), 
                Point(-2, 12, width=5), 
                width=3, color='DarkOrange1', name='r')
    punto = Point(-7, 3, width=5, name='P')
    # disegno la perpendicolare all'asse passante per il punto
    perp = Orthogonal(asse, punto, width=1)
    # trovo l'intersezione tra la perpendicolare e l'asse
    piede = Intersection(perp, asse)
    # disegno la circonferenza di centro piede e passante per punto
    circ = Circle(piede, punto, width=1)
    # trovo il simmetrico di punto rispetto a asse
    Intersection(perp, circ, -1, width=5, color='DebianRed', name="P'")
    libmanex.end(ip)

def ex_213():
    """Intersection:
    Disegna un triangolo equilatero."""
    ip = InteractivePlane('Intersection circle circle')
    # Disegno i due primi vertici
    v0=Point(-2, -1, width=5, name='A')
    v1=Point(3, 2, width=5, name='B')
    # Disegno le due circonferenze di centro p0 e p1 e passanti per p1 e p0
    c0=Circle(v0, v1, width=1)
    c1=Circle(v1, v0, width=1)
    # terzo vertice: intersezione delle due circonferenze
    v2=Intersection(c0, c1, 1, width=5, name='C')
    # triangolo per i 3 punti
    Polygon((v0, v1, v2), width=4, color='DarkSeaGreen4')
    libmanex.end(ip)

def ex_23():
    """Text:
    Scrive un titolo in due finestre grafiche."""
    ip0 = InteractivePlane('Text pale green', w=400, h=200)
    ip1 = InteractivePlane('Text blue violet', w=400, h=200)
    Text(-2, 2, "Prove di testo blue violet", 
         color='blue violet', width=20)
    Text(-2, 2, "Prove di testo pale green", 
         color='pale green', width=20, iplane=ip0)
    libmanex.end(ip0)

def ex_24():
    """Label:
    Disegna un punto e gli appiccica un'etichetta."""
    ip = InteractivePlane('Label')
    p0 = Point(7, 3, color='navy', width=10, name='A')
    Label(p0, 0, 20, "colore di A = 'navy'")
    libmanex.end(ip)

def ex_25():
    """VarText:
    Un testo che riporta la posizione dei un punto."""
    ip = InteractivePlane('VarText')
    p0 = Point(7, 3, color='green', width=10, name='A')
    VarText(-4, -3, "Posizione del punto A: ({0}; {1})",
                    (p0.xcoord(), p0.ycoord()),
                    color='green', width=10)
    libmanex.end(ip)

def ex_26():
    """VarLabel:
    Disegna un punto con un'ettichetta che riporta la sua posizione."""
    ip = InteractivePlane('VarLabel')
    p0 = Point(7, 3, color='red', width=10, name='A')
    VarLabel(p0, 0, -40,
             "A{0}", p0.coords(), color='red', width='10')
    libmanex.end(ip)

def ex_27():
    """PointOn:
    Disegna il simmetrico di un punto rispetto ad una retta."""
    ip = InteractivePlane('PointOn')
    # disegno l'asse di simmetria e il punto
    asse = Line(Point(-4, -11, width=5), 
                Point(-2, 12, width=5), 
                width=3, color='DarkOrange1', name='r')
    punto = Point(-7, 3, width=5, name='P')
    # disegno la perpendicolare all'asse passante per il punto
    perp = Orthogonal(asse, punto, width=1)
    # trovo il simmetrico di punto rispetto a asse
    PointOn(perp, -1, width=5, color='DebianRed', name="P'")
    Text(-5, -6, """P' � il simmetrico di P.""")
    libmanex.end(ip)

def ex_28():
    """ConstrainedPoint:
    Circonferenza e proiezioni sugli assi."""
    ip = InteractivePlane('ConstrainedPoint', sx=200)
    # Circonferenza
    origine = Point(0, 0, visible=False)
    unix = Point(1, 0, visible=False)
    uniy = Point(0, 1, visible=False)
    circ = Circle(origine, unix, color="gray10")
    # Punto sulla circonferenza
    cursore = ConstrainedPoint(circ, 0.25, color='magenta', width=20)
    # assi
    assex = Line(origine, unix, visible=False)
    assey = Line(origine, uniy, visible=False)
    # proiezioni
    py = Parallel(assey, cursore, visible=False)
    hx = Intersection(assex, py, color='red', width=8)
    px = Parallel(assex, cursore, visible=False)
    hy = Intersection(assey, px, color='blue', width=8)
    libmanex.end(ip)

def ex_29():
    """31: parameter.
    Scrivi i dati relativi a un punto collegato a un oggetto."""
    ip = InteractivePlane('parameter')
    c0 = Circle(Point(-6, 6, width=6), Point(-1, 5, width=6))
    c1 = Circle(Point(6, 6, width=6), Point(1, 5, width=6))
    a = PointOn(c0, 0.5, name='A')
    b = ConstrainedPoint(c1, 0.5, name='B')
    ip.newVarText(-5, -1, 'ascissa di A: {0}', a.xcoord())
    ip.newVarText(-5, -2, 'ordinata di A: {0}', a.ycoord())
    ip.newVarText(-5, -3, 'posizione di A: {0}', a.coords())
    ip.newVarText(-5, -4, 'parametro di A: {0}', a.parameter())
    ip.newVarText(5, -1, 'ascissa di B: {0}', b.xcoord())
    ip.newVarText(5, -2, 'ordinata di B: {0}', b.ycoord())
    ip.newVarText(5, -3, 'posizione di B: {0}', b.coords())
    ip.newVarText(5, -4, 'parametro di B: {0}', b.parameter())
    libmanex.end(ip)    

def ex_30():
    """Angle:
    Disegna un angolo e un angolo con i lati."""
    ip = InteractivePlane('Angle(Point, Point, Point)')
    ip.defwidth = 5
    a = Point(-2, 4, color="#40c040", name="A")
    b = Point(-5, -2, color="#40c040", name="B")
    c = Point(-8, 6, color="#40c040", name="C")
    d = Point(8, 6, color="#40c040", name="D")
    e = Point(5, -2, color="#40c040", name="E")
    f = Point(2, 4, color="#40c040", name="F")
    # angolo senza i lati
    Angle(a, b, c, color="#40c040")
    # angolo con i lati
    Angle(d, e, f, color="#c04040", sides=True)
    libmanex.end(ip)

def ex_31():
    """Angle:
    Somma di due angoli."""
    ip = InteractivePlane('Angle(Point, Point, Angle)')
    # i 2 angoli di partenza
    a = Angle(Point(-3, 7, width=6),
              Point(-7, 5, width=6),
              Point(-6, 8, width=6),
              sides=(0, 1), color="#f09000", name='alfa')
    b = Angle(Point(9, 2, width=6),
              Point(2, 3, width=6),
              Point(6, 4, width=6),
              sides=(0, 1), color="#0090f0", name='beta')
    # Punti di base dell'angolo somma di a b
    v = Point(-11, -8, width=6)
    p0 = Point(3, -10, width=6)
    # la somma degli angoli
    b1 = Angle(p0, v, b, (0, 1), color="#0090f0")
    p1 = b1.point1()
    a1 = Angle(p1, v, a, sides=True, color="#f09000")
    Text(-4, -12, "Somma di due angoli")
    libmanex.end(ip)

def ex_32():
    """Bisector:
    Disegna l'incentro di un triangolo."""
    ip = InteractivePlane('Bisector')
    # I tre vertici del triangolo
    a=Point(-7, -3, color="#40c040", width=5, name="A")
    b=Point(5, -5, color="#40c040", width=5, name="B")
    c=Point(-3, 8, color="#40c040", width=5, name="C")
    # Il triangolo
    Polygon((a, b, c))
    # Due angoli del triangolo
    cba=Angle(c, b, a)
    bac=Angle(b, a, c)
    # Le bisettrici dei due angoli
    b1=Bisector(cba, color="#a0c040")
    b2=Bisector(bac, color="#a0c040")
    # L'incentro
    Intersection(b1, b2, color="#c040c0", width=5, name="I")
    libmanex.end(ip)

def ex_33():
    """Polygonal:
    Disegna una linea spezzata aperta."""
    ip = InteractivePlane('Polygonal')
    points=(Point(-8, -3), Point(-6, -2), Point(-5, -2),
            Point(-4, 2),  Point(-2, 3),  Point(0, 4),   
            Point(2, 3),   Point(4, 2),   Point(5, -2),  
            Point(6, -2),  Point(8, -3))
    Polygonal(points, color='saddle brown', width=4)
    libmanex.end(ip)

def ex_34():
    """CurviLine:
    Disegna una linea curva aperta."""
    ip = InteractivePlane('CurviLine')
    points=(Point(-8, -3), Point(-6, -2), Point(-5, -2),
            Point(-4, 2),  Point(-2, 3),  Point(0, 4),   
            Point(2, 3),   Point(4, 2),   Point(5, -2),  
            Point(6, -2),  Point(8, -3))
    CurviLine(points, color='goldenrod', width=4)
    libmanex.end(ip)

def ex_40():
    """Calc:
    Calcolla il quadrato di un lato e la somma dei quadrati
    degli altri due di un triangolo."""
    ip = InteractivePlane('Calc')
    Circle(Point(2, 4), Point(-3, 4), width=1)
    ip.defwidth = 5
    a = Point(-3, 4, name="A")
    b = Point(7, 4, name="B")
    c = Point(-1, 8, name="C")
    ab = Segment(a, b, color="#40c040")
    bc = Segment(b, c, color="#c04040")
    ca = Segment(c, a, color="#c04040")
    q1 = Calc(lambda a: a*a, ab.length())
    q2 = Calc(lambda a, b: a*a+b*b, (bc.length(), ca.length()))
    VarText(-5, -5, "ab^2 = {0}", q1, color="#40c040")
    VarText(-5, -6, "bc^2 + ca^2 = {0}", q2, color="#c04040")
    libmanex.end(ip)

###
# Main
##
ex_23()
#libmanex.doall(locals(), 'ex_')
