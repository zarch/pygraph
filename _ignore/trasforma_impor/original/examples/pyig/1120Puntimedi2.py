#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig---------------1120Puntimedi2.py--#
#                                                                       #
#                       Punti medi di segmenti                          #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Abbiamo costruito con sola riga e compasso il punto medio di un segmento.
Ma, dato che il punto medio viene spesso utilizzato, pyig mette a 
disposizione due modi per ottenerlo:
- Punto medio di una coppia di punti
- Punto medio di un segmento

Problema
Disegnare:
- un triangolo,
- i punti medi dei suoi lati,
- i punti che stanno a met� tra questi punti

Soluzione
Una funzione che disegna un triangolo dati i vertici e che ne restituisce i 
lati, pu� far comodo (gi� vista in una puntata precedente).
Poi utilizzare gli oggetti MidSegment e MidPoints che vogliono come 
parametri rispettivamente un segmento e due punti.
"""

from pyig import *
ip = InteractivePlane()

def triangolo(v1, v2, v3, spessore, colore):
  """Disegna un triangolo, dati i vertici, e restituisce i suoi lati."""
  l1 = Segment(v1, v2, width=spessore, color=colore)
  l2 = Segment(v2, v3, width=spessore, color=colore)
  l3 = Segment(v3, v1, width=spessore, color=colore)
  return l1, l2, l3

l1, l2, l3 = triangolo(Point(-5, 0, width=6), 
                       Point(5, 0, width=6),
                       Point(0, 5*math.sqrt(3), width=6), 
                       10, "#aa0055")
#MidPoint ha come argomento un segmento
pm1 = MidPoint(l1, color="#5500aa", width=20)
pm2 = MidPoint(l2, color="#5500aa", width=20)
pm3 = MidPoint(l3, color="#5500aa", width=20)
  
triangolo(pm1, pm2, pm3, 2, "gray80")

#MidPoints ha come argomenti due punti
MidPoints(pm1, pm2, color="#5500aa", width=20)
MidPoints(pm2, pm3, color="#5500aa", width=20)
MidPoints(pm3, pm1, color="#5500aa", width=20)

###
# Messaggio finale
###

Text(-7, -5, """Triangolo con, evidenziati, i punti medi dei lati
e tre vertici di un altro triangolo concentrico
Muovi i vertici o i lati.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
