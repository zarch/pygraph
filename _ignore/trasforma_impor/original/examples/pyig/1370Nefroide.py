#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1370Nefroide.py--#
#                                                                       #
#                               Nefroide                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Nefroide

Problema
- Disegna una Nefroide come inviluppo di circonferenze

Soluzione
- Traccia la circonferenza base.
- traccia un suo diametro.
- Usa i punti vincolati e le rette perpendicolari per tracciare
  l'inviluppo di circonferenze.

"""

from pyig import *
ip = InteractivePlane()

# Numero di circonferenze dell'inviluppo,
# deve essere un numero float.
N = 20.

# Circonferenza base
centro = Point(-1, 2, color="#00f050", width=15, name="C")
punto = Point(-2, -2, color="#00f050", width=15, name="P")
c = Circle(centro, punto, color="#a87234")

# Diametro
d = Line(centro, punto, color="#a87234")

# Funzione che restituisce una Circonferenza dell'inviluppo
def circ(d, centro, color):
  n = Orthogonal(d, centro, width=1)
  punto = Intersection(n, d, width=1)
  return Circle(centro, punto, color=color, intcolor='')

for i in range(int(N)*2):
  circ(d, PointOn(c, i/N), "#a87234")

cc = circ(d, ConstrainedPoint(c, 0.5, width=7), "red")

Text(-5, -6, """Nefroide.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
