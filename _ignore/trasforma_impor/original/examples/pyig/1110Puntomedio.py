#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig---------------1110Puntomedio.py--#
#                                                                       #
#                      Punto medio di un segmento                       #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Ora abbiamo tutti gli strumenti per risolvere un classico problema da riga
e compasso: trovare il punto medio di un segmento.

Problema
Disegnare un segmento marron con un grosso punto giallo al centro.

Soluzione
Disegnato il segmento come richiesto, tracciare due circonferenze con il 
centro in un estremo e passanti per l'altro estremo. Tracciare la retta 
che passa per le loro intersezioni: il punto medio del segmento � 
l'intersezione tra il segmento stesso e questa retta.
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

from pyig import *
ip = InteractivePlane()

# Costruzione del segmento
##a=Point(5, 7)
##b=Point(-3, 2)
a = Point(-3, 2, width=6)
b = Point(5, 2, width=6)
s = Segment(a, b, width=6, color="#808000")

# Costruzione dell'asse
c1 = Circle(a, b, width=1)
c2 = Circle(b, a, width=1)
i1 = Intersection(c1, c2, 1, width=1)
i2 = Intersection(c1, c2, -1, width=1)
l = Line(i1, i2, width=1)

# Punto medio
pmedio = Intersection(s, l, width=4, color="#f0f000")

###
# Messaggio finale
###

Text(-7, -5, """Punto medio di un segmento,
Tutte le linee di costruzione sono sottilio.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
