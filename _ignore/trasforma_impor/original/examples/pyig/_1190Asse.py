# -*- coding: iso-8859-15 -*-
#--------------------------------pykig--------------------_1190Asse.py--#
#                                                                       #
#    Definizione della classe Segmento che contiene il metodo asse      #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Questo modulo contiene la definizione di una nuova classe: 'Segmento'
"""

from pyig import Segment, Orthogonal

###
# Nuova classe, che estende la classe Segment
# aggiungendo il metodo asse
###
class Segmento(Segment):                                    # 1
  """Classe che realizza un segmento con la capacit� di     # 2
     disegnare il suo asse."""
  def asse(self):                                           # 3
    """Funzione che restituisce l'asse di un segmento."""   # 4
    puntomedio = self.midpoint()                            # 5
    risultato = Orthogonal(self, puntomedio, width=1)       # 6
    return risultato                                        # 7
    
# 1 Definisce una nuova classe di nome "Segmento" che discende dalla
#   classe Segment.
#   La definizione continua finch� dura l'indentazione (il rientro
#   delle righe.
#   Gli oggetti della classe "Segmento" hanno tutte le funzionalit� degli
#   oggetti della classe "Segment" pi� il metodo "asse".
#   Potremmo dire che "Segmento" � un "Segment" capace anche di disegnare
#   il proprio asse.

# 2 Stringa di documentazione della classe Segmento.
#   � delimitata da tripli apici e pu� estendersi su pi� righe.

# 3 Definizione del metodo "asse" della classe "Segmento".
#   Ogni metodo deve avere un primo parametro, di solito chiamato "self".
#   La variabile self contiene il riferimento all'oggetto che esegue
#   il metodo.
#   Se s � un Segmento:
#   s=Segmento()
#   la chiamata di asse viene fatta in questo modo:
#   s.asse()

# 4 Stringa di documentazione del metodo asse.

# 5 Calcola il punto medio di questo segmento, del segmento contenuto
#   in self.

# 6 Calcola la perpendicolare a self passante per il punto medio di self.

# 7 Questo metodo � una funzione che restituisce una retta.
#   Il risultato � la retta, perpendicolare al segmento self, passante 
#   per il suo punto medio.
#   Utilizzando i metodi della programmazione funzionale le tre righe di
#   codice di questo metodo potevano essere riassunte in una sola:
#   return Orthogonal(self, self.midpoint(internal=True))
#   che restituisce esattamente la perpendicolare a self passante per
#   il punto medio di self. 

