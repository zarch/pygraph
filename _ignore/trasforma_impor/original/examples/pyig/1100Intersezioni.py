#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------1100Intersezioni.py--#
#                                                                       #
#               Intersezioni tra rette o circonferenze                  #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Una delle operazioni geometriche di base � l'intersezione di due linee.
Mentre due rette hanno un'unica intersezione, le coniche ne hanno due.
Quindi gli oggetti intersezione saranno diversi se cerchiamo l'intersezione
tra rette o tra coniche.

Problema
Disegnare due rette e la loro intersezione.
Disegnare due circonferenze e le loro intersezioni

Soluzione
Disegnare due rette e utilizzare l'oggetto: Intersection.
Disegnare due circonferenze e l'oggetto: Intersection.
disegnare l'intersezione tra una circonferenza e una retta.
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

from pyig import *
ip = InteractivePlane()

###
# Due linee e la loro intersezione
###

l1 = Line(Point(5, 7), Point(-3, 2))
l2 = Line(Point(2, 1), Point(-6, 6))
Intersection(l1, l2, color="#444400", width=6)

###
# Due circonferenze e le loro intersezioni
###

c1 = Circle(Point(-3, -5), Point(1, -5))
c2 = Circle(Point(2, -5), Point(-2, -5))
Intersection(c1, c2, 1, color="#440044", width=6)
Intersection(c1, c2, -1, color="#aa00aa", width=6)

Intersection(l1, c1, 1, color="#004444", width=6)
Intersection(l1, c1, -1, color="#00aaaa", width=6)

###
# Messaggio finale
###

Text(-7, -12, """Intersezioni tra rette,
tra circonferenze e tra rette e circonferenze.
Ma non si dovevano vedere anche le intersezioni
tra una retta e una circonferenza?""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
