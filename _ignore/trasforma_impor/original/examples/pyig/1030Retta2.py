#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------------1030Retta2.py--#
#                                                                       #
#              Retta per due punti con qualche attributo                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
In pyig � possibile specificare alcuni attributi per ogni oggetto visibile.
Se ne parler� pi� avanti in modo completo, ma pu� essere comodo fin da ora
specificare il colore e il nome degli oggetti visualizzati:

Problema
Riprendiamo il problema precedente colorando i punti e assegnando loro
un nome.

Soluzione
Nel creare un oggetto si possono specificare alcuni attributi come:
color, width e name:
- name � una stringa;
- color � una stringa nel formato: "#RRGGBB" dove RR, GG, BB sono 
  numeri esadecimali che definiscono le componenti rosse, verdi e blu del
  colore.
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

from pyig import *
ip = InteractivePlane()

###
# Disegna due punti
###

a = Point(7, -8, color="#ff0000", width=6, name="A")
b = Point(-5, 1, name="B", color="#00ff00", width=6)

###
# Disegna una retta per i due punti
###

Line(a, b, width=3, color="#f0f000")

###
# Messaggio finale
###

Text(-7, -5, """Retta passante per due punti
Muovi i punti, muovi le etichette, cambia il colore.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()


