#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------1000Fogliovuoto.py--#
#                                                                       #
#                 Un foglio bianco con assi e griglia                   #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Eseguendo un programma vuoto, viene aperto un foglio "vuoto".
In realt� il foglio no � del tutto vuot: sono presenti gli assi cartesiani
e una griglia.

Questo � un programma Python. Quando viene eseguito, chiama automaticamente
l'interprete Python. � la prima riga del programma che indica al sistema 
operativo quale � l'interprete da invocare:

#!/usr/bin/env python

Perch� questo programma possa costruire gli oggetti e aprire la finestra
di grafica deve caricare la libreia pyig. Questo viene fatto dal comando:

from pyig import *

Alla fine del programma, per rendere attiva la finestra grafica, si deve
dare il comando:

ip. mainloop()

Vediamo il primo:

Problema
Aprire una finestra grafica "vuota".

Soluzione
- Caricare la libreria pyig.
- Rendere attiva la finestra.
"""

###
# Chiamata della libreria pyig
###

import pyig


###
# Creazione di una finestra interattiva
###

ip = pyig.InteractivePlane()

###
# Attivazione della finestra interattiva
###

ip.mainloop()

###
# Nota:
# Questo programma, oltre alla chiamata della libreria pyig.py, 
# contiene solo commenti, quindi fa un po' pochino!
###

