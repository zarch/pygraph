# -*- coding: utf_8 -*-
#---------------------------------pyig--------------------1460Campo.py--#
#                                                                       #
#                           Campo elettrico                             #
#                                                                       #
#--Daniele Zambelli----------------GPL---------------------------2010---#

from __future__ import division

"""
Rappresentazione di un campo elettrico generato da due cariche
di segno opposto.
"""

'''

def test_12():
  """12: Test for Point."""
  ip = InteractivePlane(test_12.__doc__, sx=40, sy=40)
  Text(-7, 13, """PointD""", color='#408040', width=12)
  a0 = Point(0, -2, visible=False, name="0")
  b0 = Point(1, -2, visible=False, name="1")
  line0 = Line(a0, b0)
  cursor = ConstrainedPoint(line0, 1, name='cursor', width=5)
  par = Calc(lambda x: 0.5*x*x, cursor.parameter())
  Point(cursor.parameter(), par, width=5, color='dark turquoise')
  VarText(-5, -4, "x = {0}", cursor.parameter(),
          color='olive drab')
  VarText(-5, -5, "y = 0,5*x**2 = {0}", par, color='olive drab')
  end(ip)

def test_13():
  """13: newPoint:
  Point with object data as argouments."""
  ip = InteractivePlane('13: newVarText')
  ip.newText(0, 13, 'Finestra con due punti e le loro coordinate', 
             width=20, color='DarkOrchid3')
  p0 = ip.newPoint(-2, 7, width=8, name="P")
  xp = ip.newPoint(p0.xcoord(), 0,
                   width=8, color='dark turquoise', name='X_P')
  ip.newVarText(-5, -4, '{0}', p0.coords())
  ip.newVarText(5, -4, '{0}', xp.coords(), color='dark turquoise')
  end(ip)

'''

K = 10

from pyig import *
from pyig import _DataObject

class Charge(Point):
    def __init__(self, q, x, y, *arg, **args):
        Point.__init__(self, x, y, *arg, **args)
        self._q = q
        
    def _getq(self):
        return self._q

    def charge(self):
        """Return data object charge."""
        return _DataObject(self, self._getq, 'charge')

def xc(xa, ya, qa, xb, yb, qb, xq, yq):
    dxa = xq-xa
    dya = yq-ya
    da = math.hypot(dxa, dya)
    if da == 0: return xq
    fxa = K*(qa/da**3*dxa)
    dxb = xq-xb
    dyb = yq-yb
    db = math.hypot(dxb, dyb)
    if db == 0: return xq
    fxb = K*(qb/db**3*dxb)
    return fxa+fxb+xq

def yc(xa, ya, qa, xb, yb, qb, xq, yq):
    dxa = xq-xa
    dya = yq-ya
    da = math.hypot(dxa, dya)
    if da == 0: return yq
    fya = K*(qa/da**3*dya)
    dxb = xq-xb
    dyb = yq-yb
    db = math.hypot(dxb, dyb)
    if db == 0: return yq
    fyb = K*(qb/db**3*dyb)
    return fya+fyb+yq

ip = InteractivePlane()
# due cariche:
a = Charge(-5, -5, 0, width=5, name='A(-)', color='green')
#a.carica = -1
b = Charge(+5, 5, 0, width=5, name='B(+)', color='red')
#b.carica = +1

points = []
xmi = -14
xma = +15
ymi = -14
yma = +15
for i in range(xmi, xma):
    for j in range(ymi, yma):
#        points.append(Point(i, j, width=1, visible=False))
        q = Point(i, j, visible=False)
        xcoord = Calc(xc, (a.xcoord(), a.ycoord(), a.charge(), 
                               b.xcoord(), b.ycoord(), b.charge(), 
                               q.xcoord(), q.ycoord()))
        ycoord = Calc(yc, (a.xcoord(), a.ycoord(), a.charge(), 
                               b.xcoord(), b.ycoord(), b.charge(), 
                               q.xcoord(), q.ycoord()))
        q1 = Point(xcoord, ycoord, color='yellow', visible=False)
        vq = Vector(q, q1, width=1, color='red')

q = Point(3, 5, color='blue', width=5)
#xcoord = Calc(coord, (a, b, q))
#ycoord = Calc(coord, (a, b, q))
xcoord = Calc(xc, (a.xcoord(), a.ycoord(), a.charge(), 
                       b.xcoord(), b.ycoord(), b.charge(), 
                       q.xcoord(), q.ycoord()))
ycoord = Calc(yc, (a.xcoord(), a.ycoord(), a.charge(), 
                       b.xcoord(), b.ycoord(), b.charge(), 
                       q.xcoord(), q.ycoord()))
#print coords.data()
q1 = Point(xcoord, ycoord, color='yellow', visible=False)
vq = Vector(q, q1, width=4, color='red')
VarLabel(q, 10, 10, '({0}, {1})', (q.xcoord(), q.ycoord()))
#q1 = Point(Calc(lambda: x, y: )) TODO

ip.mainloop()
