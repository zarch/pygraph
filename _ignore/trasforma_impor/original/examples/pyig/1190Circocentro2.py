#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------1190Circocentro2.kpy--#
#                                                                       #
#      Circocentro di un triangolo e circonferenza circoscritta         #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Riprendiamo il problema del circocentro.
Per risolverlo bisognava tracciare gli assi di alcuni segmenti: i lati.
Estendiamo la classe Segment in modo da aggiungere, ad un segmento,
la capacit� di tracciare il suo asse.
La Programmazione Orientata agli Oggetti (OOP) fornisce dei meccanismi
semplici per fare questo.

Problema
- amplia la classe "Segment" aggiungendo il meto "asse"
- disegna la circoferenza circoscritta a un triangolo

Soluzione
- utilizza la classe "Segmento", derivata dalla classe "Segment"
  presente nel modulo _1170Asse.py
- disegnare la circonferenza circoscritta al triangolo

"""

from pyig import *
ip = InteractivePlane()

# Dal modulo "_1170Asse" viene caricata la definizione della
# classe "Segmento".
from _1190Asse import Segmento

# tre punti
a = Point(-2, 4, width=6)
b = Point(-3, -3, width=6)
c = Point(5, 2, width=6)

# I seguenti 3 lati non sono oggetti della classe "Segment" ma della
# nuova classe "Segmento", che *aggiunge* ai metodi della classe "Segment"
# anche il metodo "asse".
l1 = Segmento(a, b)
l2 = Segmento(b, c)
l3 = Segmento(c, a)

# assi
a1 = l1.asse()
a2 = l2.asse()
a3 = l3.asse()

# corcocentro
circocentro = Intersection(a1, a2, width=10, color="#ff66aa")

# circonferenza circoscritta:
Circle(circocentro, a)

###
# Messaggio finale
###

Text(-2, -3.5, """Triangolo,
circocentro
e circonferenza circoscritta.
Modifica colori e spessori del circocentro e della circonferenza circoscritta.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
