#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------1310Traslazione2.py--#
#                                                                       #
#                             Traslazione                               #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Traslazioni

Problema
- Disegna un poligono.
- Disegna una retta che passi per un suo vertice.
- Disegna il poligono traslato rispetto al primo.

Soluzione
- Disegnare il fascio di rette parallele a quella data, passanti
  per i vertici del poligono.
- Utilizzare i punti fissati ad un oggetto, PointOn.

"""

from pyig import *
ip = InteractivePlane()

###
# Poligono0
###
a0 = Point(-7, 5, color="#00f050", width=6, name="A")
b0 = Point(-2, -2, color="#00f050", width=6, name="B")
c0 = Point(-3, 6, color="#00f050", width=6, name="C")
d0 = Point(-4, 2, color="#00f050", width=6, name="C")
Polygon((a0, b0, c0, d0), color="#a87234")

# Retta
a1 = Point(1, 8, color="#00f050", width=6, name="A'")
ra = Line(a0, a1, width=1)

# Rette parallele a r0
rb = Parallel(ra, b0, width=1)
rc = Parallel(ra, c0, width=1)
rd = Parallel(ra, d0, width=1)

# Vertici del poligono traslato
b1 = PointOn(rb, 1, name="B'")
c1 = PointOn(rc, 1, name="C'")
d1 = PointOn(rd, 1, name="D'")

#Poligono traslato
Polygon((a1, b1, c1, d1), color="#72a834")

Text(-5, -6, """Traslazione di un poligono.""")

ip.mainloop()
