#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
#-------------------------------python------------------------other.py--#
#                                                                       #
#                             Avanzi vari                               #
#                                                                       #
#--Daniele Zambelli-----------Licence-GPL------------------------2011---#

from __future__ import print_function

''' TODO da mettere negli esempi
def ex_01newPoint():
    """newPoint:
    Crea 20 punti nel piano interattivo."""
    ip = InteractivePlane('01: newPoint')
    from random import randrange
    for cont in range(20):
        ip.newPoint(randrange(-14, 14), randrange(-14, 14),
                    color=((randrange(256), randrange(256), randrange(256))),
                    width=randrange(20),
                    name='P'+str(cont))
    libmanex.end(ip)
'''
''' TODO da mettere tra gli esempi
def ex_29():
    """Orthogonal:
    Costruzione di un quadrato dato il lato."""
    ip = InteractivePlane('')
    # Cambiare il valore della seguente variabile per rendere
    # visibili o invisibili le linee di costruzione
    
    COSTRUZIONE=False # True
    
    p1=Point(-3, -1, color="#40c040")         # due vertici consecutivi
    p2=Point(1, -2, color="#40c040")
    
    # il primo lato
    l1=Segment(p1, p2)
    
    # istruzioni per trovare il terzo vertice:
    # retta perpendicolare al lato
    l=Orthogonal(l1, p2, visible=COSTRUZIONE, width=1)
    # riportare la lunghezza del lato
    c=Circle(p2, p1, visible=COSTRUZIONE, width=1)
    # terzo vertice
    p3=Intersection(l, c, 1)
    
    # istruzioni per trovare il quarto vertice:
    # retta perpendicolare al lato
    l=Orthogonal(l1, p1, visible=COSTRUZIONE, width=1)
    # riportare la lunghezza del lato
    c=Circle(p1, p2, visible=COSTRUZIONE, width=1)
    # quarto vertice
    p4=LineCircleIntersection(l, c, 1)
    
    # Il quadrato:
    Polygon((p1, p2, p3, p4))

    libmanex.end(p)

def ex_30():
    """Parallel:
    Talete."""
    ip = InteractivePlane('')
    # tre punti
    v=Point(-7, 5, color="#40c040", name="V")
    a=Point(-8, -2, color="#40c040", name="A")
    b=Point(2, -1, color="#40c040", name="B")
    # due semirette incidenti che formano i lati dell'angolo
    la=Ray(v, a)
    lb=Ray(v, b)
    # retta che interseca i lati dell'angolo
    r0=Line(a, b)
    # retta parallela a r0 passante per un punto P
    p=Point(6, 4, color="#40c040", name="P")
    r1=Parallel(r0, p)
    # Intersezioni di questa retta, r1, con i lati dell'angolo
    m=Intersection(r1, la, width=10, color="#a04040", name="M")
    n=Intersection(r1, lb, width=10, color="#4040a0", name="N")
    # Distanze dei punti A e B dal vertice
    va=Segment(v, a).length()
    vb=Segment(v, b).length()
    # Rapporto tra queste due valori
    rab=Calc("{}/{}", (va, vb))
    # Visualizzazione di questi valori
    VarText(-3, -7, "va={};\tvb={};\tva/vb={}", (va, vb, rab))
    # Distanze dei punti M e N dal vertice
    vm=Segment(v, m).length()
    vn=Segment(v, n).length()
    # Rapporto tra queste due valori
    rmn=Calc("{}/{}", (vm, vn))
    # Visualizzazione di questi valori
    VarText(-3, -7.5, "vm={};\tvn={};\tvm/vn={}", (vm, vn, rmn))
    libmanex.end(p)
'''
''' TODO spostare negli esempi
def ex_36():
    """Circle:
    Circonferenza per 3 punti."""
    ip = InteractivePlane('')
    def Axis(p0, p1,
             visible=None, color=None, width=None, name=None):
      """Asse del segmento di estremi p0, p1."""
      # Circ. con centro in un estremo passanti per l'altro stremo
      c0=Circle(p0, p1, visible=False)
      c1=Circle(p1, p0, visible=False)
      # crea e restituisce la retta passante per le due intersezioni
      # delle circonferenze, cio� l'asse del segmento
      return Line(CircleCircleIntersection(c0, c1, -1, visible=False),
                  CircleCircleIntersection(c0, c1, +1, visible=False),
                  visible, color, width, name)
    
    def CircleB3P(p0, p1, p2,
                  visible=None, color=None, width=None, name=None):
      """Circonferenza per i 3 punti: p0, p1, p2."""
      a0=Axis(p0, p1, visible=False)                # asse del segmento p0, p1
      a1=Axis(p1, p2, visible=False)                # asse del segmento p1, p2
      c=Intersection(a0, a1, visible=False) # centro della circonferenza
      return Circle(c, p0, visible, color, width, name)
    
    # 3 punti
    a=Point(-7, -3, color="#40c040", width=5, name="A")
    b=Point(5, -5, color="#40c040", width=5, name="B")
    c=Point(0, -4, color="#40c040", width=5, name="C")
    # circonferenza che passa per questi 3 punti
    CircleB3P(a, b, c, color="#c0c040")
    libmanex.end(p)
'''

from pyig import *
def ex_37():
    """Circle:
    Angolo dati una semiretta e un altro angolo.
    Data l'utilit� di riportare un angolo, esiste una primitiva
    con lo stesso nome e con gli stessi parametri che svolge
    la stessa operazione."""
#    from pyig import *
    ip = InteractivePlane('')
    def AngleRA(point0, vertex, angle, sides,
                visible=None, color=None, width=None, name=None):
        """Angolo dato un lato e congruente ad un altro angolo.
        Funziona solo con angoli convessi...
        E ha anche un comportamento strano, quando? perch�?"""
        # salvo l'attuale valore predefinito di visible
        vis=ip.defvisible
        # i prossimi oggetti non saranno visibili
        ip.defvisible=False
        l0=angle.side0()             # lati dell'angolo
        l1=angle.side1()
        raggio0=Segment(angle.vertex(), angle.point0()) # i tre segmenti
        raggio1=Segment(angle.vertex(), angle.point1()) # usati come
        raggio2=Segment(angle.point0(), angle.point1()) # raggi
        # costruzione su ray di un triangolo congruente al triangolo
        # point0, vertex, point1
        ray=Ray(vertex, point0)
        c0=Circle(vertex, raggio0)
        c1=Circle(vertex, raggio1)
        i0=Intersection(ray, c0, 1)
        c2=Circle(i0, raggio2)
        i1=Intersection(c1, c2, 1)
        # risistemo il valore predefinito di visible
        ip.defvisible=vis
        # creo e restituisco l'angolo cercato
        return Angle(point0, vertex, i1, sides, visible, color, width, name)
    
    # un angolo
    a=Point(ip, 3, 5, color="#40c040", width=5, name="A")
    b=Point(ip, -8, 4, color="#40c040", width=5, name="B")
    c=Point(ip, -1, 8, color="#40c040", width=5, name="C")
    a1=Angle(a, b, c, (0, 1))
    # una semiretta
##    r=Ray(Point (1, 5), Point(3, -4))
    # l'angolo costruito su una semiretta congruente ad un angolo dato
    a2=AngleRA(Point (ip, 7, -2), Point(ip, -4, -3), a1, (0, 1))
##    libmanex.end(p)
    ip.mainloop()

ex_37()
'''
'''
#def exx_41():
#    """Polygon:
#    Un poligono e un poligono intrecciato."""
#    # import random
#    ip = InteractivePlane('')
#    # Tupla contenente 4 punti
#    v1=(Point(-5, 1), Point(-2, 1), Point(-2, 6), Point(-5, 6))
#    # Poligono che ha per vertici quei 4 punti
#    p1=Polygon(v1, "#ff0090")
#    # Tupla contenente 4 punti
#    v2=(Point(2, 1), Point(5, 6), Point(5, 1), Point(2, 6))
#    # Poligono che ha per vertici quei 4 punti
#    p2=Polygon(v2, "#00a590")
#    # Superficie e perimetro del primo poligono
#    # qui uso metodi della classe Poligono
#    area1=p1.surface()
#    peri1=p1.perimeter()
#    # Visualizzazione dei dati vicino al poligono
#    VarLabel(p1, 0, -15, "Area: {}; perimetro: {}", (area1, peri1))
#    # Superficie e perimetro del secondo poligono
#    # qui creo oggetti della classe Surface e Perimeter
#    area2=Surface(p2)
#    peri2=Perimeter(p2)
#    # Visualizzazione dei dati vicino al poligono
#    VarLabel(p2, 0, -15, "Area: {}; perimetro: {}", (area2, peri2))
#    libmanex.end(p)
''' TODO da mettere pi� avanti
def ex_03newPoint():
    """newPoint:
    Crea un punto con una posizione che dipende da un altro punto."""
    ip = InteractivePlane('03: newPoint')
    ip.newText(0, 13, 'Finestra con due punti',
               width=20, color='DarkOrchid3')
    p0 = ip.newPoint(-2, 7, width=8, name="P")
    px = ip.newPoint(p0.xcoord(), 0,
                     width=5, color='dark turquoise', name='X_P')
    libmanex.end(ip)

def ex_04newVarText():
    """newPointD:
    Visualizza alcune informazioni che dipendono da altri oggetti."""
    ip = InteractivePlane('04: newVarText')
    ip.newText(0, 13, 'Finestra con due punti e le loro coordinate', 
               width=20, color='DarkOrchid3')
    p0 = ip.newPoint(-2, 7, width=8, name="P")
    xp = ip.newPoint(p0.xcoord(), 0,
                     width=8, color='dark turquoise', name='X_P')
    ip.newVarText(-5, -4, '{}', p0.coords())
    ip.newVarText(5, -4, '{}', xp.coords(), color='dark turquoise')
    libmanex.end(ip)
'''

'''Deprecated
def exx_xx():
    """Intersection:
    Segmento che congiunge un punto con l'intersezione di due rette."""
    ip = InteractivePlane('LineLineIntersection')
    # Uno degli estremi del segmento
    p0 = Point(ip, 0, 7)
    # Due rette
    l1=Line(Point(ip, -5, 2, color="#00ff00", width=5),
            Point(ip, 5, -2, color="#00ff00", width=5), color="#a0a0a0")
    l2 = Line(Point(ip, -5, -2, color="#00ff00", width=5),
            Point(ip, 5, 2, color="#00ff00", width=5), color="#a0a0a0")
    # Intersezione delle due rette
    i = Intersection(l1, l2)
    # Attributi dell'intersezione
    i.color = "#ffff00"
    i.width = 10
    # Segmento che congiunge il punto p0 con il punto i
    Segment(p0, i, color="#a0f000")
    libmanex.end(ip)

def exx_xx():
    """Intersection:
    Punto che percorre una circonferenza."""
    ip = InteractivePlane('LineCircleIntersection')
    # Punto mobile
    p0 = Point(ip, 0, 7, color="#00ff00", width=5)
    # Il centro e un punto della circonferenza: invisibili
    centro = Point(ip, 0, 0, visible=False)
    punto = Point(ip, 5, -2, visible=False)
    # Circonferenza
    c = Circle(centro, punto, color="#a0a0a0")
    # Linea che congiunge il centro al punto p0
    l1 = Line(centro, p0, visible=False)
    # Intersezione di questa linea con la circonferenza
    i = Intersection(l1, c, 1)
    i.setcolor("#ffff00")
    i.setwidth(10)
    libmanex.end(ip)

def exx_xx():
    """Intersection:
    Retta passante per le intersezioni di due circonferenze."""
    ip = InteractivePlane('CircleCircleIntersection')
    # Circonferenze
    p0 = Point(ip, -5, 7, color="#00ff00")
    p1 = Point(ip, +5, 7, color="#00ff00")
    centro0 = Point(ip, -2, 0, color="#00ff00")
    centro1 = Point(ip, +2, 0, color="#00ff00")
    c0 = Circle(centro0, p0, color="#a0a0ff")
    c1 = Circle(centro1, p1, color="#ffa0a0")
    #punti di intersezione
    i0 = Intersection(c0, c1, -1, color="#ffff00", width=10)
    i1 = Intersection(c0, c1, +1)
    i1.color = "#ffff00"
    i1.width = 10
    #retta per le due intersezioni
    Line(i0, i1, color="#a0a000")
    libmanex.end(ip)
'''

