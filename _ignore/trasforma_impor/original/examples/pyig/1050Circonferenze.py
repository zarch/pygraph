#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------1050Circonferenze.py--#
#                                                                       #
#                            Circonferenze                              #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Dopo la retta il secondo strumento della geometria greca era la circonferenza.
Pyig mette a disposizione l'oggetto Circle che traccia una circonferenza
dati il centro e un punto.

Problema
Disegnare quattro circonferenze passanti per l'origine degli assi.

Soluzione
Usare l'oggetto Circle().
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

from pyig import *
ip = InteractivePlane()

"""
###
# Metodo pi� pedante
###

p = Point(0, 0)
c1 = Point(2, 2)
c2 = Point(2, -2)
c3 = Point(-2, 2)
c4 = Point(-2, -2)

Circle(c1, p)
Circle(c2, p)
Circle(c3, p)
Circle(c4, p)

###
# Metodo pi� pythonico
###

# Punto comune alle circonferenze
p = Point(0, 0)

# "Tupla" dei centri
# La Tupla � una specie di vettore immutabile che pu� contenere oggetti Python

centri = (Point(2, 2), Point(2, -2), Point(-2, 2), Point(-2, -2))

for c in centri:
  Circle(c, p)
"""
###
# Metodo che utilizza un doppio ciclo
###

# Punto comune alle circonferenze
p = Point(0, 0)

# Il doppio ciclo individua le coordinate dei quattro punti

cc = (-2, 2)

for x in cc:
  for y in cc:
    Circle(Point(x, y), p)

###
# Messaggio finale
###

Text(-2, -8, """Quattro circonferenze passanti per l'origine degli assi
Si possono muovere, ingrandire o rimpicciolire...
Che cosa non si pu� fare?.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
