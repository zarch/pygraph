#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1060Attributi.py--#
#                                                                       #
#       Vari modi per settare gli attributi degli oggetti Pyig          #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Abbiamo visto che gli oggetti Pyig hanno attributi che possono assumere
diversi valori. In particolare ogni oggetto visualizzabile ha i seguenti 
attributi:

visible:    True | False
color:      "#RRGGBB"
width:      <numero>
name:       <stringa>

Tutti questi attributi hanno dei valori predefiniti (di default) che sono:

visible:    True
color:      "#0000ff"
width:      3
name:       ""

Quindi se vogliamo che un oggetto abbia le caratteristiche predefinite, non
abbiamo bisogno di specificane gli attributi.

Ci sono diversi modi per settare i valori di questi attributi:
1. nella chiamata del costruttore:
1.1. per posizione,
1.2. per nome
2. con la chiamata ad un metodo
3. modificando i valori di default

Problema
Disegnare punti e segmenti con valori diversi degli attributi, sfruttando i 
diversi metodi.

Soluzione
� pi� difficile da spiegare a parole che da capire vedendo gli esempi.
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###
from pyig import *
ip = InteractivePlane()

###
# Punto con i valori predefiniti degli attributi
###
a = Point(-4, 6)

###
# 1. Attributi impostati nella fase di costruzione dell'oggetto punto
###
"""
 1.1. Argomenti passati per posizione
"""
b = Point(-3, 1, True, "#900090", 10, "A")
"""	
 1.2.Argomenti passati per posizione (i primi 2) e per nome (gli altri)
"""
c = Point(-2, 6, visible=True, color="#3405f0", width=20, name="B")
"""
 Quamdo si passano gli argomenti per nome si possono specificare solo alcuni
 attributi quelli diversi dai valori predefiniti, e nell'ordine che si
 preferisce:
"""
d = Point(-1, 1, width=30, color="#009090")

###
# 2. Attributi impostati dopo aver costruito l'oggetto chiamando un metodo
###
"""
 Tutti gli oggetti visibili hanno i seguenti attributi:
   visible
   width
   color
   name
 e le seguenti funzioni che restituiscono un oggetto di tipo Data:
   xcoord()
   ycoord()
   coords()
 quindi � possibile modificare questi attributi dopo aver costruito
 l'oggetto:
"""
e = Point(0, 6)
e.width = 30
e.color = "#900090"
e.name = "Punto E"

###
# Oggetti invisibili
###
"""
 A volte pu� essere comodo avere degli oggetti invisibili
 ad esempio i vertici di un triangolo che non si vuole sia modificabile.
 Ecco tre modi per "creare" punti invisibili:
"""
f = Point(1, 6, False)
g = Point(2, 1, visible=False)
h = Point(3, 6)
h.visible = False

###
# 4. Modifica dei valori predefiniti
###
"""
 Se si vuole che i prossimi oggetti creati abbiano dei particolari attributi
 predefiniti si pu� modificare le impostazioni predefinite (i valori di 
 default).
 Ogni piano (InteractivePlane) ha i propri valori di dafault per 
 i nuovi oggetti che vengono creati:

    self.defvisible = True
    self.defname = ''
    self.defwidth = 3
    self.defwidthtext = 10
    self.defcolor = '#0505a0'
    self.defdragcolor = '#05a005'
    self.defintcolor = ''
    self.defangledim = 4

 Questi possono essere cambiati, ad esempio, se voglio che i 
 prossimi oggetti abbiano tutti larghezza 5 e colore grigio:
"""
ip.defwidth = 5
ip.defcolor = 'gray'
Segment(a, b)
Segment(b, c)
Segment(c, d)
Segment(d, e, width=10)
"""
 Come si vede, i valori passati come argomenti hanno la precedenza 
 sui valori predefiniti.

 Per disegnare un triangolo con i lati tutti dello stesso colore
 e spessore:
"""
ip.defcolor = "#099050"
Segment(f, g)
Segment(g, h)
Segment(h, f)
"""ip, 
 E un rettangolo con i vertici di un colore e 
 i lati di un altro colore:
"""
ip.defwidth = 5
ip.defcolor = "#0ff0a0"
i = Point(3, -6)
j = Point(8, -6)
k = Point(8, 3)
l = Point(3, 3)

ip.defcolor = "#a05600"
Segment(i, j)
Segment(j, k)
Segment(k, l)
Segment(l, i)

###
# Messaggio finale
###

Text(-7, -5, """Punti e segmenti di diverso colore, dimensione e stile
Muovi i punti e i segmenti, qualche osservazione?
Osserva i vertici dei due poligoni e 
prova a muoverli""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
