#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------1230Inviluppi02.py--#
#                                                                       #
#                               inviluppi                               #
#                                                                       #
#--Bruno Stecca-Daniele Zambelli--GPL----------------------------2006---#

"""
Problema
Disegnare un inviluppo di segmenti in ogni quadrante del piano cartesiano.

Soluzione
E' molto simile al problema precedente, ma il maggior numero di segmenti
invita a esplorare soluzioni alternative.
Ripetere un gran numero di istruzioni *quasi* uguali, � una grande noia e
gli informatici, che in fatto di pigrizia battono addirittura i matematici,
hanno inventato dei meccanismi per farglielo fare al computer.
Le istruzioni di iterazione, i cicli, sono proprio quelle che fanno al caso
nostro.
Un estremo del segmento deve percorrere i valori 0.5, 1, 1.5, 2, ... e,
l'altro estremo i valori 9, 8.5, 8, ...
Incominciamo semplificando il problema: mettiamo gli estremi ad intervalli
interi.
Nell'ambiente IDLE si possono fare alcune prove per ottenere le sequenze di
numeri di cui abbiamo bisogno:

>>> for i in range(10):
        print(i)

Ma a noi serve che il ciclo produca contemporaneamente 0 e 10, 1 e 9,
2 e 8, ...

Non � difficile:

>>> for i in range(10):
        print(i, 9-i)

Ora abbiamo lo strumento che ci serve per risolvere il problema
semplificatO.

"""

from __future__ import print_function

import random
from pyig import *
ip = InteractivePlane()

def coloreacaso():
  """Funzione che restituisce un colore a caso."""
  return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256), 
                                         random.randrange(256),
                                         random.randrange(256))

###
# soluzione del problema semplificato
###

def inviluppo1():
  """Inviluppo nel primo quadrante."""
  for i in range(10):
    A = Point(0, i)
    B = Point(10-i, 0)
    Segment(A, B, color=coloreacaso())

"""
L'inviluppo � solo nel primo quadrante, dobbiamo trovare il modo per
per realizzarlo anche negli altri...
proviamo per� a semplificare* un po' il codice. 
Conviene usare una variabile per contenere il valore
10-i e, per semplificare, iserire la costruzione dei due punti
all'interno della costruzione del segmento:

* il concetto di "semplice", ovviamente, � molto personale: quello
che a me pu� sembrare pi� semplice pu� essere pi� complicato per un
altro e viceversa!
"""

def inviluppo2():
  """Inviluppo nel primo quadrante un po' pi� "semplice"."""
  for i in range(10):
    j = 10-i
    Segment(Point(0, i), Point(j, 0), color=coloreacaso())

"""
Passiamo ora a coprire gli altri quadranti.
Gli altri valori di cui abbiamo bisogno differiscono da quelli trovati
solo per il segno.
"""

def inviluppo3():
  """Inviluppo in tutti i quadranti."""
  for i in range(10):
    j = 10-i
    Segment(Point(0, i), Point(j, 0), color=coloreacaso())
    Segment(Point(0, -i), Point(j, 0), color=coloreacaso())
    Segment(Point(0, -i), Point(-j, 0), color=coloreacaso())
    Segment(Point(0, i), Point(-j, 0), color=coloreacaso())

"""
inviluppo3 � *quasi* giusto, osservate bene il disegno...
Come modificarlo perch� faccia esattamente quello che volevamo?
La soluzione non � difficile, prova a trovarla.
"""

def inviluppo4():
  """Inviluppo in tutti i quadranti, corretto!."""
  for i in range(10):
    pass

"""
Ma il problema iniziale chiedeva un inviluppo con intervalli diversi.
E se volessi un inviluppo pi� o meno ampio?
proviamo a parametrizzare la procedura inviluppo.
Incominciamo a cambiare la larghezza:
"""

def inviluppo5(larghezza):
  """Inviluppo in tutti i quadranti con larghezza variabile."""
  for i in range(larghezza):
    j = larghezza-i
    Segment(Point(0, i), Point(j, 0), color=coloreacaso())
    Segment(Point(0, -i), Point(j, 0), color=coloreacaso())
    Segment(Point(0, -i), Point(-j, 0), color=coloreacaso())
    Segment(Point(0, i), Point(-j, 0), color=coloreacaso())

"""
Ora aggiungiamo un parametro che indichi la distanza tra i lati.
Anche qui un po' di provacce nell'ambiente IDLE pu� aiutare...
"""

def inviluppo6(larghezza, distanza):
  """Inviluppo in tutti i quadranti con larghezza variabile e
  distanza variabili... C'� sempre l'errorino da aggiustare."""
  for i in range(int(larghezza/distanza)):
    k = i*distanza
    j = larghezza-k
    Segment(Point(0, k), Point(j, 0), color=coloreacaso())
    Segment(Point(0, -k), Point(j, 0), color=coloreacaso())
    Segment(Point(0, -k), Point(-j, 0), color=coloreacaso())
    Segment(Point(0, k), Point(-j, 0), color=coloreacaso())


#inviluppo1()
#inviluppo2()
#inviluppo3()
#inviluppo4()
#inviluppo5(7)
inviluppo6(8, 0.5)

ip.mainloop()
