#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1360Nefroide0.py--#
#                                                                       #
#                               Nefroide                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Nefroide

Problema
- Disegna una Nefroide come inviluppo di circonferenze

Soluzione
- Traccia la circonferenza base.
- traccia un suo diametro.
- Usa i punti vincolati e le rette perpendicolari per tracciare
  l'inviluppo di circonferenze.

"""

from pyig import *

ip = InteractivePlane()

# Circonferenza base
centro = Point(-1, 2, color="#00f050", width=7, name="C")
punto = Point(-2, -2, color="#00f050", width=7, name="P")
c = Circle(centro, punto, color="#a87234")

# Diametro
d=Line(centro, punto, color="#a87234")

# Funzione che restituisce una Circonferenza dell'inviluppo
def circ(c, d, costante):
  centro = ConstrainedPoint(c, costante, width=7)
  n = Orthogonal(d, centro, width=1)
  punto = Intersection(n, d, width=1)
  return Circle(centro, punto, color="#72a834")

circ(c, d, 1)

Text(-5, -6, """Nefroide.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
