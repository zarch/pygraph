#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1010Messaggio.py--#
#                                                                       #
#                 Un foglio bianco con un messaggio                     #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Oltre all'oggetto fondamentale messo a disposizione da pyig.py:
  pyigplane,
ci sono moltri altri oggetti che permettono di realizzare figure
geometriche interattive.
La prima nuova classe che utilizziamo � la classe Text. Richiede tre
argomenti obbligatori:
- una coppia di numeri,
- una stringa.

Problema
Scrivere un testo in una posizione del foglio.

Soluzione
Creare un oggetti di tipo Text.
"""

###
# Chiamata della libreria pyig
###

from pyig import *

###
# Creazione di una finestra interattiva
###

ip = InteractivePlane()

###
# Un messaggio scritto nel foglio di Kig in basso a sinistra
###

Text(-2, 8, "Foglio che contiene solo dei messaggi")
    
Text(-2, 6, u"""Un testo pu� essere posto
anche su
pi� righe""")

Text(-2, 2, u"""Un testo pu� contenere anche le lettere accentate:
������
e i caratteri:
<>&""")

Text(-2, -3, u"""Anche il testo � attivo:
pu� essere spostato nella finestra grafica trascinandolo con il mouse""",
color="#a0a000")

###
# Attivazione della finestra interattiva
###

ip.mainloop()

""" Osservazione: La stringa pu� essere delimitata dai doppi apici, dagli 
apici singoli o da una tripletta di doppi apici o apici singoli.
In questi ultimi casi, pu� anche essere scritta su pi� righe.

I tripli apici vengono anche usati per i commenti su pi� righe.
"""
