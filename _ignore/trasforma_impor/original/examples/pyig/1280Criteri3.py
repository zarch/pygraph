#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------- ---1280Criteri3.py--#
#                                                                       #
#             Terzo criterio di congruenza di triangoli                 #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Applicare il terzo criterio di congruenza di triangoli.

Problema
- Disegna un triangolo.
- Disegna una semiretta.
- Disegna il triangolo con tre lati congruienti al primo.

Soluzione
- Si possono usare le circonferenze per riportare segmenti congruenti.

"""

from pyig import *
ip = InteractivePlane()

###
# Primo triangolo
###
a0 = Point(-7, 2, color="#00f050", width=6, name="A")
b0 = Point(-4, -2, color="#00f050", width=6, name="B")
c0 = Point(-5, 6, color="#00f050", width=6, name="C")
#t0 = Polygon((a0, b0, c0), color="#a87234")

# Elementi congruenti
ab0 = Segment(a0, b0)
bc0 = Segment(b0, c0)
ca0 = Segment(c0, a0)

# Semiretta
a1 = Point(2, 2, color="#00f050", width=6, name="A'")
d = Point(8, -6, color="#00f050", width=6, name="D")
r1 = Ray(a1, d, width=1)

# Triangolo congruente
circ1 = Circle(a1, ab0, width=1)
b1 = Intersection(r1, circ1, 1, name="B'")
circ2 = Circle(a1, ca0, width=1)
circ3 = Circle(b1, bc0, width=1)
c1 = Intersection(circ2, circ3, 1, name="C'")
t1 = Polygon((a1, b1, c1), color="#72a834")

Text(-5, -6, """Terzo criterio di congruenza di triangoli.""")

ip.mainloop()
