#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pkig----------------113Punti_Prop.py--#
#                                                                       #
#                     Propriet� degli oggetti pyig                      #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Ogni oggetto visibile pyig ha alcune propriet�:
- Type: il tipo dell'oggetto;
- Coordinate: le coordinate della sua posizione nel piano cartesiano;
- XCoord: ascissa;
- YCoord: ordinata.
Ci sono due modi per ottenere una poropriet� di un oggetto:
- usando il costruttore: tipo=Type(oggetto),
- usando un metodo: tipo=oggetto.type()
Come convenzione il nome delle classi e quindi dei costruttori inizia con la
lettera maiuscola, i nomi dei metodi sono tutti minuscoli.

Problema
Disegnare:
- un oggetto pyig,
- un testo variabile per ogni propriet� dell'oggetto,

Soluzione
Si pu� usare VarText che richiede 5 argomenti:
- il piano su cui scrivere,
- la posizione (2 numeri),
- una stringa con segnaposti fatti dal percento seguito dal carattere s ({0}),
- una tupla con tanti oggetti quanti sono segnaposti.
O VarLabel che richiede i 5 argomenti:
- l'oggetto a cui viene attaccata l'etichetta,
- lo spostamento rispetto all'oggetto (2 numeri),
- una stringa con segnaposti fatti dal percento seguito dal carattere s ({0}),
- una tupla con tanti oggetti quanti sono segnaposti.
"""

from pyig import *
ip = InteractivePlane()

# Primo metodo

p1 = Point(-6, 2, width=6)

VarText(-6, 1, "Tipo: {0}", p1.type())
VarText(-6, 0, "Ascissa: {0}", p1.xcoord())
VarText(-6, -1, "Ordinata: {0}", p1.ycoord())
VarText(-6, -2, "Coordinate: {0}", p1.coords())

# Secondo metodo

p2 = Point(6, 2, width=6)

VarLabel(p2, 0., -20, "Tipo: {0}", p2.type())
VarLabel(p2, 0., -40, "Ascissa: {0}", p2.xcoord())
VarLabel(p2, 0., -60, "Ordinata: {0}", p2.ycoord())
VarLabel(p2, 0., -80, "Coordinate: {0}", p2.coords())

###
# Messaggio finale
###

Text(-7, -5, """Sotto al punto vengono scritte le propriet� del punto,
     muovi i punti e osserva la differenza.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
