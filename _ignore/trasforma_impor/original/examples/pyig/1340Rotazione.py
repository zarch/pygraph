#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1340Rotazione.py--#
#                                                                       #
#                              Rotazione                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Rotazioni

Problema
- Disegna un poligono.
- Disegna un angolo con
  - un punto in un vertice del poligono,
  - vertice nel centro di rotazione
- Disegna il poligono ruotato dell'angolo dato.

Soluzione
TODO

"""

from pyig import *
ip = InteractivePlane()

###
# Poligono0
###
a0 = Point(7, 5, color="#00f050", width=6, name="A")
b0 = Point(2, -2, color="#00f050", width=6, name="B")
c0 = Point(2, 5, color="#00f050", width=6, name="C")
d0 = Point(4, 2, color="#00f050", width=6, name="D")
Polygon((a0, b0, c0, d0), color="#a87234")

# Angolo di rotazione
v = Point(-1, 10, color="#00f050", width=6)
p0 = Point(1, 10, color="#00f050", width=6)
p1 = Point(1, 11, color="#00f050", width=6)
a = Angle(p0, v, p1)
la0 = a.side0(width=1)
la1 = a.side1(width=1)

# Centro di rotazione
o = Point(-1, -6, color="#00f050", name="O")

# Vertici del poligono ruotato
def ruotapunto(punto, centro, angolo):
  aa = Angle(punto, centro, angolo)
  la = aa.side1(width=1)
  ca = Circle(centro, Segment(centro, punto, visible=False), width=1)
  return Intersection(la, ca, 1)

a1 = ruotapunto(a0, o, a)
b1 = ruotapunto(b0, o, a)
c1 = ruotapunto(c0, o, a)
d1 = ruotapunto(d0, o, a)
    
#Poligono ruotato
Polygon((a1, b1, c1, d1), color="#72a834")

Text(-5, -12, """Rotazione di un poligono.""")

ip.mainloop()
