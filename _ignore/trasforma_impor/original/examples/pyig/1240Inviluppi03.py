#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------pyig--------------1240Inviluppi03.py--#
#                                                                       #
#                               inviluppi                               #
#                                                                       #
#--Bruno Stecca-Daniele Zambelli--GPL----------------------------2006---#

"""
Problema
Dividi due segmenti consecutivi ad intervalli regolari
e congiungi li con il metodo degli intervalli isoperimetrici
muovi poi gli estremi liberi

Soluzione
1. Costruire due segmenti consecutivi
2. dividere un segmento in parti uguali memorizzando i punti in una lista...
   Possiamo usare il teorema di Talete sulle rette parallele.
3. Riportare sul secondo segmento la stessa suddivisione memorizzando
   anche questi punti in un'altra lista.
4. Disegnarei segmenti che congiungono ogni elemento della prima lista con
   un elemento della seconda in ordine inverso.

"""

COSTRUZIONE = True    # con False non si vedono gli elementi di costruzione
                      # con True si vedono tutti gli elementi di costruzione

from pyig import *
ip = InteractivePlane()

import random
def coloreacaso():
  return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256), 
                                         random.randrange(256),
                                         random.randrange(256))

# i tre estremi, A in comune
A = Point(-5, 0, width=6, color="#00ff00")
B = Point(5, 5, width=6)
C = Point(5, -5, width=6)

# i segmenti
AB = Segment(A, B)
AC = Segment(A, C)

# per vedere o no tutti i punti e le linee di costruzione
if COSTRUZIONE:
  ip.defwidth = 1
else:
  ip.defvisible = False

# i punti equidistanti per la suddivisione del segmento AB
puntifissi = []
for i in range(10):
  puntifissi.append(Point(-5, i))

# prima retta di costruzione
sp = Line(puntifissi[-1], B)

# fascio di rette parallele alla prima e
# lista delle loro intersezioni con AB
puntiAB = []
for p in puntifissi:
  puntiAB.append(Intersection(AB, Parallel(sp, p)))

# Punti sul segmento AC
puntiAC = []
for p in puntiAB:
  puntiAC.append(Intersection(AC, Circle(A, p), 1))  

"""
Se i segmenti su AC li volessi proporzionali al ad AC invece che congruenti
a quelli sul segmento AB?
"""

ip.defwidth = 2
ip.defvisible = True

# Inviluppo di segmenti tra il primo punto in puntiAB e l'ultimo di puntiAC
# il secondo e il penultimo, ...
l=len(puntiAC)
for i in range(l):
  Segment(puntiAB[i], puntiAC[l-i-1], color=coloreacaso())
  

ip.mainloop()

