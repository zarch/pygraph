#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
#-------------------------------python---------------------demopyig.py--#
#                                                                       #
#                     Python Interactive Geometry                       #
#                                                                       #
#--Daniele Zambelli-----------Licence-GPL------------------------2005---#

from __future__ import print_function

from math import sqrt
from pyig import *
ip=InteractivePlane()

def prog():
  """Test for objects and methords."""
  base=-1; inter=0.75
  ip.defwidth = 8
  ip.defcolor = "#80d040"
  punto1 = Point(ip, -2, 0, name="A")
  punto2 = Point(ip, 2, 0, name="B")
  punto3 = Point(ip, 0, 2*sqrt(3), name="C")
#  punto3 = Point(ip, -8, 4)
  ip.defwidth = 3
  lato1 = Line(punto1, punto2)
  lato2 = Segment(punto2, punto3, color="#10ff10")
  lato3 = Segment(punto3, punto1, width=10)
  circ1 = Circle(punto1, punto2, width=1)
  punto4 = Point(ip, 0, 5)
  punto5 = Point(ip, -2, 3)
  retta1 = Line(punto4, punto5)
  intersezione1 = Intersection(lato1, retta1)
  intersezione1.width = 10
  intersezione1.color = "#40f040"
  punto6 = Point(ip, -2, 6)
  punto7 = Point(ip, -2, 8)
  retta2 = Line(punto6, punto7)
  intersezione2 = Intersection(lato1, retta2)
  intersezione3 = Intersection(retta1, circ1, 1)
  circ2 = Circle(punto6, punto7, intcolor="#f0f800", width=1)
  intersezione4 = Intersection(circ1, circ2, 1)
  t1 = Text(ip, -6, 9, "Test delle funzionalitą della libreria Pyig")
  t2 = Label(intersezione1, 0, -20, "Punto P")
  ci1 = intersezione1.coords()
  t3 = VarText(ip, 2, base-inter*0, "Coordinate del Punto P: {}", (ci1,))
  ci4 = intersezione4.coords()
  t4 = VarLabel(intersezione4, 0, 20, "{}", (ci4,))
  l1 = lato2.length()
  t5 = VarLabel(lato2, 0, 20, "{}", (l1,))
  poli1 = Polygon((intersezione2, intersezione4, punto3), "#f2f4f6")
  peri1 = poli1.perimeter()
  area1 = poli1.surface()
  t6 = VarText(ip, 2, base-inter*1, 
               "perimetro del poligono: {}; area: {}", (peri1, area1))
  peri2 = circ2.perimeter()
  area2 = circ2.surface()
  t6 = VarText(ip, 2, base-inter*2, 
               "circonf. cerchio giallo: {}; area: {}", (peri2, area2))
  c1 = Calc(lambda x, y: x+y, (area1, area2))
  t7 = VarText(ip, 2, base-inter*3, 
               "somma delle aree del poligono e del cerchio: {}", (c1,))
  r1 = Ray(intersezione4, punto3, color="#a0a0a0")
  s3 = Segment(Point(ip, 2, 7), Point(ip, 5, 7))
  circ3 = Circle(intersezione4, s3)
  t8 = VarText(ip, 2, base-inter*4.5, 
               "In questo testo ci sono oggetti dei seguenti tipi:\n" \
                      "{}, {}, {}, {}, ...", (punto1.type(), 
                                              lato1.type(), 
                                              lato2.type(), 
                                              circ1.type()))
  punto8 = MidPoints(intersezione4, Point(ip, 7, 7), width=7, color="#808080")
  lato4 = Segment(intersezione4, Point(ip, -7, 7))
  punto9 = MidPoint(lato4, width=7, color="#808080")
  t9 = VarText(ip, 2, base-inter*6.5, "s3: lunghezza={}; equazione: {},\
  pendenza: {}", \
             (s3.length(), s3.equation(), s3.slope()))
  s3.point0().color = "#faaf12"
  s3.point1().width = 15
  ortogonale = Orthogonal(lato1, intersezione4, color="#56a7f0", width=5)
  parallela = Parallel(lato1, intersezione4, color="#56a7f0", width=5)
  a1 = Angle(punto1, intersezione4, punto3, color="#fa0a00")
  ampiezza = a1.extent()
  t9 = VarText(ip, 2, base-inter*7.5, "a1: ampiezza={}", (ampiezza,))
  r2 = Bisector(a1, color="#0aa000")
  cp0_1_3 = ConstrainedPoint(lato1, 1./3, width=4, color="#fa1010")
  cp0_2_3 = ConstrainedPoint(lato1, 2./3, width=4, color="#fa1010")
  cp0_2_1 = ConstrainedPoint(lato1, 2, width=4, color="#fa1010")
  cp0_3_1 = ConstrainedPoint(lato1, 3, width=4, color="#fa1010")
  cp1_1_3 = ConstrainedPoint(ortogonale, 1./3, width=4, color="#fa1010")
  cp1_2_3 = ConstrainedPoint(ortogonale, 2./3, width=4, color="#fa1010")
  cp1_2_1 = ConstrainedPoint(ortogonale, 2, width=4, color="#fa1010")
  cp1_3_1 = ConstrainedPoint(ortogonale, 3, width=4, color="#fa1010")
  cp2_1_3 = ConstrainedPoint(parallela, 1./3, width=4, color="#fa1010")
  cp2_2_3 = ConstrainedPoint(parallela, 2./3, width=4, color="#fa1010")
  cp2_2_1 = ConstrainedPoint(parallela, 2, width=4, color="#fa1010")
  cp2_3_1 = ConstrainedPoint(parallela, 3, width=4, color="#fa1010")
  cp3_1_3 = ConstrainedPoint(r2, 1./3, width=4, color="#fa1010")
  cp3_2_3 = ConstrainedPoint(r2, 2./3, width=4, color="#fa1010")
  cp3_2_1 = ConstrainedPoint(r2, 2, width=4, color="#fa1010")
  cp3_3_1 = ConstrainedPoint(r2, 3, width=4, color="#fa1010")
  cp4_3_1 = ConstrainedPoint(circ3, 0, width=4, color="#fa8010")
  cp4_3_1 = ConstrainedPoint(circ3, 0.5, width=4, color="#fa8010")
  cp4_3_1 = ConstrainedPoint(circ3, 1, width=4, color="#fa8010")
  cp4_3_1 = ConstrainedPoint(circ3, -0.5, width=4, color="#fa8010")
  r3 = Ray(intersezione4, Point(ip, -8, 1, width=6, color="#80d040"))
  a2 = Angle(intersezione4, Point(ip, -8, 1, width=6, color="#80d040"), a1,
             color="#50ff00")
  vertici = (Point(ip, -8, 6), intersezione4, Point(ip, -8, 3), Point(ip, -6, -1))
  Polygonal(vertici, color="#ffaa00")
  CurviLine(vertici, color="#00ffaa")

prog()
ip.mainloop()
