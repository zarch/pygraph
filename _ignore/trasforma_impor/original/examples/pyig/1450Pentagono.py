# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1450Pentagono.py--#
#                                                                       #
#                               Pentagono                               #
#                                                                       #
#--Francesco Zambelli--------------GPL---------------------------2010---#

"""
Disegno di un pentagono regolare dati due vertici consecutivi.
"""

from pyig import *

ip = InteractivePlane()
# i due vertici:
a = Point(2, -2, width=5, name='A')
b = Point(7, -1, width=5, name='B')
ip.defwidth = 1

# retta per A e B
r = Line(a, b)
s0 = Segment(a, b)
# circonferenze di centro A e B
c0 = Circle(a, b)
t = Orthogonal(r, a)

ip.mainloop()
