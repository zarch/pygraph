Pyig
====

*Dove viene presentata la libreria Pyig*

Introduzione
------------

``pyig`` è una libreria che implementa la geometria interattiva. ``pyig``
contiene una classe ``InteractivePlane`` che fornisce una finestra grafica 
dove realizzare le costruzioni geometriche e alcune altre classi che 
rappresentano gli elementi della geometria interattiva: punti, rette, 
circonferenze, ...

Gli elementi di base di questa geometria sono i punti liberi che possono 
venir trascinati con il mouse. Gli altri oggetti dipendono in 
modo diretto o indiretto dai punti base. Quando viene spostato un punto base, 
tutti gli oggetti che dipendono da lui vengono ridisegnati.
Ad esempio se costruisco una retta passante per due punti, quando sposto uno
di questi punti, anche la retta si muove.
Il programma minimo che utilizza questa libreria è composto da tre 
istruzioni::

    from pyig import *                 # Carica la libreria
    ip = InteractivePlane()            # Crea un piano
    ip.mainloop()                      # Rendi interattivo il piano

Questo programma non fa un gran che: disegna una finestra grafica con un 
piano cartesiano e la rende attiva.
Se volessimo disegnare al suo interno anche una retta per due punti, potremmo 
scrivere il seguente programma:

    from pyig import *                 # Carica la libreria
    ip = InteractivePlane('Secondo')   # Crea un piano
    p0 = Point(-3, -5)                 # Disegna due punti
    p1 = Point(2, 3)
    Line(p0, p1)                       # Disegna la retta
    ip.mainloop()                      # Rendi interattivo il piano

A seconda dell'ambiente in cui viene eseguito il programma, l'ultima istruzione 
può essere necessaria o, in certi casi, essere di intralcio. È importante 
osservare il comportamento di questo programma con e senza l'istruzione::

    ip.mainloop()

e, in base a come si comporta il nostro sistema decidere se inserirla o no nel 
proprio programma. 

Negli esempi che seguono non riporterò né l'istruzione che carica tutti gli 
oggetti della libreria::

    from pyig import *

né l'struzione che rende attiva la finestra::

    ip.mainloop()

La prima è comunque necessaria e l'ultima dipende da come è configurato il 
vostro sistema.

Nella libreria sono presenti:

* Una funzione che restituisce il numero di versione corrente.
* La classe *InteractivePlane* che è un piano cartesiano con in più alcuni 
  attributi che contengono i valori predefiniti degli oggetti geometrici e 
  alcuni metodi che permettono di creare gli oggetti liberi: punti e testo.
* Un certo numero di classi che permettono di creare oggetti geometrici.

Di seguito vengonno riportati gli attributi e i metodi delle classi.
Per ognuno verrà indicato:

* lo scopo
* la sintassi
* alcune osservazioni
* un esempio di uso

``version``
-----------

**Scopo**

È una funzione che restituisce il numero di versione della libreria.

**Sintassi** ::

  version()

**Osservazioni**

Non richiede parametri, restituisce la stringa che contiene la versione.

**Esempio**

Controllare la versione della libreria. ::

    if pyig.version()<"2.9.00":
        print("versione un po' vecchiotta")
    else:
        print("versione:", pyig.version())

``InteractivePlane``
--------------------

``InteractivePlane`` estende la classe ``Plane``, quindi è in grado di fare 
tutto quello che fa ``Plane``. Cambiano i valori predefiniti che vengono 
utilizzati quando viene creato un oggetto di questa classe e ci sono alcuni 
atri attributi e metodi.

In questa sezione presento solo le differenze e aggiunte alla classe ``Plane``.
Per tutto ciò che rimane invariato si veda il capitolo relativo a ``pycart``.

Per poter creare oggetti della classi ``InteractivePlane`` bisogna importarla 
dalla libreria. In tutti gli esempi seguenti si suppone che sia già stato 
eseguito il comando::

  from pyig import *

Con questo comando vengono caricati tutti gli oggetti presenti in ``pyig``. 
Nei prossimi esempi la precedente istruzione è sottintesa, va sempre scritta
all'inizio del programma.
Se nell'esegure un esempio ottenete un messaggio che termina con:
``NameError: name 'InteractivePlane' is not defined``, forse avete dimenticato 
di caricare la libreria con il comando scritto sopra.

Di seguito sono elencati alcuni attributi della classe InteractivePlane.

``__init__``
............

**Scopo**

Crea il piano cartesiano e inizializza gli attributi di ``InteractivePlane``.

**Sintassi** ::

  <nome_variabile> = InteractivePlane(<parametri>)

**Osservazioni**

Questo metodo non viene chiamato esplicitamente, ma viene eseguito
quando si crea un oggetto di questa classe.
L'intestazione di questo metodo è::

  def __init__(self, name="Interactive geometry",
               w=600, h=600,
               sx=20, sy=None,
               ox=None, oy=None,
               axes=True, grid=True,
               axescolor='#080808', gridcolor='#080808',
               parent=None):

Si può vedere che presenta molti parametri tutti con un valore predefinito.
Nel momento in cui si crea un piano cartesiano si possono quindi decidere
le sue caratteristiche. Vediamole in dettaglio:

* titolo della finestra, valore predefinito: "Interactive geometry";
* dimensione, valori predefiniti: larghezza=600, altezza=600;
* scala di rappresentazione, valori predefiniti: una unità = 20 pixel;
* posizione dell'origine, valore predefinito: il centro della finestra;
* rappresentazione degli assi cartesiani, valore predefinito: ``True``;
* rappresentazione di una griglia di punti, valore predefinito: ``True``;
* colore degli assi valore predefinito: '#808080' (grigio).
* colore della griglia valore predefinito: '#808080'.
* riferimento alla finestra che contiene il piano cartesiano,
  valore predefinito: ``None``.

Poiché tutti i parametri hanno un valore predefinito, possiamo creare un
oggetto della classe ``InteractivePlane`` senza specificare alcun argomento: 
verranno usati tutti i valori predefiniti. Oppure possiamo specificare per 
nome gli argomenti che vogliamo siano diversi dal comportamento predefinito, 
si vedano di seguito alcuni esempi.

**Esempio**

Si vedano tutti gli esempi seguenti.

Attributi
.........

``InteractivePlane`` ha alcuni attributi che possono essere modificati dagli
utenti.

**Scopo**

Questi attributi definiscono alcuni valori predefiniti con i quali saranno 
creati i nuovi oggetti.

* ``defvisible`` stabilisce se i nuovi oggetti creati saranno visibili o 
  invisibili;
* ``defwidth`` imposta la larghezza predefinita;
* ``defwidthtext`` imposta la larghezza predefinita dei caratteri;
* ``defcolor`` imposta il colore predefinito degli oggetti;
* ``defdragcolor`` imposta il colore predefinito per gli oggetti che si 
  possono trascinare con il mouse.
* ``defintcolor`` imposta il colore predefinito per la superficie di 
  circonferenze e poligoni.

**Sintassi** ::

  <piano interattivo>.defvisible = v
  <piano interattivo>.defwidth = w
  <piano interattivo>.defwidthtext = w
  <piano interattivo>.defcolor = c
  <piano interattivo>.defdragcolor = c
  <piano interattivo>.defintern = c

**Osservazioni**

* v è un valore booleano, può essere ``True`` o ``False``.
* w è un numero che indica la larghezza in pixel.
* c può essere:

  * una stringa nel formato: "#rrggbb" dove rr, gg e bb sono numeri 
    esadecimali di due cifre che rappresentano rispettivamente le componenti 
    rossa, verde, e blu del colore;
  * Una stringa contenente il nome di un colore;
  * Una terna di numeri nell'intervallo 0-1 rappresentanti le componenti 
    rossa verde e blu.

**Esempio**

Disegna l'asse di un segmento usando linee di costruzioni sottili e grige. ::

    ip = InteractivePlane('default')
    ip.defwidth = 5
    a = ip.newPoint(2, 1, name='A')
    b = ip.newPoint(5, 3, name='B')
    ip.defcolor = 'turquoise'
    Segment(a, b)
    ip.defwidth = 1
    ip.defcolor = 'gray40'
    c0 = Circle(a, b)
    c1 = Circle(b, a)
    i0 = Intersection(c0, c1, -1)
    i1 = Intersection(c0, c1, 1)
    asse = Line(i0, i1, width=3, color='royal blue')

``newText``
...........

**Scopo**

La geometria interattiva ha alcuni oggetti di base sono oggetti che non 
dipendono da altri. ``InteractivePlane`` ha alcuni metodi che permettono di 
creare questi oggetti. ``newText`` serve per creare un nuovo *testo*.

**Sintassi** ::

  <piano>.newText(x, y, testo[, visible][, color][, width][, name])

**Osservazioni**

L'intestazione di questo metodo è::

  def newText(self, x, y, text, 
              visible=None, color=None, width=None, name=None):

Molti parametri hanno un valore predefinito, per creare un testo, bisogna
specificare la posizione dove metterlo e la stringa che verrà visualizzata::

  ip.newText(-3, 12, 'Titolo')

**Esempio**

Crea un testo nel piano interattivo. ::

    ip = InteractivePlane('newText')
    ip.newText(0, 13, 'Finestra (quasi) vuota', 
               width=20, color='DarkOrchid3')

``newPoint``
............

**Scopo**

La geometria interattiva ha alcuni oggetti di base, oggetti che non dipendono 
da altri. ``InteractivePlane`` ha alcuni metodi che permettono di creare 
questi oggetti. ``newPoint`` serve per creare un nuovo *punto*.

**Sintassi** ::

  <piano>.newPoint(x, y[, visible][, color][, width][, name])

**Osservazioni**

Normalmente è utile assegare il *punto* creato ad un identificatore.

L'intestazione di questo metodo è::

  def newPoint(self, x, y,
               visible=None, color=None, width=None, name=None):

Molti parametri hanno un valore predefinito, per creare un punto, gli unici due 
argomenti necessari sono i valori delle coordinate::

  a = ip.newPoint(3, -7)

**Esempio**

Crea un punto in una data posizione. ::

    ip = InteractivePlane('newPoint')
    ip.newText(0, 13, 'Finestra con un punto',
               width=20, color='DarkOrchid3')
    p0 = ip.newPoint(-2, 7, width=8, name="P")

``newVarText``
..............

**Scopo**

Un testo variabile è costituito da una stringa con alcuni segnaposti e da 
altrettanti dati. I segnaposti sono indicati dai due caratteri: "''%s''". 
I dati si ricavano dagli oggetti geometrici presenti nel piano.

**Sintassi** ::

  <piano>.newVarText(x, y, testo, dati[, visible][, color][, width][, name])

**Osservazioni**

L'intestazione di questo metodo è::

  def newVarText(self, x, y, text, variables,
                 visible=None, color=None, width=None, name=None):

In questo caso è necessario specificare la posizione dove dovrà essere 
visualizzata la stringa, il testo, e la (o le) variabile(i)::

  ip.newText(-3, 12, 'posizione: %s', p.coords())

**Esempio**

Visualizza alcune informazioni che dipendono da altri oggetti. ::

    ip = InteractivePlane('newVarText')
    ip.newText(0, 13, 'Finestra con un punto e le sue coordinate', 
               width=20, color='DarkOrchid3')
    p0 = ip.newPoint(-2, 7, width=8, name="P")
    ip.newVarText(-5, -4, 'P = {0}', p0.coords())

``Point``
---------

**Scopo**

Crea un *punto libero* date le coordinate della sua posizione iniziale. 

Questo oggetto è la base di ogni costruzione; dai punti liberi dipendono, 
direttamente o indirettamente, gli altri oggetti grafici.

Quando il puntatore del mouse si sovrappone ad un punto libero questo cambia 
colore.
Trascinando un punto libero, con il mouse, tutti gli oggetti che dipendono 
da lui, verranno modificati.

``Point`` essendo un oggetto che può essere trascinato con il mouse ha un 
colore predefinito diverso da quello degli altri oggetti.

**Sintassi** ::

  Point(x, y[, visible][, color][, width][, name])

.. note::
  Spesso nella pratica è necessario assegare l'oggetto creato ad un 
  identificatore in modo da poter fare riferimento ad un oggetto nella 
  costruzione di altri oggetti::
  
    <identificatore> = Point(x, y[, visible][, color][, width][, name])
  
  Si vedano gli esempi seguenti.
  
**Osservazioni**

* ``x`` e ``y`` sono due numeri, ``x`` è l'ascissa e ``y`` l'ordinata del
  punto.
* Per quanto riguarda i parametri non obbligatori si veda quanto scritto nel
  paragrafo relativo agli attributi degli oggetti visibili.

.. note::
  Nel resto del manuale riporterò solo gli argomenti obbligatori, è sottinteso 
  che tutti gli oggetti che possono essere visualizzati hanno anche i 
  parametri: ``visible``, ``color``, ``width``, ``name``.

**Esempio**

Funzione definita in N ad andamento casuale. ::

    import random
    ip = InteractivePlane('Point')
    y = 0
    for x in range(-14, 14):
        y += random.randrange(-1, 2)
        Point(x, y, color='red')

Attributi degli oggetti geometrici
..................................

**Scopo**

``Point``, come tutti gli oggetti geometrici ha degli attributi che possono 
essere determinati nel momento della creazione dell'oggetto stesso o in 
seguito. Questi attributi definiscono alcune caratteristiche degli oggetti che 
possono essere visualizzati.

* ``visible`` stabilisce se l'oggetto sarà visibili o invisibile;
* ``color`` imposta il colore dell'oggetto;
* ``width`` imposta la larghezza dell'oggetto.
* ``name`` imposta il nome dell'oggetto.

**Sintassi** ::

  <oggetto>.visible = v
  <oggetto>.color = c
  <oggetto>.width = w
  <oggetto>.name = s

**Osservazioni**

* ``v`` è un valore booleano, può essere True o False.
* ``w`` è un numero che indica la larghezza in pixel.
* ``c`` può essere:

  * una stringa nel formato: "#rrggbb" dove rr, gg e bb sono numeri 
    esadecimali di due cifre che rappresentano rispettivamente le componenti 
    rossa, verde, e blu del colore;
  * Una stringa contenente il nome di un colore;
  * Una terna di numeri nell'intervallo 0-1 rappresentanti le componenti 
    rossa verde e blu.
    
* ``s`` è una stringa

**Esempio**

Disegna tre punti: uno con i valori di default,
uno con colore dimensione e nome definiti quando viene creato,
uno con valori cambiati dopo essere stato cerato. ::

    ip = InteractivePlane('attributi')
    a = Point(-5, 3)
    b = Point(2, 3, color='indian red', width=8, name='B')
    c = Point(9, 3)
    c.color = 'dark orange'
    c.width = 8
    c.name = 'C'

Metodi degli oggetti geometrici
...............................

**Scopo**

Tutti gli oggetti geometrici hanno anche dei metodi che danno come risultato 
alcune informazioni relative all'oggetto stesso.

* ``xcoord`` l'ascissa;
* ``ycoord`` l'ordinata;
* ``coords`` le coordinate.

**Sintassi** ::

  <oggetto>.xcoord()
  <oggetto>.ycoord()
  <oggetto>.coords()

**Osservazioni**

Non richiedono argomenti e restituiscono un particolare oggetto che può essere
utilizzato all'interno di un testo variabile.

**Esempio**

Scrivi ascissa, ordinata e posizione di un punto. ::

    ip = InteractivePlane('coords, xcoord, ycoord')
    a = Point(-5, 8, name='A')
    VarText(-5, -1, 'ascissa di A: {0}', a.xcoord())
    VarText(-5, -2, 'ordinata di A: {0}', a.ycoord())
    VarText(-5, -3, 'posizione di A: {0}', a.coords())

Operazioni con i punti
......................

**Scopo**

Sono definite alcune operazioni tra punti: l'addizione, la sottrazione, 
la moltiplicazione per uno scalare e la moltiplicazione tra due punti.

**Sintassi** ::

  <identificatore> = <punto> + <punto>
  <identificatore> = <punto> - <punto>
  <identificatore> = <punto> * <numero>
  <identificatore> = <punto> * <punto>

**Osservazioni**

* È possibile così scrivere espressioni tra punti.

**Esempio**

Crea due punti poi calcola la loro somma, differenza e prodotto. ::

    ip = InteractivePlane('Point operations')
    a = Point(2, 1)
    b = Point(4, 4)
    s = a+b
    d = a-b
    t = a*3
    p = a*b
    VarLabel(a, 10, -10, 'A{0}', a.coords())
    VarLabel(b, 10, -10, 'B{0}', b.coords())
    VarLabel(s, 10, -10, 'A+B{0}', s.coords())
    VarLabel(d, 10, -10, 'A-B{0}', d.coords())
    VarLabel(t, 10, -10, '3A{0}', t.coords())
    VarLabel(p, 10, -10, 'A*B{0}', p.coords())

``Segment``
-----------

**Scopo**

Crea un segmento dati i due estremi, i due estremi sono *punti*.

**Sintassi** ::

  <identificatore> = Segment(point0, point1)

**Osservazioni**

``point0`` e ``point1`` sono due punti.

**Esempio**

Disegna un triangolo con i lati colorati in modo differente. ::

    ip = InteractivePlane('Segment')
    # creo i 3 vertici
    v0 = Point(-4, -3, width=5)
    v1 = Point( 5, -1, width=5)
    v2 = Point( 2,  6, width=5)
    # creo i 3 lati
    l0 = Segment(v0, v1, color='steel blue')
    l1 = Segment(v1, v2, color='sea green')
    l2 = Segment(v2, v0, color='saddle brown')

``length``
..........

**Scopo**

È il metodo della classe ``Segment`` che restituisce un oggetto ``data`` 
contenete la lunghezza del segmento stesso.

**Sintassi** ::

  <obj>.length()

**Osservazioni**

La lunghezza è la distanza tra ``point0`` e ``point1``.

**Esempio**

Disegna un segmento e scrivi la sua lunghezza. ::

    ip = InteractivePlane('length')
    p0 = Point(-4, 7, width=5, name='A')
    p1 = Point(8, 10, width=5, name='B')
    seg = Segment(p0, p1)
    VarText(-5, -5, 'lunghezza di AB = {0}', seg.length())

``midpoint``
............

**Scopo**

È il metodo della classe ``Segment`` che restituisce il punto medio del 
segmento.

**Sintassi** ::

  <obj>.midpoint()

**Osservazioni**

L'oggetto restituito è un punto.

**Esempio**

Disegna un segmento e il suo punto medio. ::

    ip = InteractivePlane('35: midpoint')
    seg = Segment(Point(-4, 7, width=5, name='A'),
                  Point(8, 10, width=5, name='B'))
    seg.midpoint(color='gold', width=10)

``Line``, ``Ray`` e ``Segment`` hanno dei metodi che forniscono degli oggetti 
che contengono alcune informazioni relative alla linea stessa.
Vengono elencati di seguito.

``equation``
............

**Scopo**

È il metodo presente in tutte le classi *linea* che restituisce 
l'equazione esplicita della retta o della retta che a cui appartiene l'oggetto.

**Sintassi** ::

  <obj>.equation()

**Osservazioni**

L'equazione è data sotto forma di stringa.

**Esempio**

Disegna una retta e scrivi la sua equazione . ::

    ip = InteractivePlane('equation')
    p0 = Point(-4, 7, width=5)
    p1 = Point(8, 10, width=5)
    retta = Line(p0, p1, name='r')
    VarText(-5, -5, 'equazione di r: {0}', retta.equation())

``slope``
.........

**Scopo**

È il metodo presente in tutte le classi *linea* che restituisce 
la pendenza della retta o della retta che a cui appartiene l'oggetto.

**Sintassi** ::

  <obj>.slope()

**Osservazioni**

Se la retta è verticale la pendenza restituita è ``None``.

**Esempio**

Disegna una semiretta e scrivi la sua pendenza . ::

    ip = InteractivePlane('slope')
    p0 = Point(-4, 7, width=5)
    p1 = Point(8, 10, width=5)
    semiretta = Ray(p0, p1, name='r')
    VarText(-5, -5, 'pendenza di r: {0}', semiretta.slope())

``point0`` e ``point1``
.......................

**Scopo**

Sono i metodi presenti in tutte le classi *linea* che restituiscono 
rispettivamente il *punto0* e il *punto1* dell'oggetto.

* Il *punto0* è il primo punto della retta passante per due punti, il 
  primo estremo del segmento, l'origine della semiretta, il vertice
  dell'angolo per la bisettrice, il punto per cui passa la parallela o il
  punto in cui la perpendicolare interseca la retta.
* Il *punto1* è il secondo punto della retta passante per due punti, il 
  secondo estremo del segmento, il punto per cui passa la semiretta, 
  un punto della bisettrice, un punto della parallela o il punto per cui 
  passa la perpendicolare.

**Sintassi** ::

  <obj>.point0()
  <obj>.point1()

**Esempio**

Disegna un segmento e scrivi le coordinate dei suoi estremi. ::

    ip = InteractivePlane('point0 point1')
    seg = Segment(Point(-4, 7, width=5, name='A'),
                  Point(8, 10, width=5, name='B'))
    VarText(-5, -5, 'A{0}', seg.point0().coords())
    VarText(-5, -6, 'B{0}', seg.point1().coords())

``MidPoints``
-------------

**Scopo**

Crea il punto medio tra due punti.

**Sintassi** ::

  MidPoints(point0, point1)

**Osservazioni**

``point0`` e ``point1`` sono due punti.

**Esempio**

Punto medio tra due punti. ::

    ip = InteractivePlane('MidPoints')
    # creo due punti
    p0 = Point(-2, -5)
    p1 = Point(4, 7)
    # cambio i loro attributi
    p0.color = "#00a600"
    p0.width = 5
    p1.color = "#006a00"
    p1.width = 5
    # creao il punto medio tra p0 e p1
    m = MidPoints(p0, p1, name='M')
    # cambio gli attributi di m
    m.color = "#f0f000"
    m.width = 10

``MidPoint``
------------

**Scopo**

Crea il punto medio di un segmento

**Sintassi** ::

  MidPoint(segment)

**Osservazioni**

``segment`` è un oggetto che ha un ``point0`` e un ``point1``.

**Esempio**

Punto medio di un segmento. ::

    ip = InteractivePlane('MidPoint')
    # creo un segmento
    s=Segment(Point(-2, -1, color="#a60000", width=5),
              Point(5, 7, color="#6a0000", width=5), 
              color="#a0a0a0")
    # creo il suo punto medio
    MidPoint(s, color="#6f6f00", width=10, name='M')

``Line``
--------

**Scopo**

Crea una retta per due punti.

**Sintassi** ::

  Line(point0, point1)

**Osservazioni**

``point0`` e ``point1`` sono, indovina un po', due punti.

Vedi anche i metodi delle classi *linea* presentati nella classe ``Segment``.

**Esempio**

Triangolo delimitato da rette. ::

    ip = InteractivePlane('Line')
    # creo i 3 punti
    a=Point(0, 0)
    b=Point(1, 5)
    c=Point(5, 1)
    # creo i 3 lati
    Line(a, b, color="#dead34")
    Line(b, c, color="#dead34")
    Line(c, a, color="#dead34")

``Ray``
-------

**Scopo**

Traccia una semiretta con l'origine in un punto e passante per un altro 
punto.

**Sintassi** ::

  Ray(point0, point1)

**Osservazioni**

``point0`` è l'origine della semiretta che passa per ``point1``.

Vedi anche i metodi delle classi *linea* presentati nella classe ``Segment``.

**Esempio**

Triangolo delimitato da semirette. ::

    ip = InteractivePlane('Ray')
    # creo i 3 punti
    a=Point(0, 0)
    b=Point(1, 5)
    c=Point(5, 1)
    # creo i 3 lati
    Ray(a, b, color="#de34ad")
    Ray(b, c, color="#de34ad")
    Ray(c, a, color="#de34ad")

``Orthogonal``
--------------

**Scopo**

Crea la retta perpendicolare ad una retta data passante per un punto.

**Sintassi** ::

  Orthogonal(line, point)

**Osservazioni**

``line`` è la retta alla quale si costruisce la perpendicolare passante per 
``point``.

Vedi anche i metodi delle classi *linea* presentati nella classe ``Segment``.

**Esempio**

Disegna la perpendicolare ad una retta data passante per un punto. ::

    ip = InteractivePlane('Orthogonal')
    retta = Line(Point(-4, -1, width=5), 
                 Point(6, 2, width=5), 
                 width=3, color='DarkOrange1', name='r')
    punto = Point(-3, 5, width=5, name='P')
    Orthogonal(retta, punto)

``Parallel``
------------

**Scopo**

Crea la retta parallela ad una retta data passante per un punto.

**Sintassi** ::

  Parallel(line, point)

**Osservazioni**

``line`` è la retta alla quale si costruisce la parallela passante per 
``point``.

Vedi anche i metodi delle classi *linea* presentati nella classe ``Segment``.

**Esempio**

Disegna la parallela ad una retta data passante per un punto. ::

    ip = InteractivePlane('Parallel')
    retta = Line(Point(-4, -1, width=5), 
                 Point(6, 2, width=5), 
                 width=3, color='DarkOrange1', name='r')
    punto = Point(-3, 5, width=5, name='P')
    Parallel(retta, punto)

``Vector``
----------

**Scopo**

Crea un segmento orientato dati i due estremi o il punto di applicazione e
un altro vettore.

**Sintassi** ::

  <identificatore> = Vector(point0, point1)
  <identificatore> = Vector(point0, vector)

**Osservazioni**

``point0`` e ``point1`` sono punti vector è un vettore.

È anche possibile operare con i vettori sono a disposizione la somma, 
la differenza, l'opposto, il prodotto e il quoziente per uno scalare.

**Esempio**

Disegna alcuni vettori e il risultato di alcune operazioni tra di essi.

    ip = InteractivePlane('Vector')
    Text(-7, 13, """Vector""", color='#408040', width=12)
    a = Point(-5, 10, name="A", width=6)
    b = Point(-1, 13, name="B", width=6)
    c = Point(-3, 5, name="C", width=6)
    d = Point(-10, 6, name="D", width=6)
    vect0 = Vector(a, b, width=6, color='navy', name='V0')
    vect1 = Vector(a, c, width=6, color='red', name='V1')
    vect2 = Vector(d, vect1, color='red', name='V2')
    vect3 = vect0 + vect1
    vect4 = vect0 - vect1
    vect5 = vect0 * 3
    vect6 = -vect0 / 3
    vect6.color = 'red'
    VarText(7, -2, '{0}', vect0.components(), (.2, .4, .8))

``Polygon``
-----------

**Scopo**

Crea un poligono data una sequenza di vertici.

**Sintassi** ::

  Polygon(points)

**Osservazioni**

``points`` è una sequenza di punti, può essere una lista (delimitata da 
parentesi quadre) o una tupla (delimitata da parentesi tonde).

**Esempio**

Disegna un poligono date le coordinate dei vertici. ::
    
    ip = InteractivePlane('24: Polygon')
    # Lista di coordinate
    coords = ((-8, -3), (-6, -2), (-5, -2), (-4, 2), (-2, 3), (0, 4),
              (2, 3), (4, 2), (5, -2), (6, -2), (8, -3))
    # Costruzione di una lista di punti partendo da una lista di coordinate:
    # listcompreension
    ip.defwidth = 5
    points = [Point(ip, x, y) for x,y in coords]
    Polygon(points, color='HotPink3')

``perimeter`` e ``surface``
...........................

**Scopo**

Sono metodi presenti in tutte le classi *figura*, restituiscono 
la lunghezza del contorno e l'area della superficie dell'oggetto.

**Sintassi** ::

  <figura>.perimeter()
  <figura>.surface()

**Osservazioni**

Sono metodi degli oggetti che sono *figure piane* e non richiede argomenti.

**Esempio**

Scrive alcune informazioni relative a un poligono. ::

    poli = Polygon((Point(-7, -3, width=5, name="A"),
                    Point(5, -5, width=5, name="B"),
                    Point(-3, 8, width=5, name="C")),
                    width=4, color='magenta', intcolor='olive drab')
    VarText(-3, -6, "perimetro={0}", poli.perimeter(), color='magenta')
    VarText(-3, -7, "area={0}", poli.surface(), color='olive drab')

``Circle``
----------

**Scopo**

Circonferenza dato il centro e un punto o il centro e il raggio (un segmento).

**Sintassi** ::

  Circle(center, point)
  Circle(center, segment)

**Osservazioni**

``center`` è il centro della circonferenza passante per ``point`` o di raggio 
segment.

Vedi anche i metodi delle classi *figure piane* presentati nella classe 
``Polygon``.

**Esempio**

Circonferenze con centro nell'origine. ::

    ip = InteractivePlane('Circle(Point, Point)')
    origine = Point(0, 0, visible=False, name="O")
    p0 = Point(-7, -3, width=5, name="P")
    Circle(origine, p0, color="#c0c0de", width=4)
    raggio = Segment(Point(-7, 9, width=5, name="A"),
                     Point(-4, 9, width=5, name="B"))
    Circle(origine, raggio, color="#c0c0de", width=4)

``radius``
..........

**Scopo**

Restituisce un *oggetto dato* che contiene la lunghezza del raggio della 
circonferenza.

**Sintassi** ::

  <circonferenza>.radius()

**Osservazioni**

È un metodo degli oggetti circonferenza e non richiede argomenti.

**Esempio**

Visualizza il raggio di una circonferenza. ::

    ip = InteractivePlane('radius')
    centro = Point(3, 4, name="C")
    p0 = Point(-5, 4, width=5, name="P")
    c = Circle(centro, p0, color="#c0c0de", width=4)
    VarText(-5, -1, 'raggio: {0}', c.radius())

``Intersection``
----------------

**Scopo**

Crea il punto di intersezione tra due oggetti.

**Sintassi** ::

  Intersection(obj0, obj1)
  Intersection(obj0, obj1, which)

**Osservazioni**

``obj0`` e ``obj1`` possono essere rette o circonferenze. Se uno dei due 
oggetti è una circonferenza è necessario specificare quale delle due 
intersezioni verrà restituita indicando come terzo parametro ``+1`` o ``-1``.

**Esempio**

Disegna una circonferenza tangente a una retta. ::

    ip = InteractivePlane('Intersection line line')
    # Disegno retta e punto
    retta = Line(Point(-4, -1, width=5), 
                 Point(6, 2, width=5), 
                 width=3, color='DarkOrange1', name='r')
    punto = Point(-3, 5, width=5, name='P')
    # trovo il punto di tangenza
    perpendicolare = Orthogonal(retta, punto, width=1)
    p_tang = Intersection(retta, perpendicolare, width=5)
    # disegno la circonferenza
    Circle(punto, p_tang, width=4, color='IndianRed')

Disegna il simmetrico di un punto rispetto ad una retta. ::

    ip = InteractivePlane('Intersection line circle')
    # disegno l'asse di simmetria e il punto
    asse = Line(Point(-4, -11, width=5), 
                Point(-2, 12, width=5), 
                width=3, color='DarkOrange1', name='r')
    punto = Point(-7, 3, width=5, name='P')
    # disegno la perpendicolare all'asse passante per il punto
    perp = Orthogonal(asse, punto, width=1)
    # trovo l'intersezione tra la perpendicolare e l'asse
    piede = Intersection(perp, asse)
    # disegno la circonferenza di centro piede e passante per punto
    circ = Circle(piede, punto, width=1)
    # trovo il simmetrico di punto rispetto a asse
    Intersection(perp, circ, -1, width=5, color='DebianRed', name="P'")

Disegna un triangolo equilatero. ::

    ip = InteractivePlane('Intersection circle circle')
    # Disegno i due primi vertici
    v0=Point(-2, -1, width=5, name='A')
    v1=Point(3, 2, width=5, name='B')
    # Disegno le due circonferenze di centro p0 e p1 e passanti per p1 e p0
    c0=Circle(v0, v1, width=1)
    c1=Circle(v1, v0, width=1)
    # terzo vertice: intersezione delle due circonferenze
    v2=Intersection(c0, c1, 1, width=5, name='C')
    # triangolo per i 3 punti
    Polygon((v0, v1, v2), width=4, color='DarkSeaGreen4')
    
``Text``
--------

**Scopo**

Crea un testo posizionato in un punto del piano.

**Sintassi** ::

  Text(x, y, text[, iplane=None])

**Osservazioni**

* ``x`` e ``y`` sono due numeri interi o razionali relativi ``x`` è l'ascissa 
  e ``y`` l'ordinata del punto.
* ``text`` è la stringa che verrà visualizzata.
* Se sono presenti più piani interattivi, si può specificare l'argomento 
  ``iplane`` per indicare in quale di questi la scritta deve essere 
  visualizzata.

**Esempio**

Scrive un titolo in due finestre grafiche. ::

    ip0 = InteractivePlane('Text pale green', w=400, h=200)
    ip1 = InteractivePlane('Text blue violet', w=400, h=200)
    Text(-2, 2, "Prove di testo blue violet", 
         color='blue violet', width=20)
    Text(-2, 2, "Prove di testo pale green", 
         color='pale green', width=20, iplane=ip0)
         
``Label``
---------

**Scopo**

Crea un testo legato a un oggetto.

**Sintassi** ::

  Label(obj, x, y, text)

**Osservazioni**

La stringa contenuta in ``text`` viene posizionata alla distanza di ``x`` 
pixel in orizzontale e di ``y`` pixel in verticale dall'oggetto ``obj``.

**Esempio**

Disegna un punto e gli appiccica un'etichetta. ::

    ip = InteractivePlane('Label')
    p0 = Point(7, 3, color='navy', width=10, name='A')
    Label(p0, 0, 20, "colore di A = 'navy'")

``VarText``
-----------

**Scopo**

Crea un testo variabile. Il testo contiene dei "segnaposto" che verranno 
sostituiti con i valori prodotti dai dati presenti nel parametro variables.

**Sintassi** ::

  VarText(x, y, text, variables[, iplane=None])

**Osservazioni**

* ``x`` e ``y`` sono due numeri interi o razionali relativi ``x`` è l'ascissa 
  e ``y`` l'ordinata del punto.
* ``text`` è la stringa che contiene la parte costante e i segnaposto.
* In genere i *segnaposto* saranno nella forma: "{0}" che indica a Python di 
  convertire in stringa il risultato prodotto dal dato. 
* ``variables`` è un dato o una tupla di dati.
* Se sono presenti più piani interattivi, si può specificare l'argomento 
  ``iplane`` per indicare in quale di questi la scritta deve essere 
  visualizzata.


**Esempio**

Un testo che riporta la posizione dei un punto. ::

    ip = InteractivePlane('VarText')
    p0 = Point(7, 3, color='green', width=10, name='A')
    VarText(-4, -3, "Posizione del punto A: ({0}; {1})",
                    (p0.xcoord(), p0.ycoord()),
                    color='green', width=10)

``VarLabel``
------------

**Scopo**

Testo variabile legato ad un oggetto.

**Sintassi** ::

  VarLabel(obj, x, y, text, variables)

**Osservazioni**

vedi le osservazioni di ``Label`` e ``VarText``.

**Esempio**

Disegna un punto con un'ettichetta che riporta la sua posizione. ::

    ip = InteractivePlane('VarLabel')
    p0 = Point(7, 3, color='red', width=10, name='A')
    VarLabel(p0, 0, -40,
             "A{0}", p0.coords(), color='red', width='10')

``PointOn``
-----------

**Scopo**

Punto disegnato su un oggetto in una posizione fissa.

**Sintassi** ::

  PointOn(obj, parameter)

**Osservazioni**

L'oggetto deve essere una linea o una retta o una circonferenza, 
``parameter`` è un numero che individua una precisa posizione sull'oggetto. 
Sia le rette sia le circonferenze hanno una loro metrica che è legata ai punti
base dell'oggetto. Su una retta una semiretta o un segmento ``point0`` 
corrisponde al parametro 0 mentre ``point1`` corrisponde al parametro 1.
Nelle circoferenze il punto di base della circonferenza stessa corrisponde 
al parametro 0 l'intera circonferenza vale 2.
Il punto creato con ``PointOn`` non può essere trascinato con il mouse.

**Esempio**

Disegna il simmetrico di un punto rispetto ad una retta. ::

    ip = InteractivePlane('PointOn')
    # disegno l'asse di simmetria e il punto
    asse = Line(Point(-4, -11, width=5), 
                Point(-2, 12, width=5), 
                width=3, color='DarkOrange1', name='r')
    punto = Point(-7, 3, width=5, name='P')
    # disegno la perpendicolare all'asse passante per il punto
    perp = Orthogonal(asse, punto, width=1)
    # trovo il simmetrico di punto rispetto a asse
    PointOn(perp, -1, width=5, color='DebianRed', name="P'")
    Text(-5, -6, """P' è il simmetrico di P.""")

``ConstrainedPoint``
--------------------

**Scopo**

Punto legato ad un oggetto.

**Sintassi** ::

  ConstrainedPoint(obj, parameter)

**Osservazioni**

Per quanto riguarda ``parameter``, valgono le osservazioni fatte per 
``PoinOn``.
Questo punto però può essere trascinato con il mouse pur restando sempre 
sull'oggetto. Dato che può essere trascinato con il mouse 
ha un colore di default diverso da quello degli altri oggetti.

**Esempio**

Circonferenza e proiezioni sugli assi. ::

    ip = InteractivePlane('ConstrainedPoint', sx=200)
    # Circonferenza
    origine = Point(0, 0, visible=False)
    unix = Point(1, 0, visible=False)
    uniy = Point(0, 1, visible=False)
    circ = Circle(origine, unix, color="gray10")
    # Punto sulla circonferenza
    cursore = ConstrainedPoint(circ, 0.25, color='magenta', width=20)
    # assi
    assex = Line(origine, unix, visible=False)
    assey = Line(origine, uniy, visible=False)
    # proiezioni
    py = Parallel(assey, cursore, visible=False)
    hx = Intersection(assex, py, color='red', width=8)
    px = Parallel(assex, cursore, visible=False)
    hy = Intersection(assey, px, color='blue', width=8)

``parameter``
.............

**Scopo**

I punti legati agli oggetti hanno un metodo che permette di ottenere il 
parametro. 

**Sintassi** ::

  <constrained point>.parameter()

**Osservazioni**

In ``PointOn`` il parametro è fissato nel momento della costruzione 
dell'oggetto. In ``ConstrainedPoint`` il parametro può essere variato 
trascinando il punto con il mouse.

**Esempio**

Scrivi i dati relativi a un punto collegato a un oggetto. ::

    ip = InteractivePlane('parameter')
    c0 = Circle(Point(-6, 6, width=6), Point(-1, 5, width=6))
    c1 = Circle(Point(6, 6, width=6), Point(1, 5, width=6))
    a = PointOn(c0, 0.5, name='A')
    b = ConstrainedPoint(c1, 0.5, name='B')
    ip.newVarText(-5, -1, 'ascissa di A: {0}', a.xcoord())
    ip.newVarText(-5, -2, 'ordinata di A: {0}', a.ycoord())
    ip.newVarText(-5, -3, 'posizione di A: {0}', a.coords())
    ip.newVarText(-5, -4, 'parametro di A: {0}', a.parameter())
    ip.newVarText(5, -1, 'ascissa di B: {0}', b.xcoord())
    ip.newVarText(5, -2, 'ordinata di B: {0}', b.ycoord())
    ip.newVarText(5, -3, 'posizione di B: {0}', b.coords())
    ip.newVarText(5, -4, 'parametro di B: {0}', b.parameter())

``Angle``
---------

**Scopo**

Angolo dati tre punti o due punti e un altro angolo.
Il secondo punto rappresenta il vertice. Il verso di costruzione dell'angolo 
è quello antiorario.

**Sintassi** ::

  Angle(point0, vertex, point1[, sides])
  Angle(point0, vertex, angle[, sides])

**Osservazioni**

L'argomento ``sides`` può valere:

* ``True`` (o ``(0, 1)``): vengono disegnati i lati;
* ``0``: viene disegnato il lato 0;
* ``1``: viene disegnato il lato 1;

``Angle`` fornisce i seguenti metodi dal significato piuttosto evidente::

* ``extent``: ampiezza dell'angolo;
* ``bisector``: bisettrice;
* ``vertex``: il vertice;
* ``point0``: il punto base 0;
* ``point1``: il punto base 1;
* ``side0``: il lato 0;
* ``side1``: il lato1.

**Esempio**

Disegna un angolo e un angolo con i lati. ::

    ip = InteractivePlane('Angle(Point, Point, Point)')
    ip.defwidth = 5
    a = Point(-2, 4, color="#40c040", name="A")
    b = Point(-5, -2, color="#40c040", name="B")
    c = Point(-8, 6, color="#40c040", name="C")
    d = Point(8, 6, color="#40c040", name="D")
    e = Point(5, -2, color="#40c040", name="E")
    f = Point(2, 4, color="#40c040", name="F")
    # angolo senza i lati
    Angle(a, b, c, color="#40c040")
    # angolo con i lati
    Angle(d, e, f, color="#c04040", sides=True)

Somma di due angoli. ::

    ip = InteractivePlane('Angle(Point, Point, Angle)')
    # i 2 angoli di partenza
    a = Angle(Point(-3, 7, width=6),
              Point(-7, 5, width=6),
              Point(-6, 8, width=6),
              sides=(0, 1), color="#f09000", name='alfa')
    b = Angle(Point(9, 2, width=6),
              Point(2, 3, width=6),
              Point(6, 4, width=6),
              sides=(0, 1), color="#0090f0", name='beta')
    # Punti di base dell'angolo somma di a b
    v = Point(-11, -8, width=6)
    p0 = Point(3, -10, width=6)
    # la somma degli angoli
    b1 = Angle(p0, v, b, (0, 1), color="#0090f0")
    p1 = b1.point1()
    a1 = Angle(p1, v, a, sides=True, color="#f09000")
    Text(-4, -12, "Somma di due angoli")

``Bisector``
------------

**Scopo**

Retta bisettrice di un angolo.

**Sintassi** ::

  Bisector(angle)

**Osservazioni**

Vedi ``Ray``.

**Esempio**

Disegna l'incentro di un triangolo. ::

    ip = InteractivePlane('Bisector')
    # I tre vertici del triangolo
    a=Point(-7, -3, color="#40c040", width=5, name="A")
    b=Point(5, -5, color="#40c040", width=5, name="B")
    c=Point(-3, 8, color="#40c040", width=5, name="C")
    # Il triangolo
    Polygon((a, b, c))
    # Due angoli del triangolo
    cba=Angle(c, b, a)
    bac=Angle(b, a, c)
    # Le bisettrici dei due angoli
    b1=Bisector(cba, color="#a0c040")
    b2=Bisector(bac, color="#a0c040")
    # L'incentro
    Intersection(b1, b2, color="#c040c0", width=5, name="I")

``Polygonal``
-------------

**Scopo**

Poligonale data una sequenza di vertici.

**Sintassi** ::

  Polygonal(points)

**Osservazioni**

Vedi ``Polygon``.

**Esempio**

Disegna una linea spezzata aperta. ::

    ip = InteractivePlane('Polygonal')
    points=(Point(-8, -3), Point(-6, -2), Point(-5, -2),
            Point(-4, 2),  Point(-2, 3),  Point(0, 4),   
            Point(2, 3),   Point(4, 2),   Point(5, -2),  
            Point(6, -2),  Point(8, -3))
    Polygonal(points, color='saddle brown', width=4)

``CurviLine``
-------------

**Scopo**

Linea curva determinata da una sequenza di vertici.

**Sintassi** ::

  CurviLine(points)

**Osservazioni**

Vedi ``Polygon``.

**Esempio**

Disegna una linea spezzata aperta. ::

    ip = InteractivePlane('CurviLine')
    points=(Point(-8, -3), Point(-6, -2), Point(-5, -2),
            Point(-4, 2),  Point(-2, 3),  Point(0, 4),   
            Point(2, 3),   Point(4, 2),   Point(5, -2),  
            Point(6, -2),  Point(8, -3))
    CurviLine(points, color='goldenrod', width=4)

``Calc``
--------

**Scopo**

Dato che contiene il risultato di un calcolo.

**Sintassi** ::

  Calc(function, variables)

**Osservazioni**

* ``function`` è una funzione python, al momento del calcolo, alla
  funzione vengono passati come argomenti il contenuto di ``variables``. 
* ``variables`` è un oggetto ``Data`` o una tupla che contiene oggetti 
  Data. Il risultato è memorizzato all'interno dell'oggetto Calc e può 
  essere visualizzato con ``VarText`` o utilizzato per definire la posizione 
  di un punto.

**Esempio**

Calcolla il quadrato di un lato e la somma dei quadrati degli altri due di 
un triangolo. ::

    ip = InteractivePlane('Calc')
    Circle(Point(2, 4), Point(-3, 4), width=1)
    ip.defwidth = 5
    a = Point(-3, 4, name="A")
    b = Point(7, 4, name="B")
    c = Point(-1, 8, name="C")
    ab = Segment(a, b, color="#40c040")
    bc = Segment(b, c, color="#c04040")
    ca = Segment(c, a, color="#c04040")
    q1 = Calc(lambda a: a*a, ab.length())
    q2 = Calc(lambda a, b: a*a+b*b, (bc.length(), ca.length()))
    VarText(-5, -5, "ab^2 = {0}", q1, color="#40c040")
    VarText(-5, -6, "bc^2 + ca^2 = {0}", q2, color="#c04040")

Elenco Funzioni, Classi, Attributi e Metodi di ``pyig``
-------------------------------------------------------

Funzioni di ``pyig``
....................

.. glossary::

  ``version``
    Restituisce la versione della libreria.

Classi di ``pyig``
..................

.. glossary::

  ``Angle``
    Angolo dati tre punti o due punti e un angolo, il secondo punto 
    rappresenta il vertice. 
    Il verso di costruzione dell'angolo è quello antiorario.

  ``Bisector``
    Retta bisettrice di un angolo.

  ``Circle``
    Circonferenza dato il centro e un punto o il centro e un raggio 
    (un segmento).

  ``ConstrainedPoint``
    Punto legato ad un oggetto.

  ``CurviLine``
    Linea curva determinata da una sequenza di vertici.

  ``Calc``
    Dato che contiene il risultato di un calcolo.

  ``InteractivePlane``
    Crea il piano cartesiano e inizializza gli attributi del *piano*. 

  ``Intersection``
    Crea il punto di intersezione tra due rette.

  ``Label``
    Crea un testo legato a un oggetto.

  ``Line``
    Crea una retta per due punti.

  ``MidPoint``
    Crea il punto medio di un segmento
    
  ``MidPoints``
    Crea il punto medio tra due punti.

  ``Orthogonal``
    Crea la retta perpendicolare ad una retta data passante per un punto.

  ``Parallel``
    Crea la retta parallela ad una retta data passante per un punto.

  ``Point``
    Crea un *punto libero* date le coordinate della sua posizione iniziale. 
  
  ``PointOn``
    Punto disegnato su un oggetto in una posizione fissa.

  ``Polygon``
    Crea un poligono data una sequenza di vertici.
    
  ``Polygonal``
    Poligonale data una sequenza di vertici.

  ``Ray``
    Traccia una semiretta con l'origine in un punto e passante per un altro 
    punto.
 
  ``Segment``
    Crea un segmento dati i due estremi, i due estremi sono *punti*.

  ``Text``
    Crea un testo posizionato in un punto del piano.

  ``VarText``
    Crea un testo variabile. Il testo contiene dei "segnaposto" che verranno 
    sostituiti con i valori prodotti dai dati presenti nel parametro variables.

  ``VarLabel``
    Testo variabile legato ad un oggetto.

  ``Vector``
    Testo variabile legato ad un oggetto.

Attributi
.........

.. glossary::

  ``<oggetto_visibile>.color`` 
    Attributo degli oggetti geometrici: imposta il colore dell'oggetto;
    
  ``<oggetto_visibile>.name`` 
    Attributo degli oggetti geometrici: imposta il nome dell'oggetto.

  ``<oggetto_visibile>.visible`` 
    Attributo degli oggetti geometrici: stabilisce se l'oggetto sarà 
    visibili o invisibile;
    
  ``<oggetto_visibile>.width`` 
    Attributi degli oggetti geometrici: imposta la larghezza dell'oggetto.
    
  ``<piano_interattivo>.defvisible``
    Attributo del piano: stabilisce se i nuovi oggetti creati saranno, 
    in modo predefinito, visibili o invisibili;
    
  ``<piano_interattivo>.defwidth`` 
    Attributo del piano: imposta la larghezza predefinita;
    
  ``<piano_interattivo>.defwidthtext`` 
    Attributo del piano: imposta la larghezza predefinita dei caratteri;
    
  ``<piano_interattivo>.defcolor`` 
    Attributo del piano: imposta il colore predefinito degli oggetti;
    
  ``<piano_interattivo>.defdragcolor`` 
    Attributo del piano: imposta il colore predefinito per gli oggetti che si 
    possono trascinare con il mouse.
    
  ``<piano_interattivo>.defintcolor`` 
    Attributo del piano: imposta il colore predefinito per la superficie di 
    circonferenze e poligoni.

Metodi
......

.. glossary::

  ``<circonferenza>.radius``
    Metodo delle classi *circonferenza* che  restituisce un oggetto ``data`` 
    che contiene la lunghezza del raggio della circonferenza.

  ``<figura>.perimeter``
    Metodo delle classi *figura* che  restituisce un oggetto ``data`` 
    contenete la lunghezza del contorno dell'oggetto.

  ``<figura>.surface``
    Metodo delle classi *figura* che  restituisce un oggetto ``data`` 
    contenete l'area della superficie dell'oggetto.

  ``<linea_retta>.equation``
    Metodo delle classi *linea* che restituisce un oggetto ``data`` 
    contenete l'equazione esplicita della retta a cui appartiene l'oggetto.

  ``<linea_retta>.point1``
    Metodo delle classi *linea* che restituisce il *punto1*.

  ``<linea_retta>.slope``
    È il metodo presente in tutte le classi *linea* che restituisce 
    la pendenza della retta o della retta che a cui appartiene l'oggetto.

  ``<oggetto>.type`` 
    Restituisce il tipo dell'oggetto.

  ``<oggetto_visibile>.coords`` 
    Restituisce un dato che contiene le coordinate.

  ``<oggetto_visibile>.point0``
    Metodo delle classi *linea* che restituisce il *punto0*.

  ``<oggetto_visibile>.xcoord`` 
    Metodo degli oggetti visualizzabili: restituisce un dato che contiene
    l'ascissa.
    
  ``<oggetto_visibile>.ycoord`` 
    Metodo degli oggetti visualizzabili: restituisce un dato che contiene
    l'ordinata.

  ``<punto_legato>.parameter``
    Metodo dei punti legati agli oggetti che restituisce un oggetto ``data`` 
    contenete il parametro. 

  ``<segmento>.length``
    Metodo della classe ``Segment`` che restituisce un oggetto ``data`` 
    contenete la lunghezza del segmento stesso.

  ``<segmento>.midpoint``
    Metodo della classe ``Segment`` che restituisce il punto medio 
    del segmento.
