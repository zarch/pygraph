Un programma
============

*Dove si scrive il primo programma*

Iniziare un programma
---------------------

La possibilità di scrivere, rivedere, modificare, rieseguire linee di comandi
permette di realizzare disegni anche piuttosto complicati utilizzando solo
la shell di ``IDLE``. Ma se vogliamo risolvere problemi più complessi dobbiamo
realizzare tante funzioni e non è detto che riusciamo a farlo all'interno di 
un'unica sessione di lavoro. Dobbiamo scrivere un programma 
``Python`` in una finestra di editor e salvarlo in un file in modo da poterlo
riprendere, correggere, completare. Un programma è un testo che contiene una
sequenza di funzioni, di comandi e di commenti.
Il programma deve essere scritto in un testo *piano*, senza nessun tipo di
formattazione per cui non va usato, per scriverlo, un normale elaboratore
di testi, ma un "editor" che permette di scrivere e di salvare esattamente
e solo i caratteri immessi dalla tastiera.

Oltre alla shell, che abbiamo utilizzato fin'ora, ``IDLE`` metta a
disposizione anche un editor di testo e sarà quello che utilizzaremo.
Per iniziare un nuovo programma, dal menu ``File`` scegliamo la voce
``New Window`` e apparirà una nuova finestra completamente vuota con il
cursore lampeggiante in alto a sinistra.

Prima ancora di incominciare a riempirla, pensiamo ad un nome da dare al
programma, il nome deve essere significativo per noi. Nel mio caso, dato che
questo è il primo programma che ho scritto per il manualetto l'ho chiamato:
"man1.py".

.. note::
  L'estensione ".py" indica che questo file contiene un programma scritto 
  in ``Python``. ``IDLE`` non aggiunge l'estensione al file quindi bisogna
  ricordarsi di terminare sempre il nome del programma con le tre lettere:
  "``.py``" se vogliamo che il testo sia riconosciuto come un programma 
  scritto in ``Python``.

Pensato ad un nome adatto, salviamo il file prestando ben attenzione a dove
viene salvato, in modo da riuscire a ritrovarlo in seguito:
dal menu ``File`` scegliamo la voce ``Salva``, spostiamoci nella cartella
nella quale abbiamo deciso di mettere il programma, scriviamo il nome del
programma e confermiamo con il tasto ``<Invio>``.

I commenti
----------

I comandi e le funzioni sono già stati visti dei capitoli precedenti.
I commenti sono molto importanti per poter capire programmi scritti da altre
persone ma anche programmi scritti da noi stessi, magari qualche tempo prima.
Ci sono due tipi di commenti: commenti di linea e commenti su più linee. Il
carattere cancelletto, “#”, indica l'inizio di un commento che si estende fino
alla fine della riga. Quando l'interprete ``Python`` incontra il carattere “#”
passa ad interpretare la riga seguente::

  # questo è un commento

Se abbiamo bisogno di scrivere un commento che si estenda su più linee
possiamo iniziare ogni linea con il carattere “#” oppure iniziare e terminare
il commento con tre virgolette """ di seguito::

  """Questo e' un
  commento scritto su
  piu' linee"""

Il primo mattone del programma
------------------------------

Rimbocchiamoci le maniche e iniziamo a scrivere il nostro primo programma.

Ogni programma deve essere ben commentato, in particolare deve avere
un'intestazione che indichi l'interprete, il nome del file, l'autore, la data
il tipo di licenza e, ovviamente, un titolo.

In questo esempio ci poniamo l'obiettivo di far disegnare alla tartaruga un
muro di mattoni quadrati. L'intestazione del programma potrebbe essere::

  #----------------------Python-pyturtle-----------------man1.py--#
  #                                                               #
  #                       Muro iterativo                          #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2005--#

Scritta l'intestazione, se vogliamo, possiamo anche eseguire il programma.
Possiamo farlo attraverso il menu::

  Run - Run module

o premendo semplicemente il tasto:: 

  <F5>. 

Per tutta risposta ``IDLE`` ci chiede se volgiamo salvare le modifiche 
apportate al programma. Noi, ovviamente, confermiamo.

.. note::
  dato che ogni volta che vogliamo eseguire il programma, vogliamo
  anche salvare le modifiche, possiamo andare su
  menu ``Option-Configure IDLE`` e nella scheda ``General`` selezionare
  ``No Prompt``. D'ora in poi, dopo la pressione del tasto ``F5`` non ci
  verrà più richiesta la conferma del salvataggio del file.

``Python`` lo interpreterà correttamente, non darà alcun messaggio di errore,
ma, essendo composto solo da commenti non eseguirà assolutamente niente.
Incominciamo allora ad aggiungere istruzioni. Poiché vogliamo utilizzare
la grafica della tartaruga, come primo comando dobbiamo caricare la libreria,
creare un piano e chiedere al piano di produrre una nuova tartaruga::

  from pyturtle import TurtlePlane
  tp = TurtlePlane()
  tina = Turtle()

Ora, per costruire un muro di mattoni quadrati, abbiamo bisogno di una
funzione che disegni quadrati::

  def quadrato(lato):
      """Disegna un quadrato di dato lato."""
      for i in range(4)
          tina.forward(lato)
          tina.left(90)

Ormai siamo super esperti in quadrati... Ma questa volta aggiungiamo un
commento come prima riga della funzione. Si chiama *docstring* e ogni
funzione, oltre ad un nome significativo, dovrebbe averne una.

Ora dobbiamo eseguire il programma (``Run - Run module`` o più velocemente 
tasto ``<F5>``). A questo punto i casi sono due:

#. Non succede assolutamente niente.
#. ``Python`` scrive alcune strane righe nella shell di ``IDLE``.

Nel primo caso, vuol dire che è andato tutto bene, ``Python`` non ha eseguito
niente perché noi abbiamo definito una funzione, ma non abbiamo dato il
comando di eseguirla e giustamente il computer non si prende la responsabilità
di farlo di propria iniziativa.

Nel secondo caso, le strane scritte sono un messaggio di errore:
``Python`` non è riuscito a interpretare quello che abbiamo scritto e quindi
non ha definito la procedura.

Tranello! Se la procedura è stata scritta esattamente come riportato sopra, 
``Python`` ci risponde con::

  File ".../sorgenti/graph/man1.py", line 10
    for i in range(4)
                     ^
  SyntaxError: invalid syntax

Questo messaggio ci dà le seguenti informazioni:

* in quale file si trova l'errore: .../sorgenti/graph/man1.py
* la linea dove ha incontrato un errore: line 10,
* il punto in cui l'interprete si è bloccato,
* il tipo di errore: SyntaxError: invalid syntax

Già, ci siamo dimenticati i ":", correggiamola::

  def quadrato(lato):
      """Disegna un quadrato di dato lato."""
      for i in range(4):
          tina.forward(lato)
          tina.left(90)

Ora dobbiamo eseguire di nuovo il programma tasto ``<F5>`` o menu: 
``Run - Run module``.
Se tutto è andato bene, non dovrebbe succedere niente. Spostiamoci nella shell
di ``IDLE`` e diamo il comando::

  >>> quadrato(57)

Se non ci sono altri errori dovremmo ottenere un quadrato. Possiamo provare il
funzionamento della funzione ``quadrato(lato)`` con diversi valori
dell'argomento.
È anche possibile aggiungere questo comando in fondo al programma. In questo
modo, ogni volta che si esegue il programma verrà creata una tartaruga e
disegnato un quadrato. Tutto il nostro programma è ora::

  #----------------------Python-pyturtle-----------------man1.py--#
  #                                                               #
  #                       Muro iterativo                          #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2005--#

  from pyturtle import TurtlePlane
  tp = TurtlePlane()
  tina =  Turtle()

  def quadrato(lato):
      """ Disegna un quadrato di dato lato """
      for i in range(4):
          tina.forward(lato)
          tina.left(90)

  quadrato(57)

Eseguiamolo, se tutto funziona correttamente appare la finestra della grafica
della tartaruga con dentro un quadrato.
Bene, abbiamo scritto, salvato e provato il nostro primo programma
funzionante, possiamo rilassarci un po' prima di procedere con il lavoro...

Uno strato di mattoni
---------------------

Disegnare un intero muro di quadrati può sembrare un'impresa piuttosto
complicata. L'intero muro può essere pensato come composto da più file e ogni
fila da più quadrati::

  **muro** ---(fatto da)---> **fila** ---(fatta da)---> **quadrato** 

La funzione che disegna un quadrato l'abbiamo realizzata, proviamo allora a
realizzare la funziona che realizza una fila di quadrati tutti uguali. Questa
funzione avrà bisogno di due informazioni: la dimensione dei quadrati e il
numero di quadrati da mettere in una fila. Cioè vorremmo poter scrivere::

  >>> fila(20, 15)

per ottenere una fila di ``15`` quadrati di lato ``20``. Per il numero di
volte che abbiamo deciso, la funzione ``fila`` deve disegnare un quadrato
e spostare la tartaruga avanti di un tratto uguale al lato del quadrato. La
funzione potrebbe essere::

  def fila(lato, numero):
      """Disegna una fila di mattoni."""
      for j in range(numero):
          quadrato(lato)
          tina.forward(lato)

Aggiungiamo al programma la funzione ``fila(lato, numero)``, eseguiamo il
programma e proviamo la funzione modificando gli argomenti in modo da
realizzare file più o meno lunghe di quadrati più o meno grandi.

La fila di quadrati viene disegnata, ma, la procedura ``fila``, non è
"trasparente": oltre a disegnare una fila di quadretti,
**sposta la tartaruga** senza rimetterla dove l'aveva trovata.
È molto importante che ogni funzione faccia solo quello che dice di fare,
senza effetti collaterali. Quindi la funzione ``fila`` deve preoccuparsi di
rimettere a posto Tartaruga. Di quanto l'ha spostata? Per ``numero`` volte è
andata avanti di ``lato`` passi, in totale ``numero*lato``.
Per rimettere a posto Tartaruga bisogna, finito il ciclo, farla
indietreggiare della stessa quantità::

  def fila(lato, numero):
      """Disegna una fila di mattoni."""
      for j in range(numero):
          quadrato(lato)
          tina.forward(lato)
      tina.back(numero*lato)

La fila di quadrati che abbiamo ottenuto non richiama per niente dei mattoni,
sono troppo appiccicati: un po' di malta tra uno e l'altro? Per distanziarli
un po' basta allungare lo spostamento dopo ogni quadrato e ovviamente anche
al ritorno::

  def fila(lato, numero):
      """Disegna una fila di mattoni."""
      for j in range(numero):
          quadrato(lato)
          tina.forward(lato+5)
      tina.back(numero*(lato+5))

Funziona? Sì, ma non va bene. Esistono due tipi di errori quelli che bloccano
il programma detti anche errori di sintassi e quelli che fanno fare al
programma una cosa diversa da quella che volevamo, gli errori di semantica.
Nel nostro caso la procedura funziona però non disegna quadrati staccati l'uno
dall'altro, ma uniti da una linea non certo bella da vedere. Lo spostamento
tra un quadrato e l'altro deve essere fatto senza lasciare segno::

  def fila(lato, numero):
      """Disegna una fila di mattoni."""
      for j in range(numero):
          quadrato(lato)
          tina.up()
          tina.forward(lato+5)
          tina.down()
      tina.up()
      tina.back(numero*(lato+5))
      tina.down()

Ora va e la distanza tra due quadrati sembra abbastanza adeguata, ma se
cambiamo il lato dei quadrati, andrà sempre bene? Possiamo parametrizzare
anche quella, ma lo facciamo dandole come valore predefinito 5::

  def fila(lato, numero, spazio_colonne=5):
      """Disegna una fila di mattoni."""
      for j in range(numero):
          quadrato(lato)
          tina.up()
          tina.forward(lato+spazio_colonne)
          tina.down()
      tina.up()
      tina.back(numero*(lato+spazio_colonne))
      tina.down()

In questo modo la funzione fila può essere chiamata con due o con tre argomenti:

* Se viene chiamata con due argomenti, il terzo viene automaticamente posto
  uguale a 5,
* Se viene chiamata con tre argomenti, il terzo parametro assumerà il terzo
  valore. ::

  >>> fila(30, 3)

Disegna una fila di 3 quadrati di lato 30 distanziati di 5 passi, ::

  >>> fila(30, 3, 12)

Disegna una fila di 3 quadrati di lato 30 distanziati di 12 passi.

Salviamo, eseguiamo e proviamo la funzione fila chiandola con 2 o con 3
parametri con diversi valori. Funziona.
A questo punto il nostro programma è il seguente::

  #----------------------Python-pyturtle-----------------man1.py--#
  #                                                               #
  #                       Muro iterativo                          #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2005--#

  from pyturtle import TurtlePlane
  tp = TurtlePlane()
  tina = Turtle()

  def quadrato(lato):
      """Disegna un quadrato di dato lato."""
      for i in range(4):
          tina.forward(lato)
          tina.left(90)

  def fila(lato, numero, spazio_colonne=5):
      """Disegna una fila di mattoni."""
      for j in range(numero):
          quadrato(lato)
          tina.up()
          tina.forward(lato+spazio_colonne)
          tina.down()
      tina.up()
      tina.back(numero*(lato+spazio_colonne))
      tina.down()

  tina.up()
  tina.back(200)
  tina.down()
  fila(20, 15)

L'intero muro
-------------

Possiamo essere soddisfatti del risultato ottenuto. Ora passiamo alla
costruzione del muro. Cos'è un muro? È una ``pila`` di ``file`` di 
``mattoni``. ::

  def muro():
      """Disegna un muro di quadrati."""
      for i in range(15):
          fila(20, 18,)
          tina.up()
          tina.left(90)
          tina.forward(20+5)
          tina.right(90)
          tina.down()
      tina.up()
      tina.left(90)
      tina.forward(15*(20+5))
      tina.right(90)
      tina.down()

Proviamola... funziona!

Ma c'è una certa differenza tra un programma che funziona e un buon programma.
Se vogliamo imparare a programmare non dobbiamo accontentarci di un programma
che funziona, dobbiamo affinare una certa sensibilità anche all'esaspetto
estetico. Non dobbiamo affezionarci troppo al nostro prodotto, ma cercare di
migliorarlo.

Ristrutturazione
----------------

Ci sono un paio di cose che stonano nella funzione ``muro``:

* cinque righe si ripetono quasi identiche, e non va bene che in un programma
  si ripetano blocchi di istruzioni (quasi) identiche;
* ci sono troppi numeri;

Partiamo dal primo problema: la soluzione è costruire una funzione che le
esegua con un solo comando.

Le tre righe che vanno da ``tina.up()`` a
``tina.down()`` producono uno spostamento di Tartaruga senza disegnare e
perpendicolare alla sua direzione. Possiamo generalizzare questo
comportamento aggiungendo oltre allo spostamento verticale uno orizzontale.
Possiamo costruire una funzione ``sposta`` che riceve come argomenti lo
spostamento nella direzione della Tartaruga e lo spostamento nella sua
direzione perpendicolare. Questa funzione dovrà avere quindi due parametri::

  def sposta(avanti=0, sinistra=0):
      """Effettua uno spostamento orizzontale e verticale
         di Tartaruga senza disegnare la traccia."""
      tina.up()
      tina.forward(avanti)
      tina.left(90)
      tina.forward(sinistra)
      tina.right(90)
      tina.down()

.. Note:: in queste funzioni ho utilizzato un terzo metodo per inserire un
  argomento in un parametro: il passaggio dell'argomento "per nome".

Scrivendo i parametri in questo modo, possiamo chiamare la funzione sposta in
vari modi:

* ``sposta()``, non fa niente;
* ``sposta(47)``, sposta avanti Tartaruga di 47 unità senza tracciare segni;
* ``sposta(47, 61)``, sposta Tartaruga avanti di 47 e a sinistra di 61 unità
  senza tracciare segni;
* ``sposta(sinistra=61)``, sposta Tartaruga a sinistra di 61 unità
  senza tracciare segni;

Anche la funzione ``fila`` può utilizzare questa funzione::

  def fila(lato, numero, spazio_colonne=5):
      """Disegna una fila di mattoni quadrati."""
      for j in range(numero):
          quadrato(lato)
          sposta(lato+spazio_colonne)
      sposta(-numero*(lato+spazio_colonne))

E muro diventa::

  def muro():
      """Disegna un muro di quadrati."""
      for i in range(15):
          fila(20, 18,)
          sposta(sinistra=20+5)
      sposta(sinistra=-15*(20+5))

Confrontando la versione precedente di ``muro`` con questa si può notare
una bella semplificazione!

Nella funzione ``fila``, l'istruzione::

  sposta(lato+spazio_colonne)

significa: chiama la funzione ``sposta`` associando al primo parametro il
risultato del calcolo: ``lato+spazio_colonne``.

Nella funzione ``muro``, l'istruzione::

  sposta(sinistra=20+5)

significa: chiama la funzione ``sposta`` associando al parametro di nome
``sinistra`` il risultato del calcolo: ``20+5``.

Ora occupiamoci di eliminare un po' di numeri parametrizzando anche la
procedura ``muro``. I numeri presenti nella funzione riguardano: la lunghezza
del lato, il numero di righe, il numero di colonne, lo spazio tra le righe,
lo spazio tra le colonne. Questi due ultimi valori possono essere lasciati di
default uguali a 5.

Trasformandoli tutti in parametri la funzione ``muro`` diventa::

  def muro(lato, righe, colonne, spazio_colonne=5, spazio_righe=5):
      """Disegna un muro di mattoni."""
      for i in range(righe):
          fila(lato, colonne, spazio_colonne)
          sposta(sinistra=lato+spazio_righe)
      sposta(sinistra=-righe*(lato+spazio_righe))

E tutto il programma::

  #----------------------Python-pyturtle-----------------man1.py--#
  #                                                               #
  #                       Muro iterativo                          #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2005--#

  from pyturtle import TurtlePlane
  tp = TurtlePlane()
  tina = Turtle()

  def sposta(o=0, v=0):
      """Effettua uno spostamento orizzontale e verticale di Tartaruga
         senza disegnare la traccia."""
      tina.up()
      tina.forward(o); tina.left(90); tina.forward(v); tina.right(90)
      tina.down()

  def quadrato(lato):
      """Disegna un quadrato di dato lato vuoto o pieno."""
      for i in range(4):
          tina.forward(lato)
          tina.left(90)

  def fila(lato, colonne, spazio_colonne=5):
      """Disegna una fila di mattoni quadrati."""
      for j in range(colonne):
          quadrato(lato)
          sposta(o=lato+spazio_colonne)
      sposta(o=-colonne*(lato+spazio_colonne))

  def muro(lato, righe, colonne, spazio_righe=5, spazio_colonne=5):
      """Disegna un muro di mattoni."""
      for i in range(righe):
          fila(lato, colonne, spazio_colonne)
          sposta(v=lato+spazio_righe)
      sposta(v=-righe*(lato+spazio_righe))

  sposta(-250, -190)
  muro(20, 15, 20)

Compattiamo il codice
---------------------

Funziona tutto a meraviglia, o almeno dovrebbe funzionare se non ho
introdotto qualche errore! Potremmo considerarci soddisfatti se l'istinto del
programmatore non rodesse dentro... Perdendo un po' in chiarezza possiamo
compattare di più il codice. Vale la pena? Dipende da ciò che si vuole
ottenere, comunque, prima di dare un giudizio proviamo un'altra versione.

Prima avevamo staccato delle righe di codice per fare una funzione separata
che realizzasse gli spostamenti data la componente orizzontale e verticale.
Ora fondiamo due funzioni che hanno degli elementi in comune: la procedura
``muro`` e la procedura ``fila``. L'intero muro si può ottenere annidando,
uno dentro l'altro due cicli: il ciclo più esterno impila le file e quello più
interno allinea quadrati. In pratica al posto della chiamata alla procedura
``fila(...)``, trascriviamo tutte le sue istruzioni. Dobbiamo anche aggiustare
i nomi e l'indentazione::

  def muro(lato, righe, colonne, spazio_colonne=5, spazio_righe=5):
      """Disegna un muro di mattoni."""
      for i in range(righe):
          for j in range(colonne):
              quadrato(lato)
              sposta(avanti=lato+spazio_colonne)
          sposta(avanti=-colonne*(lato+spazio_colonne))
          sposta(sinistra=lato+spazio_righe)
      sposta(sinistra=-righe*(lato+spazio_righe))

I due cicli annidati devono avere due variabili diverse
(spesso si usano per queste variabili di ciclo i nomi ``i`` e ``j``).

Possiamo ancora eliminare una riga di codice! Come?

Alla fine del ciclo più interno ci sono due chiamate alla funzione sposta.
Possono essere fuse in un'unica chiamata. E con questo il programma è
terminato... ::

  #----------------------Python-pyturtle-----------------man1.py--#
  #                                                               #
  #                       Muro iterativo                          #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2005--#

  from pyturtle import TurtlePlane
  tp = TurtlePlane()
  tina = Turtle()

  def sposta(o=0, v=0):
      """Effettua uno spostamento orizzontale e verticale di Tartaruga
         senza disegnare la traccia."""
      tina.up()
      tina.forward(o); tina.left(90); tina.forward(v); tina.right(90)
      tina.down()

  def quadrato(lato):
      """Disegna un quadrato di dato lato vuoto o pieno."""
      for i in range(4):
          tina.forward(lato)
          tina.left(90)

  def muro(lato, righe, colonne, spazio_righe=5, spazio_colonne=5):
      for i in range(righe):
          for j in range(colonne):
              quadrato(lato)
              sposta(avanti=lato+spazio_colonne)
          sposta(-colonne*(lato+spazio_colonne), lato+spazio_righe)
      sposta(sinistra=-righe*(lato+spazio_righe))

  sposta(-250, -190)
  muro(20, 15, 20)

...o quasi...

Riassumendo
-----------

* Un programma è un documento di testo che contiene istruzioni e funzioni.
* Scritto un programma bisogna salvarlo: menu: ``File – Save`` ed eseguirlo:
  menu: ``Run - Run module``.
* Le funzioni definite in un programma possono essere eseguite anche
  dall'ambiente shell ``IDLE``.
* Quando è possibile è meglio sostituire i numeri e le costanti presenti in
  una funzione con parametri.
* Le procedure possono avere anche dei parametri con dei valori predefiniti
  (di *default*).
* Gli argomenti di una funzione possono essere passati anche *per nome*.
* Più linee di istruzioni che si ripetono all'interno di un programma possono
  essere raggruppate in un'unica funzione.
* All'interno di un ciclo possono essere annidati altri cicli, bisogna fare
  attenzione al nome delle variabili.
