.. pygraph documentation master file, created by
   sphinx-quickstart on Tue Jul  7 16:54:20 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pygraph's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 3

   01intro
   02comandi
   03turtle
   04vocab
   05functions
   06parameters
   07program
   08newclass
   09modclass
   10ricorsione
   11pycart
   12pyplot
   13pyturtle
   14pyig

Indici e tavole
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

