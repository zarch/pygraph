Come modificare il comportamento di una classe
==============================================

*Dove rompiamo un po' gli schemi e costruiamo un metodo che oscura un metodo
del genitore.*

Estendere una classe: spostamento casuale
-----------------------------------------

Nel capitolo precedente abbiamo realizzato un muro di quadrati. Tutti in
ordine ben allineati, tutti uguali... Un po' troppo in ordine, un po' troppo
uguali... Proviamo a mettere un po' di disordine nello schema. Invece che
disegnare un quadrato con il primo lato in direzione della tartaruga, possiamo
fare in maniera che il quadrato sia spostato casualmente. Sorge subito un
problema: ``Python`` non ha un comando per ottenere dei valori casuali. Niente
paura, c'è una libreria che ci fornisce la funzione adatta. La libreria è
``random`` e la funzione che ci interessa è ``randrange(<numero>)`` che
restituisce un numero intero compreso tra zero incluso e ``<numero>`` escluso.

Possiamo ripensare la procedura quadrato in questo modo::

  definisco quadrato(lato) così:
    metto nella variabile angolo un numero casuale tra 0 e 30
    metto nella variabile spostamento un numero casuale tra 0 e 30
    ruoto tartaruga di angolo e la sposto di spostamento
    disegno il quadrato
    rimetto a posto tartaruga

Ora se utilizzassimo la programmazione classica dovremmo prendere il programma
scritto precedentemente e modificare la procedura quadrato. La programmazione
ad oggetti ci permette un meccanismo diverso:

* si crea una classe discendente della classe ``Ingegnere``,
* si modifica il metodo ``quadrato(...)``,

La nuova classe così costruita possiede tutte le caratteristiche della vecchia
classe ma con il metodo quadrato diverso. Proviamola::

  #----------------------python-pyturtle-----------------man3.py--#
  #                                                               #
  #                         Terremoto                             #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2009--#

  from man2 import TurtlePlane, Ingegnere
  from random import randrange

  class Architetto(Ingegnere):
    def quadrato(self, lato):
      """Disegna un mattone spostato rispetto alla posizione
      attuale di Tartaruga."""
      angolo=randrange(30)
      spostamento=randrange(30)
      self.up()
      self.right(angolo); self.forward(spostamento)
      self.down()
      Ingegnere.quadrato(self, lato)
      self.up()
      self.back(spostamento); self.left(angolo)
      self.down()

  tp = TurtlePlane()
  michelangelo = Architetto()
  michelangelo.sposta(-250, -180)
  michelangelo.muro(20, 15, 20)
  tp.mainloop()

Funziona e non funziona. Funziona perché abbiamo ottenuto i quadrati
scombinati, come volevamo. Ma perché in un'altra finestra vengono disegnati
anche i quadrati perfettamente schierati? Il fatto è che la prima volta che
viene letta la libreria ``man2.py``, questa viene anche eseguita, quindi, in
particolare, vengono eseguite le sue ultime tre righe che producono il disegno
dei quadrati tutti diritti. Se proviamo ad eseguire un'altra volta ``man3.py``
verranno disegnati solo i quadrati scombinati, perché la libreria, già letta 
in questa sessione di lavoro, non viene riletta. Python mette a disposizione 
gli strumenti (semplici) per evitare questo meccanismo, ma la loro 
comprensione va al di là dei nostri scopi. Quindi non preoccupiamoci di questo 
strano comportamento.

L'effetto ottenuto pare abbastanza naturale, ma cosa avviene nell'interprete?
``michelangelo`` è un oggetto della classe ``Architetto``.
Il comando ``michelangelo.sposta(-250, -180)`` chiama il metodo ``sposta`` di
``Architetto`` il quale chiama il metodo ``forward`` di ``Turtle``...
``michelangelo.muro(20, 15, 20)`` chiama il metodo ``muro`` di ``Ingegnere``
e questo chiama il metodo ``quadrato`` di ``Architetto`` e il metodo ``sposta``
di ``Ingegnere``. Questo comportamento non è semplice per il computer, ma
appare naturale per il programmatore. Questo meccanismo permette di estendere
a piacere librerie senza doverle modificare. In questo modo si possono
realizzare librerie stabili, solide perché condivise e provate da molti
utilizzatori, ma adattabili alle proprie esigenze.

Estendere una classe: aggiungiamo i colori
------------------------------------------

E se mi fossi stancato del bianco e nero e volessi dei mattoni colorati?
Probabilmente la cosa più semplice potrebbe essere quella di modificare il
metodo ``quadrato``, ma proviamo a utilizzare ancora l'ereditarietà. Chiudiamo
questo programma e apriamo una nuova finestra di editor dove definiamo una
nuova classe, chiamiamola ``Artista``, discendente da ``Architetto``. Questa
classe dovrà ancora modificare il metodo ``quadrato``. Dovrà:

* scegliere un colore,
* attivare il comando di riempimento
* disegnare il quadrato,
* riempirlo con il colore scelto

L'attributo ``color`` di Tartaruga accetta diversi tipi di argomenti:
Ci sono vari modi per definire il colore di una tartaruga::

  <tartaruga>.color = <nome di un colore>

ad esempio::

  tina.color = 'pink'

oppure::

  <tartaruga>.color = (<red>, <green>, <blue>)

dove <red>, <green>, e <blue> sono dei numeri razionali compresi tra 0 e 1 che
rappresentano l'intensità dei tre colori fondamentali rosso, verde e blu. Ad
esempio:

  tina.color = (0.5, 0, 0.5)

oppure::

  <tartaruga>.color = <stringacolore>

dove <stringacolore> è una stringa nella forma: "#RRGGBB" dove RR, GG, BB sono
tre numeri esadecimali che rappresentano le componenti rossa, verde e blu del
colore::

  tina.color = "#f257a0"

Noi utilizziamo la seconda forma in modo da prendere a caso le componenti dei
colori in ogni quadrato.

Per riempire di un colore una figura si usa il metodo ``fill(0|1)``. Quando
viene chiamato Il metodo ``fill(1)``, Tartaruga tiene nota delle parti da
riempire, quando viene chiamato ``fill(0)``, Tartaruga le riempie. Quindi, se
si vuole colorare una figura, si deve chiamare il metodo ``fill`` con
l'argomento uguale a ``1`` prima di iniziare a disegnarla e ``fill`` con
l'argomento uguale a ``0`` alla fine.
A questo punto la figura viene riempita con il colore attuale di Tartaruga.::

  #----------------------python-pyturtle-----------------man4.py--#
  #                                                               #
  #                     Terremoto a colori                        #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2009--#

  from man3 import TurtlePlane, Architetto
  from random import random
  #from random import randrange

  class Artista(Architetto):

    def quadrato(self, lato):
      """Disegna un mattone colorato."""
      self.fill(1)
      self.setcolor(random(), random(), random())
      Architetto.quadrato(self, lato)
      self.fill(0)

  tp = TurtlePlane()
  raffaello = Artista()
  raffaello.sposta(-250, -180)
  raffaello.muro(20, 15, 20)
  tp.mainloop()

Riassumendo
-----------

* Una classe discendente di un'altra, non solo può estendere i metodi di
  quest'ultima, ma può anche modificare il suo comportamento ridefinendo
  alcuni suoi metodi.
* Per ridefinire un metodo basta definirne uno con lo stesso nome.
* È l'interprete che si incarica di eseguire i metodi corretti tra tutti
  quelli che hanno lo stesso nome.
* Una classe può forzare l'interprete ad eseguire un metodo di una sua classe
  antenata anteponendo al nome del metodo il nome della classe. Ad esempio il
  metodo ``quadrato`` di ``Artista`` chiama il metodo ``quadrato`` della
  classe ``Architetto`` in questo modo::

    Architetto.quadrato(self, lato)
* Una classe discendente può quindi aggiungere metodi alla classe genitrice
  oppure ridefinire alcuni modificandone il comportamento.

