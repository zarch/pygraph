Una nuova classe
================

*Dove definiamo la nostra prima classe*

Classi e oggetti
----------------

Riassumiamo quello che avviene quando si esegue il programma ``man1.py``:

#. viene caricata dalla libreria ``pyturtle.py`` la classe ``TurtlePlane``;
#. viene creato un oggetto della classe TurtlePlane();
#. viene creata una tartaruga;
#. vengono definite tre funzioni (``sposta()``, ``quadrato()``, ``muro()``);
#. viene eseguito il programma principale che sposta la tartaruga e chiama 
   la funzione ``muro`` che chiama le funzioni ``quadrato`` e ``sposta``.

Nel programma precedente abbiamo già creato e utilizzato un *oggetto*.
Ora poniamo l'attenzione sulla creazione di classi di oggetti.
Elementi fondamentali di un Programma Orientato agli Oggetti (OOP) sono:

#. classi
#. attributi
#. metodi
#. oggetti

La libreria ``PyTurtle`` definisce le *classi* ``TurtlePlane`` e ``Turtle``. 
Ogni *oggetto* della *classe* ``Turtle`` ha diverse caratteristiche proprie, 
sono i suoi **attributi**: 
``color``, ``width``, ``position``, ``direction``, ...

e ogni *oggetto* della *classe* ``Turtle`` è in grado di eseguire alcuni
comandi sono i suoi **metodi**:
``forward(<numero>)``, ``back(<numero>)``, ...

.. note:
  per l'elenco completo di attributi e metodi si vedano i capitoli dedicati 
  alle singole librerie.

Una *classe* può essere utilizzata per:

#. costruire *oggetti* con le proprietà e i metodi di quella *classe*;
#. costruire altre *classi* che ampliano quella *classe*.

Nel nostro primo programma abbiamo costruito un *oggetto* della *classe*
``TurtlePlane``, di nome "tp" e abbiamo chiesto a "tp" con il metodo
``newTurtle``di creare un oggetto della classe ``Turtle`` e l'abbiamo
collegata al nome "tina"::

  tp = TurtlePlane()
  tina = tp.newTurtle()

.. note:: Per creare un oggetto basta scrivere il nome dell'oggetto seguito
  dal simbolo "=" e dal nome della classe con una coppia di parentesi.
  Bisogna prestare attenzione a due aspetti. Primo, se il nome della classe
  è “TurtlePlane” non si può sperare che il comando funzioni scrivendo
  “turtleplane” o “TURTLEPLANE”. Secondo, il nome della classe deve essere
  seguito da una coppia  di parentesi; solo in questo modo ``tp`` diventa un
  oggetto della classe ``TurtlePlane``, senza parentesi diventa un altro
  nome per la classe ``TurtlePlane``.

Si può creare una nuova tartaruga anche direttamente, senza invocare un metodo
di ``TurtlePlane``, in questo caso però bisogna specificare a quale piano deve
appartenere la nuova tartaruga::

  tp = TurtlePlane()
  tina = Turtle( )

I metodi sono funzioni che possono essere eseguite
da tutti gli oggetti di quella classe. Per dire ad un oggetto di una classe
di eseguire un suo metodo, devo scrivere il nome dell'oggetto seguito dal
punto, dal nome del metodo e da una coppia di parentesi contenenti,
eventualmente, gli argomenti necessari. In pratica se ``tina`` è un oggetto
della classe ``Turtle`` l'istruzione::

  tina.forward(97)

chiede all'*oggetto* collegato al nome ``tina`` di eseguire il suo *metodo*
``forward`` e ``97`` è l'*argomento* passato a questo metodo.

Quindi l'istruzione precedente comanda alla tartaruga ``tina`` di avanzare
di 97 passi.

  tp.reset()

comanda a ``tp`` di ripulire tutto il piano e di riportare la situazione allo
stato iniziale.
Perché ogni metodo deve essere preceduto dal nome dell'oggetto?
Non sarebbe più semplice scrivere solo: ``forward(97)`` o ``reset()``?
Il fatto è che possiamo creare quanti oggetti vogliamo della classe Turtle
quindi quando diamo un comando, dobbiamo specificare a quale oggetto quel
comando è diretto::

  tp = TurtlePlane()
  tina = Turtle()
  pina = Turtle()
  gina = Turtle()
  pina.left(120)
  gina.left(240)
  tina.forward(50)
  pina.forward(100)
  gina.forward(200)

In questo caso vengono create tre tartarughe,
vengono sfasate di 120 gradi l'una dall'altra e infine vengono fatte avanzare
di tre lunghezze diverse.

Gli attributi definiscono lo stato di un oggetto. Ad esempio, per cambiare
il colore della tartaruga e della sua penna si deve modificare il suo 
attributo ``color``:
``color = <colore>`` dove ``<colore>`` è una stringa che contiene il nome di
un colore, o ``color = (<rosso>, <verde>, <blu>)`` dove ``<rosso>``,
``<verde>``, ``<blu>`` sono tre numeri decimali compresi tra 0 e 1::

  tina.color = "purple"
  tina.color = (0.4, 0.4, 0.4)   # grigio

Viceversa se voglio memorizzare in una variabile l'attuale colore di una
tartaruga potrò assegnare ad una variabile il valore di ``color``::

  colore_attuale_di_tina = tina.color

Nuove classi
------------

Una caratteristica importante delle classi è l'ereditarietà. Una classe può
venir derivata da un'altra classe essere cioè figlia di un'altra classe; la
classe figlia eredita dalla classe genitrice tutte gli attributi e i metodi e
può:

* aggiungere altri attributi,
* aggiungere altri metodi,
* modificare i metodi della classe genitrice.

Buona parte della programmazione OOP consiste nel progettare e realizzare
classi e gerarchie di classi di oggetti. Realizzare una nuova classe può
essere un lavoro molto complicato ma potrebbe essere anche molto semplice
quando la classe che realizziamo estende qualche altra classe già funzionante.

Come esercizio proviamo a costruire la classe di una tartaruga che sappia
costruire un muro. Come al solito, prima di mettere mano a grandi opere,
iniziamo a lavorare ad un problema abbastanza semplice e conosciuto.
Voglio avere una tartaruga che oltre a saper fare tutto quello che sanno
fare le altre tartarughe sappia anche disegnare mattoni quadrati:
la nuova tartaruga deve quindi estendere le capacità di ``Turtle``.
La sintassi che ``Python`` mette a disposizione per estendere la gerarchia di
una classe è::

  class <nome di una nuova classe>(<nome di una classe esistente>):

In pratica noi possiamo creare una nuova specie di ``Turtle`` in questo modo::

  >>> from pyturtle import TurtlePlane, Turtle  # importa la libreria
  >>> class Ingegnere(Turtle):                  # crea una nuova classe
           pass                                 # non fa niente

  >>> tp = TurtlePlane()
  >>> leonardo = Ingegnere()        # crea un oggetto della nuova classe
  >>> leonardo.forward(100)         # esegue un metodo della nuova classe

La prima riga importa dalla libreria ``pyturtle`` le classi ``TurtlePlane`` e 
``Turtle``. 
La seconda e terza definiscono una nuova classe che si chiama ``Ingegnere`` 
e che non ha e non fa niente in più di ``Turtle``.
Le tre righe seguenti: creano un piano associato alla parola "tp" e creano un
oggetto della classe `Ingegnere`` legato alla parola "leonardo".

L'ultima linea comanda a ``leonardo`` di eseguire un metodo di ``Ingegnere``.
Ma come può farlo, se non abbiamo definito nessun metodo di ``Ingegnere``?
Non importa, ``Ingegnere`` è un discendente di ``Turtle`` e quindi, già alla
nascita, sa fare tutto quello che sa fare ``Turtle``.

Ora estendiamo le capacità di ``Ingegnere`` in modo che sappia disegnare
mattoni quadrati.

Sempre nella shell di IDLE proviamo ad aggiungere alla classe ``Ingegnere`` il
metodo ``quadrato()`` nel modo più intuitivo possibile::

  >>> class Ingegnere(Turtle):
          def quadrato(lato):
              for i in range(4):
                  forward(lato)
                  left(90)

``Python`` non dà errori di sintassi: è già un buon segno! Ora creiamo un
oggetto di tipo ``Ingegnere``::

  >>> tp = TurtlePlane()
  >>> leonardo = Ingegnere()

E anche qui tutto bene!! Ora comandiamo a ``leonardo`` di disegnare un
quadrato::

  >>> leonardo.quadrato(80)
  Traceback (most recent call last):
    File "<pyshell#18>", line 1, in ?
      leonardo.quadrato(80)
  TypeError: quadrato() takes exactly 1 argument (2 given)

Accidenti, non va!!! E non solo non funziona, ma ci dà un errore decisamente
assurdo: ``Python`` si lamenta che quadrato vuole un argomento e noi gliene
avremmo passati due!? È strabico? Non sa contare?? È stupido???
Boh, mah, forse...
Chi ha programmato questo linguaggio ha deciso che l'oggetto che deve eseguire
un metodo viene passato come primo parametro del metodo stesso. 
Noi scriviamo::

  leonardo.quadrato(80)

in realtà viene eseguito::

  Ingegnere.quadrato(leonardo, 80)

.. note::
  provare per credere.

Quindi se ``quadrato`` è un metodo di una classe deve avere un primo parametro
dentro il quale viene messo il riferimento all'oggetto che deve eseguire il
metodo stesso. Riscriviamo la classe e il metodo::

  >>> class Ingegnere(Turtle):
          def quadrato(self, lato):
              for i in range(4):
                  forward(lato)
                  left(90)

Ora ``quadrato`` ha i due parametri: uno per contenere l'oggetto che deve
eseguire il metodo e uno per contenere la lunghezza del lato. Da notare che il
primo parametro potrebbe avere qualunque nome, ma è uso comune chiamarlo
``self`` (e conviene attenersi a questo uso). Ora creiamo un oggetto di questa
nuova classe e proviamo il metodo::

  >>> leonardo=Ingegnere()
  >>> leonardo.quadrato(80)
  Traceback (most recent call last):
    File "<pyshell#26>", line 1, in ?
      leonardo.quadrato(80)
    File "<pyshell#24>", line 4, in quadrato
      forward(lato)
  NameError: global name 'forward' is not defined

Ancora qualcosa che non va... Eppure questa volta quadrato ha i due parametri
richiesti! Infatti l'errore è cambiato: ci dice che non esiste un nome globale
``forward``. Infatti ``forward`` è un metodo della classe ``Turtle`` e quindi
può essere eseguito solo da un oggetto di questa classe. Ma dove lo trovo un
oggetto della classe ``Turtle`` mentre sto definendo la mia nuova classe?
Se osserviamo bene, proprio la soluzione al precedente errore ce l'ha messo
a disposizione, è proprio l'oggetto contenuto in ``self``.
Dobbiamo scrivere: ``self.forward(lato)``. Terzo tentativo::

  >>> class Ingegnere(Turtle):
          def quadrato(self, lato):
              for i in range(4):
                  self.forward(lato)
                  self.left(90)

Ovviamente quello che facciamo per ``forward`` lo dobbiamo fare anche per
``left``. Creiamo l'*ingegnere* e proviamo ancora il nuovo metodo ricorretto::

  >>> leonardo=Ingegnere(plane=tp)
  >>> leonardo.quadrato(80)

Va!!! Ora abbiamo una classe ``Ingegnere`` che oltre a saper fare tutto
quello che sanno fare tutte le Tartarughe sa anche disegnare quadrati.
Fissiamo il traguardo raggiunto scrivendo la nuova classe nel file
“man2.py”::

  #----------------------python-pyturtle-----------------man2.py--#
  #                                                               #
  #                  Muro fatto da Ingegnere                      #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2009--#

  from pyturtle import TurtlePlane, Turtle

  class Ingegnere(Turtle):

      def quadrato(self, lato):
          """Disegna un quadrato di dato lato."""
          for i in range(4):
              self.forward(lato)
              self.left(90)

  tp=TurtlePlane()
  leonardo=Ingegnere()
  leonardo.quadrato(20)

Salviamo, eseguiamo, ... correggiamo gli errori che inevitabilmente sono stati
fatti, ..., rieseguiamo...

Ora possiamo prendere le altre funzioni del programma man1.py e
trasformarle in metodi aggiungendo il parametro ``self`` dove serve::

  #----------------------python-pyturtle-----------------man2.py--#
  #                                                               #
  #                  Muro fatto da Ingegnere                      #
  #                                                               #
  #--Daniele Zambelli------License: GPL---------------------2005--#

  from pyturtle import TurtlePlane, Turtle

  class Ingegnere(Turtle):

      def sposta(self, o=0, v=0):
          """Effettua uno spostamento orizzontale e verticale di
          Tartaruga senza disegnare la traccia."""
          self.up()
          self.forward(o); self.left(90)
          self.forward(v); self.right(90)
          self.down()

      def quadrato(self, lato):
          """Disegna un quadrato di dato lato."""
          for i in range(4):
              self.forward(lato)
              self.left(90)

      def muro(self, lato, righe, colonne,
               spazio_righe=5, spazio_colonne=5):
          """Disegna un muro di mattoni quadrati."""
          for i in range(righe):
              for j in range(colonne):
                  self.quadrato(lato)
                  self.sposta(o=lato+spazio_colonne)
              self.sposta(o=-colonne*(lato+spazio_colonne),
                          v=lato+spazio_righe)
          self.sposta(v=-righe*(lato+spazio_righe))

  tp = TurtlePlane()
  leonardo = Ingegnere()
  leonardo.sposta(-250, -190)
  leonardo.muro(20, 15, 20)
  tp.mainloop()

A parte la complicazione del parametro ``self``, possiamo vedere come
``Python`` ci permetta di definire nuove classi in modo estremamente semplice.
Utilizzando l'ereditarietà, cioè scrivendo classi derivate da altre già
realizzate da altri, possiamo ottenere, con poche righe di programma classi:

* potenti, perché estendono altre classi,
* sicure, perché le classi genitrici sono utilizzate da molti altri
  programmatori,
* ulteriormente estendibili, ma questo lo vedremo nel prossimo capitolo.

Riassumendo
-----------

* Per definire una classe si usa il comando ``class <nome della classe>():``
* Per definire una classe discendente da un'altra si utilizza il comando::

    ``class <nome della classe figlia>(<nome della classe genitrice>):``
* Quando si scrive una classe discendente da un'altra basta scrivere i metodi
  che si aggiungono ai metodi della classe genitrice o che li sostituiscono.
* Quando si definisce un metodo di una classe si deve mettere come primo
  parametro il nome di una variabile che conterrà l'oggetto stesso, di solito
  ``self``.
* All'interno di una classe, il parametro ``self`` permette di richiamare i 
  metodi e le proprietà dell'oggetto stesso.

