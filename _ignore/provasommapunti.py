# -*- coding: utf-8 -*-
#-------------------------------python------------------test_pycart.py--#
#                                                                       #
#                      Prova per somma di punti                         #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2015---#

"""
Attributi di un punto ottenuto come somma di un punto e un vettore.
"""

import pyig as ig

ip = ig.InteractivePlane()

p_0 = ig.Point(2, -5, width=6, color='red', name="A")
v_0 = ig.Vector(ig.Point(0, 0, width=6),
                ig.Point(4, 6, width=6), color='pink')
p_1 = p_0 + v_0

ip.mainloop()