# -*- coding: utf-8 -*-
"""
grazie Daniele,per me è un onore poterti dare del tu.Io adoro python,lo trovo divertente e abbastanza semplice da imparare,anche per me che non so nulla di programmazione e penso che tu hai fatto un'opera meravigliosa scrivendo il tuo manuale e rendendolo disponibile
Qui sotto ci sono gli errori e il codice da  me scritto.Io utilizzo windows vista e python 3.0:

>>> from pycart import Plane
>>> p=Plane()
>>> p=Plane()
>>> p.axes(True)
Traceback (most recent call last):
  File "<pyshell#3>", line 1, in <module>
    p.axes(True)
  File "C:\Python30\lib\site-packages\pygraph\pycart.py", line 297, in axes
    canvas.create_line(0, y_o, w, y_o, fill=color)
  File "C:\Python30\lib\tkinter\__init__.py", line 2149, in create_line
    return self._create('line', args, kw)
  File "C:\Python30\lib\tkinter\__init__.py", line 2137, in _create
    *(args + self._options(cnf, kw))))
_tkinter.TclError: unknown color name "1"
>>> p1=(2,4)
>>> p2=(-4,3)
>>> p.drawpoint(p1)
Traceback (most recent call last):
  File "<pyshell#6>", line 1, in <module>
    p.drawpoint(p1)
AttributeError: 'Plane' object has no attribute 'drawpoint'
>>> p.drawpoint(p2)
Traceback (most recent call last):
  File "<pyshell#7>", line 1, in <module>
    p.drawpoint(p2)
AttributeError: 'Plane' object has no attribute 'drawpoint'
>>> 
grazie mille della tua cortesia e disponibilità
"""

from pycart import Plane, Pen

p = Plane()
p.axes('red')
biro = Pen()
biro.drawpoint((3, 5), width=5, color='green')

p.mainloop()

